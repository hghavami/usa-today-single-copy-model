/*
 * Created on Mar 27, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.bo.blueChip;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipLocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipMultiWeekProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.to.BlueChipProductOrderTOIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf;
import com.usatoday.singlecopy.model.interfaces.markets.MarketIntf;
import com.usatoday.singlecopy.model.interfaces.users.UserIntf;
import com.usatoday.singlecopy.model.transferObjects.blueChip.BlueChipProductOrderTO;
import com.usatoday.singlecopy.model.util.MarketCache;

/**
 * @author aeast
 * @date Mar 27, 2008
 * @class BlueChipLocationBO
 * 
 * This class is the implementationn of a Blue Chip Location.
 * 
 */
public class BlueChipLocationBO implements BlueChipLocationIntf, Comparable<BlueChipLocationIntf> {

    // This hashmap should contain a collection of BlueChipMultiWeekProductOrderIntf objects
    // The key of the map is the BlueChipMultiWeekProductOrderIntf.getHashKey() which equates to Product Order ID
	private HashMap<String, BlueChipMultiWeekProductOrderIntf> multiWeekProductOrderData = new HashMap<String, BlueChipMultiWeekProductOrderIntf>();
        
    private String address1 = null;
    private String locationID = null;
    private String locationName = null;
    private String phoneNumber = null;
    private String owningMarketID = null;
    
    private UserIntf owningUser = null;
    
    
    /**
     * 
     * @param transferObjects This method accepts a collection of BlueChipProductOrderTOIntf objects any other types are ignored
     * 
     * @return An array of BlueChipLocationIntf objects for use in blue chip draw management
     */
    public static BlueChipLocationIntf[] objectFactory(Collection<BlueChipProductOrderTO> transferObjects, UserIntf owningUser) {
        HashMap<String, BlueChipLocationBO> locationsMap = new HashMap<String, BlueChipLocationBO>();
        BlueChipLocationIntf locationsArray[] = null;
        
        if (transferObjects != null && transferObjects.size() > 0) {
            Iterator<BlueChipProductOrderTO> itr = transferObjects.iterator();
            while(itr.hasNext()) {
                BlueChipProductOrderTOIntf to = itr.next();
                    
                String locID = to.getLocationID();
                    
                BlueChipLocationBO locBO = null;
                if (locationsMap.containsKey(locID)) {
                    // location already converted to BO so just add this 
                    // to's data
                    locBO = locationsMap.get(locID);
                    locBO.addProductOrderData(to);
                }
                else {
                    // create new location BO
                    locBO = new BlueChipLocationBO(to, owningUser);
                    locationsMap.put(locID, locBO);
                }
            }
        }
        
        Iterator<BlueChipLocationBO> locItr = locationsMap.values().iterator();
        int index = 0;
        // initialize return array
        locationsArray = new BlueChipLocationIntf[locationsMap.size()];
        
        while (locItr.hasNext()) {
            BlueChipLocationIntf loc = (BlueChipLocationIntf)locItr.next();

            // following method will add a dummy week of data 
            // if there is no week existing for the current product date
            // this KLUDGE is done for the case of pending starts.
           	((BlueChipLocationBO)loc).validatePOData();
            
            locationsArray[index++] = loc;
        }
        return locationsArray;
    }
    /**
     * 
     */
    private BlueChipLocationBO() {
        super();
    }
    
    private BlueChipLocationBO(BlueChipProductOrderTOIntf source, UserIntf owningUser) {
        super();
        
        this.owningUser = owningUser;
        
        // copy relevant data to this object
        this.address1 = source.getLocationAddress1();
        this.locationID = source.getLocationID();
        this.locationName = source.getLocationName();
        this.phoneNumber = source.getLocationPhone();
        this.owningMarketID = source.getMarketID();
        
        this.addProductOrderData(source);
    }
    
    // method to do any changes to data if needed (for special cases)
    private void validatePOData() {
    	try {
	        Iterator<BlueChipMultiWeekProductOrderIntf> multiWeekItr = this.multiWeekProductOrderData.values().iterator();
	        
	        // Iterate over each po in the list
	        while (multiWeekItr.hasNext()) {
	        	BlueChipMultiWeekProductOrderIntf multiWeekPO = multiWeekItr.next();
	        	
	            BlueChipProductOrderIntf po = multiWeekPO.getNextDistributionDateProductOrder();
	            DateTime d = multiWeekPO.getNextDistributionDate();
	            
	            if (po == null) {
	            	BlueChipProductOrderTO dummyWeek = new BlueChipProductOrderTO();
	            	dummyWeek.setLocationAddress1(this.address1);
	            	dummyWeek.setLocationID(this.locationID);
	            	dummyWeek.setLocationName(this.locationName);
	            	dummyWeek.setLocationPhone(this.phoneNumber);
	            	dummyWeek.setMarketID(this.getOwningMarketID());
	            	dummyWeek.setMaxDrawThreshold(multiWeekPO.getMaxDrawThreshold());
	            	dummyWeek.setMinDrawThreshold(multiWeekPO.getMinDrawThreshold());
					dummyWeek.setProductID(multiWeekPO.getProductCode());
					dummyWeek.setProductOrderID(multiWeekPO.getProductOrderID());
					dummyWeek.setProductOrderSequenceNumber(multiWeekPO.getSequenceNumber());
					dummyWeek.setProductOrderType(multiWeekPO.getProductOrderTypeCode());
	
					int dayOfWeek = d.getDayOfWeek();
	            	int sunday = BlueChipProductOrderWeekIntf.SUNDAY.intValue();
	            	
	            	int daysToAdd = sunday - dayOfWeek;
	            	d = d.plusDays(daysToAdd);
	            	
	            	dummyWeek.setWeekEndingDate(d.toString("yyyyMMdd"));
	            	
	            	this.addProductOrderData(dummyWeek);
	            }
	        }
    	}
    	catch (Exception e) {
    		System.out.println("BlueChipLocationBO::validatePOData() - Failed to dummy up week of data: " + e.getMessage());
    	}
    }
    
    /**
     * 
     * @param source The source Transfer object that has data to build 
     * 		  the business objects from. Method is only called from static
     * 		  object factory method and private constructor method
     */
    private void addProductOrderData(BlueChipProductOrderTOIntf source) {
        String poID = source.getProductOrderID();
        
        BlueChipMultiWeekProductOrderIntf poData = null;
        if (this.multiWeekProductOrderData.containsKey(poID)) {
            poData = this.multiWeekProductOrderData.get(poID);
            poData = BlueChipMultiWeekProductOrderBO.objectFactory((BlueChipMultiWeekProductOrderBO)poData, source, this);
        }
        else {
            poData = BlueChipMultiWeekProductOrderBO.objectFactory(null, source, this);
            this.multiWeekProductOrderData.put(poID, poData);
        }
        
    }
    
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipLocationIntf#getProductOrdersForDay(org.joda.time.DateTime)
     */
    public Collection<BlueChipProductOrderIntf> getProductOrdersForDay(DateTime day) {
        ArrayList<BlueChipProductOrderIntf> poList = new ArrayList<BlueChipProductOrderIntf>();

        if (day == null) {
            return poList;
        }
        

        Iterator<BlueChipMultiWeekProductOrderIntf> multiWeekItr = this.multiWeekProductOrderData.values().iterator();
        
        // Iterate over each po in the list
        while (multiWeekItr.hasNext()) {
            BlueChipMultiWeekProductOrderIntf multiWeekPO = multiWeekItr.next();
            
            BlueChipProductOrderIntf po = multiWeekPO.getProductOrderForDate(day);
            
            if (po != null) {
                poList.add(po);
            }
            
        }
        
        return poList;
    }

    @Override
	public Collection<BlueChipProductOrderIntf> getProductOrdersThatCutOffToday() {
        ArrayList<BlueChipProductOrderIntf> poList = new ArrayList<BlueChipProductOrderIntf>();


        Iterator<BlueChipMultiWeekProductOrderIntf> multiWeekItr = this.multiWeekProductOrderData.values().iterator();
        
        // Iterate over each po in the list
        while (multiWeekItr.hasNext()) {
            BlueChipMultiWeekProductOrderIntf multiWeekPO = multiWeekItr.next();
            
            poList.addAll(multiWeekPO.getCutOffDateProductOrders());
            
        }
        
        return poList;
	}
	/* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipLocationIntf#getChangedProductOrders()
     */
    public Collection<BlueChipMultiWeekProductOrderIntf> getChangedProductOrders() {
        ArrayList<BlueChipMultiWeekProductOrderIntf> poList = new ArrayList<BlueChipMultiWeekProductOrderIntf>();

        Iterator<BlueChipMultiWeekProductOrderIntf> multiWeekItr = this.multiWeekProductOrderData.values().iterator();
        
        // Iterate over each po in the list
        while (multiWeekItr.hasNext()) {
            BlueChipMultiWeekProductOrderIntf multiWeekPO = multiWeekItr.next();
            
            if (multiWeekPO.containsChangedWeeks()) {
                poList.add(multiWeekPO);
            }
            
        }
        
        return poList;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipLocationIntf#getNextDistributionDateProductOrders()
     */
    public Collection<BlueChipProductOrderIntf> getNextDistributionDateProductOrders() {
        ArrayList<BlueChipProductOrderIntf> poList = new ArrayList<BlueChipProductOrderIntf>();

        Iterator<BlueChipMultiWeekProductOrderIntf> multiWeekItr = this.multiWeekProductOrderData.values().iterator();
        
        // Iterate over each po in the list
        while (multiWeekItr.hasNext()) {
            BlueChipMultiWeekProductOrderIntf multiWeekPO = multiWeekItr.next();
            
            BlueChipProductOrderIntf po = multiWeekPO.getNextDistributionDateProductOrder();
            
            if (po != null) {
                poList.add(po);
            }
            
        }
        
        return poList;
    }

    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(BlueChipLocationIntf o) {
        // We will sort locations by location Name
        BlueChipLocationIntf otherObj = o;
        String sourceName = this.getLocationName();
        String compareToName = otherObj.getLocationName();
        return sourceName.compareTo(compareToName);
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf#getLocationID()
     */
    public String getLocationID() {
        return this.locationID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf#getLocationName()
     */
    public String getLocationName() {
        return this.locationName;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf#getAddress1()
     */
    public String getAddress1() {
        return this.address1;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf#getPhoneNumber()
     */
    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf#getLocationHashKey()
     */
    public String getLocationHashKey() {
        // location id serves as the hash key
        return this.locationID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf#getSequenceNumber()
     */
    public int getSequenceNumber() {
        // locations don't have a sequence id for blue chip
        return 0;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf#getProductOrders()
     * @return The collection of BlueChipMultiWeekProductOrderIntf objects associated with this location, sorted
     */
    public Collection<ProductOrderIntf> getProductOrders() {
        ArrayList<BlueChipMultiWeekProductOrderBO> locs = new ArrayList<BlueChipMultiWeekProductOrderBO>(this.multiWeekProductOrderData.size());
        
        for (BlueChipMultiWeekProductOrderIntf aWeek : this.multiWeekProductOrderData.values()) {
        	locs.add((BlueChipMultiWeekProductOrderBO)aWeek);
        }
        Collections.sort(locs);
        
        ArrayList<ProductOrderIntf> sortedPOs = new ArrayList<ProductOrderIntf>(locs);
        return sortedPOs;
    }

    /* This method was inherited from a the LocationIntf interface. For blue chip draw the returned object
     * is actually a BlueChipMultiWeekProductOrderIntf object.
     * 
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf#getProductOrder(java.lang.String)
     * 
     */
    public ProductOrderIntf getProductOrder(String productOrderID) {
        BlueChipMultiWeekProductOrderIntf mwpo = (BlueChipMultiWeekProductOrderIntf)this.multiWeekProductOrderData.get(productOrderID);
        return mwpo;
    }

    public MarketIntf getOwningMarket() {
        MarketIntf m = null;
        m = MarketCache.getInstance().getMarket(this.owningMarketID);
        return m;
    }
    
    public String getOwningMarketID() {
        return this.owningMarketID;
    }
    
    // call this method to clear all reference to objects during a logout.
    public void clear() {
        if (this.multiWeekProductOrderData != null && !this.multiWeekProductOrderData.isEmpty()) {
            Iterator<BlueChipMultiWeekProductOrderIntf> itr = this.multiWeekProductOrderData.values().iterator();
            while( itr.hasNext()) {
                BlueChipMultiWeekProductOrderIntf mwpo = itr.next();
                mwpo.clear();
            }
            this.multiWeekProductOrderData.clear();
        }
    }
    
    /**
     *  @return a Collection of DrawUpdateErrorIntf objects
     */
    public Collection<DrawUpdateErrorIntf> getAllUpdateErrors() {
        ArrayList<DrawUpdateErrorIntf> errors = new ArrayList<DrawUpdateErrorIntf>();
        
        if (this.multiWeekProductOrderData != null && !this.multiWeekProductOrderData.isEmpty()) {
            Iterator<BlueChipMultiWeekProductOrderIntf> itr = this.multiWeekProductOrderData.values().iterator();
            while (itr.hasNext()) {
                BlueChipMultiWeekProductOrderIntf mwpo = itr.next();
                DrawUpdateErrorIntf[] errorArray = mwpo.getUpdateErrors();
                if (errorArray != null) {
                    CollectionUtils.addAll(errors, errorArray);
                }
            }
        }
        return errors;
    }
    
    /**
     * 
     *
     */
    public void clearAllUpdateErrors() {
        if (this.multiWeekProductOrderData != null && !this.multiWeekProductOrderData.isEmpty()) {
            Iterator<BlueChipMultiWeekProductOrderIntf> itr = this.multiWeekProductOrderData.values().iterator();
            while (itr.hasNext()) {
                BlueChipMultiWeekProductOrderIntf mwpo = itr.next();
                mwpo.clearUpdateErrors();
            }
        }
    }
    
	public UserIntf getOwningUser() {
		return owningUser;
	}
}
