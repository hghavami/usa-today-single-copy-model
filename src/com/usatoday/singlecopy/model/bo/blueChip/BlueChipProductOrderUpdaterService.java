/*
 * Created on May 14, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.bo.blueChip;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.bo.users.UserBO;
import com.usatoday.singlecopy.model.integration.BlueChipDrawDAO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipLocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipMultiWeekProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf;
import com.usatoday.singlecopy.model.transferObjects.DrawUpdateErrorTO;
import com.usatoday.singlecopy.model.transferObjects.blueChip.BlueChipProductOrderUpdateTO;

/**
 * @author aeast
 * @date May 14, 2008
 * @class BlueChipProductOrderUpdaterService
 * 
 * 
 * 
 */
public class BlueChipProductOrderUpdaterService {

    
    /**
     * 
     */
    public BlueChipProductOrderUpdaterService() {
        super();
    }

    /**
     * 
     * @param locations  - The collection of locations assigned to a user.
     * @throws UsatException - if the collection contains something other than BlueChipLocationIntf objects
     */
    public void saveChangedProductOrders(UserBO user) throws UsatException {
        
        if (user == null || user.getBlueChipLocations().size() == 0) {
            return;
        }

        Collection<BlueChipLocationIntf> locations = user.getBlueChipLocations().values();
        
        ArrayList<BlueChipProductOrderUpdateTO> poTo = new ArrayList<BlueChipProductOrderUpdateTO>();
        
        Iterator<BlueChipLocationIntf> itr = locations.iterator();
        ArrayList<BlueChipMultiWeekProductOrderIntf> changedMultiWeeks = new ArrayList<BlueChipMultiWeekProductOrderIntf>();
        
        while (itr.hasNext()) {
            BlueChipLocationIntf loc = itr.next();
            
            Collection<BlueChipMultiWeekProductOrderIntf> changedPOs = loc.getChangedProductOrders();
            if (changedPOs != null && !changedPOs.isEmpty()) {
                Iterator<BlueChipMultiWeekProductOrderIntf> multiWeekItr = changedPOs.iterator();
                while (multiWeekItr.hasNext()) {
                    BlueChipMultiWeekProductOrderIntf mwpo = multiWeekItr.next();
                    
                    // clear any previous update erros
                    mwpo.clearUpdateErrors();
                    
                    // add to list of pos that have changes for later synch with returned errros
                    changedMultiWeeks.add(mwpo);
                    
                    Collection<BlueChipProductOrderWeekIntf> changedWeeks = mwpo.getChangedWeeks();
                    Iterator<BlueChipProductOrderWeekIntf> changeWeekItr = changedWeeks.iterator();
                    while(changeWeekItr.hasNext()) {
                        
                        BlueChipProductOrderWeekIntf week = changeWeekItr.next();
                        BlueChipProductOrderUpdateTO to = this.generateUpdateTO(week);
                        poTo.add(to);
                    }
                }
            }
        }  // end while more locations
        
        Collection<DrawUpdateErrorTO> updateErrors = null;
        
        if (poTo.size() > 0) {
            BlueChipDrawDAO dao = new BlueChipDrawDAO();
            updateErrors = dao.updateBlueChipDraw(user.getSessionID(), user.getEmailAddress(), poTo);
            
            // if update errros process them
            if (!updateErrors.isEmpty()) {
                Iterator<DrawUpdateErrorTO> errorItr = updateErrors.iterator();
                while (errorItr.hasNext()) {
                    
                    DrawUpdateErrorIntf error = errorItr.next();
                    
                    // now iterate over po weeks until get right po
    	            Iterator<BlueChipMultiWeekProductOrderIntf> itrMulti = changedMultiWeeks.iterator();
    	            
    	            boolean applied = false;
    	            while (itrMulti.hasNext() && !applied) {
    	                BlueChipMultiWeekProductOrderIntf mwpo = itrMulti.next();
    	                
    	                if (error.getLocationID().equalsIgnoreCase(mwpo.getOwningLocation().getLocationID())) {
    	                    if (error.getProductOrderID().equalsIgnoreCase(mwpo.getProductOrderID())) {
    	                        try {
	    	                        BlueChipProductOrderBO po = (BlueChipProductOrderBO)mwpo.getProductOrderForDate(error.getErrorDate());
	    	                        
	    	                        po.addUpdateError(error);
	    	                        applied = true;
    	                        }
    	                        catch (ClassCastException ce) {
    	                            // skip it should never happen
    	                        }
    	                    } // end if correct poid
        	                
    	                } // end if correct location
    	                
    	            } // end while more weeks to check
                    
                } // end while more errors
                
                throw new UsatException("Not all product orders could be saved.");
            } // end if update errros
            else {
                // mark all pos as saved
            	for (BlueChipMultiWeekProductOrderIntf mwpo : changedMultiWeeks) {
	                mwpo.setSaved(mwpo);
            	}                
            }
        }
    } // end save changed pos

    /**
     * 
     * @param po
     * @return
     */
    private BlueChipProductOrderUpdateTO generateUpdateTO(BlueChipProductOrderWeekIntf week) {
        BlueChipProductOrderUpdateTO to = new BlueChipProductOrderUpdateTO();
        
        to.setLocationID(week.getOwningLocation().getLocationID());
        to.setProductOrderID(week.getProductOrderID());

        String weekEndingDate = week.getWeekEndingDate().toString("yyyyMMdd");
        to.setWeekEndingDate(weekEndingDate);

        to.setMondayDraw(week.getMondayPO().getDraw());
        to.setUpdateMonday(week.getMondayPO().isChanged());
        
        to.setTuesdayDraw(week.getTuesdayPO().getDraw());
        to.setUpdateTuesday(week.getTuesdayPO().isChanged());
        
        to.setWednesdayDraw(week.getWednesdayPO().getDraw());
        to.setUpdateWednesday(week.getWednesdayPO().isChanged());
        
        to.setThursdayDraw(week.getThursdayPO().getDraw());
        to.setUpdateThursday(week.getThursdayPO().isChanged());
        
        to.setFridayDraw(week.getFridayPO().getDraw());
        to.setUpdateFriday(week.getFridayPO().isChanged());
        
        to.setSaturdayDraw(week.getSaturdayPO().getDraw());
        to.setUpdateSaturday(week.getSaturdayPO().isChanged());
        
        to.setSundayDraw(week.getSundayPO().getDraw());
        to.setUpdateSunday(week.getSundayPO().isChanged());
        
        return to;
    }
}
