/*
 * Created on Mar 28, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.bo.blueChip;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.usatoday.singlecopy.model.integration.BlueChipDrawDAO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipLocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipMultiWeekProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderHistoryIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.to.BlueChipProductOrderTOIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf;
import com.usatoday.singlecopy.model.transferObjects.blueChip.BlueChipProductOrderHistoryTO;
import com.usatoday.singlecopy.model.util.ProductDescriptionCache;

/**
 * @author aeast
 * @date Mar 28, 2008
 * @class BlueChipMultiWeekProductOrderBO
 * 
 * This class is the implementation class for the representation of multiple weeks worth 
 * of product order data for a single Product Order ID.
 * 
 */
public class BlueChipMultiWeekProductOrderBO implements
        BlueChipMultiWeekProductOrderIntf, Comparable<BlueChipMultiWeekProductOrderIntf> {

    private String productCode;
    private int maxDrawThreshold = 0;
    private int minDrawThreshold = 0;
    private DateTime nextDistributionDate = null;
    private BlueChipLocationIntf owningLocation = null;
    private String productDescription = null;
    private String productName = null;
    private String productOrderID = null;
    private String productOrderTypeCode = null;
    private int sequenceNumber = 0;
    
    private DateTime earliestWeek = null;
    private DateTime latestWeek = null;
    
    // Following collections and fields facilitate accessing the weeks of data
    private BlueChipProductOrderIntf nextDistributionDatePO = null;
    private BlueChipProductOrderWeekIntf nextDistributionDatePOWeek = null;
    
    //  list of weekly po objects keyed of the string representation of 
    //  week ending date YYYYMMDD
    private HashMap<String, BlueChipProductOrderWeekIntf> weeklyPOData = new HashMap<String, BlueChipProductOrderWeekIntf>();  
    
    private BlueChipProductOrderHistoryIntf historicalData = null;
    
    public static BlueChipMultiWeekProductOrderIntf objectFactory(BlueChipMultiWeekProductOrderBO weeklyPOData, BlueChipProductOrderTOIntf source, BlueChipLocationIntf parentLocation) {
        BlueChipMultiWeekProductOrderIntf mwpo = null;
        
        if (weeklyPOData != null) {
            // Simply add another week to this object
            mwpo = weeklyPOData;
            weeklyPOData.addWeekOfPOData(source);
        }
        else {
            // instantiate a new object
            mwpo = new BlueChipMultiWeekProductOrderBO(source, parentLocation);
        }
        
        return mwpo;
    }
    /**
     * 
     */
    private BlueChipMultiWeekProductOrderBO() {
        super();
    }

    private BlueChipMultiWeekProductOrderBO(BlueChipProductOrderTOIntf source, BlueChipLocationIntf parentLocation) {
        super();
        
        // initialize variables
        this.maxDrawThreshold = source.getMaxDrawThreshold();
        this.minDrawThreshold = source.getMinDrawThreshold();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
        try {
            this.nextDistributionDate = formatter.parseDateTime(source.getNextDistributionDate());

        }
        catch (Exception e) {
           // failed to format date
           System.out.println("BlueChipMultiWeekProductOrderBO() - Invalid distribution date from DB: " + source.getNextDistributionDate() );
        }
        this.owningLocation = parentLocation;
        this.productCode = source.getProductID();
        this.productDescription = ProductDescriptionCache.getInstance().getProductDesription(this.productCode).getDescription();
        this.productName = source.getProductName();
        this.productOrderID = source.getProductOrderID();
        this.productOrderTypeCode = source.getProductOrderType();
        this.sequenceNumber = source.getProductOrderSequenceNumber();
        
        // call method to add Daily POs
        this.addWeekOfPOData(source);
    }
    
    private void addWeekOfPOData(BlueChipProductOrderTOIntf source) {
        
        BlueChipProductOrderWeekIntf aWeek = null;
        String weekEndingDateString = source.getWeekEndingDate();
        
        if (!this.weeklyPOData.containsKey(weekEndingDateString)) {
           aWeek = BlueChipProductOrderWeekBO.objectFactory(source, this);
           this.weeklyPOData.put(aWeek.getHashKey(), aWeek);
           // set the quick accessor to the week and PO with next distribution date
           if (aWeek.containsNextDistributionDate()) {
               this.nextDistributionDatePOWeek = aWeek;
               this.nextDistributionDatePO = aWeek.getProductOrderForDay(this.nextDistributionDate.getDayOfWeek());
           }
           
           if (this.earliestWeek == null) {
           		this.earliestWeek = aWeek.getWeekEndingDate();
           		this.latestWeek = aWeek.getWeekEndingDate();
           }
           else {
           		// update earliest and latest weeks of data.
           		if (this.earliestWeek.isAfter(aWeek.getWeekEndingDate())) {
           			this.earliestWeek = aWeek.getWeekEndingDate();
           		}
           		if (this.latestWeek.isBefore(aWeek.getWeekEndingDate())) {
           			this.latestWeek = aWeek.getWeekEndingDate();
           		}
           }
        }
        else {
            // this PO already exist
            System.out.println("Attempting to add Same PO twice: POID:" + source.getProductOrderID() + "  Loc: " + this.getOwningLocation().getLocationName());
        }
        
    }
        
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipMultiWeekProductOrderIntf#getProductName()
     */
    public String getProductName() {
        return this.productName;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipMultiWeekProductOrderIntf#getHashKey()
     */
    public String getHashKey() {
        return this.getProductOrderID();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipMultiWeekProductOrderIntf#getNextDistributionDateProductOrderWeek()
     */
    public BlueChipProductOrderWeekIntf getNextDistributionDateProductOrderWeek() {
        return this.nextDistributionDatePOWeek;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipMultiWeekProductOrderIntf#getNextDistributionDateProductOrder()
     */
    public BlueChipProductOrderIntf getNextDistributionDateProductOrder() {
        return this.nextDistributionDatePO;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipMultiWeekProductOrderIntf#getNextDistributionDate()
     */
    public DateTime getNextDistributionDate() {
        return this.nextDistributionDate;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipMultiWeekProductOrderIntf#getChangedWeeks()
     */
    public Collection<BlueChipProductOrderWeekIntf> getChangedWeeks() {
        ArrayList<BlueChipProductOrderWeekIntf> changedWeeks = new ArrayList<BlueChipProductOrderWeekIntf>();

        Iterator<BlueChipProductOrderWeekIntf> itr = this.weeklyPOData.values().iterator();
        
        while (itr.hasNext()) {
            BlueChipProductOrderWeekIntf aWeek = itr.next();
            if (aWeek.isChanged()) {
                changedWeeks.add(aWeek);
            }
        }
        
        return changedWeeks;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipMultiWeekProductOrderIntf#containsChangedWeeks()
     */
    public boolean containsChangedWeeks() {
        boolean containsChanges = false;
        
        Iterator<BlueChipProductOrderWeekIntf> itr = this.weeklyPOData.values().iterator();
        
        while (itr.hasNext()) {
            BlueChipProductOrderWeekIntf aWeek = itr.next();
            if (aWeek.isChanged()) {
                containsChanges = true;
                break;
            }
        }
        
        return containsChanges;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipMultiWeekProductOrderIntf#getWeekAfter(com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf)
     */
    public BlueChipProductOrderWeekIntf getWeekAfter(
            BlueChipProductOrderWeekIntf thisWeek) {
        BlueChipProductOrderWeekIntf nextWeek = null;
        
        if (thisWeek != null) {
            DateTime nextWeekEndingDate = thisWeek.getWeekEndingDate().plusDays(7);
            String nextWeekHashKey = nextWeekEndingDate.toString("yyyyMMdd");
            nextWeek = (BlueChipProductOrderWeekIntf)this.weeklyPOData.get(nextWeekHashKey);
            if (nextWeek == null) {
            	if (thisWeek.getWeekEndingDate().isBefore(this.latestWeek)){

            		// skip to next avaialalbe week
            		// there should be a week in the future
            		while ((nextWeek == null) && nextWeekEndingDate.isBefore(this.latestWeek)) {
            			System.out.println("Skipping a week: " + nextWeekHashKey);
            			// back up another week
            			nextWeekEndingDate = nextWeekEndingDate.plusDays(7);
            			nextWeekHashKey = nextWeekEndingDate.toString("yyyyMMdd");
            			nextWeek = (BlueChipProductOrderWeekIntf)this.weeklyPOData.get(nextWeekHashKey);
            		}
            		
            	}
            }
        }
        return nextWeek;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipMultiWeekProductOrderIntf#getWeekBefore(com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf)
     */
    public BlueChipProductOrderWeekIntf getWeekBefore(
            BlueChipProductOrderWeekIntf thisWeek) {
        BlueChipProductOrderWeekIntf prevWeek = null;
        
        if (thisWeek != null) {
            DateTime prevWeekEndingDate = thisWeek.getWeekEndingDate().minusDays(7);
            String prevWeekHashKey = prevWeekEndingDate.toString("yyyyMMdd");
            prevWeek = (BlueChipProductOrderWeekIntf)this.weeklyPOData.get(prevWeekHashKey);
            if (prevWeek == null) {
            	if (thisWeek.getWeekEndingDate().isAfter(this.earliestWeek)){
            		// skip to next avaialalbe week
            		// there should be a week in the past
            		while ((prevWeek == null) && prevWeekEndingDate.isAfter(this.earliestWeek)) {
            			// back up another week
            			prevWeekEndingDate = prevWeekEndingDate.minusDays(7);
            			prevWeekHashKey = prevWeekEndingDate.toString("yyyyMMdd");
            			prevWeek = (BlueChipProductOrderWeekIntf)this.weeklyPOData.get(prevWeekHashKey);
            		}
            	}
            }
        }
        return prevWeek;
    }

   /* private BlueChipProductOrderWeekIntf generateDummyWeek(String weekEndingDate) {
    	BlueChipProductOrderWeekIntf dWeek = null;
    	BlueChipProductOrderTO dummyWeek = new BlueChipProductOrderTO();
    	dummyWeek.setCutOffTime(this.getCutOffTime().toString("hh:mm"));
    	dummyWeek.setLocationAddress1(this.getOwningLocation().getAddress1());
    	dummyWeek.setLocationID(this.getOwningLocation().getLocationID());
    	dummyWeek.setLocationName(this.getOwningLocation().getLocationName());
    	dummyWeek.setLocationPhone(this.getOwningLocation().getPhoneNumber());
    	dummyWeek.setMarketID(((BlueChipLocationIntf)this.getOwningLocation()).getOwningMarketID());;
    	dummyWeek.setMaxDrawThreshold(this.getMaxDrawThreshold());
    	dummyWeek.setMinDrawThreshold(this.getMinDrawThreshold());
		dummyWeek.setProductID(this.getProductCode());
		dummyWeek.setProductOrderID(this.getProductOrderID());
		dummyWeek.setProductOrderSequenceNumber(this.getSequenceNumber());
		dummyWeek.setProductOrderType(this.getProductOrderTypeCode());

    	dummyWeek.setWeekEndingDate(weekEndingDate);
    	
    	dWeek = BlueChipProductOrderWeekBO.objectFactory(dummyWeek, this);
    	return dWeek;
    }*/
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipMultiWeekProductOrderIntf#getWeekContainingDate(org.joda.time.DateTime)
     */
    public BlueChipProductOrderWeekIntf getWeekContainingDate(DateTime date) {
        BlueChipProductOrderWeekIntf aWeek = null;
        
        if (date == null) {
            return null;
        }
        
        Iterator<BlueChipProductOrderWeekIntf> itr = this.weeklyPOData.values().iterator();
        
        while (itr.hasNext()) {
            BlueChipProductOrderWeekIntf tempWeek = itr.next();
            DateTime latestBounds = tempWeek.getWeekEndingDate().plusDays(1);
            DateTime earliest = tempWeek.getWeekEndingDate().minusDays(7);
            
            if (earliest.isBefore(date) && latestBounds.isAfter(date)) {
                aWeek = tempWeek;
                break;
            }
        }
        
        return aWeek;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipMultiWeekProductOrderIntf#getProductOrderForDate(org.joda.time.DateTime)
     */
    public BlueChipProductOrderIntf getProductOrderForDate(DateTime date) {
        
        BlueChipProductOrderIntf po = null;
        
        if (date == null) {
            return null;
        }
        
        Iterator<BlueChipProductOrderWeekIntf> itr = this.weeklyPOData.values().iterator();
        
        while (itr.hasNext()) {
            BlueChipProductOrderWeekIntf tempWeek = itr.next();
            DateTime latestBounds = tempWeek.getWeekEndingDate().plusDays(1);
            DateTime earliest = tempWeek.getWeekEndingDate().minusDays(7);
            
            if (earliest.isBefore(date) && latestBounds.isAfter(date)) {
                po = tempWeek.getProductOrderForDay(date.getDayOfWeek());
                break;
            }
        }
        return po;
    }

    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(BlueChipMultiWeekProductOrderIntf o) {
        //BlueChipMultiWeekProductOrderIntf otherObj = (BlueChipMultiWeekProductOrderIntf)o;
        //String sourceName = this.productOrderID;
        //String compareToName = otherObj.getProductOrderID();
        //return sourceName.compareTo(compareToName);
        BlueChipMultiWeekProductOrderIntf otherObj = o;
        Integer sourceInt = new Integer(this.getSequenceNumber());
        Integer compareToInt = new Integer(otherObj.getSequenceNumber());
        return sourceInt.compareTo(compareToInt);
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getProductOrderID()
     */
    public String getProductOrderID() {
        return this.productOrderID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getProductOrderTypeCode()
     */
    public String getProductOrderTypeCode() {
        return this.productOrderTypeCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getProductCode()
     */
    public String getProductCode() {
        return this.productCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getSequenceNumber()
     */
    public int getSequenceNumber() {
        return this.sequenceNumber;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getProductDescription()
     */
    public String getProductDescription() {
        return this.productDescription;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getMaxDrawThreshold()
     */
    public int getMaxDrawThreshold() {
        return this.maxDrawThreshold;
    }

    public int getMinDrawThreshold() {
        return this.minDrawThreshold;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#isChanged()
     */
    public boolean isChanged() {
        boolean changed = false;
        
        if (this.weeklyPOData.isEmpty()) {
            return changed;
        }
        
        Iterator<BlueChipProductOrderWeekIntf> itr = this.weeklyPOData.values().iterator();
        
        while (itr.hasNext()) {
            BlueChipProductOrderWeekIntf aWeek = itr.next();
            if (aWeek.isChanged()) {
                changed = true;
                break;
            }
        }
        
        return changed;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getTotalDraw()
     */
    public int getTotalDraw() {
        int totalDraw = 0;
        
        if (this.weeklyPOData.isEmpty()) {
            return totalDraw;
        }
        
        Iterator<BlueChipProductOrderWeekIntf> itr = this.weeklyPOData.values().iterator();
        
        while (itr.hasNext()) {
            BlueChipProductOrderWeekIntf aWeek = itr.next();
            totalDraw += aWeek.getWeeklyDrawTotal();
        }

        return totalDraw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getTotalReturns()
     */
    public int getTotalReturns() {
        // We don't allow blue chip returns so always return 0
        return 0;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getOwningLocation()
     */
    public LocationIntf getOwningLocation() {
        return this.owningLocation;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getUpdateErrors()
     */
    public DrawUpdateErrorIntf[] getUpdateErrors() {
        DrawUpdateErrorIntf[] errorArray = null;
        
        Collection<DrawUpdateErrorIntf> errorCollection = new ArrayList<DrawUpdateErrorIntf>();
        if (this.weeklyPOData != null && !this.weeklyPOData.isEmpty()){
            Iterator<BlueChipProductOrderWeekIntf> itr = this.weeklyPOData.values().iterator();
            while (itr.hasNext()){
                BlueChipProductOrderWeekIntf week = itr.next();
                errorArray = week.getMondayPO().getUpdateErrors();
                if (errorArray != null) {
                    CollectionUtils.addAll(errorCollection, errorArray);
                }
                
                errorArray = week.getTuesdayPO().getUpdateErrors();
                if (errorArray != null) {
                    CollectionUtils.addAll(errorCollection, errorArray);
                }
                
                errorArray = week.getWednesdayPO().getUpdateErrors();
                if (errorArray != null) {
                    CollectionUtils.addAll(errorCollection, errorArray);
                }

                errorArray = week.getThursdayPO().getUpdateErrors();
                if (errorArray != null) {
                    CollectionUtils.addAll(errorCollection, errorArray);
                }

                errorArray = week.getFridayPO().getUpdateErrors();
                if (errorArray != null) {
                    CollectionUtils.addAll(errorCollection, errorArray);
                }
                
                errorArray = week.getSaturdayPO().getUpdateErrors();
                if (errorArray != null) {
                    CollectionUtils.addAll(errorCollection, errorArray);
                }
                
                errorArray = week.getSundayPO().getUpdateErrors();
                if (errorArray != null) {
                    CollectionUtils.addAll(errorCollection, errorArray);
                }
            }
        }
        if (errorCollection.size() > 0) {
            errorArray = (DrawUpdateErrorIntf[])errorCollection.toArray(new DrawUpdateErrorIntf[errorCollection.size()]);
        }
        return errorArray;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#setSaved(com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf)
     */
    public void setSaved(ProductOrderIntf newSource) {
        if (this.weeklyPOData != null && !this.weeklyPOData.isEmpty()){
            Iterator<BlueChipProductOrderWeekIntf> itr = this.weeklyPOData.values().iterator();
            while (itr.hasNext()){
                BlueChipProductOrderWeekIntf week = itr.next();
                week.getMondayPO().setSaved(week.getMondayPO());
                week.getTuesdayPO().setSaved(week.getTuesdayPO());
                week.getWednesdayPO().setSaved(week.getWednesdayPO());
                week.getThursdayPO().setSaved(week.getThursdayPO());
                week.getFridayPO().setSaved(week.getFridayPO());
                week.getSaturdayPO().setSaved(week.getSaturdayPO());
                week.getSundayPO().setSaved(week.getSundayPO());
            }
        }
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#hasEdittableFields()
     */
    public boolean hasEdittableFields() {
        boolean hasOpenFields = false;

        if (this.weeklyPOData.isEmpty()) {
            return hasOpenFields;
        }
        
        Iterator<BlueChipProductOrderWeekIntf> itr = this.weeklyPOData.values().iterator();
        
        while (itr.hasNext() && !hasOpenFields) {
            BlueChipProductOrderWeekIntf aWeek = itr.next();
            Iterator<BlueChipProductOrderIntf> posItr = aWeek.getDailyProductOrders().iterator();
            while (posItr.hasNext()) {
                BlueChipProductOrderIntf po = posItr.next();
                if (po.getAllowDrawEdits()) {
                    hasOpenFields = true;
                    break;
                }
            }
            
        }
        
        return hasOpenFields;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#setPOAsDeleted()
     */
    public void setPOAsDeleted() {
        if (this.weeklyPOData != null && !this.weeklyPOData.isEmpty()){
            Iterator<BlueChipProductOrderWeekIntf> itr = this.weeklyPOData.values().iterator();
            while (itr.hasNext()){
                BlueChipProductOrderWeekIntf week = itr.next();
                week.getMondayPO().setPOAsDeleted();
                week.getTuesdayPO().setPOAsDeleted();
                week.getWednesdayPO().setPOAsDeleted();
                week.getThursdayPO().setPOAsDeleted();
                week.getFridayPO().setPOAsDeleted();
                week.getSaturdayPO().setPOAsDeleted();
                week.getSundayPO().setPOAsDeleted();
            }
        }
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#clearUpdateErrors()
     */
    public void clearUpdateErrors() {
        if (this.weeklyPOData != null && !this.weeklyPOData.isEmpty()){
            Iterator<BlueChipProductOrderWeekIntf> itr = this.weeklyPOData.values().iterator();
            while (itr.hasNext()){
                BlueChipProductOrderWeekIntf week = (BlueChipProductOrderWeekIntf)itr.next();
                week.getMondayPO().clearUpdateErrors();
                week.getTuesdayPO().clearUpdateErrors();
                week.getWednesdayPO().clearUpdateErrors();
                week.getThursdayPO().clearUpdateErrors();
                week.getFridayPO().clearUpdateErrors();
                week.getSaturdayPO().clearUpdateErrors();
                week.getSundayPO().clearUpdateErrors();
            }
        }
    }

    public void clear() {
       if (this.weeklyPOData != null && !this.weeklyPOData.isEmpty()){
           Iterator<BlueChipProductOrderWeekIntf> itr = this.weeklyPOData.values().iterator();
           while (itr.hasNext()){
               BlueChipProductOrderWeekIntf week = itr.next();
               week.clear();
           }
           this.weeklyPOData.clear();
       }
       this.nextDistributionDatePO = null;
       this.nextDistributionDatePOWeek = null;
       this.owningLocation = null;
    }
	@Override
	public Collection<BlueChipProductOrderWeekIntf> getAllWeeks() {
		return this.weeklyPOData.values();
	}
	
	@Override
	public BlueChipProductOrderHistoryIntf getProductOrderHistory() {
				
		if (this.historicalData == null){
			
			// query DB
			BlueChipDrawDAO dao = new BlueChipDrawDAO();

			try {
				
				Collection<BlueChipProductOrderHistoryTO> histTOs = dao.fetchBlueChipProductOrderHistory(this.owningLocation.getOwningUser().getEmailAddress(), this.owningLocation.getOwningUser().getSessionID(), this.owningLocation.getLocationID(), this.productOrderID );
				
				this.historicalData = BlueChipProductOrderHistoryBO.objectFactory(histTOs, this.productOrderID);
				
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
			
		return historicalData;
		
	}
	@Override
	public Collection<BlueChipProductOrderIntf> getCutOffDateProductOrders() {
		ArrayList<BlueChipProductOrderIntf> pos = new ArrayList<BlueChipProductOrderIntf>();
		
		for (BlueChipProductOrderWeekIntf aWeek : this.weeklyPOData.values()) {
			pos.addAll(aWeek.getDailyProductOrdersThatCutOffToday());
		}
		return pos;
	}
}
