package com.usatoday.singlecopy.model.bo.blueChip;

import java.util.HashMap;
import java.util.Iterator;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderHistoryInstanceIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderHistoryIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderHistoryWeekIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf;
import com.usatoday.singlecopy.model.transferObjects.blueChip.BlueChipProductOrderHistoryTO;

public class BlueChipProductOrderHistoryWeekBO implements
		BlueChipProductOrderHistoryWeekIntf, Comparable<BlueChipProductOrderHistoryWeekIntf> {

    private DateTime weekEndingDate = null;
    private String productOrderID = null;
    
    private BlueChipProductOrderHistoryIntf parent = null;
    
    // Map of daily POs keyed off ot the day of week
    private HashMap<Integer, BlueChipProductOrderHistoryInstanceIntf> dailyPOs = new HashMap<Integer, BlueChipProductOrderHistoryInstanceIntf>();

    public static BlueChipProductOrderHistoryWeekBO objectFactory(BlueChipProductOrderHistoryTO source, BlueChipProductOrderHistoryIntf parent) {
    	BlueChipProductOrderHistoryWeekBO weekPO = null;
        
        weekPO = new BlueChipProductOrderHistoryWeekBO(source, parent);
        
        return weekPO;
    }

    private BlueChipProductOrderHistoryWeekBO(BlueChipProductOrderHistoryTO source, BlueChipProductOrderHistoryIntf parent) {
        super();
        
        
        // set values
        this.parent = parent;
        this.productOrderID = source.getProductOrderID();
        
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
        try {
            this.weekEndingDate = formatter.parseDateTime(source.getWeekEndingDate());
        }
        catch (Exception e) {
           // failed to format date
           System.out.println("Invalid week ending date from DB (Weekly History): " + source.getWeekEndingDate());
        }

        //generate Monday po
        BlueChipProductOrderHistoryInstanceIntf po = new BlueChipProductOrderHistoryInstanceBO(source.getMondayDraw(), source.getMondayReturns(), this.weekEndingDate.minusDays(6));
        this.dailyPOs.put(BlueChipProductOrderWeekIntf.MONDAY, po);
        
        //generate Tuesday po
        po = new BlueChipProductOrderHistoryInstanceBO(source.getTuesdayDraw(), source.getTuesdayReturns(), this.weekEndingDate.minusDays(5));
        this.dailyPOs.put(BlueChipProductOrderWeekIntf.TUESDAY, po);
        
        //generate Wednesday po
        po =  new BlueChipProductOrderHistoryInstanceBO(source.getWednesdayDraw(), source.getWednesdayReturns(), this.weekEndingDate.minusDays(4));
        this.dailyPOs.put(BlueChipProductOrderWeekIntf.WEDNESDAY, po);
        
        //generate Thursday po
        po = new BlueChipProductOrderHistoryInstanceBO(source.getThursdayDraw(), source.getThursdayReturns(),this.weekEndingDate.minusDays(3));
        this.dailyPOs.put(BlueChipProductOrderWeekIntf.THURSDAY, po);
        
        //generate Friday po
        po = new BlueChipProductOrderHistoryInstanceBO(source.getFridayDraw(), source.getFridayReturns(),this.weekEndingDate.minusDays(2));
        this.dailyPOs.put(BlueChipProductOrderWeekIntf.FRIDAY, po);
        
        //generate Saturday po
        po = new BlueChipProductOrderHistoryInstanceBO(source.getSaturdayDraw(), source.getSaturdayReturns(),this.weekEndingDate.minusDays(1));
        this.dailyPOs.put(BlueChipProductOrderWeekIntf.SATURDAY, po);
        
        //generate Sunday po
        po = new BlueChipProductOrderHistoryInstanceBO(source.getSundayDraw(), source.getSundayReturns(),this.weekEndingDate);
        this.dailyPOs.put(BlueChipProductOrderWeekIntf.SUNDAY, po);
        
    }
    
    
	@Override
	public BlueChipProductOrderHistoryInstanceIntf getFridayPO() {
		return this.dailyPOs.get(BlueChipProductOrderWeekIntf.FRIDAY);
	}

	@Override
	public String getHashKey() {
        String hashKey = this.weekEndingDate.toString("yyyyMMdd");
        return hashKey;
	}

	@Override
	public BlueChipProductOrderHistoryInstanceIntf getMondayPO() {
		return this.dailyPOs.get(BlueChipProductOrderWeekIntf.MONDAY);
	}

	@Override
	public BlueChipProductOrderHistoryInstanceIntf getProductOrderForDay(int day) {
        if (day >= DateTimeConstants.MONDAY && day <= DateTimeConstants.SUNDAY) {
            return this.dailyPOs.get(new Integer(day));
        }
        return null;
	}

	@Override
	public String getProductOrderID() {
		return this.productOrderID;
	}

	@Override
	public BlueChipProductOrderHistoryInstanceIntf getSaturdayPO() {
		return this.dailyPOs.get(BlueChipProductOrderWeekIntf.SATURDAY);
	}

	@Override
	public BlueChipProductOrderHistoryInstanceIntf getSundayPO() {
		return this.dailyPOs.get(BlueChipProductOrderWeekIntf.SUNDAY);
	}

	@Override
	public BlueChipProductOrderHistoryInstanceIntf getThursdayPO() {
		return this.dailyPOs.get(BlueChipProductOrderWeekIntf.THURSDAY);
	}

	@Override
	public BlueChipProductOrderHistoryInstanceIntf getTuesdayPO() {
		return this.dailyPOs.get(BlueChipProductOrderWeekIntf.TUESDAY);
	}

	@Override
	public BlueChipProductOrderHistoryInstanceIntf getWednesdayPO() {
		return this.dailyPOs.get(BlueChipProductOrderWeekIntf.WEDNESDAY);
	}

	@Override
	public DateTime getWeekEndingDate() {
		return this.weekEndingDate;
	}

	@Override
	public int getWeeklyDrawTotal() {
        int weeklyDrawTotal = 0;
        if (this.dailyPOs != null && !this.dailyPOs.isEmpty()) {
            Iterator<BlueChipProductOrderHistoryInstanceIntf> itr = this.dailyPOs.values().iterator();
            while (itr.hasNext()) {
                BlueChipProductOrderHistoryInstanceIntf bcpo = itr.next();
                weeklyDrawTotal += bcpo.getTotalDraw();
            }
        }
        return weeklyDrawTotal;
	}

	public BlueChipProductOrderHistoryIntf getParent() {
		return parent;
	}

	@Override
	public int compareTo(BlueChipProductOrderHistoryWeekIntf o) {
		
        return this.getHashKey().compareTo(o.getHashKey());
	}

}
