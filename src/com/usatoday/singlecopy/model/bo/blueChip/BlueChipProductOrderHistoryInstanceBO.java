package com.usatoday.singlecopy.model.bo.blueChip;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderHistoryInstanceIntf;

public class BlueChipProductOrderHistoryInstanceBO implements
		BlueChipProductOrderHistoryInstanceIntf {

	private int draw = -1;
	private int returns = -1;
	private DateTime date = null;
	
	public BlueChipProductOrderHistoryInstanceBO(int draw, int returns, DateTime date) {
		super();
		this.draw = draw;
		this.returns = returns;
		this.date = date;
	}

	
	@Override
	public DateTime getDate() {
		return this.date;
	}

	@Override
	public int getDraw() {
		return this.draw;
	}

	@Override
	public int getReturns() {
		return this.returns;
	}


	@Override
	public int getTotalDraw() {
		// TODO Auto-generated method stub
		if (this.draw >= 0) {
			return this.draw;
		}
		return 0;
	}

}
