/*
 * Created on Mar 28, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.bo.blueChip;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipLocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipMultiWeekProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.to.BlueChipProductOrderTOIntf;

/**
 * @author aeast
 * @date Mar 28, 2008
 * @class BlueChipProductOrderWeekBO
 * 
 * This class holds a full week of PO data for an account
 * 
 */
public class BlueChipProductOrderWeekBO implements
        BlueChipProductOrderWeekIntf, Comparable<BlueChipProductOrderWeekIntf> {

    private BlueChipMultiWeekProductOrderIntf parent = null;
    private BlueChipLocationIntf owningLocation = null;
    private DateTime weekEndingDate = null;
    
    // Map of daily POs keyed off ot the day of week
    private HashMap<Integer, BlueChipProductOrderIntf> dailyPOs = new HashMap<Integer, BlueChipProductOrderIntf>();
    
    /**
     * 
     * @param source  The transer object to build the model with
     * @param parent  The owning BlueChipMultiWeekProductOrder Object
     * @return The object representing the transfer object.
     */
    public static BlueChipProductOrderWeekIntf objectFactory(BlueChipProductOrderTOIntf source, BlueChipMultiWeekProductOrderIntf parent) {
        BlueChipProductOrderWeekIntf weekPO = null;
        
        weekPO = new BlueChipProductOrderWeekBO(source, parent);
        
        return weekPO;
    }
    
    /**
     * 
     */
    private BlueChipProductOrderWeekBO() {
        super();
    }

    private BlueChipProductOrderWeekBO(BlueChipProductOrderTOIntf source, BlueChipMultiWeekProductOrderIntf parent) {
        super();

        // set values
        this.parent = parent;
        this.owningLocation = (BlueChipLocationIntf)parent.getOwningLocation();
        
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
        try {
            this.weekEndingDate = formatter.parseDateTime(source.getWeekEndingDate());
        }
        catch (Exception e) {
           // failed to format date
           System.out.println("Invalid week ending date from DB: " + source.getWeekEndingDate());
        }

        DateTimeFormatter cutOffFormatter = DateTimeFormat.forPattern("yyyyMMdd kkmm");
        DateTime cutOffDateTime = null;
        
        //generate Monday po
        BlueChipProductOrderBO po = new BlueChipProductOrderBO();
        po.setAllowDrawEdits(source.isMondayAllowDrawEdits());
        po.setBaseDraw(source.getMondayBaseDraw());
        po.setDate(this.weekEndingDate.minusDays(6));
        po.setDayType(source.getMondayDayType());
        po.setInitialDraw(source.getMondayDraw());
        if (parent.getNextDistributionDate().getDayOfYear() == po.getDate().getDayOfYear()) {
            po.setNextDistributionDate(true);
        }
        
        try {
        	cutOffDateTime = cutOffFormatter.parseDateTime(source.getMondayCutOffDate() + " " + source.getMondayCutOffTime());             
        	po.setCutOffDateTime(cutOffDateTime);
        }
        catch (Exception e) {
			po.setCutOffDateTime(null); // ignore
		}
        
        po.setOwningLocation(this.owningLocation);
        po.setOwningWeek(this);
        BlueChipProductOrderBO original = new BlueChipProductOrderBO();
        original.setInitialDraw(po.getDraw()); // we only care about draw value changes.
        po.setOriginalPO(original);
        this.dailyPOs.put(BlueChipProductOrderWeekIntf.MONDAY, po);
        
        //generate Tuesday po
        po = new BlueChipProductOrderBO();
        po.setAllowDrawEdits(source.isTuesdayAllowDrawEdits());
        po.setBaseDraw(source.getTuesdayBaseDraw());
        po.setDate(this.weekEndingDate.minusDays(5));
        po.setDayType(source.getTuesdayDayType());
        po.setInitialDraw(source.getTuesdayDraw());
        if (parent.getNextDistributionDate().getDayOfYear() == po.getDate().getDayOfYear()) {
            po.setNextDistributionDate(true);
        }
        try {
        	cutOffDateTime = cutOffFormatter.parseDateTime(source.getTuesdayCutOffDate() + " " + source.getTuesdayCutOffTime());             
        	po.setCutOffDateTime(cutOffDateTime);
        }
        catch (Exception e) {
			po.setCutOffDateTime(null); // ignore
		}
        po.setOwningLocation(this.owningLocation);
        po.setOwningWeek(this);
        original = new BlueChipProductOrderBO();
        original.setInitialDraw(po.getDraw()); // we only care about draw value changes.
        po.setOriginalPO(original);
        this.dailyPOs.put(BlueChipProductOrderWeekIntf.TUESDAY, po);
        
        //generate Wednesday po
        po = new BlueChipProductOrderBO();
        po.setAllowDrawEdits(source.isWednesdayAllowDrawEdits());
        po.setBaseDraw(source.getWednesdayBaseDraw());
        po.setDate(this.weekEndingDate.minusDays(4));
        po.setDayType(source.getWednesdayDayType());
        po.setInitialDraw(source.getWednesdayDraw());
        if (parent.getNextDistributionDate().getDayOfYear() == po.getDate().getDayOfYear()) {
            po.setNextDistributionDate(true);
        }
        try {
        	cutOffDateTime = cutOffFormatter.parseDateTime(source.getWednesdayCutOffDate() + " " + source.getWednesdayCutOffTime());             
        	po.setCutOffDateTime(cutOffDateTime);
        }
        catch (Exception e) {
			po.setCutOffDateTime(null); // ignore
		}
        po.setOwningLocation(this.owningLocation);
        po.setOwningWeek(this);
        original = new BlueChipProductOrderBO();
        original.setInitialDraw(po.getDraw()); // we only care about draw value changes.
        po.setOriginalPO(original);
        this.dailyPOs.put(BlueChipProductOrderWeekIntf.WEDNESDAY, po);
        
        //generate Thursday po
        po = new BlueChipProductOrderBO();
        po.setAllowDrawEdits(source.isThursdayAllowDrawEdits());
        po.setBaseDraw(source.getThursdayBaseDraw());
        po.setDate(this.weekEndingDate.minusDays(3));
        po.setDayType(source.getThursdayDayType());
        po.setInitialDraw(source.getThursdayDraw());
        if (parent.getNextDistributionDate().getDayOfYear() == po.getDate().getDayOfYear()) {
            po.setNextDistributionDate(true);
        }
        try {
        	cutOffDateTime = cutOffFormatter.parseDateTime(source.getThursdayCutOffDate() + " " + source.getThursdayCutOffTime());             
        	po.setCutOffDateTime(cutOffDateTime);
        }
        catch (Exception e) {
			po.setCutOffDateTime(null); // ignore
		}
        po.setOwningLocation(this.owningLocation);
        po.setOwningWeek(this);
        original = new BlueChipProductOrderBO();
        original.setInitialDraw(po.getDraw()); // we only care about draw value changes.
        po.setOriginalPO(original);
        this.dailyPOs.put(BlueChipProductOrderWeekIntf.THURSDAY, po);
        
        //generate Friday po
        po = new BlueChipProductOrderBO();
        po.setAllowDrawEdits(source.isFridayAllowDrawEdits());
        po.setBaseDraw(source.getFridayBaseDraw());
        po.setDate(this.weekEndingDate.minusDays(2));
        po.setDayType(source.getFridayDayType());
        po.setInitialDraw(source.getFridayDraw());
        if (parent.getNextDistributionDate().getDayOfYear() == po.getDate().getDayOfYear()) {
            po.setNextDistributionDate(true);
        }
        try {
        	cutOffDateTime = cutOffFormatter.parseDateTime(source.getFridayCutOffDate() + " " + source.getFridayCutOffTime());             
        	po.setCutOffDateTime(cutOffDateTime);
        }
        catch (Exception e) {
			po.setCutOffDateTime(null); // ignore
		}
        po.setOwningLocation(this.owningLocation);
        po.setOwningWeek(this);
        original = new BlueChipProductOrderBO();
        original.setInitialDraw(po.getDraw()); // we only care about draw value changes.
        po.setOriginalPO(original);
        this.dailyPOs.put(BlueChipProductOrderWeekIntf.FRIDAY, po);
        
        //generate Saturday po
        po = new BlueChipProductOrderBO();
        po.setAllowDrawEdits(source.isSaturdayAllowDrawEdits());
        po.setBaseDraw(source.getSaturdayBaseDraw());
        po.setDate(this.weekEndingDate.minusDays(1));
        po.setDayType(source.getSaturdayDayType());
        po.setInitialDraw(source.getSaturdayDraw());
        if (parent.getNextDistributionDate().getDayOfYear() == po.getDate().getDayOfYear()) {
            po.setNextDistributionDate(true);
        }
        try {
        	cutOffDateTime = cutOffFormatter.parseDateTime(source.getSaturdayCutOffDate() + " " + source.getSaturdayCutOffTime());             
        	po.setCutOffDateTime(cutOffDateTime);
        }
        catch (Exception e) {
			po.setCutOffDateTime(null); // ignore
		}
        po.setOwningLocation(this.owningLocation);
        po.setOwningWeek(this);
        original = new BlueChipProductOrderBO();
        original.setInitialDraw(po.getDraw()); // we only care about draw value changes.
        po.setOriginalPO(original);
        this.dailyPOs.put(BlueChipProductOrderWeekIntf.SATURDAY, po);
        
        //generate Sunday po
        po = new BlueChipProductOrderBO();
        po.setAllowDrawEdits(source.isSundayAllowDrawEdits());
        po.setBaseDraw(source.getSundayBaseDraw());
        po.setDate(this.weekEndingDate);
        po.setDayType(source.getSundayDayType());
        po.setInitialDraw(source.getSundayDraw());
        if (parent.getNextDistributionDate().getDayOfYear() == po.getDate().getDayOfYear()) {
            po.setNextDistributionDate(true);
        }
        try {
        	cutOffDateTime = cutOffFormatter.parseDateTime(source.getSundayCutOffDate() + " " + source.getSundayCutOffTime());             
        	po.setCutOffDateTime(cutOffDateTime);
        }
        catch (Exception e) {
			po.setCutOffDateTime(null); // ignore
		}
        po.setOwningLocation(this.owningLocation);
        po.setOwningWeek(this);
        original = new BlueChipProductOrderBO();
        original.setInitialDraw(po.getDraw()); // we only care about draw value changes.
        po.setOriginalPO(original);
        this.dailyPOs.put(BlueChipProductOrderWeekIntf.SUNDAY, po);
        
    }
    
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getHashKey()
     */
    public String getHashKey() {
        // use weed ending date
        String hashKey = this.weekEndingDate.toString("yyyyMMdd");
        return hashKey;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getWeekEndingDate()
     */
    public DateTime getWeekEndingDate() {
        return this.weekEndingDate;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getNextDistributionDate()
     */
    public DateTime getNextDistributionDate() {
        return this.parent.getNextDistributionDate();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getProductOrderID()
     */
    public String getProductOrderID() {
        return this.parent.getProductOrderID();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getProductOrderTypeCode()
     */
    public String getProductOrderTypeCode() {
        return this.parent.getProductOrderTypeCode();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getProductCode()
     */
    public String getProductCode() {
        return this.parent.getProductCode();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getSequenceNumber()
     */
    public int getSequenceNumber() {
        return this.parent.getSequenceNumber();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getProductDescription()
     */
    public String getProductDescription() {
        return this.parent.getProductDescription();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getProductName()
     */
    public String getProductName() {
        return this.parent.getProductName();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getMaxDrawThreshold()
     */
    public int getMaxDrawThreshold() {
        return this.parent.getMaxDrawThreshold();
    }
    public int getMinDrawTheshold() {
        return this.parent.getMinDrawThreshold();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#isChanged()
     */
    public boolean isChanged() {
        Iterator<BlueChipProductOrderIntf> dayItr = this.dailyPOs.values().iterator();
        while (dayItr.hasNext()) {
            BlueChipProductOrderIntf po = dayItr.next();
            if (po.isChanged()) {
                // we can stop checking on first day changed.
                return true;
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getOwningLocation()
     */
    public BlueChipLocationIntf getOwningLocation() {
        return this.owningLocation;
    }

    /**
     * @return The multi week version of this PO (the one that contains this week, it's parent
     */
    public BlueChipMultiWeekProductOrderIntf getOwningMultiWeek() {
        return this.parent;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getDailyProductOrders()
     */
    public Collection<BlueChipProductOrderIntf> getDailyProductOrders() {
        return new ArrayList<BlueChipProductOrderIntf>(this.dailyPOs.values());
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getMondayPO()
     */
    public BlueChipProductOrderIntf getMondayPO() {
        return this.dailyPOs.get(BlueChipProductOrderWeekIntf.MONDAY);
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getTuesdayPO()
     */
    public BlueChipProductOrderIntf getTuesdayPO() {
        return this.dailyPOs.get(BlueChipProductOrderWeekIntf.TUESDAY);
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getWednesdayPO()
     */
    public BlueChipProductOrderIntf getWednesdayPO() {
        return this.dailyPOs.get(BlueChipProductOrderWeekIntf.WEDNESDAY);
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getThursdayPO()
     */
    public BlueChipProductOrderIntf getThursdayPO() {
        return this.dailyPOs.get(BlueChipProductOrderWeekIntf.THURSDAY);
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getFridayPO()
     */
    public BlueChipProductOrderIntf getFridayPO() {
        return this.dailyPOs.get(BlueChipProductOrderWeekIntf.FRIDAY);
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getSaturdayPO()
     */
    public BlueChipProductOrderIntf getSaturdayPO() {
        return this.dailyPOs.get(BlueChipProductOrderWeekIntf.SATURDAY);
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getSundayPO()
     */
    public BlueChipProductOrderIntf getSundayPO() {
        return this.dailyPOs.get(BlueChipProductOrderWeekIntf.SUNDAY);
    }

    /**
     * 
     * @param dayOfWeek
     * @param po
     */
    protected void setProductOrderForDay(int dayOfWeek, BlueChipProductOrderIntf po) {
        if (dayOfWeek >= DateTimeConstants.MONDAY && dayOfWeek <= DateTimeConstants.SUNDAY) {
            this.dailyPOs.put(new Integer(dayOfWeek), po);
        }
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getProductOrderForDay(int)
     */
    public BlueChipProductOrderIntf getProductOrderForDay(int day) {
        if (day >= DateTimeConstants.MONDAY && day <= DateTimeConstants.SUNDAY) {
            return this.dailyPOs.get(new Integer(day));
        }
        return null;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf#getWeeklyDrawTotal()
     */
    public int getWeeklyDrawTotal() {
        int weeklyDrawTotal = 0;
        if (this.dailyPOs != null && !this.dailyPOs.isEmpty()) {
            Iterator<BlueChipProductOrderIntf> itr = this.dailyPOs.values().iterator();
            while (itr.hasNext()) {
                BlueChipProductOrderIntf bcpo = itr.next();
                weeklyDrawTotal += bcpo.getTotalDraw();
            }
        }
        return weeklyDrawTotal;
    }

    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(BlueChipProductOrderWeekIntf o) {
        // use week ending date for comparison
        BlueChipProductOrderWeekIntf otherObj = o;
        return this.getHashKey().compareTo(otherObj.getHashKey());
    }

    /**
     *  @return true if week contains next distribution date, otherwise false
     */
    public boolean containsNextDistributionDate() {
        boolean nextDistroInWeek = false;

        if (this.dailyPOs != null && !this.dailyPOs.isEmpty()) {
            Iterator<BlueChipProductOrderIntf> itr = this.dailyPOs.values().iterator();
            while (itr.hasNext()) {
                BlueChipProductOrderIntf bcpo = itr.next();
                if (bcpo.isNextDistributionDate()) {
                    nextDistroInWeek = true;
                    break;
                }
            }
        }
        
        return nextDistroInWeek;
    }
    /**
     * clears all references to object for memory management
     */
    public void clear() {
       if (this.dailyPOs != null && !this.dailyPOs.isEmpty()){
           Iterator<BlueChipProductOrderIntf> itr = this.dailyPOs.values().iterator();
           while (itr.hasNext()) {
               BlueChipProductOrderIntf po = itr.next();
               po.clear();
           }
           this.dailyPOs.clear();
       }
       this.owningLocation = null;
       this.parent = null;
       this.weekEndingDate = null;
    }

	@Override
	public boolean getHasCutOffDaysForToday() {
		boolean hasCutOffDaysToday = false;

		for (BlueChipProductOrderIntf po : this.dailyPOs.values()) {
			try {
				if (po.isTodayCutOffDay()) {
					hasCutOffDaysToday = true;
					break;
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return hasCutOffDaysToday;
	}

	@Override
	public Collection<BlueChipProductOrderIntf> getDailyProductOrdersThatCutOffToday() {
        ArrayList<BlueChipProductOrderIntf> pos = new ArrayList<BlueChipProductOrderIntf>();
		for (BlueChipProductOrderIntf po : this.dailyPOs.values()) {
			try {
				if (po.isTodayCutOffDay()) {
					pos.add(po);
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
        
		return pos;
	}
}
