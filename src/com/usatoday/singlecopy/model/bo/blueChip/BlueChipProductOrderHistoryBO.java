package com.usatoday.singlecopy.model.bo.blueChip;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderHistoryIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderHistoryWeekIntf;
import com.usatoday.singlecopy.model.transferObjects.blueChip.BlueChipProductOrderHistoryTO;

public class BlueChipProductOrderHistoryBO implements
		BlueChipProductOrderHistoryIntf {

	private HashMap<String, BlueChipProductOrderHistoryWeekIntf> history = null;
	private String poid = null;
	
    public static BlueChipProductOrderHistoryBO objectFactory(Collection<BlueChipProductOrderHistoryTO> source, String productOrderID) {
    	BlueChipProductOrderHistoryBO historyObj = new BlueChipProductOrderHistoryBO(source, productOrderID);
    	
    	return historyObj;
    
    }
	private BlueChipProductOrderHistoryBO() {
		super();
		
		this.history = new HashMap<String, BlueChipProductOrderHistoryWeekIntf>();
	}

	private BlueChipProductOrderHistoryBO(Collection<BlueChipProductOrderHistoryTO> source, String productOrderID) {
		super();
		
		this.poid = productOrderID;
		
		this.history = new HashMap<String, BlueChipProductOrderHistoryWeekIntf>();
		
		for (BlueChipProductOrderHistoryTO to : source) {
			BlueChipProductOrderHistoryWeekIntf aWeek = BlueChipProductOrderHistoryWeekBO.objectFactory(to, this);
			this.history.put(aWeek.getHashKey(), aWeek);
		}
	}
	
	@Override
	public Collection<BlueChipProductOrderHistoryWeekIntf> getAllWeeks() {
		// history.values();
		
        ArrayList<BlueChipProductOrderHistoryWeekBO> locs = new ArrayList<BlueChipProductOrderHistoryWeekBO>(this.history.size());
        
        for (BlueChipProductOrderHistoryWeekIntf aWeek : this.history.values()) {
        	locs.add((BlueChipProductOrderHistoryWeekBO)aWeek);
        }
        Collections.sort(locs);
        
        Collections.reverse(locs);
        
        ArrayList<BlueChipProductOrderHistoryWeekIntf> sortedPOs = new ArrayList<BlueChipProductOrderHistoryWeekIntf>(locs);
        
        return sortedPOs;
		
	}

	@Override
	public int getNumberOfHistoryWeeks() {
		return this.history.size();
	}
	@Override
	public String getProductOrderID() {
		return this.poid;
	}

	
}
