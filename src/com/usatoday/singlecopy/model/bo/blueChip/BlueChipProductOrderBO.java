/*
 * Created on Mar 28, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.bo.blueChip;

import java.util.ArrayList;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipLocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderWeekIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf;

/**
 * @author aeast
 * @date Mar 28, 2008
 * @class BlueChipProductOrderBO
 * 
 * 
 */
public class BlueChipProductOrderBO implements BlueChipProductOrderIntf,
        Comparable<BlueChipProductOrderIntf> {

    private boolean allowDrawEdits = false;
    private boolean isNextDistributionDate = false;
    
    private int baseDraw = 0;
    private DateTime date = null;
    private DateTime cutOffDateTime = null;
    private String dayType = null;
    private int draw = -1;
    
    private BlueChipProductOrderIntf originalPO = null;
    private BlueChipLocationIntf owningLocation = null;
    private BlueChipProductOrderWeekIntf owningWeek = null;
    
    private ArrayList<DrawUpdateErrorIntf> updateErrors = new ArrayList<DrawUpdateErrorIntf>();
    
    /**
     * 
     */
    protected BlueChipProductOrderBO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderIntf#getDate()
     */
    public DateTime getDate() {
        return this.date;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderIntf#isNextDistributionDate()
     */
    public boolean isNextDistributionDate() {
        return this.isNextDistributionDate;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderIntf#getDraw()
     */
    public int getDraw() {
        return this.draw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderIntf#setDraw(int)
     */
    public void setDraw(int quantity) throws UsatException {
        
        if (quantity >= this.getMinDrawThreshold() && quantity <= this.getMaxDrawThreshold()) {
            this.draw = quantity;
        }
        else {
            throw new UsatException("Draw not in valid range of values:  "+ this.getMinDrawThreshold() + " <= DRAW <= " + this.getMaxDrawThreshold() );
        }
    }

    /**
     * 
     * @param initialDraw The value that came from the db. Only used when object is created.
     */
    protected void setInitialDraw(int initialDraw) {
        this.draw = initialDraw;
    }
    
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderIntf#getBaseDraw()
     */
    public int getBaseDraw() {
        
        return this.baseDraw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderIntf#getAllowDrawEdits()
     */
    public boolean getAllowDrawEdits() {
        return this.allowDrawEdits;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderIntf#getDayType()
     */
    public String getDayType() {
        return this.dayType;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderIntf#getOriginalProductOrder()
     */
    public BlueChipProductOrderIntf getOriginalProductOrder() {
        return this.originalPO;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderIntf#getOwningWeek()
     */
    public BlueChipProductOrderWeekIntf getOwningWeek() {
        return this.owningWeek;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipProductOrderIntf#getProductName()
     */
    public String getProductName() {
        String tempVal = "";
        if (this.owningWeek != null) {
           tempVal = this.owningWeek.getProductName();
        }
        return tempVal;
    }

    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(BlueChipProductOrderIntf o) {
        // use day of week to compare
        try {
            BlueChipProductOrderIntf otherObj = o;
            Integer sourceInt = new Integer(this.getDate().getDayOfWeek());
            Integer compareToInt = new Integer(otherObj.getDate().getDayOfWeek());
            return sourceInt.compareTo(compareToInt);
        }
        catch (Exception e) {
            // ignore
        }
        return 0;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getProductOrderID()
     */
    public String getProductOrderID() {
        String tempVal = "";
        if (this.owningWeek != null) {
           tempVal = this.owningWeek.getProductOrderID();
        }
        return tempVal;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getProductOrderTypeCode()
     */
    public String getProductOrderTypeCode() {
        String tempVal = "";
        if (this.owningWeek != null) {
           tempVal = this.owningWeek.getProductOrderTypeCode();
        }
        return tempVal;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getProductCode()
     */
    public String getProductCode() {
        String tempVal = "";
        if (this.owningWeek != null) {
           tempVal = this.owningWeek.getProductCode();
        }
        return tempVal;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getSequenceNumber()
     */
    public int getSequenceNumber() {
        int tempVal = 0;
        if (this.owningWeek != null) {
           tempVal = this.owningWeek.getSequenceNumber();
        }
        return tempVal;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getProductDescription()
     */
    public String getProductDescription() {
        String tempVal = "";
        if (this.owningWeek != null) {
           tempVal = this.owningWeek.getProductDescription();
        }
        return tempVal;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getMaxDrawThreshold()
     */
    public int getMaxDrawThreshold() {
        int tempVal = 0;
        if (this.owningWeek != null) {
           tempVal = this.owningWeek.getMaxDrawThreshold();
        }
        return tempVal;
    }

    public int getMinDrawThreshold() {
        int tempVal = 0;
        if (this.owningWeek != null) {
           tempVal = this.owningWeek.getMinDrawTheshold();
        }
        return tempVal;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#isChanged()
     */
    public boolean isChanged() {
        if (this.draw != this.originalPO.getDraw()) {
            return true;
        }
        return false;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getTotalDraw()
     */
    public int getTotalDraw() {
        int totDraw = 0;
        if (this.draw > -1) {
            totDraw = this.draw;
        }
        return totDraw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getTotalReturns()
     */
    public int getTotalReturns() {
        // Blue Chips don't have returns so always 0
        return 0;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getOwningLocation()
     */
    public LocationIntf getOwningLocation() {
        return this.owningLocation;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getUpdateErrors()
     */
    public DrawUpdateErrorIntf[] getUpdateErrors() {
        if (this.updateErrors.size() > 0) {
            return (DrawUpdateErrorIntf[])this.updateErrors.toArray(new DrawUpdateErrorIntf[this.updateErrors.size()]);
        }
        else {
            return null;
        }
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#setSaved(com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf)
     */
    public void setSaved(ProductOrderIntf newSource) {
        if (newSource instanceof BlueChipProductOrderIntf) {
            BlueChipProductOrderBO newSourceBO = new BlueChipProductOrderBO();
            BlueChipProductOrderBO s = (BlueChipProductOrderBO)newSource;
            newSourceBO.setInitialDraw(s.getDraw());
            this.originalPO = (BlueChipProductOrderIntf)newSourceBO;
            
            this.updateErrors.clear();
        }
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#hasEdittableFields()
     */
    public boolean hasEdittableFields() {
        // Only one field is open for edits for blue chip
        return this.allowDrawEdits;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#setPOAsDeleted()
     */
    public void setPOAsDeleted() {
        this.originalPO = null;
        
        // set values to unset
        this.draw = -1;
        
        // turn off all edit fields
        this.allowDrawEdits = false;

    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#clearUpdateErrors()
     */
    public void clearUpdateErrors() {
        this.updateErrors.clear();
    }

    public void setAllowDrawEdits(boolean allowDrawEdits) {
        this.allowDrawEdits = allowDrawEdits;
    }
    public void setBaseDraw(int baseDraw) {
        this.baseDraw = baseDraw;
    }
    public void setDate(DateTime date) {
        this.date = date;
    }
    public void setDayType(String dayType) {
        this.dayType = dayType;
    }
    public void setNextDistributionDate(boolean isNextDistributionDate) {
        this.isNextDistributionDate = isNextDistributionDate;
    }
    public void setOriginalPO(BlueChipProductOrderIntf originalPO) {
        this.originalPO = originalPO;
    }
    public void setOwningLocation(BlueChipLocationIntf owningLocation) {
        this.owningLocation = owningLocation;
    }
    public void setOwningWeek(BlueChipProductOrderWeekIntf owningWeek) {
        this.owningWeek = owningWeek;
    }
    public void resetToOriginal() {
        if (this.originalPO != null) {
            this.setInitialDraw(this.originalPO.getDraw());
        }
    }

    public void addUpdateError(DrawUpdateErrorIntf updateError) {
        this.updateErrors.add(updateError);
    }
    
    // clear references to other objects for memory management. called on logout
    public void clear() {
        this.originalPO = null;
        this.owningLocation = null;
        this.owningWeek = null;
        this.clearUpdateErrors();
    }

	@Override
	public DateTime getCutOffDateTime() {
		return this.cutOffDateTime;
	}

	public void setCutOffDateTime(DateTime cutOffDateTime) {
		this.cutOffDateTime = cutOffDateTime;
	}

	@Override
	public boolean isPastCutOffDateTime() {
		boolean pastCutOff = true;

		DateTime cutOff = this.cutOffDateTime;
    	
    	if (cutOff != null && cutOff.isAfterNow()) {
    		pastCutOff = false;
    	}
		
		return pastCutOff;
	}

	@Override
	public boolean isTodayCutOffDay() {
		boolean todayIsCutOff = false;
		
		DateTime cutOff = this.cutOffDateTime;
		
		if (cutOff != null) {
			DateTime now = new DateTime();
			if (cutOff.getDayOfYear() == now.getDayOfYear()) {
				if (cutOff.getYear() == now.getYear()) {
					todayIsCutOff = true;
				}
			}
		}
		
		return todayIsCutOff;
	}
}
