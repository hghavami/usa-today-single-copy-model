/*
 * Created on Aug 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.bo.markets;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.integration.MarketDAO;
import com.usatoday.singlecopy.model.interfaces.markets.MarketIntf;
import com.usatoday.singlecopy.model.transferObjects.MarketTO;

/**
 * @author aeast
 * @date Aug 2, 2007
 * @class MarketBO
 * 
 * Read Only Market Information.
 * 
 */
public class MarketBO implements MarketIntf {
    
    private String id = null;
    private String description = null;
    private String localPhone = null;
    private String rdlPhone = null;
    private String emailAddress = null;

    /**
     * 
     */
    public MarketBO(MarketIntf source) {
        super();
        this.id = source.getID();
        this.description = source.getDescription();
        this.localPhone = source.getLocalPhone();
        this.rdlPhone = source.getRDLPhone();
        this.emailAddress = source.getEmailAddress();
    }

    public static MarketIntf fetchMarket(String marketID) throws UsatException {
        MarketIntf market = null;

        MarketDAO dao = new MarketDAO();
        
        market = dao.fetchMarket(marketID);
        
        return market;
    }
    
    public static Collection<MarketIntf> fetchMarkets() throws UsatException {
        Collection<MarketIntf> markets = new ArrayList<MarketIntf>();
        Collection<MarketTO> toMarkets = null;
        

        MarketDAO dao = new MarketDAO();
        
        toMarkets = dao.fetchMarkets();
        
        Iterator<MarketTO> itr = toMarkets.iterator();
        // convert tranfer objects to business objects
        while(itr.hasNext()) {
            MarketIntf mTO = itr.next();
            markets.add(new MarketBO(mTO));
        }
        
        return markets;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.markets.MarketIntf#getID()
     */
    public String getID() {
        return this.id;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.markets.MarketIntf#getDescription()
     */
    public String getDescription() {
        return this.description;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.markets.MarketIntf#getLocalPhone()
     */
    public String getLocalPhone() {
        return this.localPhone;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.markets.MarketIntf#getRDLPhone()
     */
    public String getRDLPhone() {
        return this.rdlPhone;
    }

    public String getEmailAddress() {
        return this.emailAddress;
    }
    
    public boolean getHasEmailAddress() {
        if (this.emailAddress != null && this.emailAddress.length() > 0) {
            return true;
        }
        else {
            return false;
        }
    }
}
