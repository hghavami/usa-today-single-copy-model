/*
 * Created on Mar 30, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.bo.users;

import com.usatoday.singlecopy.model.interfaces.users.AccessRightsIntf;

/**
 * @author aeast
 * @date Mar 30, 2007
 * @class AccessRightBO
 * 
 * This class represents access to part of the application
 * 
 */
public class AccessRightBO implements AccessRightsIntf {

    private String accessRight = null;
    private String emailReminderTimePreference = null;
    private AccessRightsIntf original = null;
    private boolean receivingEmailReminders = false;
    
    /**
     * 
     */
    public AccessRightBO() {
        super();
    }
    
    public AccessRightBO(AccessRightsIntf src) {
        this.accessRight = src.getAccessRight();
        this.emailReminderTimePreference = src.getEmailReminderTimePreference();
        this.receivingEmailReminders = src.isReceivingEmailReminders();
        original = src;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.users.EmailScheduleSentTimeIntf#getAccessRight()
     */
    public String getAccessRight() {
        return this.accessRight;
    }

    public String getEmailReminderTimePreference() {
        return this.emailReminderTimePreference;
    }
    
    public boolean isChanged() {
        if (this.receivingEmailReminders != this.original.isReceivingEmailReminders()) {
            return true;
        }

        if (!this.emailReminderTimePreference.equalsIgnoreCase(this.original.getEmailReminderTimePreference())) {
            return true;
        }

        return false;
    }  
    public boolean isReceivingEmailReminders() {
        return this.receivingEmailReminders;
    }
    /**
     * @param emailReminderTimePreference The emailReminderTimePreference to set.
     */
    public void setEmailReminderTimePreference(String emailReminderTimePreference) {
        this.emailReminderTimePreference = emailReminderTimePreference;
    }
    /**
     * @param receivesEmailReminders The receivesEmailReminders to set.
     */
    public void setReceivingEmailReminders(boolean receivesEmailReminders) {
        this.receivingEmailReminders = receivesEmailReminders;
    }
    
    public void setSave() {
        if (this.getEmailReminderTimePreference() != null && 
                !this.getEmailReminderTimePreference().trim().equals("")) {
            this.original.setEmailReminderTimePreference(this.getEmailReminderTimePreference());            
        }
        this.original.setReceivingEmailReminders(this.isReceivingEmailReminders());
    }
}
	
