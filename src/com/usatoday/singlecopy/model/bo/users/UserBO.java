/*
 * Created on Feb 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.bo.users;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.lang.RandomStringUtils;

import com.usatoday.singlecopy.NotAuthorizedException;
import com.usatoday.singlecopy.OptOutFailedException;
import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.UsatLoginFailedException;
import com.usatoday.singlecopy.model.bo.blueChip.BlueChipLocationBO;
import com.usatoday.singlecopy.model.bo.drawmngmt.RouteBO;
import com.usatoday.singlecopy.model.integration.BlueChipDrawDAO;
import com.usatoday.singlecopy.model.integration.RouteDAO;
import com.usatoday.singlecopy.model.integration.UserDAO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.BlueChipLocationIntf;
import com.usatoday.singlecopy.model.interfaces.markets.MarketIntf;
import com.usatoday.singlecopy.model.interfaces.users.AccessRightsIntf;
import com.usatoday.singlecopy.model.interfaces.users.GenericUpdateErrorIntf;
import com.usatoday.singlecopy.model.interfaces.users.UpdateUserErrorIntf;
import com.usatoday.singlecopy.model.interfaces.users.UserIntf;
import com.usatoday.singlecopy.model.transferObjects.GenericUpdateErrorTO;
import com.usatoday.singlecopy.model.transferObjects.RouteTO;
import com.usatoday.singlecopy.model.transferObjects.UpdateUserErrorTO;
import com.usatoday.singlecopy.model.transferObjects.blueChip.BlueChipProductOrderTO;
import com.usatoday.singlecopy.model.util.MarketCache;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;


/**
 * @author aeast
 * @date Feb 2, 2007
 * @class UserBO
 * 
 * Concrete implementation of a User.
 * 
 */
public class UserBO implements UserIntf {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1808765823591817075L;
	private static final String paddingPool = "ABC1DEFG3HIJK5LMN7OPQ8RSTUVWXYZ";
    /**
     * 
     * @param email
     * @param password
     * @return
     * @throws UsatException
     */
    
    public static UserBO authenticateUser(String email, String password) throws UsatException, UsatLoginFailedException {
        
        // validate email/password
        if (email == null || email.trim().length() == 0) {
            throw new UsatException("Invalid Email Address.");
        }
        
        if (password == null || password.trim().length() == 0) {
            throw new UsatException("Invalid Password.");
        }
        
        String padding = null;
        
        try {
        	if (email.length() > 4) {
            	padding = email.substring(0,4) + RandomStringUtils.random(11, UserBO.paddingPool);
        	}
        	else {
        		padding = email + RandomStringUtils.random(13,UserBO.paddingPool);
        	}
        	padding = padding.toUpperCase();
        }
        catch (Exception e) {
        	padding = RandomStringUtils.random(15, UserBO.paddingPool);
        }
        
        String sessionID = UserBO.generateSessionID(padding);

        UserDAO dao = new UserDAO();
        
        if (password.length() > 10) {
            // invalid password so blank it out so we get the failed attempt logged on back end
            password = "          ";
        }

                
        UserIntf userIntf = dao.getUserByEmailAndPassword(email, password, sessionID);
        
        // password is not returned from db since we already know what it is.
        // make sure it gets set in original to for later use in update user
        // transactions
        userIntf.setPassword(password);
        
        // convert user transfer object ot business object
        UserBO user = new UserBO(userIntf);
        user.sessionID = sessionID;
        
        if (USATApplicationConstants.debug) {
        	System.out.println("Created new session id: " + sessionID);
        }
        
        return user;
    }
    
    /**
     * 
     * @param uid
     * @return A user
     * @throws UsatException
     * @throws UsatLoginFailedException
     */
    public static UserBO autoAuthUser(String uid) throws UsatException, UsatLoginFailedException {
        
        // validate uid
        if (uid == null || uid.trim().length() == 0) {
            throw new UsatException("Invalid UID.");
        }

        String padding = null;
        
        try {
        	if (uid.length() > 4) {
            	padding = uid.substring(0,4) + RandomStringUtils.random(11, UserBO.paddingPool);
        	}
        	else {
        		padding = uid + RandomStringUtils.random(13,UserBO.paddingPool);
        	}
        	padding = padding.toUpperCase();
        }
        catch (Exception e) {
        	padding = RandomStringUtils.random(15, UserBO.paddingPool);
        }
        
        String sessionID = UserBO.generateSessionID(padding);

        UserDAO dao = new UserDAO();
        
        if (uid.length() > 25) {
            // invalid password so blank it out so we get the failed attempt logged on back end
            uid = "          ";
        }
        
        UserIntf userIntf = dao.getUserByAutoLogin(uid, sessionID);
        
        // convert user transfer object ot business object
        UserBO user = new UserBO(userIntf);
        user.sessionID = sessionID;

        if (USATApplicationConstants.debug) {
        	System.out.println("Created new session id: " + sessionID);
        }
        
        return user;
    }
    
    public static boolean forgotPassword(String email) throws UsatException {
        
        // validate email/password
        if (email == null || email.trim().length() == 0) {
            throw new UsatException("Invalid Email Address.");
        }
        
        
        UserDAO dao = new UserDAO();
        
      
        return dao.forgotPasswordRequest(email);
        
    }
    
    private static String generateSessionID(String padding) {
        long time = System.currentTimeMillis();
        
        // remove all apostrophes
        padding = padding.replaceAll("'", "");
        
        String aSessionID = Long.toString(time);
    	int length = aSessionID.length();
        if (length > 20) {
            int beginIndex = aSessionID.length()-20;
            aSessionID = aSessionID.substring(beginIndex);
        }
        
        // max session id is 25
    	int numToFill = 25-length;
    	if (numToFill > 0 && padding != null) {
    		String tempString = padding;
    		if (padding.length() > numToFill) {
    			tempString = padding.substring(0, numToFill-1);
    		}
    		aSessionID = tempString + aSessionID;
    	}
        
        return aSessionID;
    }
    
    public static boolean optOutOfEmails(String uid) throws OptOutFailedException {
        boolean optedOut = false;
        String sessionID = UserBO.generateSessionID(uid);
        
        UserDAO dao = new UserDAO();
        
        if (uid.length() > 25) {
            // invalid password so blank it out so we get the failed attempt logged on back end
            uid = "          ";
        }

        Collection<GenericUpdateErrorTO> errorMsgs = null;
                
        try {
            errorMsgs = dao.autoOptOut(sessionID, uid);            
        }
        catch (Exception e) {
            throw new OptOutFailedException("Unexpected Failure: " + e.getMessage(), e);
        }

        if (errorMsgs != null && !errorMsgs.isEmpty()) {
            OptOutFailedException failed = new OptOutFailedException("Opt Out Failed.");
            Collection <GenericUpdateErrorIntf> errMsgs = new ArrayList<GenericUpdateErrorIntf>(errorMsgs); 
            failed.setErrorMessages(errMsgs);
            throw failed;
        }
        
        optedOut = true;
        
        return optedOut;
    }
    
    private Collection<AccessRightsIntf> accessRights = null;
    private boolean authenticated = false;
    
    // Blue Chip Draw Assigned locations - usually for usat staff or hotels
    private HashMap<String, BlueChipLocationIntf> blueChipLocations = null;
    private String displayName = null;

    private String emailAddress = null;
    private UserIntf original = null;    
    private String password = null;
    private String phoneNumber = null;
    
    // probably move this to a subclass - Usat staff or contractors
    private HashMap<String, RouteIntf> routes = null;
    
    private String sessionID = null;
    
    // FUTURE: These are the set of preferences for this user. Key/Value pairs. Key to hashmap is the preference Name.
    // private HashMap preferences = null;
    
    /**
     * 
     */
    public UserBO() {
        super();
        this.accessRights = new ArrayList<AccessRightsIntf>();
    }

    private UserBO(UserIntf source) {
        super();
        
        this.authenticated = true;
        this.emailAddress = source.getEmailAddress();
        this.password = source.getPassword();
        this.displayName = source.getDisplayName();
        this.phoneNumber = source.getPhoneNumber();
        this.original = source;
        
        // convert access rights to BO objects.
        this.accessRights = new ArrayList<AccessRightsIntf>();
        Iterator<AccessRightsIntf> itr = source.getAccessRights().iterator();
        while (itr.hasNext()) {
            AccessRightsIntf aRight = itr.next();
            AccessRightBO right = new AccessRightBO(aRight);
            this.accessRights.add(right);
        }
    }
    
    public AccessRightsIntf getAccessRight(String type) {
        AccessRightsIntf right = null;
        if (!this.accessRights.isEmpty()) {
            Iterator<AccessRightsIntf> itr = this.accessRights.iterator();
            while(itr.hasNext()) {
                right = itr.next();
                if (right.getAccessRight().equalsIgnoreCase(type)) {
                    break;
                }
                else {
                    right = null;
                }
            }
        }
        return right;
    }
    /**
     * @return Returns the accessRights.
     */
    public Collection<AccessRightsIntf> getAccessRights() {
        return this.accessRights;
    }
    
    public boolean getAuthenticated() {
        return this.authenticated;
    }
    
    public BlueChipLocationIntf getBlueChipLocation(String locationID)throws NotAuthorizedException, UsatException {
        BlueChipLocationIntf loc = null;
        if (this.getBlueChipLocations().containsKey(locationID)) {
            loc = (BlueChipLocationIntf)this.getBlueChipLocations().get(locationID);
        }
        return loc;
    }
	
	/**
	 * 
	 * @return The map of assigned blue chip locations for this user
	 *         locations are keyed by the location id.
	 * 
	 */
    public HashMap<String, BlueChipLocationIntf> getBlueChipLocations() throws NotAuthorizedException, UsatException {
        if (this.hasAccess(AccessRightsIntf.BLUE_CHIP_DRAW)) {
	        if (this.blueChipLocations == null) {
	            // load locations from DB
	            BlueChipDrawDAO dao = new BlueChipDrawDAO();
	            
	            HashMap<String, BlueChipLocationIntf> locationList = new HashMap<String, BlueChipLocationIntf>();
	            Collection<BlueChipProductOrderTO> source = null;
	            
	            source = dao.fetchBlueChipLocationsForUser(this.getEmailAddress(), this.sessionID);
	            
	            BlueChipLocationIntf[] locs = BlueChipLocationBO.objectFactory(source, this);
	            
	            if (locs != null &&  locs.length > 0) {
	                for (int i = 0; i < locs.length; i++) {
	                    locationList.put(locs[i].getLocationID(), locs[i]);
	                }
	            }
	            this.blueChipLocations = locationList; 
	            
	        }
        }
        else {
            throw new NotAuthorizedException("Not Authorized To Perform Reqeusted Function");
        }
        return this.blueChipLocations;
    }

    /**
     * @return Returns the displayName.
     */
    public String getDisplayName() {
        return this.displayName;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.UserIntf#getEmailAddress()
     */
    public String getEmailAddress() {
        return this.emailAddress;
    }
	
	/**
	 * 
	 * @return
	 * @throws UsatException
	 */
	public Collection<MarketIntf> getMarketsThatPertainToUser() throws UsatException {
	    
	    HashMap<String, MarketIntf> markets = new HashMap<String, MarketIntf>();
	    try {
		    HashMap<String, BlueChipLocationIntf> bChipLocs = this.getBlueChipLocations();
	        if (bChipLocs != null && !bChipLocs.isEmpty()) {
	        	
	        	for (BlueChipLocationIntf bLoc : bChipLocs.values()) {
	                MarketIntf m = bLoc.getOwningMarket();
	    	        if (markets.containsKey(m.getID())) {
	    	            continue;
	    	        }
	    	        else {
	    	            markets.put(m.getID(), m);
	    	        }
	        	}
	        	
	        }
	    }
	    catch (UsatException ue) {
	        // ignore..may not have access to blue chip
	    }
	    
	    try {
		    Collection<RouteIntf> routes = this.getRouteAsCollection();
		    
		    for (RouteIntf route : routes) {
		        String marketID = route.getMarketID();
		        
		        if (markets.containsKey(marketID)) {
		            continue;
		        }
		        else {
		            MarketIntf market = MarketCache.getInstance().getMarket(marketID);
		            markets.put(marketID, market);
		        }		    	
		    }
		    
	    }
	    catch (Exception ue) {
	        // ignore if don't have accesss to routes
	    }
	    return new ArrayList<MarketIntf>(markets.values());
	}
    
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.UserIntf#getPassword()
     */
    public String getPassword() {
        return this.password;
    }
    /**
     * @return Returns the phoneNumber.
     */
    public String getPhoneNumber() {
        return this.phoneNumber;
    }
    
    /**
     * 
     * @param key The route ID
     * @return
     */
    public RouteBO getRoute(Object key) throws NotAuthorizedException, UsatException {
        RouteBO r = null;
        if (this.getRoutes().containsKey(key)) {
            r = (RouteBO)this.getRoutes().get(key);
        }
        return r;
    }
    
    public Collection<RouteIntf> getRouteAsCollection() throws NotAuthorizedException, UsatException {
        return this.getRoutes().values();
    }
    /**
     * @return Returns the routes.
     */
    public HashMap<String, RouteIntf> getRoutes() throws NotAuthorizedException, UsatException {
        if (this.hasAccess(AccessRightsIntf.ROUTE_DRAW_MNGMT)) {
	        if (this.routes == null) {
	            // load routes from DB
	            RouteDAO dao = new RouteDAO();
	            
	            HashMap<String, RouteIntf> routeList = new HashMap<String, RouteIntf>();
	            HashMap<String, RouteTO> source = dao.fetchRouteList(this.getEmailAddress());
	            
	            for (RouteTO r : source.values()) {
	                RouteBO rbo = new RouteBO(r);
	                rbo.setCurrentUserKey(this.getEmailAddress());
	                String key = rbo.getMarketID() + rbo.getDistrictID() + rbo.getRouteID();
	                routeList.put(key, rbo);	            	
	            }
	            
	            this.routes = routeList; 
	            
	        }
        }
        else {
            throw new NotAuthorizedException("Not Authorized To Perform Reqeusted Function");
        }
        return this.routes;
    }
    
    public String getSessionID() {
        return this.sessionID;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.users.UserIntf#hasAccess(java.lang.String)
     */
    public boolean hasAccess(String accessRightCode) {
        boolean hasAccess = false;
        if (this.accessRights != null) {
        	
        	for(AccessRightsIntf right : this.accessRights) {
                if (right.getAccessRight().equalsIgnoreCase(accessRightCode)) {
                    hasAccess = true;
                    break;
                }        		
        	}
        }
        
        return hasAccess;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.UserIntf#isUserAuthenticated()
     */
    public boolean isUserAuthenticated() {
        return this.authenticated;
    }
 
    /**
     * helper method to clear user fields if they log out.
     *
     */
    public void logout() {
        this.authenticated = false;
        this.password = "";
        this.displayName = "";
        if (this.accessRights != null) {
            this.accessRights.clear();
        }
        this.phoneNumber = "";
        if (this.routes != null && !this.routes.isEmpty()) {
        	for(RouteIntf r : this.routes.values()) {
        		r.clear();
        	}
        	
            this.routes.clear();
    	}
        this.original = null;
        if (this.blueChipLocations != null && !this.blueChipLocations.isEmpty()) {
            Collection<BlueChipLocationIntf> locs = this.blueChipLocations.values();
            Iterator<BlueChipLocationIntf> itr = locs.iterator();
            while (itr.hasNext()) {
                BlueChipLocationIntf loc = itr.next();
                loc.clear();
            }
            this.blueChipLocations.clear();
        }
        
    }
    
    /**
     * This method reloads the blue chip data from the database. Any unsaved changes will be lost.
     * @throws NotAuthorizedException
     * @throws UsatException
     */
    public void reloadBlueChipLocations() throws NotAuthorizedException, UsatException {
        if (!this.hasAccess(AccessRightsIntf.BLUE_CHIP_DRAW)) {
            throw new NotAuthorizedException("Not Authorized To Perform Reqeusted Function");
        }

        // clear all blue current blue chip locations
        if (this.blueChipLocations != null && !this.blueChipLocations.isEmpty()) {
        	
        	for (BlueChipLocationIntf loc : this.blueChipLocations.values()) {
                loc.clear();
        	}
            this.blueChipLocations.clear();
        }
        // force a reload
        this.blueChipLocations = null;
        this.getBlueChipLocations();
    }
	/* (non-Javadoc)
	 * @see com.usatoday.singlecopy.model.interfaces.users.UserIntf#save()
	 */
	public void save() throws UsatException {
         
		 // validate email/password
		String tempEmail="";
		String tempPassword="";
		boolean updateUser = false;
		ArrayList<UpdateUserErrorTO> errors = new ArrayList<UpdateUserErrorTO>();
		
		 if (this.emailAddress == null || this.emailAddress.equals("") || this.emailAddress.trim().length() == 0) {		 	
		 	tempEmail = original.getEmailAddress(); 
		 	this.emailAddress = original.getEmailAddress();
         } else {
         	tempEmail = this.emailAddress.toUpperCase();
         	updateUser = true;
         }

		 if (this.password == null || this.password.equals("") || this.password.trim().length() == 0) {		 	
		 	tempPassword = original.getPassword();  
		 	this.password = original.getPassword();
         } else {
         	tempPassword = this.password;
         	updateUser = true;
         }
		
		 // Read thru access rights and if changed update access rights email prefs
		 try {
	       AccessRightsIntf accessRight = null;
	        if (!this.accessRights.isEmpty()) {
	            Iterator<AccessRightsIntf> itr = this.accessRights.iterator();
	            while(itr.hasNext()) {
	                accessRight = itr.next();
	                if (accessRight != null && accessRight.isChanged()) {
	        			UserDAO dao = new UserDAO();
	        			errors.addAll(dao.updateAccessRight(original.getEmailAddress(),this.sessionID, accessRight));
	        			if (errors.size() == 0) {
	        			    accessRight.setSave();
	        			}
	                }
	            }
	        }		 
		 } catch(Exception e) {
		     
		 }
				 
		if (updateUser) { 
			UserDAO dao = new UserDAO();
			try {
				errors.addAll(dao.updateUser(this.sessionID, original.getEmailAddress(),tempEmail,tempPassword));			    
			} catch (Exception e) {
			    System.out.println ("User profile changes could not be saved because: " + e);
			}

			
			if (errors != null && errors.size() > 0) {
			    
			    // reset email / password to original values
			    this.emailAddress = original.getEmailAddress();
			    this.password = original.getPassword();
			    
			    StringBuffer errorBuf = new StringBuffer();
			    Iterator<UpdateUserErrorTO> itr = errors.iterator();
			    while (itr.hasNext()) {
			        UpdateUserErrorIntf eTO = (UpdateUserErrorIntf)itr.next();
			        errorBuf.append(":").append(eTO.getErrorMessage()).append(": ");
			    }
			    
			    throw new UsatException("Failed To Save User: " + errorBuf.toString());
			}
			else {
			    //update original with saved values
			    this.original.setEmailAddress(tempEmail);
			    this.original.setPassword(tempPassword);
			}
			
		}

	}
    /**
     * @param displayName The displayName to set.
     */
    public void setDisplayName(String firstName) {
        this.displayName = firstName;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.UserIntf#setEmailAddress(java.lang.String)
     */
    public void setEmailAddress(String emailAddress)throws UsatException  {
        
        if (this.emailAddress.equalsIgnoreCase(emailAddress)) {
            return;
        }
        
       
   	 if (emailAddress == null || emailAddress.equals("") || this.emailAddress.trim().length() == 0) {    	
   	 	//
   	 } else {
   	 	if (emailAddress.length() > 4 ) {
		
   	 		int indexOfAtChar=emailAddress.indexOf("@");

   	 		if(indexOfAtChar > 0)
   	 		{
   	 			int indexOfDotChar =
   	 				emailAddress.indexOf(".",indexOfAtChar);
   	 			if(indexOfDotChar > 0)
   	 			{
   	 				System.out.println ("Valid Email Address.");
   	 				this.emailAddress = emailAddress;
   	 			}
   	 			else
   	 			{
   	 				System.out.println
					("Invalid Email Address- " +
					"Missing character '.' after '@'.");
   	 				throw new UsatException("Invalid Email Address- " +
   	 				"Missing character '.' after '@'.");
          	
   	 			}
          
          
   	 		} else {
      	
   	 			System.out.println("Invalid Email Address- " +
   	 			"Missing character'@' .");
   	 			throw new UsatException("Invalid Email Address- " +
   	 			"Missing character'@' .");      	
    
      	
   	 		}
   	 	} else{
   	 		System.out.println("Invalid Email Address- " +
   	 		"Should be at least 5 characters in length.");
      		throw new UsatException("Invalid Email Address- " +
      		"Should be at least 5 characters in length .");
        	
   	 	}
    		
   	 } 
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.UserIntf#setPassword(java.lang.String)
     */
    public void setPassword(String password) throws UsatException {
    	boolean numericFound = false;
    	
    	if (this.password == null || this.password.equals(password)) {
    	    return;
    	}
    	
     	     
   	 if (password == null || password.equals("") || this.password.trim().length() == 0) {    	
   	 	//
   	 } else {
    		if (password.length() > 5 && password.length() < 11  ) {
    				for(int i=0; i<password.length(); i++)
    					{
    						boolean check = Character.isDigit(password.charAt(i));
        				
    							if(check)
    								{
    									numericFound = true;
    								} 
    					}

    				if (numericFound == false) { 
    						throw new UsatException("Invalid Password.  At least 1 numeric value required");
    				}
    		}	else {
    					throw new UsatException("Invalid Password.  Length must be > 5 and < 11 characters long");
    		}
    		
   	 } 
    		
   	 
    		
        this.password = password;
       
    }
    /**
     * @param phoneNumber The phoneNumber to set.
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    /**
     * @param routes The routes to set.
     */
    public void setRoutes(HashMap<String, RouteIntf> routes) {
        this.routes = routes;
    }
    
    /**
     * 
     * @param authenticated
     */
    //private void setUserAuthenticated(boolean authenticated) {
     //   this.authenticated = authenticated;
    //}
    
}
