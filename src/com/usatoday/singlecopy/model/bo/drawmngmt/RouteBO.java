/*
 * Created on Feb 16, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.bo.drawmngmt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Interval;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.bo.drawmngmt.instance.DailyDrawManagementBO;
import com.usatoday.singlecopy.model.bo.drawmngmt.instance.DailyLocationBO;
import com.usatoday.singlecopy.model.bo.drawmngmt.instance.WeeklyDrawManagementBO;
import com.usatoday.singlecopy.model.bo.drawmngmt.instance.WeeklyLocationBO;
import com.usatoday.singlecopy.model.integration.RouteDAO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.BroadcastMessageIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyDrawManagementIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.RouteDrawManagementIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyDrawManangementIntf;
import com.usatoday.singlecopy.model.interfaces.users.UserIntf;
import com.usatoday.singlecopy.model.transferObjects.BroadcastMessageTO;
import com.usatoday.singlecopy.model.transferObjects.LocationTO;
import com.usatoday.singlecopy.model.transferObjects.DrawManagementTO;

/**
 * @author aeast
 * @date Feb 16, 2007
 * @class RouteBO
 * 
 * This class represents a Route.
 * 
 */
public class RouteBO implements RouteIntf, RouteDrawManagementIntf, Comparable<RouteIntf> {

    private String routeID = null;
    private String districtID = null;
    private String marketID = null;
    private String routeDescription = null;
    
    // current user's email
    private String currentUserKey = null;
    
    // following dates are used to restrict the dates
    // a user can select to work the RDL by day
    private DateTime rdlBeginDate = null;
    private DateTime rdlEndDate = null;

    // following dates are used to restrict the dates
    // a user can select to work the weekly draw
    private DateTime weeklyDrawBeginDate = null;
    private DateTime weeklyDrawEndDate = null;
    
    private Collection<String> weeklyDrawPublications = null;
    
    //WeeklyEndDatesHashMap weeklyDrawEndDates = null;
    
    /**
     * 
     */
    public RouteBO() {
        super();
    }

    public RouteBO(RouteIntf source) {
        this.districtID = source.getDistrictID();
        this.routeDescription = source.getRouteDescription();
        this.marketID = source.getMarketID();
        this.routeID = source.getRouteID();
        this.rdlBeginDate = source.getRDLBeginDate();
        this.rdlEndDate = source.getRDLEndDate();
        this.weeklyDrawBeginDate = source.getWeeklyDrawBeginDate();
        this.weeklyDrawEndDate = source.getWeeklyDrawEndDate();
        
        this.weeklyDrawPublications = source.getWeeklyDrawPublications();
        if (this.weeklyDrawPublications == null) {
            this.weeklyDrawPublications = new ArrayList<String>();
        }
    }
    
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.RouteIntf#getRouteID()
     */
    public String getRouteID() {
        return routeID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.RouteIntf#getDistrictID()
     */
    public String getDistrictID() {
        return districtID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.RouteIntf#getMarketID()
     */
    public String getMarketID() {
        return marketID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.RouteIntf#getRouteDescription()
     */
    public String getRouteDescription() {
        return routeDescription;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.RouteIntf#getReturnDrawBeginDate()
     */
    public DateTime getRDLBeginDate() {
        return rdlBeginDate;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.RouteIntf#getReturnDrawEndDate()
     */
    public DateTime getRDLEndDate() {
        return rdlEndDate;
    }

    /**
     * @param districtID The districtID to set.
     */
    @SuppressWarnings("unused")
	private void setDistrictID(String districtID) {
        this.districtID = districtID;
    }
    /**
     * @param weeklyDrawEndDates The weeklyDrawEndDates to set.
    public void setWeeklyDrawEndDates(WeeklyEndDatesHashMap weeklyEndDates) {
        this.weeklyDrawEndDates = weeklyEndDates;
    }
     */
    /**
     * @return Returns the weeklyDrawEndDates.
     * Method deleted because of change in weekly draw dates
    public synchronized WeeklyEndDatesHashMap getWeeklyDrawDates() throws UsatException {
        if (this.weeklyDrawEndDates == null) {
            WeeklyEndDatesHashMap eDates = new WeeklyEndDatesHashMap();
            // pull from DB
            RouteDAO dao = new RouteDAO();
            Collection dateTO = dao.fetchRouteDates(this.currentUserKey, this.getMarketID(), this.getDistrictID(), this.getRouteID());
            Iterator itr = dateTO.iterator();
            while (itr.hasNext()) {
                RouteDateTO to = (RouteDateTO)itr.next();
                String pub = to.getProductID();
                DateTime start = to.getEarliestWeekEndingDate();
                DateTime end = to.getLatestWeekEndingDate();
                
                WeeklyDrawEndDatesBO endDates = eDates.getEndDatesForPub(pub);
                if (endDates == null) {
                    endDates = new WeeklyDrawEndDatesBO();
                    endDates.setPubCode(pub);
                    ArrayList pubEndDates = new ArrayList();
                    endDates.setWeeklyEndDates(pubEndDates);
                    eDates.put(pub, endDates);
                }
                
                Collection pubEndingDates = endDates.getWeeklyEndDates();
                pubEndingDates.add(start);
                DateTime interimDate = start.plusDays(7);
                while (interimDate.isBefore(end)) {
                    pubEndingDates.add(interimDate);
                    interimDate = interimDate.plusDays(7);
                }
                pubEndingDates.add(end);
                
            }
            
            this.weeklyDrawEndDates = eDates;
        }
        return this.weeklyDrawEndDates;
    }
     */
    
    /**
     * @param marketID The marketID to set.
     */
    @SuppressWarnings("unused")
	private void setMarketID(String marketID) {
        this.marketID = marketID;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf#getWeeklyDrawBeginDate()
     */
    public DateTime getWeeklyDrawBeginDate() {
        
        if (this.weeklyDrawBeginDate.isBefore(this.weeklyDrawEndDate)) {
            return this.weeklyDrawBeginDate;
        }
        else {
            return this.weeklyDrawEndDate.minusDays(6);
        }
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf#getWeeklyDrawEndDate()
     */
    public DateTime getWeeklyDrawEndDate() {
        return this.weeklyDrawEndDate;
    }
    
    /**
     * @param weeklyDrawBeginDate The weeklyDrawBeginDate to set.
     */
    @SuppressWarnings("unused")
	private void setWeeklyDrawBeginDate(DateTime weeklyDrawBeginDate) {
        this.weeklyDrawBeginDate = weeklyDrawBeginDate;
    }
    /**
     * @param weeklyDrawEndDate The weeklyDrawEndDate to set.
     */
    @SuppressWarnings("unused")
	private void setWeeklyDrawEndDate(DateTime weeklyDrawEndDate) {
        this.weeklyDrawEndDate = weeklyDrawEndDate;
    }
    /**
     * @param rdlBeginDate The rdlBeginDate to set.
     */
    @SuppressWarnings("unused")
	private void setRDLBeginDate(DateTime returnDrawBeginDate) {
        this.rdlBeginDate = returnDrawBeginDate;
    }
    /**
     * @param rdlEndDate The rdlEndDate to set.
     */
    @SuppressWarnings("unused")
	private void setRDLEndDate(DateTime returnDrawEndDate) {
        this.rdlEndDate = returnDrawEndDate;
    }
    /**
     * @param routeDescription The routeDescription to set.
     */
    @SuppressWarnings("unused")
	private void setRouteDescription(String routeDescription) {
        this.routeDescription = routeDescription;
    }
    /**
     * @param routeID The routeID to set.
     */
    @SuppressWarnings("unused")
	private void setRouteID(String routeID) {
        this.routeID = routeID;
    }
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
   // public int compareTo(Object o) {
    //    if (o instanceof RouteIntf) {
     //       RouteIntf otherObj = (RouteIntf)o;
      //      String thisKey = this.getMarketID() + this.getDistrictID() + this.getRouteID();
       //     String otherKey = otherObj.getMarketID() + otherObj.getDistrictID() + otherObj.getRouteID();
        //    return thisKey.compareTo(otherKey);
       // }
       // return 0;
    //}
    
    
    /*
     *  (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.RouteDrawManagementIntf#retrieveWeeklyDraw(org.joda.time.DateTime, java.lang.String, com.usatoday.singlecopy.model.interfaces.users.UserIntf)
     * 
     */    
    public WeeklyDrawManangementIntf retrieveWeeklyDraw(
            DateTime weekEndingDate, String productCode, UserIntf requestingUser, boolean returnsOnly)
            throws UsatException {
        
        // validate parameters 
        if (weekEndingDate == null || requestingUser == null || productCode == null) {
            throw new UsatException("retrieveWeeklyDraw::Invalid Request Parameters.");
        }
        
        int dayOfWeek = weekEndingDate.getDayOfWeek();
        int addDays = DateTimeConstants.SUNDAY - dayOfWeek;
        weekEndingDate = weekEndingDate.plusDays(addDays);
        
        boolean endDateValid = false;
        DateTime endDateTime = this.getWeeklyDrawEndDate().plusDays(1);
        Interval interval = new Interval(this.getWeeklyDrawBeginDate(), endDateTime);
        endDateValid = interval.contains(weekEndingDate); 
        if (!endDateValid) {
            throw new UsatException("Invalid Week Ending Specified: " + weekEndingDate.toString("yyyyMMdd"));
        }
        
        String requestedDate = weekEndingDate.toString("yyyyMMdd");
        
        RouteDAO dao = new RouteDAO();
        
        DrawManagementTO wdmto =dao.fetchDrawByWeek(requestingUser.getEmailAddress(), this.getMarketID(), this.getDistrictID(), this.getRouteID(), requestedDate, productCode, returnsOnly); 
        Collection<LocationTO> locationTO = wdmto.getLocations(); 
        
        // set up Weekly Draw Management object
        WeeklyDrawManagementBO wdm = new WeeklyDrawManagementBO();
        wdm.setProductCode(productCode);
        wdm.setRoute(this);
        wdm.setUser(requestingUser);
        wdm.setWeekEndingDate(weekEndingDate);
        
        wdm.setLateReturnsFlag(wdmto.isLateReturnsWeek());
        
        HashMap<String, LocationIntf> locations = new HashMap<String, LocationIntf>();
        wdm.setLocations(locations);
        
        // convert to Business Objects
        Iterator<LocationTO> locationItr = locationTO.iterator();
        while(locationItr.hasNext()) {
            LocationTO loc = locationItr.next();
            // create weekly
            WeeklyLocationBO wLoc = new WeeklyLocationBO(loc);
            locations.put(wLoc.getLocationHashKey(), wLoc);
        }
        
        wdm.determineIfRDLHasEdittableFields();
        
        return wdm;
    }
    
    
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.RouteDrawManagementIntf#retrieveDailyDraw(org.joda.time.DateTime, com.usatoday.singlecopy.model.interfaces.users.UserIntf)
     */
    public DailyDrawManagementIntf retrieveDailyDraw(DateTime date,
            UserIntf requestingUser) throws UsatException {
        
        boolean dateValid = false;
        DateTime endDateTime = this.getRDLEndDate().plusDays(1);
        Interval interval = new Interval(this.getRDLBeginDate(), endDateTime);
        dateValid = interval.contains(date); 
        if (!dateValid) {
            throw new UsatException("Invalid RDL Date Specified: " + date.toString("yyyyMMdd"));
        }
        
        RouteDAO dao = new RouteDAO();
        
        
        DrawManagementTO dto = dao.fetchRDL(requestingUser.getEmailAddress(), this.getMarketID(), this.getDistrictID(), this.getRouteID(), date);
        Collection<LocationTO> locationTO = dto.getLocations();          
        
        // set up Weekly Draw Management object
        DailyDrawManagementBO ddm = new DailyDrawManagementBO();
        
        ddm.setRoute(this);
        ddm.setUser(requestingUser);
        ddm.setRDLDate(date);
        ddm.setLateReturnsFlag(dto.isLateReturnsWeek());
        
        HashMap<String, LocationIntf> locations = new HashMap<String, LocationIntf>();
        ddm.setLocations(locations);
        
        // conver to Business Objects
        Iterator<LocationTO> locationItr = locationTO.iterator();
        while(locationItr.hasNext()) {
            LocationTO loc = locationItr.next();
            // create weekly
            DailyLocationBO dLoc = new DailyLocationBO(loc);
            locations.put(dLoc.getLocationHashKey(), dLoc);
        }
        
        ddm.determineIfRDLHasEdittableFields();
        
        return ddm;
    }
    
    /**
     * @return Returns the weeklyDrawPublications.
     */
    public Collection<String> getWeeklyDrawPublications() {
        return this.weeklyDrawPublications;
    }
    /**
     * @param weeklyDrawPublications The weeklyDrawPublications to set.
     */
    @SuppressWarnings("unused")
	private void setWeeklyDrawPublications(Collection<String> weeklyDrawPublications) {
        this.weeklyDrawPublications = weeklyDrawPublications;
    }
    
    public Collection<BroadcastMessageIntf> getRouteBroadcastMessages() throws UsatException {
        Collection<BroadcastMessageTO> messages = null;
        RouteDAO dao = new RouteDAO();
        messages = dao.fetchRouteMessages(this.currentUserKey, this.marketID, this.districtID, this.routeID);
        
        Collection<BroadcastMessageIntf> msgs = new ArrayList<BroadcastMessageIntf>(messages);
        return msgs;
    }
    /**
     * @return Returns the currentUserKey.
     */
    public String getCurrentUserKey() {
        return this.currentUserKey;
    }
    /**
     * @param currentUserKey The currentUserKey to set.
     */
    public void setCurrentUserKey(String currentUserKey) {
        this.currentUserKey = currentUserKey;
    }
    /**
     * Clear collections and references during a logout
     */
    public void clear() {
        this.weeklyDrawPublications.clear();
    }

	@Override
	public int compareTo(RouteIntf o) {
        RouteIntf otherObj = o;
        String thisKey = this.getMarketID() + this.getDistrictID() + this.getRouteID();
        String otherKey = otherObj.getMarketID() + otherObj.getDistrictID() + otherObj.getRouteID();
        return thisKey.compareTo(otherKey);

	}
}
