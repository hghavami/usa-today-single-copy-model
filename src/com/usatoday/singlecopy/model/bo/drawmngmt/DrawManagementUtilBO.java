/*
 * Created on Oct 10, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.bo.drawmngmt;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.integration.DrawManagmentInfoDAO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.DrawManagementUtilIntf;

/**
 * @author aeast
 * @date Oct 10, 2007
 * @class DrawManagementUtilBO
 * 
 * 
 * 
 */
public class DrawManagementUtilBO implements DrawManagementUtilIntf {

    private static DateTime distributionDate = null;
    private static DateTime distributionDateLastLoadTime = null;
    
    private static DateTime defaultRDLDate = null;
    private static DateTime defaultRDLDateLastLoadTime = null;
    
    /**
     * 
     */
    public DrawManagementUtilBO() {
        super();
        
        try {
            processDistributionDate();
        }
        catch (UsatException ue) {
            System.out.println("DrawManagementUtilBO: Failed to load distribution date: " + ue.getMessage());
        }
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.DrawManagementUtilIntf#getDistributionDate()
     */
    public DateTime getDistributionDate() throws UsatException {
        
        this.processDistributionDate();
        
        return new DateTime(DrawManagementUtilBO.distributionDate);
    }

    private synchronized void processDistributionDate() throws UsatException {
    
        if (DrawManagementUtilBO.distributionDate == null || 
            DrawManagementUtilBO.distributionDateLastLoadTime == null ||
            DrawManagementUtilBO.distributionDateLastLoadTime.isBeforeNow() ) {
         
            DrawManagmentInfoDAO dao = new DrawManagmentInfoDAO();
            
            DrawManagementUtilBO.distributionDate = dao.fetchDistributionDate();
            DrawManagementUtilBO.distributionDateLastLoadTime = new DateTime();
            DrawManagementUtilBO.distributionDateLastLoadTime = DrawManagementUtilBO.distributionDateLastLoadTime.plusMinutes(10);
        }
    }
    
    /**
     * 
     * @return The default date to display for working with RDL
     * @throws UsatException
     */
    public DateTime getDefaultRDLDate() throws UsatException {
        
        this.processDefaultRDLDate();
        
        return new DateTime(DrawManagementUtilBO.defaultRDLDate);
    }
    
    private synchronized void processDefaultRDLDate() throws UsatException {
        
            if (DrawManagementUtilBO.defaultRDLDate == null || 
                DrawManagementUtilBO.defaultRDLDateLastLoadTime == null ||
                DrawManagementUtilBO.defaultRDLDateLastLoadTime.isBeforeNow() ) {
             
                DrawManagmentInfoDAO dao = new DrawManagmentInfoDAO();
                
                DrawManagementUtilBO.defaultRDLDate = dao.fetchDefaultRDLDate();
                DrawManagementUtilBO.defaultRDLDateLastLoadTime = new DateTime();
                DrawManagementUtilBO.defaultRDLDateLastLoadTime = DrawManagementUtilBO.distributionDateLastLoadTime.plusMinutes(30);
            }
        }

}
