/*
 * Created on Mar 13, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.bo.drawmngmt;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderTypeIntf;

/**
 * @author aeast
 * @date Mar 13, 2007
 * @class ProductOrderTypeBO
 * 
 * 
 * 
 */
public class ProductOrderTypeBO implements ProductOrderTypeIntf {

    private String description = null;
    private String productCode = null;
    private String productOrderTypeCode = null;
    
    /**
     * 
     */
    public ProductOrderTypeBO() {
        super();
    }

    public ProductOrderTypeBO(ProductOrderTypeIntf source){
        super();
        if (source != null) {
            this.description = source.getDescription();
            this.productCode = source.getProductCode();
            this.productOrderTypeCode = source.getProductOrderTypeCode();
        }
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.ProductOrderTypeIntf#getDescription()
     */
    public String getDescription() {
        return this.description;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.ProductOrderTypeIntf#getProductCode()
     */
    public String getProductCode() {
        return this.productCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.ProductOrderTypeIntf#getProductOrderTypeCode()
     */
    public String getProductOrderTypeCode() {
        return this.productOrderTypeCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.ProductOrderTypeIntf#getKey()
     */
    public String getKey() {
        return this.getProductCode() + this.getProductOrderTypeCode();
    }

    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @param productCode The productCode to set.
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    /**
     * @param productOrderTypeCode The productOrderTypeCode to set.
     */
    public void setProductOrderTypeCode(String productOrderTypeCode) {
        this.productOrderTypeCode = productOrderTypeCode;
    }
}
