/*
 * Created on Apr 23, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.bo.drawmngmt.instance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.bo.drawmngmt.LocationBO;
import com.usatoday.singlecopy.model.bo.drawmngmt.ProductOrderBO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawManagementInstanceIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawManagementLocationIntf;
import com.usatoday.singlecopy.model.interfaces.users.UserIntf;

/**
 * @author aeast
 * @date Apr 23, 2007
 * @class DrawManagementBO
 * 
 * This base clas represents an instance of a draw day or week.
 * 
 */
public abstract class DrawManagementBO implements DrawManagementInstanceIntf {

    private RouteIntf route = null;
    private HashMap<String, LocationIntf> locations = null;
    private UserIntf user = null;
    
    protected int originalTotalDraw = -1;
    protected int originalTotalReturns = -1;
    protected boolean lateReturnsFlag = false;
    private boolean hasEdittableFields = true;
    
    private boolean updateErrorsFlag = false;
    private String globalUpdateErrorMessage =  "";
    
    /**
     * 
     */
    public DrawManagementBO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawManagementInstanceIntf#getRoute()
     */
    public RouteIntf getRoute() {
        return this.route;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawManagementInstanceIntf#getUser()
     */
    public UserIntf getUser() {
        return this.user;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawManagementInstanceIntf#getLocations()
     */
    public Collection<LocationIntf> getLocations() {
    	ArrayList<LocationBO> locBOArray = new ArrayList<LocationBO>();
    	
    	for (LocationIntf locIntf : this.locations.values()) {
    		if (locIntf instanceof LocationBO) {
    			locBOArray.add((LocationBO)locIntf);
    		}
    	}
    	
        Collections.sort(locBOArray);
        
        ArrayList<LocationIntf> aList = new ArrayList<LocationIntf>();
        aList.addAll(locBOArray);
        return aList;
    }

    /**
     * 
     * @param key
     * @return
     */
    public LocationIntf getLocation(String key) {
        LocationIntf loc = null;
        loc = (LocationIntf)this.locations.get(key);
        return loc;
    }
    
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawManagementInstanceIntf#getChangedProductOrders()
     */
    public Collection<ProductOrderIntf> getChangedProductOrders() {
        ArrayList<ProductOrderIntf> changedPOs = new ArrayList<ProductOrderIntf>();
        
        if (this.getLocations() != null && this.getLocations().size() > 0) {
            Iterator<LocationIntf> itr = this.getLocations().iterator();
            while(itr.hasNext()) {
                DrawManagementLocationIntf loc = (DrawManagementLocationIntf)itr.next();
                Collection<ProductOrderIntf> productOrders = loc.getProductOrders();
                Iterator<ProductOrderIntf> poItr = productOrders.iterator();
                while (poItr.hasNext()) {
                    ProductOrderIntf po = poItr.next();
                    if (po.isChanged()) {
                        changedPOs.add(po);
                    }
                }
            }
        }

        return changedPOs;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawManagementInstanceIntf#getNumberOfChangedProductOrders()
     */
    public int getNumberOfChangedProductOrders() {
        Collection<ProductOrderIntf> changes = this.getChangedProductOrders();
        return changes.size();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawManagementInstanceIntf#getNumberOfLocations()
     */
    public int getNumberOfLocations() {
        return this.locations.size();
    }
    
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawManagementInstanceIntf#saveChanges()
     */
    public abstract void saveChanges() throws UsatException;
    /**
     * @param locations The locations to set.
     */
    public void setLocations(HashMap<String, LocationIntf> locations) {
        this.locations = locations;
    }
    /**
     * @param route The route to set.
     */
    public void setRoute(RouteIntf route) {
        this.route = route;
    }
    /**
     * @param user The user to set.
     */
    public void setUser(UserIntf user) {
        this.user = user;
    }

    public int getTotalReturns() {
        int totalReturns = 0;
        if (this.getLocations() != null && this.getLocations().size() > 0) {
            Iterator<LocationIntf> itr = this.getLocations().iterator();
            while(itr.hasNext()) {
                DrawManagementLocationIntf loc = (DrawManagementLocationIntf)itr.next();
                Collection<ProductOrderIntf> productOrders = loc.getProductOrders();
                Iterator<ProductOrderIntf> poItr = productOrders.iterator();
                while (poItr.hasNext()) {
                    ProductOrderIntf po = poItr.next();
                    totalReturns += po.getTotalReturns();
                }
            }
        }
        return totalReturns;
    }

    public int getTotalDraw() {
        int totalDraw = 0;
        if (this.getLocations() != null && this.getLocations().size() > 0) {
            Iterator<LocationIntf> itr = this.getLocations().iterator();
            while(itr.hasNext()) {
                DrawManagementLocationIntf loc = (DrawManagementLocationIntf)itr.next();
                Collection<ProductOrderIntf> productOrders = loc.getProductOrders();
                Iterator<ProductOrderIntf> poItr = productOrders.iterator();
                while (poItr.hasNext()) {
                    ProductOrderIntf wpo = poItr.next();
                    totalDraw += wpo.getTotalDraw();
                }
            }
        }
        return totalDraw;
    }

    public int getOriginalTotalDraw() {
        // this only needs to be calculated one time since it can't change
        if (this.originalTotalDraw == -1) {
            int totalDraw = 0;
            if (this.getLocations() != null && this.getLocations().size() > 0) {
                Iterator<LocationIntf> itr = this.getLocations().iterator();
                while(itr.hasNext()) {
                    DrawManagementLocationIntf loc = (DrawManagementLocationIntf)itr.next();
                    Collection<ProductOrderIntf> productOrders = loc.getProductOrders();
                    Iterator<ProductOrderIntf> poItr = productOrders.iterator();
                    while (poItr.hasNext()) {
                        ProductOrderBO po = (ProductOrderBO) poItr.next();
                        totalDraw += po.getOriginalTotalDraw();
                    }
                } // end while more locations
                this.originalTotalDraw = totalDraw;
            }
            else {
                this.originalTotalDraw = 0;
            }
        }
        return this.originalTotalDraw;
    }

    public int getOriginalTotalReturns() {
        if (this.originalTotalReturns == -1) {
            int totalReturns = 0;
            if (this.getLocations() != null && this.getLocations().size() > 0) {
                Iterator<LocationIntf> itr = this.getLocations().iterator();
                while(itr.hasNext()) {
                    DrawManagementLocationIntf loc = (DrawManagementLocationIntf)itr.next();
                    Collection<ProductOrderIntf> productOrders = loc.getProductOrders();
                    Iterator<ProductOrderIntf> poItr = productOrders.iterator();
                    while (poItr.hasNext()) {
                        ProductOrderBO wpo = (ProductOrderBO) poItr.next();
                        totalReturns += wpo.getOriginalTotalReturns();
                    }
                } // end while more locations
                this.originalTotalReturns = totalReturns;
            }
            else {
                this.originalTotalReturns = 0;
            }
        }
        return this.originalTotalReturns;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawManagementInstanceIntf#getUpdateErrorsFlag()
     */
    public boolean getUpdateErrorsFlag() {
        return this.updateErrorsFlag;
    }
    
    public void setUpdateErrorsFlag(boolean errorsExist) {
        this.updateErrorsFlag = errorsExist;
    }

    /**
     * @return Returns the lateReturnsFlag.
     */
    public boolean getLateReturnsFlag() {
        return this.lateReturnsFlag;
    }

    /**
     * @param lateReturnsFlag The lateReturnsFlag to set.
     */
    public void setLateReturnsFlag(boolean lateReturnsFlag) {
        this.lateReturnsFlag = lateReturnsFlag;
    }
    
    protected void clearAllPOErrors() {
        // clear all errors from previous submissions
        Iterator<LocationIntf> locItr = this.getLocations().iterator();
        while (locItr.hasNext()) {
            LocationIntf loc = locItr.next();
            Iterator<ProductOrderIntf> poItr = loc.getProductOrders().iterator();
            while (poItr.hasNext()) {
                ProductOrderBO po = (ProductOrderBO)poItr.next();
                po.clearUpdateErrors();
            }
        }
    }
    /**
     * @return Returns the hasEdittableFields.
     */
    public boolean hasEdittableFields() {
        return this.hasEdittableFields;
    }
    
    /**
     * @param hasEdittableFields The hasEdittableFields to set.
     */
    @SuppressWarnings("unused")
	private void setHasEdittableFields(boolean hasEdittableFields) {
        this.hasEdittableFields = hasEdittableFields;
    }
    
    public void determineIfRDLHasEdittableFields() {
        // initialize to false
        this.hasEdittableFields = false;
        
        Iterator<LocationIntf> locItr = this.getLocations().iterator();
        while (locItr.hasNext() && !this.hasEdittableFields) {
            LocationIntf loc =locItr.next();
            Iterator<ProductOrderIntf> poItr = loc.getProductOrders().iterator();
            while (poItr.hasNext() && !this.hasEdittableFields) {
                ProductOrderBO po = (ProductOrderBO)poItr.next();
                if (po.hasEdittableFields()) {
                    this.hasEdittableFields = true;
                }
            }
        }
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawManagementInstanceIntf#getGlobalUpdateErrorMessage()
     */
    public String getGlobalUpdateErrorMessage() {
        if (this.globalUpdateErrorMessage == null) {
            this.globalUpdateErrorMessage = "";
        }
        return this.globalUpdateErrorMessage;
    }
    
    /**
     * @param globalUpdateErrorMessage The globalUpdateErrorMessage to set.
     */
    protected void setGlobalUpdateErrorMessage(String globalUpdateErrorMessage) {
        this.globalUpdateErrorMessage = globalUpdateErrorMessage;
    }
    
    /**
     * Clear any referenced data for use in logging out
     */
    public void clear() {
        if (this.locations != null && !this.locations.isEmpty()) {
            Iterator<LocationIntf> itr = this.locations.values().iterator();
	        while (itr.hasNext()) {
	            LocationIntf loc = itr.next();
	            loc.clear();
	        }
	        this.locations.clear();
        }
        this.route = null;
        this.user = null;
        this.locations = null;
    }
}
