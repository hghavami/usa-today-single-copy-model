/*
 * Created on Apr 20, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.bo.drawmngmt.instance;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.bo.drawmngmt.ProductOrderBO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyReadOnlyProductOrderIntf;
import com.usatoday.singlecopy.model.util.ZeroDrawReasonCodeCache;

/**
 * @author aeast
 * @date Apr 20, 2007
 * @class WeeklyProductOrderBO
 * 
 * This class represents an instance of a product order as it pertains to the weekly product orders view.
 * 
 * 
 */
public class WeeklyProductOrderBO extends ProductOrderBO implements WeeklyProductOrderIntf {

    // used for comparison 
    private WeeklyReadOnlyProductOrderIntf originalPO = null;
    
    //  Draw Fields
    // Monday
    private int mondayDraw = -1;
    private int mondayReturns = -1;
    private boolean allowMondayDrawEdits = false;
    private boolean allowMondayReturnsEdits = false;
    private String mondayDayType = null;
    private String mondayZeroDrawReasonCode = "";
    private String mondayChangeCode = "N";

    // Tuesday
    private int tuesdayDraw = -1;
    private int tuesdayReturns = -1;
    private boolean allowTuesdayDrawEdits = false;
    private boolean allowTuesdayReturnsEdits = false;
    private String tuesdayDayType = null;
    private String tuesdayZeroDrawReasonCode = "";
    private String tuesdayChangeCode = "N";

    // Wednesday
    private int wednesdayDraw = -1;
    private int wednesdayReturns = -1;
    private boolean allowWednesdayDrawEdits = false;
    private boolean allowWednesdayReturnsEdits = false;
    private String wednesdayDayType = null;
    private String wednesdayZeroDrawReasonCode = "";
    private String wednesdayChangeCode = "N";

    // Thursday
    private int thursdayDraw = -1;
    private int thursdayReturns = -1;
    private boolean allowThursdayDrawEdits = false;
    private boolean allowThursdayReturnsEdits = false;
    private String thursdayDayType = null;
    private String thursdayZeroDrawReasonCode = "";
    private String thursdayChangeCode = "N";

    // Friday
    private int fridayDraw = -1;
    private int fridayReturns = -1;
    private boolean allowFridayDrawEdits = false;
    private boolean allowFridayReturnsEdits = false;
    private String fridayDayType = null;
    private String fridayZeroDrawReasonCode = "";
    private String fridayChangeCode = "N";

    // Saturday
    private int saturdayDraw = -1;
    private int saturdayReturns = -1;
    private boolean allowSaturdayDrawEdits = false;
    private boolean allowSaturdayReturnsEdits = false;
    private String saturdayDayType = null;
    private String saturdayZeroDrawReasonCode = "";
    private String saturdayChangeCode = "N";

    // Sunday
    private int sundayDraw = -1;
    private int sundayReturns = -1;
    private boolean allowSundayDrawEdits = false;
    private boolean allowSundayReturnsEdits = false;
    private String sundayDayType = null;
    private String sundayZeroDrawReasonCode = "";
    private String sundayChangeCode = "N";

    /**
     * 
     */
    public WeeklyProductOrderBO() {
        super();
        
    }

    /**
     * @param source
     */
    public WeeklyProductOrderBO(WeeklyProductOrderIntf source) {
        super(source);
        
        // save off original value object
        this.originalPO = source;

        // the following method sets all fields to that of the original
        // so it is used for initialization as well.
        this.resetFieldsToOriginal();
    }

    public boolean getAllowFridayDrawEdits() {
        return this.allowFridayDrawEdits;
    }
    public void setAllowFridayDrawEdits(boolean allowFridayDrawEdits) {
        this.allowFridayDrawEdits = allowFridayDrawEdits;
    }
    public boolean getAllowFridayReturnsEdits() {
        return this.allowFridayReturnsEdits;
    }
    public void setAllowFridayReturnsEdits(boolean allowFridayReturnsEdits) {
        this.allowFridayReturnsEdits = allowFridayReturnsEdits;
    }
    public boolean getAllowMondayDrawEdits() {
        return this.allowMondayDrawEdits;
    }
    public void setAllowMondayDrawEdits(boolean allowMondayDrawEdits) {
        this.allowMondayDrawEdits = allowMondayDrawEdits;
    }
    public boolean getAllowMondayReturnsEdits() {
        return this.allowMondayReturnsEdits;
    }
    public void setAllowMondayReturnsEdits(boolean allowMondayReturnsEdits) {
        this.allowMondayReturnsEdits = allowMondayReturnsEdits;
    }
    public boolean getAllowSaturdayDrawEdits() {
        return this.allowSaturdayDrawEdits;
    }
    public void setAllowSaturdayDrawEdits(boolean allowSaturdayDrawEdits) {
        this.allowSaturdayDrawEdits = allowSaturdayDrawEdits;
    }
    public boolean getAllowSaturdayReturnsEdits() {
        return this.allowSaturdayReturnsEdits;
    }
    public void setAllowSaturdayReturnsEdits(boolean allowSaturdayReturnsEdits) {
        this.allowSaturdayReturnsEdits = allowSaturdayReturnsEdits;
    }
    public boolean getAllowSundayDrawEdits() {
        return this.allowSundayDrawEdits;
    }
    public void setAllowSundayDrawEdits(boolean allowSundayDrawEdits) {
        this.allowSundayDrawEdits = allowSundayDrawEdits;
    }
    public boolean getAllowSundayReturnsEdits() {
        return this.allowSundayReturnsEdits;
    }
    public void setAllowSundayReturnsEdits(boolean allowSundayReturnsEdits) {
        this.allowSundayReturnsEdits = allowSundayReturnsEdits;
    }
    public boolean getAllowThursdayDrawEdits() {
        return this.allowThursdayDrawEdits;
    }
    public void setAllowThursdayDrawEdits(boolean allowThursdayDrawEdits) {
        this.allowThursdayDrawEdits = allowThursdayDrawEdits;
    }
    public boolean getAllowThursdayReturnsEdits() {
        return this.allowThursdayReturnsEdits;
    }
    public void setAllowThursdayReturnsEdits(boolean allowThursdayReturnsEdits) {
        this.allowThursdayReturnsEdits = allowThursdayReturnsEdits;
    }
    public boolean getAllowTuesdayDrawEdits() {
        return this.allowTuesdayDrawEdits;
    }
    public void setAllowTuesdayDrawEdits(boolean allowTuesdayDrawEdits) {
        this.allowTuesdayDrawEdits = allowTuesdayDrawEdits;
    }
    public boolean getAllowTuesdayReturnsEdits() {
        return this.allowTuesdayReturnsEdits;
    }
    public void setAllowTuesdayReturnsEdits(boolean allowTuesdayReturnsEdits) {
        this.allowTuesdayReturnsEdits = allowTuesdayReturnsEdits;
    }
    public boolean getAllowWednesdayDrawEdits() {
        return this.allowWednesdayDrawEdits;
    }
    public void setAllowWednesdayDrawEdits(boolean allowWednesdayDrawEdits) {
        this.allowWednesdayDrawEdits = allowWednesdayDrawEdits;
    }
    public boolean getAllowWednesdayReturnsEdits() {
        return this.allowWednesdayReturnsEdits;
    }
    public void setAllowWednesdayReturnsEdits(boolean allowWednesdayReturnsEdits) {
        this.allowWednesdayReturnsEdits = allowWednesdayReturnsEdits;
    }
    public String getFridayChangeCode() {
        return this.fridayChangeCode;
    }
    public void setFridayChangeCode(String fridayChangeCode) {
        this.fridayChangeCode = fridayChangeCode;
    }
    public String getFridayDayType() {
        return this.fridayDayType;
    }
    public void setFridayDayType(String fridayDayType) {
        this.fridayDayType = fridayDayType;
    }
    public int getFridayDraw() {
        return this.fridayDraw;
    }
    /**
     * @param fridayDraw
     */
    public void setFridayDraw(int fridayDraw)throws UsatException {
        if (fridayDraw == this.fridayDraw) {
            return;
        }
        if (!this.allowFridayDrawEdits) {
            throw new UsatException("Friday Draw Field is not open for edits.");
        }

        if (fridayDraw > this.getFridayMaxDrawThreshold()) {
            throw new UsatException("Max Draw Exceeded");
        }
        if (fridayDraw < 0) {
            throw new UsatException("Draw value must be a positive integer, or zero.");
        }
        this.fridayDraw = fridayDraw;
    }
    
    public int getFridayMaxDrawThreshold() {
        int maxDraw = (this.fridayDraw > this.getMaxDrawThreshold()) ? this.fridayDraw : this.getMaxDrawThreshold();
        return maxDraw;
    }
    
    public int getFridayReturns() {
        return this.fridayReturns;
    }
    public void setFridayReturns(int fridayReturns) throws UsatException  {
        if (fridayReturns == this.fridayReturns) {
            return;
        }
        if (!this.allowFridayReturnsEdits) {
            throw new UsatException("Friday Returns is not open for edits.");
        }
        if (fridayReturns < -1) {
            throw new UsatException("Returns must be a positive integer, zero, or blank");
        }
        this.fridayReturns = fridayReturns;
    }
    public String getFridayZeroDrawReasonCode() {
        return this.fridayZeroDrawReasonCode;
    }
    public void setFridayZeroDrawReasonCode(String fridayZeroDrawReasonCode) throws UsatException {
        ZeroDrawReasonCodeCache zdc = ZeroDrawReasonCodeCache.getInstance();
        if (zdc.containsCode(fridayZeroDrawReasonCode) || "".equalsIgnoreCase(fridayZeroDrawReasonCode)) {
            this.fridayZeroDrawReasonCode = fridayZeroDrawReasonCode;
        }
        else {
            throw new UsatException("Invalid Zero Draw Reason Code: " + fridayZeroDrawReasonCode);
        }
    }
    public String getMondayChangeCode() {
        return this.mondayChangeCode;
    }
    public void setMondayChangeCode(String mondayChangeCode) {
        this.mondayChangeCode = mondayChangeCode;
    }
    public String getMondayDayType() {
        return this.mondayDayType;
    }
    public void setMondayDayType(String mondayDayType) {
        this.mondayDayType = mondayDayType;
    }
    public int getMondayDraw() {
        return this.mondayDraw;
    }
    public void setMondayDraw(int mondayDraw) throws UsatException {
        if (mondayDraw == this.mondayDraw) {
            return;
        }
        if (!this.allowMondayDrawEdits) {
            throw new UsatException("Monday Draw Field is not open for edits.");
        }
        
        if (mondayDraw > this.getMondayMaxDrawThreshold()) {
            throw new UsatException("Max Draw Exceeded");
        }
        if (mondayDraw < 0) {
            throw new UsatException("Draw value must be a positive integer, or zero.");
        }
        this.mondayDraw = mondayDraw;
    }
    
    public int getMondayMaxDrawThreshold() {
        int maxDraw = (this.mondayDraw > this.getMaxDrawThreshold()) ? this.mondayDraw : this.getMaxDrawThreshold();
        return maxDraw;
    }
        
    public int getMondayReturns() {
        return this.mondayReturns;
    }
    public void setMondayReturns(int mondayReturns) throws UsatException  {
        if (mondayReturns == this.mondayReturns) {
            return;
        }
        if (!this.allowMondayReturnsEdits) {
            throw new UsatException("Monday Returns is not open for edits.");
        }
        if (mondayReturns < -1) {
            throw new UsatException("Returns must be a positive integer, zero, or blank");
        }
        this.mondayReturns = mondayReturns;
    }
    public String getMondayZeroDrawReasonCode() {
        return this.mondayZeroDrawReasonCode;
    }
    public void setMondayZeroDrawReasonCode(String mondayZeroDrawReasonCode) throws UsatException {
        ZeroDrawReasonCodeCache zdc = ZeroDrawReasonCodeCache.getInstance();
        if (zdc.containsCode(mondayZeroDrawReasonCode) || "".equalsIgnoreCase(mondayZeroDrawReasonCode)) {
            this.mondayZeroDrawReasonCode = mondayZeroDrawReasonCode;
        }
        else {
            throw new UsatException("Invalid Zero Draw Reason Code: " + mondayZeroDrawReasonCode);
        }
    }
    public String getSaturdayChangeCode() {
        return this.saturdayChangeCode;
    }
    public void setSaturdayChangeCode(String saturdayChangeCode) {
        this.saturdayChangeCode = saturdayChangeCode;
    }
    public String getSaturdayDayType() {
        return this.saturdayDayType;
    }
    public void setSaturdayDayType(String saturdayDayType) {
        this.saturdayDayType = saturdayDayType;
    }
    public int getSaturdayDraw() {
        return this.saturdayDraw;
    }
    public void setSaturdayDraw(int saturdayDraw) throws UsatException {
        if (saturdayDraw == this.saturdayDraw) {
            return;
        }
        if (!this.allowSaturdayDrawEdits) {
            throw new UsatException("Saturday Draw Field is not open for edits.");
        }
        
        if (saturdayDraw > this.getSaturdayMaxDrawThreshold()) {
            throw new UsatException("Max Draw Exceeded");
        }
        if (saturdayDraw < 0) {
            throw new UsatException("Draw value must be a positive integer, or zero.");
        }
        this.saturdayDraw = saturdayDraw;
    }
    
    public int getSaturdayMaxDrawThreshold() {
        int maxDraw = (this.saturdayDraw > this.getMaxDrawThreshold()) ? this.saturdayDraw : this.getMaxDrawThreshold();
        return maxDraw;
    }
    
    public int getSaturdayReturns() {
        return this.saturdayReturns;
    }
    public void setSaturdayReturns(int saturdayReturns) throws UsatException  {
        if (saturdayReturns == this.saturdayReturns) {
            return;
        }
        if (!this.allowSaturdayReturnsEdits) {
            throw new UsatException("Saturday Returns is not open for edits.");
        }
        if (saturdayReturns < -1) {
            throw new UsatException("Returns must be a positive integer, zero, or blank");
        }
        this.saturdayReturns = saturdayReturns;
    }
    public String getSaturdayZeroDrawReasonCode() {
        return this.saturdayZeroDrawReasonCode;
    }
    public void setSaturdayZeroDrawReasonCode(String saturdayZeroDrawReasonCode) throws UsatException {
        ZeroDrawReasonCodeCache zdc = ZeroDrawReasonCodeCache.getInstance();
        if (zdc.containsCode(saturdayZeroDrawReasonCode) || "".equalsIgnoreCase(saturdayZeroDrawReasonCode)) {
            this.saturdayZeroDrawReasonCode = saturdayZeroDrawReasonCode;
        }
        else {
            throw new UsatException("Invalid Zero Draw Reason Code: " + saturdayZeroDrawReasonCode);
        }
    }
    public String getSundayChangeCode() {
        return this.sundayChangeCode;
    }
    public void setSundayChangeCode(String sundayChangeCode) {
        this.sundayChangeCode = sundayChangeCode;
    }
    public String getSundayDayType() {
        return this.sundayDayType;
    }
    public void setSundayDayType(String sundayDayType) {
        this.sundayDayType = sundayDayType;
    }
    public int getSundayDraw() {
        return this.sundayDraw;
    }
    public void setSundayDraw(int sundayDraw)  throws UsatException {
        if (sundayDraw == this.sundayDraw) {
            return;
        }
        if (!this.allowSundayDrawEdits) {
            throw new UsatException("Sunday Draw Field is not open for edits.");
        }
        
        if (sundayDraw > this.getSundayMaxDrawThreshold()) {
            throw new UsatException("Max Draw Exceeded");
        }
        if (sundayDraw < 0) {
            throw new UsatException("Draw value must be a positive integer, or zero.");
        }
        this.sundayDraw = sundayDraw;
    }
    
    public int getSundayMaxDrawThreshold() {
        int maxDraw = (this.sundayDraw > this.getMaxDrawThreshold()) ? this.sundayDraw : this.getMaxDrawThreshold();
        return maxDraw;
    }
            
    public int getSundayReturns() {
        return this.sundayReturns;
    }
    public void setSundayReturns(int sundayReturns) throws UsatException  {
        if (sundayReturns == this.sundayReturns) {
            return;
        }
        if (!this.allowSundayReturnsEdits) {
            throw new UsatException("Sunday Returns is not open for edits.");
        }
        if (sundayReturns < -1) {
            throw new UsatException("Returns must be a positive integer, zero, or blank");
        }
        this.sundayReturns = sundayReturns;
    }
    public String getSundayZeroDrawReasonCode() {
        return this.sundayZeroDrawReasonCode;
    }
    public void setSundayZeroDrawReasonCode(String sundayZeroDrawReasonCode) throws UsatException {
        ZeroDrawReasonCodeCache zdc = ZeroDrawReasonCodeCache.getInstance();
        if (zdc.containsCode(sundayZeroDrawReasonCode)|| "".equalsIgnoreCase(sundayZeroDrawReasonCode)) {
            this.sundayZeroDrawReasonCode = sundayZeroDrawReasonCode;        }
        else {
            throw new UsatException("Invalid Zero Draw Reason Code: " + sundayZeroDrawReasonCode);
        }
    }
    public String getThursdayChangeCode() {
        return this.thursdayChangeCode;
    }
    public void setThursdayChangeCode(String thursdayChangeCode) {
        this.thursdayChangeCode = thursdayChangeCode;
    }
    public String getThursdayDayType() {
        return this.thursdayDayType;
    }
    public void setThursdayDayType(String thursdayDayType) {
        this.thursdayDayType = thursdayDayType;
    }
    public int getThursdayDraw() {
        return this.thursdayDraw;
    }
    public void setThursdayDraw(int thursdayDraw)  throws UsatException {
        if (thursdayDraw == this.thursdayDraw) {
            return;
        }
        if (!this.allowThursdayDrawEdits) {
            throw new UsatException("Thursday Draw Field is not open for edits.");
        }
        
        if (thursdayDraw > this.getThursdayMaxDrawThreshold()) {
            throw new UsatException("Max Draw Exceeded");
        }
        if (thursdayDraw < 0) {
            throw new UsatException("Draw value must be a positive integer, or zero.");
        }
        this.thursdayDraw = thursdayDraw;
    }
    
    public int getThursdayMaxDrawThreshold() {
        int maxDraw = (this.thursdayDraw > this.getMaxDrawThreshold()) ? this.thursdayDraw : this.getMaxDrawThreshold();
        return maxDraw;
    }
    
    public int getThursdayReturns() {
        return this.thursdayReturns;
    }
    public void setThursdayReturns(int thursdayReturns) throws UsatException  {
        if (thursdayReturns == this.thursdayReturns) {
            return;
        }
        if (!this.allowThursdayReturnsEdits) {
            throw new UsatException("Thursday Returns is not open for edits.");
        }
        if (thursdayReturns < -1) {
            throw new UsatException("Returns must be a positive integer, zero, or blank");
        }
        this.thursdayReturns = thursdayReturns;
    }
    public String getThursdayZeroDrawReasonCode() {
        return this.thursdayZeroDrawReasonCode;
    }
    public void setThursdayZeroDrawReasonCode(String thursdayZeroDrawReasonCode) throws UsatException {
        ZeroDrawReasonCodeCache zdc = ZeroDrawReasonCodeCache.getInstance();
        if (zdc.containsCode(thursdayZeroDrawReasonCode)|| "".equalsIgnoreCase(thursdayZeroDrawReasonCode)) {
            this.thursdayZeroDrawReasonCode = thursdayZeroDrawReasonCode;
        }
        else {
            throw new UsatException("Invalid Zero Draw Reason Code: " + thursdayZeroDrawReasonCode);
        }
    }
    public String getTuesdayChangeCode() {
        return this.tuesdayChangeCode;
    }
    public void setTuesdayChangeCode(String tuesdayChangeCode) {
        this.tuesdayChangeCode = tuesdayChangeCode;
    }
    public String getTuesdayDayType() {
        return this.tuesdayDayType;
    }
    public void setTuesdayDayType(String tuesdayDayType) {
        this.tuesdayDayType = tuesdayDayType;
    }
    public int getTuesdayDraw() {
        return this.tuesdayDraw;
    }
    public void setTuesdayDraw(int tuesdayDraw) throws UsatException {
        if (tuesdayDraw == this.tuesdayDraw) {
            return;
        }
        if (!this.allowTuesdayDrawEdits) {
            throw new UsatException("Tuesday Draw Field is not open for edits.");
        }
        
        if (tuesdayDraw > this.getTuesdayMaxDrawThreshold()) {
            throw new UsatException("Max Draw Exceeded");
        }
        if (tuesdayDraw < 0) {
            throw new UsatException("Draw value must be a positive integer, or zero.");
        }
        this.tuesdayDraw = tuesdayDraw;
    }

    public int getTuesdayMaxDrawThreshold() {
        int maxDraw = (this.tuesdayDraw > this.getMaxDrawThreshold()) ? this.tuesdayDraw : this.getMaxDrawThreshold();
        return maxDraw;
    }
    
    public int getTuesdayReturns() {
        return this.tuesdayReturns;
    }
    public void setTuesdayReturns(int tuesdayReturns) throws UsatException  {
        if (tuesdayReturns == this.tuesdayReturns) {
            return;
        }
        if (!this.allowTuesdayReturnsEdits) {
            throw new UsatException("Tuesday Returns is not open for edits.");
        }
        if (tuesdayReturns < -1) {
            throw new UsatException("Returns must be a positive integer, zero, or blank");
        }
        this.tuesdayReturns = tuesdayReturns;
    }
    public String getTuesdayZeroDrawReasonCode() {
        return this.tuesdayZeroDrawReasonCode;
    }
    public void setTuesdayZeroDrawReasonCode(String tuesdayZeroDrawReasonCode) throws UsatException {
        ZeroDrawReasonCodeCache zdc = ZeroDrawReasonCodeCache.getInstance();
        if (zdc.containsCode(tuesdayZeroDrawReasonCode)|| "".equalsIgnoreCase(tuesdayZeroDrawReasonCode)) {
            this.tuesdayZeroDrawReasonCode = tuesdayZeroDrawReasonCode;
        }
        else {
            throw new UsatException("Invalid Zero Draw Reason Code: " + tuesdayZeroDrawReasonCode);
        }
    }
    public String getWednesdayChangeCode() {
        return this.wednesdayChangeCode;
    }
    public void setWednesdayChangeCode(String wednesdayChangeCode) {
        this.wednesdayChangeCode = wednesdayChangeCode;
    }
    public String getWednesdayDayType() {
        return this.wednesdayDayType;
    }
    public void setWednesdayDayType(String wednesdayDayType) {
        this.wednesdayDayType = wednesdayDayType;
    }
    public int getWednesdayDraw() {
        return this.wednesdayDraw;
    }
    public void setWednesdayDraw(int wednesdayDraw) throws UsatException {
        if (wednesdayDraw == this.wednesdayDraw) {
            return;
        }
        if (!this.allowWednesdayDrawEdits) {
            throw new UsatException("Wednesday Draw Field is not open for edits.");
        }

        if (wednesdayDraw > this.getWednesdayMaxDrawThreshold()) {
            throw new UsatException("Max Draw Exceeded");
        }
        if (wednesdayDraw < 0) {
            throw new UsatException("Draw value must be a positive integer, or zero.");
        }
        this.wednesdayDraw = wednesdayDraw;
    }
    
    public int getWednesdayMaxDrawThreshold() {
        int maxDraw = (this.wednesdayDraw > this.getMaxDrawThreshold()) ? this.wednesdayDraw : this.getMaxDrawThreshold();
        return maxDraw;
    }
    
    public int getWednesdayReturns() {
        return this.wednesdayReturns;
    }
    public void setWednesdayReturns(int wednesdayReturns) throws UsatException  {
        if (wednesdayReturns == this.wednesdayReturns) {
            return;
        }
        if (!this.allowWednesdayReturnsEdits) {
            throw new UsatException("Wednesday Returns is not open for edits.");
        }
        if (wednesdayReturns < -1) {
            throw new UsatException("Returns must be a positive integer, zero, or blank");
        }
        this.wednesdayReturns = wednesdayReturns;
    }
    public String getWednesdayZeroDrawReasonCode() {
        return this.wednesdayZeroDrawReasonCode;
    }
    public void setWednesdayZeroDrawReasonCode(String wednesdayZeroDrawReasonCode)  throws UsatException{
        ZeroDrawReasonCodeCache zdc = ZeroDrawReasonCodeCache.getInstance();
        if (zdc.containsCode(wednesdayZeroDrawReasonCode)|| "".equalsIgnoreCase(wednesdayZeroDrawReasonCode)) {
            this.wednesdayZeroDrawReasonCode = wednesdayZeroDrawReasonCode;
        }
        else {
            throw new UsatException("Invalid Zero Draw Reason Code: " + wednesdayZeroDrawReasonCode);
        }
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#isChanged()
     */
    public boolean isChanged() {
        // call base class isChanged method
        this.poChanged = false;
        
        if (this.hasEdittableFields()) {
	        super.isChanged();
	        
	        // Check Monday Values and set Change Code field.
	        this.mondayChangeCode = this.dayChanged(this.mondayReturns, this.mondayDraw, this.mondayZeroDrawReasonCode, this.originalPO.getMondayReturns(),this.originalPO.getMondayDraw(), this.originalPO.getMondayZeroDrawReasonCode());
	        
	        // Check Tuesday Values
	        this.tuesdayChangeCode = this.dayChanged(this.tuesdayReturns, this.tuesdayDraw, this.tuesdayZeroDrawReasonCode, this.originalPO.getTuesdayReturns(), this.originalPO.getTuesdayDraw(), this.originalPO.getTuesdayZeroDrawReasonCode());
	
	        // check Wednesday Values
	        this.wednesdayChangeCode = this.dayChanged(this.wednesdayReturns, this.wednesdayDraw, this.wednesdayZeroDrawReasonCode, this.originalPO.getWednesdayReturns(), this.originalPO.getWednesdayDraw(), this.originalPO.getWednesdayZeroDrawReasonCode() );
	
	        // check Thursday Values
	        this.thursdayChangeCode = this.dayChanged(this.thursdayReturns, this.thursdayDraw, this.thursdayZeroDrawReasonCode, this.originalPO.getThursdayReturns(), this.originalPO.getThursdayDraw(), this.originalPO.getThursdayZeroDrawReasonCode() );
	        
	        // check Friday Values
	        this.fridayChangeCode = this.dayChanged(this.fridayReturns, this.fridayDraw, this.fridayZeroDrawReasonCode, this.originalPO.getFridayReturns(), this.originalPO.getFridayDraw(), this.originalPO.getFridayZeroDrawReasonCode() );
	
	        // check Saturday Values
	        this.saturdayChangeCode = this.dayChanged(this.saturdayReturns, this.saturdayDraw, this.saturdayZeroDrawReasonCode, this.originalPO.getSaturdayReturns(), this.originalPO.getSaturdayDraw(), this.originalPO.getSaturdayZeroDrawReasonCode() );
	
	        // check Sunday Values
	        this.sundayChangeCode = this.dayChanged(this.sundayReturns, this.sundayDraw, this.sundayZeroDrawReasonCode, this.originalPO.getSundayReturns(), this.originalPO.getSundayDraw(), this.originalPO.getSundayZeroDrawReasonCode() );
        }
        
        return this.poChanged;
    }
    
    protected void resetFieldsToOriginal() {
        if (this.originalPO != null) {
	        this.allowMondayDrawEdits = this.originalPO.getAllowMondayDrawEdits();
	        this.allowMondayReturnsEdits = this.originalPO.getAllowMondayReturnsEdits();
	        this.mondayChangeCode = this.originalPO.getMondayChangeCode();
	        this.mondayDayType = this.originalPO.getMondayDayType();
	        this.mondayDraw = this.originalPO.getMondayDraw();
	        this.mondayReturns = this.originalPO.getMondayReturns();
	        this.mondayZeroDrawReasonCode = this.originalPO.getMondayZeroDrawReasonCode();
	
	        this.allowTuesdayDrawEdits = this.originalPO.getAllowTuesdayDrawEdits();
	        this.allowTuesdayReturnsEdits = this.originalPO.getAllowTuesdayReturnsEdits();
	        this.tuesdayChangeCode = this.originalPO.getTuesdayChangeCode();
	        this.tuesdayDayType = this.originalPO.getTuesdayDayType();
	        this.tuesdayDraw = this.originalPO.getTuesdayDraw();
	        this.tuesdayReturns = this.originalPO.getTuesdayReturns();
	        this.tuesdayZeroDrawReasonCode = this.originalPO.getTuesdayZeroDrawReasonCode();
	        
	        this.allowWednesdayDrawEdits = this.originalPO.getAllowWednesdayDrawEdits();
	        this.allowWednesdayReturnsEdits = this.originalPO.getAllowWednesdayReturnsEdits();
	        this.wednesdayChangeCode = this.originalPO.getWednesdayChangeCode();
	        this.wednesdayDayType = this.originalPO.getWednesdayDayType();
	        this.wednesdayDraw = this.originalPO.getWednesdayDraw();
	        this.wednesdayReturns = this.originalPO.getWednesdayReturns();
	        this.wednesdayZeroDrawReasonCode = this.originalPO.getWednesdayZeroDrawReasonCode();
	        
	        this.allowThursdayDrawEdits = this.originalPO.getAllowThursdayDrawEdits();
	        this.allowThursdayReturnsEdits = this.originalPO.getAllowThursdayReturnsEdits();
	        this.thursdayChangeCode = this.originalPO.getThursdayChangeCode();
	        this.thursdayDayType = this.originalPO.getThursdayDayType();
	        this.thursdayDraw = this.originalPO.getThursdayDraw();
	        this.thursdayReturns = this.originalPO.getThursdayReturns();
	        this.thursdayZeroDrawReasonCode = this.originalPO.getThursdayZeroDrawReasonCode();
	        
	        this.allowFridayDrawEdits = this.originalPO.getAllowFridayDrawEdits();
	        this.allowFridayReturnsEdits = this.originalPO.getAllowFridayReturnsEdits();
	        this.fridayChangeCode = this.originalPO.getFridayChangeCode();
	        this.fridayDayType = this.originalPO.getFridayDayType();
	        this.fridayDraw = this.originalPO.getFridayDraw();
	        this.fridayReturns = this.originalPO.getFridayReturns();
	        this.fridayZeroDrawReasonCode = this.originalPO.getFridayZeroDrawReasonCode();
	        
	        this.allowSaturdayDrawEdits = this.originalPO.getAllowSaturdayDrawEdits();
	        this.allowSaturdayReturnsEdits = this.originalPO.getAllowSaturdayReturnsEdits();
	        this.saturdayChangeCode = this.originalPO.getSaturdayChangeCode();
	        this.saturdayDayType = this.originalPO.getSaturdayDayType();
	        this.saturdayDraw = this.originalPO.getSaturdayDraw();
	        this.saturdayReturns = this.originalPO.getSaturdayReturns();
	        this.saturdayZeroDrawReasonCode = this.originalPO.getSaturdayZeroDrawReasonCode();
	        
	        this.allowSundayDrawEdits = this.originalPO.getAllowSundayDrawEdits();
	        this.allowSundayReturnsEdits = this.originalPO.getAllowSundayReturnsEdits();
	        this.sundayChangeCode = this.originalPO.getSundayChangeCode();
	        this.sundayDayType = this.originalPO.getSundayDayType();
	        this.sundayDraw = this.originalPO.getSundayDraw();
	        this.sundayReturns = this.originalPO.getSundayReturns();
	        this.sundayZeroDrawReasonCode = this.originalPO.getSundayZeroDrawReasonCode();
        }
    }
    
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getTotalDraw()
     */
    public int getTotalDraw() {
        int totalDraw = (this.getMondayDraw() > -1) ? this.getMondayDraw() : 0;
        totalDraw += (this.getTuesdayDraw() > -1) ? this.getTuesdayDraw() : 0;
        totalDraw += (this.getWednesdayDraw() > -1) ? this.getWednesdayDraw() : 0;
        totalDraw += (this.getThursdayDraw() > -1) ? this.getThursdayDraw() : 0;
        totalDraw += (this.getFridayDraw() > -1) ? this.getFridayDraw() : 0;
        totalDraw += (this.getSaturdayDraw() > -1) ? this.getSaturdayDraw() : 0;
        totalDraw += (this.getSundayDraw() > -1) ? this.getSundayDraw() : 0;
        
        return totalDraw;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getTotalReturns()
     */
    public int getTotalReturns() {
        int totalReturns = (this.getMondayReturns() > -1) ? this.getMondayReturns() : 0;
        totalReturns += (this.getTuesdayReturns() > -1) ? this.getTuesdayReturns() : 0;
        totalReturns += (this.getWednesdayReturns() > -1) ? this.getWednesdayReturns() : 0;
        totalReturns += (this.getThursdayReturns() > -1) ? this.getThursdayReturns() : 0;
        totalReturns += (this.getFridayReturns() > -1) ? this.getFridayReturns() : 0;
        totalReturns += (this.getSaturdayReturns() > -1) ? this.getSaturdayReturns() : 0;
        totalReturns += (this.getSundayReturns() > -1) ? this.getSundayReturns() : 0;

        return totalReturns;
    }
        
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        String me = null;

        StringBuffer meBuf = new StringBuffer();
        
        meBuf.append("POID: " + this.getProductOrderID() + " Mon Draw(" + this.getAllowMondayDrawEdits() + "): " + this.getMondayDraw() + " Mon ZDR: " + this.getMondayZeroDrawReasonCode() + " Mon Retrns(" + this.getAllowMondayReturnsEdits() + "): " + this.getMondayReturns()); 
        meBuf.append(" Tues Draw(" + this.getAllowTuesdayDrawEdits() + "): " + this.getTuesdayDraw() + " Tues ZDR: " + this.getTuesdayZeroDrawReasonCode() + " Tues Retrns(" + this.getAllowTuesdayReturnsEdits() + "): " + this.getTuesdayReturns());
        meBuf.append(" Wed Draw(" + this.getAllowWednesdayDrawEdits() + "): " + this.getTuesdayDraw() + " Wed ZDR: " + this.getWednesdayZeroDrawReasonCode() + " Wed Retrns(" + this.getAllowWednesdayReturnsEdits() + "): " + this.getWednesdayReturns()); 
        meBuf.append(" Thurs Draw(" + this.getAllowThursdayDrawEdits() + "): " + this.getThursdayDraw() + " Thurs ZDR: " + this.getThursdayZeroDrawReasonCode() + " Thurs Retrns(" + this.getAllowThursdayReturnsEdits() + "): " + this.getThursdayReturns());
        meBuf.append(" Fri Draw(" + this.getAllowFridayDrawEdits() + "): " + this.getFridayDraw() + " Fri ZDR: " + this.getFridayZeroDrawReasonCode() + " Fri Retrns(" + this.getAllowFridayReturnsEdits() + "): " + this.getFridayReturns());
        meBuf.append(" Sat Draw(" + this.getAllowSaturdayDrawEdits() + "): " + this.getSaturdayDraw() + " Sat ZDR: " + this.getSaturdayZeroDrawReasonCode() + " Sat Retrns(" + this.getAllowSaturdayReturnsEdits() + "): " + this.getSaturdayReturns());
        meBuf.append(" Sun Draw(" + this.getAllowSundayDrawEdits() + "): " + this.getSundayDraw() + " Sun ZDR: " + this.getSundayZeroDrawReasonCode() + " Sun Retrns(" + this.getAllowSundayReturnsEdits() + "): " + this.getSundayReturns());
        
        me = meBuf.toString();
        
        return me;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#setSaved(com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf)
     */
    public void setSaved(ProductOrderIntf newSource) {
        // create a new copy of this object for use as the original
        WeeklyProductOrderBO newOrig = new WeeklyProductOrderBO(this);
        super.setSaved(newOrig);
        this.originalPO = newOrig;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.bo.drawmngmt.ProductOrderBO#isValidPO()
     */
    public boolean isValidPO() {
        boolean validPO = true;
        
        // call base class checker first
        validPO = super.isValidPO();
        
        // validate fields
        if (this.mondayReturns > this.mondayDraw) {
            validPO = false;
        }
        if (this.tuesdayReturns > this.tuesdayDraw) {
            validPO = false;
        }
        if (this.wednesdayReturns > this.wednesdayDraw) {
            validPO = false;
        }
        if (this.thursdayReturns > this.thursdayDraw) {
            validPO = false;
        }
        if (this.fridayReturns > this.fridayDraw) {
            validPO = false;
        }
        if (this.saturdayReturns > this.saturdayDraw) {
            validPO = false;
        }
        if (this.sundayReturns > this.sundayDraw) {
            validPO = false;
        }

        return validPO;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#hasEdittableFields()
     * 
     * Determines if any field in the PO allows edits
     */
    public boolean hasEdittableFields() {
        boolean allowEdits = false;
        if (
            this.getAllowMondayDrawEdits() ||
            this.getAllowMondayReturnsEdits() ||
            this.getAllowWednesdayDrawEdits() ||
            this.getAllowWednesdayReturnsEdits() ||
            this.getAllowFridayDrawEdits() ||
            this.getAllowFridayReturnsEdits() ||
            this.getAllowSaturdayDrawEdits() ||
            this.getAllowSaturdayReturnsEdits() ||
            this.getAllowSundayDrawEdits() ||
            this.getAllowSundayReturnsEdits() ||
            this.getAllowThursdayDrawEdits() ||
            this.getAllowThursdayReturnsEdits() ||
            this.getAllowTuesdayDrawEdits() ||
            this.getAllowTuesdayReturnsEdits()) {
            allowEdits = true;
        }
        
        return allowEdits;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#setPOAsDeleted()
     */
    public void setPOAsDeleted() {
        this.originalPO = null;
        
        // set values to unset
        this.mondayDraw = -1;
        this.mondayReturns = -1;
        this.tuesdayDraw = -1;
        this.tuesdayReturns = -1;
        this.wednesdayDraw = -1;
        this.wednesdayReturns = -1;
        this.thursdayDraw = -1;
        this.thursdayReturns = -1;
        this.fridayDraw = -1;
        this.fridayReturns = -1;
        this.saturdayDraw = -1;
        this.saturdayReturns = -1;
        this.sundayDraw = -1;
        this.sundayReturns = -1;
        
        // turn off all edit fields
        this.setAllowFridayDrawEdits(false);
        this.setAllowFridayReturnsEdits(false);
        this.setAllowMondayDrawEdits(false);
        this.setAllowMondayReturnsEdits(false);
        this.setAllowSaturdayDrawEdits(false);
        this.setAllowSaturdayReturnsEdits(false);
        this.setAllowSundayDrawEdits(false);
        this.setAllowSundayReturnsEdits(false);
        this.setAllowThursdayDrawEdits(false);
        this.setAllowThursdayReturnsEdits(false);
        this.setAllowTuesdayDrawEdits(false);
        this.setAllowTuesdayReturnsEdits(false);
        this.setAllowWednesdayDrawEdits(false);
        this.setAllowWednesdayReturnsEdits(false);
        
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyProductOrderIntf#getOriginalProductOrder()
     */
    public WeeklyReadOnlyProductOrderIntf getOriginalProductOrder() {
        return this.originalPO;
    }
    
    /**
     * Clear references to objects
     */
    public void clear() {
        super.clear();
        this.originalPO = null;
    }
}
