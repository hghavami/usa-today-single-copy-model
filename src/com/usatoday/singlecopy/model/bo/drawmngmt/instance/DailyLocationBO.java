/*
 * Created on Jun 12, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.bo.drawmngmt.instance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.bo.drawmngmt.LocationBO;
import com.usatoday.singlecopy.model.bo.drawmngmt.ProductOrderBO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyDrawLocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf;
import com.usatoday.singlecopy.model.transferObjects.LocationTO;

/**
 * @author aeast
 * @date Jun 12, 2007
 * @class DailyLocationBO
 * 
 * Represents an instance of a location applicable to RDL
 * 
 */
public class DailyLocationBO extends LocationBO implements  DailyDrawLocationIntf{

    /**
     * 
     */
    public DailyLocationBO() {
        super();
    }

    /**
     * @param source
     */
    public DailyLocationBO(LocationTO source) {
        super(source);
        this.productOrders = new ArrayList<ProductOrderIntf>();
        
        // convert Product Orders to Daily Product order BOs.
        Collection<ProductOrderIntf> poTO = source.getProductOrders();
        
        Iterator<ProductOrderIntf> potoItr = poTO.iterator();
        while (potoItr.hasNext()){
            DailyProductOrderIntf dpo = (DailyProductOrderIntf)potoItr.next();
            DailyProductOrderBO dpobo = new DailyProductOrderBO(dpo);
            dpobo.setOwningLocation(this);
            this.productOrders.add(dpobo);
        }       
    }
    
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyDrawLocationIntf#getRDLDate()
     */
    public DateTime getRDLDate() {
        return this.getDate();
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawManagementLocationIntf#getProductOrders()
     */
    public Collection<ProductOrderIntf> getProductOrders() {
        ArrayList<ProductOrderIntf> aList = null;
        
        ArrayList<ProductOrderBO> poBOList = new ArrayList<ProductOrderBO>();
        for (ProductOrderIntf poIntf : this.productOrders) {
        	ProductOrderBO poBO = (ProductOrderBO)poIntf;
        	poBOList.add(poBO);
        }
        
        Collections.sort(poBOList);
        
        aList = new ArrayList<ProductOrderIntf>(poBOList);
        return aList;
    }

}
