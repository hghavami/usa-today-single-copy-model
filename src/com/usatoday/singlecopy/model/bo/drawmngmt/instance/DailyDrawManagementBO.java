/*
 * Created on Jun 12, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.bo.drawmngmt.instance;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.bo.drawmngmt.ProductOrderBO;
import com.usatoday.singlecopy.model.integration.RouteDAO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyDrawManagementIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf;
import com.usatoday.singlecopy.model.transferObjects.DrawUpdateErrorTO;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;

/**
 * @author aeast
 * @date Jun 12, 2007
 * @class DailyDrawManagementBO
 * 
 * 
 * 
 */
public class DailyDrawManagementBO extends DrawManagementBO implements DailyDrawManagementIntf {

    private DateTime RDLDate = null;
    
    /**
     * 
     */
    public DailyDrawManagementBO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawManagementInstanceIntf#saveChanges()
     */
    public void saveChanges() throws UsatException {
        
        int numberPOsDeletedOnBackEnd = 0;
        
        Collection<ProductOrderIntf> changedPOs = this.getChangedProductOrders();
        if (changedPOs.size() > 0 ) {
            
            this.clearAllPOErrors();
                        
            // atttempt to save changes
            RouteDAO dao = new RouteDAO();

            // this hash keeps track of PO's with errors so others can be set as saved.
            HashMap<String, DrawUpdateErrorIntf> errorKeys = new HashMap<String, DrawUpdateErrorIntf>();
            

            Collection<DrawUpdateErrorTO> errorRecords = null;
            
            try {
                errorRecords = dao.updateDailyDraw(this.getUser().getEmailAddress(), this.getRoute().getMarketID(), this.getRoute().getDistrictID(), this.getRoute().getRouteID(), this.getRDLDate(), changedPOs);
            }
            catch (UsatException ue) {
                this.setUpdateErrorsFlag(true);
                throw ue;
            }

            if (errorRecords.size() > 0) {
                this.setUpdateErrorsFlag(true);
                
                if (USATApplicationConstants.debug) {
                    System.out.println("Update Returned Errors: " + errorRecords.size());
                }
                
                // problems, attach errors to product orders
                Iterator<DrawUpdateErrorTO> eItr = errorRecords.iterator();
                while(eItr.hasNext()) {
                    DrawUpdateErrorIntf err = (DrawUpdateErrorIntf)eItr.next();

                    if (USATApplicationConstants.debug) {
                        System.out.println("\tUpdate Error: Loc ID: " + err.getLocationID() + "    POID: " + err.getProductOrderID() + "  APPLIES TO: " + err.getAppliesTo() + " MESSAGE: " + err.getErrorMessage());
                    }

                    String key = err.getMarketID() + err.getDistrictID() + err.getRouteID() + err.getLocationID();
                    LocationIntf loc = this.getLocation(key);
                    if (loc == null) {
                        if (USATApplicationConstants.debug) {
                            System.out.println("No Matching Location for POID: " + err.getProductOrderID());
                        }
                        continue;
                    }
                    
                    Iterator<ProductOrderIntf> poItr = loc.getProductOrders().iterator();
                    while (poItr.hasNext()) {
                        ProductOrderBO po = (ProductOrderBO)poItr.next();
                        if (po.getProductOrderID().equalsIgnoreCase(err.getProductOrderID())) {
                            if (err.getAppliesTo().equalsIgnoreCase(DrawUpdateErrorIntf.PO_DELETED)) {
                                // disable PO set error detail string.
                                numberPOsDeletedOnBackEnd++;
                                po.setPOAsDeleted();
                            }
                            else {
                                po.addUpdateError(err);
                                errorKeys.put(key+err.getProductOrderID(), err);
                            }
                            break;
                        }
                    }
                } // end while more errors
                
                // mark successful updates as saved
                Iterator<ProductOrderIntf> itr = changedPOs.iterator();
                while(itr.hasNext()) {
                    DailyProductOrderBO dpo = (DailyProductOrderBO)itr.next();
                    String key = dpo.getOwningLocation().getLocationHashKey();
                    key += dpo.getProductOrderID();
                    
                    if (!errorKeys.containsKey(key)){
                        dpo.setSaved(dpo);
                    }
                }                

                if (numberPOsDeletedOnBackEnd > 0) {
                    this.setGlobalUpdateErrorMessage(numberPOsDeletedOnBackEnd + " Product Orders were deleted since you first requested the draw data. Updates to the deleted product orders were not saved.");
                }
                
            }
            else {
                this.setUpdateErrorsFlag(false);
                // Iterate over changed POs and update status/mark as saved
                Iterator<ProductOrderIntf> itr = changedPOs.iterator();
                while(itr.hasNext()) {
                    DailyProductOrderIntf dpo = (DailyProductOrderIntf)itr.next();
                    dpo.setSaved(dpo);
                }
            }
        }
        
    }

    /**
     * @return Returns the rDLDate.
     */
    public DateTime getRDLDate() {
        return this.RDLDate;
    }
    /**
     * @param date The rDLDate to set.
     */
    public void setRDLDate(DateTime date) {
        this.RDLDate = date;
    }
}
