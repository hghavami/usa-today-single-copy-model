/*
 * Created on Apr 23, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.bo.drawmngmt.instance;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.bo.drawmngmt.ProductOrderBO;
import com.usatoday.singlecopy.model.integration.RouteDAO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyDrawManangementIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyProductOrderIntf;
import com.usatoday.singlecopy.model.transferObjects.WeeklyDrawUpdateErrorTO;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;

/**
 * @author aeast
 * @date Apr 23, 2007
 * @class WeeklyDrawManagementBO
 * 
 * This class represents an instance of weekly draw
 * 
 */
public class WeeklyDrawManagementBO extends DrawManagementBO implements WeeklyDrawManangementIntf {

    private DateTime weekEndingDate = null;
    private String productCode = null;
    /**
     * 
     */
    public WeeklyDrawManagementBO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyDrawManangementIntf#getWeekEndingDate()
     */
    public DateTime getWeekEndingDate() {
        return this.weekEndingDate;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawManagementInstanceIntf#saveChanges()
     */
    public void saveChanges() throws UsatException {
        
        int numberPOsDeletedOnBackEnd = 0;
        
        Collection<ProductOrderIntf> changedPOs = this.getChangedProductOrders();
        if (changedPOs.size() > 0 ) {
            
            // remove any previous save errors
            this.clearAllPOErrors();
            
            // atttempt to save changes
            RouteDAO dao = new RouteDAO();
            Collection<WeeklyDrawUpdateErrorTO> errorRecords = null;
            try {
                errorRecords = dao.updateWeeklyDraw(this.getUser().getEmailAddress(), this.getRoute().getMarketID(), this.getRoute().getDistrictID(), this.getRoute().getRouteID(), this.getProductCode(), this.weekEndingDate, this.lateReturnsFlag, changedPOs);
            }
            catch (UsatException ue) {
                this.setUpdateErrorsFlag(true);
                throw ue;
            }
            
            if (errorRecords.size() > 0) {
                this.setUpdateErrorsFlag(true);
                
                if (USATApplicationConstants.debug) {
                    System.out.println("Update Returned Errors: " + errorRecords.size());
                }

                // this hash keeps track of PO's with errors so others can be set as saved.
                HashMap<String, DrawUpdateErrorIntf> errorKeys = new HashMap<String, DrawUpdateErrorIntf>();
                
                // problems, attach errors to product orders
                Iterator<WeeklyDrawUpdateErrorTO> eItr = errorRecords.iterator();
                while(eItr.hasNext()) {
                    DrawUpdateErrorIntf err = (DrawUpdateErrorIntf)eItr.next();
                    if (USATApplicationConstants.debug) {
                        System.out.println("\tUpdate Error: Loc ID: " + err.getLocationID() + "    POID: " + err.getProductOrderID() + "  APPLIES TO: " + err.getAppliesTo() + "  DATE: " + err.getErrorDate() + " MESSAGE: " + err.getErrorMessage());
                    }

                    String key = err.getMarketID() + err.getDistrictID() + err.getRouteID() + err.getLocationID();
                    LocationIntf loc = this.getLocation(key);
                    if (loc == null) {
                        if (USATApplicationConstants.debug) {
                            System.out.println("No Matching Location for POID: " + err.getProductOrderID());
                        }
                        continue;
                    }
                    
                    Iterator<ProductOrderIntf> poItr = loc.getProductOrders().iterator();
                    while (poItr.hasNext()) {
                        ProductOrderBO po = (ProductOrderBO)poItr.next();
                        if (po.getProductOrderID().equalsIgnoreCase(err.getProductOrderID())) {
                            if (err.getAppliesTo().equalsIgnoreCase(DrawUpdateErrorIntf.PO_DELETED)) {
                                // disable PO set error detail string.
                                numberPOsDeletedOnBackEnd++;
                                po.setPOAsDeleted();
                            }
                            else {
                                po.addUpdateError(err);
                                errorKeys.put(key+err.getProductOrderID(), err);
                            }
                            break;
                        }
                    }
                } // end while more errors
                
                // mark successful updates as saved
                Iterator<ProductOrderIntf> itr = changedPOs.iterator();
                while(itr.hasNext()) {
                    WeeklyProductOrderBO wpo = (WeeklyProductOrderBO)itr.next();
                    String key = wpo.getOwningLocation().getLocationHashKey();
                    key += wpo.getProductOrderID();
                    
                    if (!errorKeys.containsKey(key)){
                        wpo.setSaved(wpo);
                    }
                }
                
                if (numberPOsDeletedOnBackEnd > 0) {
                    this.setGlobalUpdateErrorMessage(numberPOsDeletedOnBackEnd + " Product Orders were deleted since you first requested the draw data. Updates to the deleted product orders were not saved.");
                }
                
            }
            else {
                this.setUpdateErrorsFlag(false);
                // Iterate over changed POs and update status/mark as saved
                Iterator<ProductOrderIntf> itr = changedPOs.iterator();
                while(itr.hasNext()) {
                    WeeklyProductOrderIntf wpo = (WeeklyProductOrderIntf)itr.next();
                    wpo.setSaved(wpo);
                }
            }
        }
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyDrawManangementIntf#getProductCode()
     */
    public String getProductCode() {
        return this.productCode;
    }
    
    /**
     * @param productCode The productCode to set.
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    /**
     * @param weekEndingDate The weekEndingDate to set.
     */
    public void setWeekEndingDate(DateTime weekEndingDate) {
        this.weekEndingDate = weekEndingDate;
    }
}
