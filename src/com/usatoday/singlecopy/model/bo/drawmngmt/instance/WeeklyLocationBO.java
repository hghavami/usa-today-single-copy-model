/*
 * Created on Apr 23, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.bo.drawmngmt.instance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.bo.drawmngmt.LocationBO;
import com.usatoday.singlecopy.model.bo.drawmngmt.ProductOrderBO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyDrawLocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyProductOrderIntf;
import com.usatoday.singlecopy.model.transferObjects.LocationTO;

/**
 * @author aeast
 * @date Apr 23, 2007
 * @class WeeklyLocationBO
 * 
 * 
 * 
 */
public class WeeklyLocationBO extends LocationBO implements
        WeeklyDrawLocationIntf {

    /**
     * 
     */
    public WeeklyLocationBO() {
        super();
    }

    /**
     * @param source
     */
    public WeeklyLocationBO(LocationTO source) {
        super(source);
        
        this.productOrders = new ArrayList<ProductOrderIntf>();
        
        // convert Product Orders to Weekly Product order BOs.
        Collection<ProductOrderIntf> poTO = source.getProductOrders();
        
        Iterator<ProductOrderIntf> potoItr = poTO.iterator();
        while (potoItr.hasNext()){
            WeeklyProductOrderIntf wpo = (WeeklyProductOrderIntf)potoItr.next();
            WeeklyProductOrderBO wpobo = new WeeklyProductOrderBO(wpo);
            wpobo.setOwningLocation(this);
            this.productOrders.add(wpobo);
        }
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyDrawLocationIntf#getWeekEndingDate()
     */
    public DateTime getWeekEndingDate() {
        return this.getDate();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawManagementLocationIntf#getProductOrders()
     */
    public Collection<ProductOrderIntf> getProductOrders() {

        ArrayList<ProductOrderIntf> aList = null;
        
        ArrayList<ProductOrderBO> poBOList = new ArrayList<ProductOrderBO>();
        for (ProductOrderIntf poIntf : this.productOrders) {
        	ProductOrderBO poBO = (ProductOrderBO)poIntf;
        	poBOList.add(poBO);
        }
        
        Collections.sort(poBOList);
        
        aList = new ArrayList<ProductOrderIntf>(poBOList);
        return aList;
    }

}
