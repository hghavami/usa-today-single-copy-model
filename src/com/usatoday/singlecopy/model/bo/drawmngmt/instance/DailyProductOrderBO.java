/*
 * Created on Jun 12, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.bo.drawmngmt.instance;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.bo.drawmngmt.ProductOrderBO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyReadOnlyProductOrderIntf;
import com.usatoday.singlecopy.model.util.ZeroDrawReasonCodeCache;

/**
 * @author aeast
 * @date Jun 12, 2007
 * @class DailyProductOrderBO
 * 
 * Represents and instance of a daily RDL Producdt Order
 * 
 */
public class DailyProductOrderBO extends ProductOrderBO implements
        DailyProductOrderIntf {
    
    private boolean lateReturnsFlag = false;
    private boolean allowDrawEdits = false;
    private boolean allowReturnsEdits = false;
    private String changeCode = null;
    private String dayType = null;
    private int draw = -1;
    private int returns = -1;
    private int drawDayReturns = 0;
    private int maxReturns = 0;
    private DateTime RDLDate = null;
    private DateTime returnDate = null;
    private String zeroDrawReasonCode = null;
    
    private DailyReadOnlyProductOrderIntf originalDailyPO = null;
    
    /**
     * 
     */
    public DailyProductOrderBO() {
        super();
    }

    /**
     * @param source
     */
    public DailyProductOrderBO(DailyProductOrderIntf source) {
        super(source);
        
        this.originalDailyPO = source;
        
        this.resetFieldsToOriginal();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.bo.drawmngmt.ProductOrderBO#resetFieldsToOriginal()
     */
    protected void resetFieldsToOriginal() {
        
        if (this.originalDailyPO != null) {
	        this.returns = this.originalDailyPO.getReturns();
	        this.draw = this.originalDailyPO.getDraw();
	        this.zeroDrawReasonCode = this.originalDailyPO.getZeroDrawReasonCode();
	        this.allowDrawEdits = this.originalDailyPO.getAllowDrawEdits();
	        this.allowReturnsEdits = this.originalDailyPO.getAllowReturnsEdits();
	        this.changeCode = this.originalDailyPO.getChangeCode();
	        this.dayType = this.originalDailyPO.getDayType();
	        this.lateReturnsFlag = this.originalDailyPO.getLateReturnsFlag();
	        this.maxReturns = this.originalDailyPO.getMaxReturns();
	        this.RDLDate = this.originalDailyPO.getRDLDate();
	        this.returnDate = this.originalDailyPO.getReturnDate();
            this.drawDayReturns = this.originalDailyPO.getDrawDayReturns();
        }
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getTotalDraw()
     */
    public int getTotalDraw() {
        return (this.getDraw() > -1) ? this.getDraw() : 0;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getTotalReturns()
     */
    public int getTotalReturns() {
        return (this.getReturns() > -1) ? this.getReturns() : 0;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getRDLDate()
     */
    public DateTime getRDLDate() {
        return this.RDLDate;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getReturnDate()
     */
    public DateTime getReturnDate() {
        return this.returnDate;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getDayType()
     */
    public String getDayType() {
        return this.dayType;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getLateReturnsWeek()
     */
    public boolean getLateReturnsFlag() {
        return this.lateReturnsFlag;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getChangeCode()
     */
    public String getChangeCode() {
        return this.changeCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getDraw()
     */
    public int getDraw() {
        return this.draw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#setDraw(int)
     */
    public void setDraw(int draw) throws UsatException{
        //  draw can be set to the higher of the recommended maximum or the current draw value
        int maxDraw = (this.draw > this.getMaxDrawThreshold()) ? this.draw : this.getMaxDrawThreshold();

        if (draw > maxDraw) {
            throw new UsatException("Max Draw Exceeded");
        }
        if (draw < -1) {
            throw new UsatException("Draw value must be a positive integer or zero.");
        }
        this.draw = draw;

    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getAllowDrawEdits()
     */
    public boolean getAllowDrawEdits() {
        return this.allowDrawEdits;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getReturns()
     */
    public int getReturns() {
        return this.returns;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getMaxReturns()
     */
    public int getMaxReturns() {
        return this.maxReturns;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#setReturns(int)
     */
    public void setReturns(int returns)  throws UsatException {
        if (returns < -1) {
            throw new UsatException("Returns must be a positive integer, zero, or blank.");
        }
        else if (returns > this.getMaxReturns()) {
            throw new UsatException("Returns cannot exceed draw value of previous distribution date: " + this.getMaxReturns());
        }
        this.returns = returns;
        
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getAllowReturnsEdits()
     */
    public boolean getAllowReturnsEdits() {
        return this.allowReturnsEdits;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getZeroDrawReasonCode()
     */
    public String getZeroDrawReasonCode() {
        return this.zeroDrawReasonCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#setZeroDrawReasonCode(java.lang.String)
     */
    public void setZeroDrawReasonCode(String code) throws UsatException {
        ZeroDrawReasonCodeCache zdc = ZeroDrawReasonCodeCache.getInstance();
        if (code == null) {
            code = "";
        }
        if (zdc.containsCode(code) || code.trim().equalsIgnoreCase("")) {
            this.zeroDrawReasonCode = code;
        }
        else {
            throw new UsatException("Invalid Zero Draw Reason Code: " + code);
        }
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.bo.drawmngmt.ProductOrderBO#isValidPO()
     */
    public boolean isValidPO() {
        boolean validPO = true;
        
        // call base class checker first
        validPO = super.isValidPO();
        
        if (this.getAllowReturnsEdits() && (this.returns > this.maxReturns)) {
            validPO = false;
        }
        
        //  draw can be set to the higher of the recommended maximum or the current draw value
        int maxDraw = (this.draw > this.getMaxDrawThreshold()) ? this.draw : this.getMaxDrawThreshold();
        if (this.getAllowDrawEdits() && (this.draw > maxDraw)) {
            validPO = false;
        }
            
        return validPO;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#isChanged()
     */
    public boolean isChanged() {
        this.poChanged = false;

        if (this.hasEdittableFields()) {
	        super.isChanged();
	        
	        this.changeCode = this.dayChanged(this.returns, this.draw, this.zeroDrawReasonCode, this.originalDailyPO.getReturns(),this.originalDailyPO.getDraw(), this.originalDailyPO.getZeroDrawReasonCode());
        }
        
        return this.poChanged;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#hasEdittableFields()
     */
    public boolean hasEdittableFields() {
        boolean allowEdits = false;
        if (this.getAllowDrawEdits() || this.getAllowReturnsEdits() ) {
            allowEdits = true;
        }
        
        return allowEdits;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#setPOAsDeleted()
     */
    public void setPOAsDeleted() {
        this.originalDailyPO = null;
        
        // set values to unset
        this.draw = -1;
        this.returns = -1;
        
        // turn off all edit fields
        this.allowDrawEdits = false;
        this.allowReturnsEdits = false;

    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getMaxDrawThreshold()
     */
    public int getMaxDrawThreshold() {
        
        //  draw can be set to the higher of the recommended maximum or the current draw value
        // super.getMaxDrawThreshold is CRITICAL, so that parent implementation is called and infinite recursion avoided
        int maxDraw = (this.draw > super.getMaxDrawThreshold()) ? this.draw : super.getMaxDrawThreshold();
        return maxDraw;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getOriginalProductOrder()
     */
    public DailyReadOnlyProductOrderIntf getOriginalProductOrder() {
        return this.originalDailyPO;
    }
        
    /**
     * Clear object references
     */
    public void clear() {
        super.clear();
        this.originalDailyPO = null;
    }
    
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyReadOnlyProductOrderIntf#getDrawDayReturns()
     */
    public int getDrawDayReturns() {
        // TODO Auto-generated method stub
        return drawDayReturns;
    }
}
