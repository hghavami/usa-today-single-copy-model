/*
 * Created on Apr 19, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.bo.drawmngmt;

import java.util.ArrayList;

import com.usatoday.singlecopy.model.bo.drawmngmt.instance.DailyProductOrderBO;
import com.usatoday.singlecopy.model.bo.drawmngmt.instance.WeeklyProductOrderBO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf;

/**
 * @author aeast
 * @date Apr 19, 2007
 * @class ProductOrderBO
 * 
 * This is an abstract base class for product order instances. The concrete classes
 * are the DailyProductOrderBO and the WeeklyProductOrderBO.
 * 
 * @see DailyProductOrderBO
 * @see WeeklyProductOrderBO
 * 
 */
public abstract class ProductOrderBO implements ProductOrderIntf, Comparable<ProductOrderIntf> {

    private String productOrderID = null;
    private String productCode = null;
    private String productDescription = null;
    private int maxDrawThreshold = -1;
    private String productOrderTypeCode = null;
    private int sequenceNumber = 0;
    
    private ProductOrderIntf originalProductOrder = null;
    protected boolean poChanged = false;
    
    private LocationIntf owningLocation = null;
    
    private ArrayList<DrawUpdateErrorIntf> updateErrors = new ArrayList<DrawUpdateErrorIntf>();
    
    /**
     * 
     */
    public ProductOrderBO() {
        super();
        
    }

    public ProductOrderBO(ProductOrderIntf source) {
        this.maxDrawThreshold = source.getMaxDrawThreshold();
        this.productCode = source.getProductCode();
        this.productDescription = source.getProductDescription();
        this.productOrderID = source.getProductOrderID();
        this.productOrderTypeCode = source.getProductOrderTypeCode();
        this.sequenceNumber = source.getSequenceNumber();
        
        this.originalProductOrder = source;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getProductOrderID()
     */
    public String getProductOrderID() {
        return this.productOrderID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getProductOrderTypeCode()
     */
    public String getProductOrderTypeCode() {
        return this.productOrderTypeCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getProductCode()
     */
    public String getProductCode() {
        return this.productCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getSequenceNumber()
     */
    public int getSequenceNumber() {
        return this.sequenceNumber;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getProductDescription()
     */
    public String getProductDescription() {
        // may need to delete this are replace with a lookup in a cache or something
        return this.productDescription;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getMaxDrawThreshold()
     */
    public int getMaxDrawThreshold() {
        return this.maxDrawThreshold;
    }

    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(ProductOrderIntf o) {
        ProductOrderIntf otherObj = o;
        Integer sourceInt = new Integer(this.getSequenceNumber());
        Integer compareToInt = new Integer(otherObj.getSequenceNumber());
        return sourceInt.compareTo(compareToInt);
    }

    /**
     * This method should be over loaded in the sub-classes to further check if the 
     * product order has changed.
     */
    public boolean isChanged() {
        boolean changed = false;
        if (this.sequenceNumber != this.originalProductOrder.getSequenceNumber()) {
            changed = true;
            this.poChanged = true;
        }
        return changed;
    }

    /**
     * 
     * @param returns 
     * @param draw
     * @param zeroDrawReasonCode
     * @param originalReturns
     * @param originalDraw
     * @param original
     * @return The changeCode value for the day being examined.
     */
    protected String dayChanged(int returns, int draw, String zeroDrawReasonCode, int originalReturns, int originalDraw, String originalZeroDrawReasonCode) {
    
        String changeCode = ProductOrderIntf.NEITHER_RETURNS_NOR_DRAW_CHANGED;
            
        // Check Values and determine Change Code field.
        if (returns != originalReturns ||
            draw != originalDraw ||
            !zeroDrawReasonCode.equalsIgnoreCase(originalZeroDrawReasonCode)) {
            
            // indicated that the po has changed.
            this.poChanged = true;
            boolean returnChanged = false;
            boolean drawChanged = false;
            
            if (returns != originalReturns) {
                returnChanged = true;
            }
            
            if (draw != originalDraw) {
                drawChanged = true;
            }
            
            if (returnChanged && drawChanged) {
                changeCode = ProductOrderIntf.RETURNS_AND_DRAW_CHANGED;
            }
            else if (returnChanged) {
                changeCode = ProductOrderIntf.RETURNS_CHANGED;
            }
            else if (drawChanged) {
                changeCode = ProductOrderIntf.DRAW_CHANGED;
            }
            else {
                changeCode = ProductOrderIntf.NEITHER_RETURNS_NOR_DRAW_CHANGED;
            }
        }
        
        return changeCode;
    }
    
    /**
     * Resets mutable fields to that of the original po.
     * Makes call to subclass method to reset fields specific to sub classes
     *
     */
    public void resetProductOrderToOriginal() {
        // only need to update those fields which have setters
        if (this.originalProductOrder != null) {
            this.sequenceNumber = originalProductOrder.getSequenceNumber();
            this.resetFieldsToOriginal();
        }
        // make sure changed flag is reset to false
        this.poChanged = false;
    }

    protected abstract void resetFieldsToOriginal();
    
    public abstract int getTotalDraw();
    public abstract int getTotalReturns();
  
    public int getOriginalTotalDraw() {
       return this.originalProductOrder.getTotalDraw();
    }
    public int getOriginalTotalReturns() {
        return this.originalProductOrder.getTotalReturns();
    }
    
    
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getOwningLocation()
     */
    public LocationIntf getOwningLocation() {
        return this.owningLocation;
    }
    /**
     * @param owningLocation The owningLocation to set.
     */
    public void setOwningLocation(LocationIntf owningLocation) {
        this.owningLocation = owningLocation;
    }
    /**
     * @return Returns the updateError.
     */
    public DrawUpdateErrorIntf[] getUpdateErrors() {
        if (this.updateErrors.size() > 0) {
            return (DrawUpdateErrorIntf[])this.updateErrors.toArray(new DrawUpdateErrorIntf[this.updateErrors.size()]);

        }
        else {
            return null;
        }
    }
    /**
     * @param updateError The updateError to set.
     */
    public void addUpdateError(DrawUpdateErrorIntf updateError) {
        this.updateErrors.add(updateError);
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#setSaved()
     */
    public void setSaved(ProductOrderIntf newSource) {

        this.originalProductOrder = newSource;
        this.updateErrors.clear();
        this.poChanged = false;        
    }
    
    public void clearUpdateErrors() {
        this.updateErrors.clear();
    }
    
    public boolean isValidPO() {
        boolean validPO = true;
        
        // perform any base class business rule validations
        return validPO;
    }
    
    /**
     * Cleare references to objects
     */
    public void clear() {
        this.originalProductOrder = null;
        this.owningLocation = null;
        if (this.updateErrors != null && !this.updateErrors.isEmpty()) {
            this.updateErrors.clear();
        }
        this.updateErrors = null;
    }
}
