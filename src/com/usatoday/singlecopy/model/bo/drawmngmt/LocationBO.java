/*
 * Created on Apr 23, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.bo.drawmngmt;

import java.util.Collection;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;

/**
 * @author aeast
 * @date Apr 23, 2007
 * @class LocationBO
 * 
 * 
 * 
 */
public class LocationBO implements LocationIntf, Comparable<LocationIntf> {

    private String locationHashKey = null;
    private String locationID = null;
    private String address1 = null;
    private String phoneNumber = null;
    private String locationName = null;
    private DateTime date = null;
    protected Collection<ProductOrderIntf> productOrders = null;
    private int locationSequenceNumber = 0;
    
    /**
     * 
     */
    public LocationBO() {
        super();
    }

    public LocationBO(LocationIntf source) {
        this.locationHashKey = source.getLocationHashKey();
        this.locationID = source.getLocationID();
        this.address1 = source.getAddress1();
        this.locationName = source.getLocationName();
        this.locationSequenceNumber = source.getSequenceNumber();
        this.phoneNumber = source.getPhoneNumber();
        //this.date = source.getDate();
        
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf#getLocationID()
     */
    public String getLocationID() {
        return this.locationID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf#getLocationName()
     */
    public String getLocationName() {
        return this.locationName;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf#getAddress1()
     */
    public String getAddress1() {
        return this.address1;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf#getLocationHashKey()
     */
    public String getLocationHashKey() {
        return this.locationHashKey;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf#getSequenceNumber()
     */
    public int getSequenceNumber() {
        return this.locationSequenceNumber;
    }

    /**
     * @param date The date to set.
     */
    protected void setDate(DateTime date) {
        this.date = date;
    }
    /**
     * @param productOrders The productOrders to set.
     */
    protected void setProductOrders(Collection<ProductOrderIntf> productOrders) {
        this.productOrders = productOrders;
    }
    /**
     * @return Returns the date.
     */
    protected DateTime getDate() {
        return this.date;
    }
    /**
     * @return Returns the phoneNumber.
     */
    public String getPhoneNumber() {
        return this.phoneNumber;
    }
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(LocationIntf o) {
        LocationIntf otherObj = (LocationIntf)o;
        Integer sourceInt = new Integer(this.getSequenceNumber());
        Integer compareToInt = new Integer(otherObj.getSequenceNumber());
        return sourceInt.compareTo(compareToInt);
    }

    public Collection<ProductOrderIntf> getProductOrders() {
        return this.productOrders;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf#getProductOrder(java.lang.String)
     */
    public ProductOrderIntf getProductOrder(String productOrderID) {
        if (productOrderID == null) {
            return null;
        }
        ProductOrderIntf po = null;
        
        if (this.productOrders != null) {
            Iterator<ProductOrderIntf> itr = this.productOrders.iterator();
            while (itr.hasNext()) {
                ProductOrderIntf tempPO = itr.next();
                if (tempPO.getProductOrderID().equalsIgnoreCase(productOrderID)) {
                    po = tempPO;
                    break;
                }
            }
            
        }
        return po;
    }
    
    /**
     * Clear references to objects
     */
    public void clear() {
        if (this.productOrders != null && !this.productOrders.isEmpty()) {
            Iterator<ProductOrderIntf> itr = this.productOrders.iterator();
            while (itr.hasNext()) {
                ProductOrderIntf po = itr.next();
                po.clear();
            }
            this.productOrders.clear();
        }
        this.productOrders = null;
    }
}
