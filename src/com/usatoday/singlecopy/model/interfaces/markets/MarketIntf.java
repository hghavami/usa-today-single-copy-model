/*
 * Created on Aug 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.markets;

/**
 * @author aeast
 * @date Aug 2, 2007
 * @class MarketIntf
 * 
 * This interface represents the methods that can be called on a market instance.
 * 
 */
public interface MarketIntf {

    public String getID();
    public String getDescription();
    public String getLocalPhone();
    public String getRDLPhone();
    public String getEmailAddress();
    public boolean getHasEmailAddress();
}
