/*
 * Created on Apr 11, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.users;

/**
 * @author aeast
 * @date Apr 11, 2008
 * @class UserPreferenceIntf
 * 
 * This interface represents a user's preference. Preferences can be 
 * set to persist from one session to the next.
 * 
 * Preferences are stored in key/value pairs
 * 
 */
public interface UserPreferenceIntf {

    public String getPreferenceName();
    
    public String getPreferenceValue();
    
}
