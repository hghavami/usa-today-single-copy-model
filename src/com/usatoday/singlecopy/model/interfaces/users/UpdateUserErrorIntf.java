/*
 * Created on Aug 30, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.users;

/**
 * @author aeast
 * @date Aug 30, 2007
 * @class UpdateUserErrorIntf
 * 
 * Interface for user update errors.
 * 
 */
public interface UpdateUserErrorIntf extends GenericUpdateErrorIntf {

   	public String getEmailAddress();
}
