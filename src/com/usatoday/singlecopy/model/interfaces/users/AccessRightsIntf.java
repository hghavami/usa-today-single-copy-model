/*
 * Created on Mar 29, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.users;



/**
 * @author aeast
 * @date Mar 29, 2007
 * @class EmailScheduleSentTimeIntf
 * 
 * 
 * 
 */
public interface AccessRightsIntf {

    public static final String ROUTE_DRAW_MNGMT = "RTE";
    public static final String BLUE_CHIP_DRAW = "BCP";
    
    
    public String getAccessRight();
    public boolean isReceivingEmailReminders();
    public String getEmailReminderTimePreference();
    public void setReceivingEmailReminders(boolean receivesEmailReminders);
    public void setEmailReminderTimePreference(String emailReminderTimePreference);
    public boolean isChanged();
    public void setSave();
}
