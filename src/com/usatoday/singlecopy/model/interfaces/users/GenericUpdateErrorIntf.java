/*
 * Created on May 16, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.users;

/**
 * @author aeast
 * @date May 16, 2008
 * @class GenericUpdateErrorIntf
 * 
 * Represents an error message.
 * 
 */
public interface GenericUpdateErrorIntf {
   	public String getErrorMessage();
}
