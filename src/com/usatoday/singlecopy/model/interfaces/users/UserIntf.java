/*
 * Created on Feb 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.users;

import java.io.Serializable;
import java.util.Collection;

import com.usatoday.singlecopy.UsatException;

/**
 * @author aeast
 * @date Feb 2, 2007
 * @class UserIntf
 * 
 * This interface represents the base public interface to the 
 * User's of the Single Copy (Champion) Web Portal.
 * 
 */
public interface UserIntf extends Serializable {
    
    /**
     * 
     * @return email address of the user
     */
    public String getEmailAddress();
    /**
     * 
     * @param emailAddress
     */
    public void setEmailAddress(String emailAddress) throws UsatException;
    /**
     * 
     * @return user's password
     */
    public String getPassword();
    /**
     * 
     * @return user's phone number
     */
    public String getPhoneNumber();
    /**
     * 
     * @param password
     */
    public void setPassword(String password) throws UsatException;
    
    /**
     * 
     * @param password
     */
    public void save()  throws UsatException ;


    /**
     * 
     * @param name
     */
    public void setDisplayName(String name);
    
    /**
     * 
     * @return
     */
    public String getDisplayName();
    
    
    public boolean isUserAuthenticated();
    
    public Collection<AccessRightsIntf> getAccessRights();
    
    public boolean hasAccess(String accessRightCode);
    
    public String getSessionID();
    
    public AccessRightsIntf getAccessRight(String type);
}
