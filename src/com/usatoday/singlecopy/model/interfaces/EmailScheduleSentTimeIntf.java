/*
 * Created on Mar 29, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces;


/**
 * @author hghavami
 * @date Mar 29, 2007
 * @class EmailScheduleSentTimeIntf
 * 
 * 
 * 
 */
public interface EmailScheduleSentTimeIntf {

    public String getScheduleCode();
    public String getScheduleCodeDesc();
}
