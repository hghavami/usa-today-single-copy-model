/*
 * Created on May 4, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt;

/**
 * @author aeast
 * @date May 4, 2007
 * @class ProductDescriptionIntf
 * 
 * This interface provides read only access to product information
 * 
 */
public interface ProductDescriptionIntf {

    public String getProductCode();
    public String getDescription();
    public String getAbbreviation();
}
