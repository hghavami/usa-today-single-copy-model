/*
 * Created on Apr 9, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt.instance;

import org.joda.time.DateTime;


/**
 * @author aeast
 * @date Apr 9, 2007
 * @class DailyDrawLocationIntf
 * 
 * 
 * 
 */
public interface DailyDrawLocationIntf extends DrawManagementLocationIntf {

    public DateTime getRDLDate();
}
