/*
 * Created on Jun 13, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt.instance;

import org.joda.time.DateTime;

/**
 * @author aeast
 * @date Jun 13, 2007
 * @class DrawUpdateErrorIntf
 * 
 * This interface represents the methods for an error condition on a draw update.
 * 
 */
public interface DrawUpdateErrorIntf {
    public static final String DRAW_ERROR = "D";
    public static final String RETURN_ERROR = "R";
    public static final String BOTH_ERROR = "B";
    public static final String ZDR_ERROR = "Z";
    public static final String PO_DELETED = "X";
    
    public String getMarketID();
    public String getDistrictID();
    public String getRouteID();
    public String getLocationID();
    public String getProductOrderID();
    public DateTime getErrorDate();
    public String getAppliesTo();
    public String getErrorMessage();

}
