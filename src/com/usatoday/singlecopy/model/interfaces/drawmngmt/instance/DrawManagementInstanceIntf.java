/*
 * Created on Apr 23, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt.instance;

import java.util.Collection;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf;
import com.usatoday.singlecopy.model.interfaces.users.UserIntf;

/**
 * @author aeast
 * @date Apr 23, 2007
 * @class DrawManagementInstanceIntf
 * 
 * This interface represents the base instance of a RDL or weekly product order
 * management.
 * 
 * 
 */
public interface DrawManagementInstanceIntf {
    public RouteIntf getRoute();
    public UserIntf getUser();    
    public Collection<LocationIntf> getLocations();
    public LocationIntf getLocation(String key);
    public Collection<ProductOrderIntf> getChangedProductOrders();
    public int getNumberOfChangedProductOrders();
    public int getTotalDraw();
    public int getTotalReturns();
    public int getNumberOfLocations();
    public int getOriginalTotalDraw();
    public int getOriginalTotalReturns();
    public boolean getLateReturnsFlag();
    public boolean hasEdittableFields();

    public void saveChanges() throws UsatException;
    public boolean getUpdateErrorsFlag();
    public void setUpdateErrorsFlag(boolean errorFlag);
    public String getGlobalUpdateErrorMessage();
    public void clear();
}
