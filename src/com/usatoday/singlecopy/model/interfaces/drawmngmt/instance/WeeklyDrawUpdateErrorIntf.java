/*
 * Created on Jun 13, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt.instance;

import org.joda.time.DateTime;

/**
 * @author aeast
 * @date Jun 13, 2007
 * @class WeeklyDrawUpdateErrorIntf
 * 
 * 
 * 
 */
public interface WeeklyDrawUpdateErrorIntf extends DrawUpdateErrorIntf {
    public DateTime getWeekEndingDate();
}
