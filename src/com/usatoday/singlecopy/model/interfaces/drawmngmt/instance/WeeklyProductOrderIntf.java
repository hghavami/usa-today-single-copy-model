/*
 * Created on Apr 9, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt.instance;

import com.usatoday.singlecopy.UsatException;


/**
 * @author aeast
 * @date Apr 9, 2007
 * @class WeeklyProductOrderIntf
 * 
 * This class represents what can be done to a modifiable product order
 * 
 */
public interface WeeklyProductOrderIntf extends WeeklyReadOnlyProductOrderIntf {

    public WeeklyReadOnlyProductOrderIntf getOriginalProductOrder();
    
    // Monday
    public void setMondayZeroDrawReasonCode(String code) throws UsatException;
    public void setMondayDraw(int draw) throws UsatException;
    public void setMondayReturns(int returns) throws UsatException;
    
    // Tuesday
    public void setTuesdayZeroDrawReasonCode(String code) throws UsatException;
    public void setTuesdayDraw(int draw) throws UsatException;
    public void setTuesdayReturns(int returns) throws UsatException;
    
    // Wednesday
    public void setWednesdayZeroDrawReasonCode(String code) throws UsatException;
    public void setWednesdayDraw(int draw) throws UsatException;
    public void setWednesdayReturns(int returns) throws UsatException;

    // Thursday
    public void setThursdayZeroDrawReasonCode(String code) throws UsatException;
    public void setThursdayDraw(int draw) throws UsatException;
    public void setThursdayReturns(int returns) throws UsatException;

    // Friday
    public void setFridayZeroDrawReasonCode(String code) throws UsatException;
    public void setFridayDraw(int draw) throws UsatException;
    public void setFridayReturns(int returns) throws UsatException;

    // Saturday
    public void setSaturdayZeroDrawReasonCode(String code) throws UsatException;
    public void setSaturdayDraw(int draw) throws UsatException;
    public void setSaturdayReturns(int returns) throws UsatException;

    // Sunday
    public void setSundayZeroDrawReasonCode(String code) throws UsatException;
    public void setSundayDraw(int draw) throws UsatException;
    public void setSundayReturns(int returns) throws UsatException;
}
