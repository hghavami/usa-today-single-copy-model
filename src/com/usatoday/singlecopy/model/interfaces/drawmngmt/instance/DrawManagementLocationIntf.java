/*
 * Created on Apr 19, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt.instance;

import java.util.Collection;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;

/**
 * @author aeast
 * @date Apr 19, 2007
 * @class DrawManagementLocationIntf
 * 
 * This interface is implemented by locations that are created for a particular
 * press run.
 * 
 */
public interface DrawManagementLocationIntf extends LocationIntf {

    public Collection<ProductOrderIntf> getProductOrders();
}
