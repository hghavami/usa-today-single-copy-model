/*
 * Created on Apr 23, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt.instance;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.interfaces.users.UserIntf;

/**
 * @author aeast
 * @date Apr 23, 2007
 * @class RouteDrawManagementIntf
 * 
 * This interface is implemented by any class that permits working with draw management daily/weekly instances of run lists.
 * 
 */
public interface RouteDrawManagementIntf {
    
    /**
     * 
     * @param weekEndingDate
     * @param productCode
     * @param requestingUser
     * @return
     * @throws UsatException
     */
    public WeeklyDrawManangementIntf retrieveWeeklyDraw(DateTime weekEndingDate, String productCode, UserIntf requestingUser, boolean returnsOnly) throws UsatException;
    
    /**
     * 
     * @param RDL date
     * @param requestingUser
     * @return
     * @throws UsatException
     */
    public DailyDrawManagementIntf retrieveDailyDraw(DateTime date, UserIntf requestingUser) throws UsatException;
}
