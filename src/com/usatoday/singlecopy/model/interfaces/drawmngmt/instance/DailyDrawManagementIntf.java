/*
 * Created on Apr 23, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt.instance;

import org.joda.time.DateTime;

/**
 * @author aeast
 * @date Apr 23, 2007
 * @class DailyDrawManagementIntf
 * 
 * This inteface expands on the draw management interface for daily draw management.
 * 
 */
public interface DailyDrawManagementIntf extends DrawManagementInstanceIntf {

    public DateTime getRDLDate();
    
}
