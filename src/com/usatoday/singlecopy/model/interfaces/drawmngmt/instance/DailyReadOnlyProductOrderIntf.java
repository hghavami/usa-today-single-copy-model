/*
 * Created on Nov 13, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt.instance;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;

/**
 * @author aeast
 * @date Nov 13, 2007
 * @class DailyReadOnlyProductOrderIntf
 * 
 * This class represents a read only product order
 * 
 */
public interface DailyReadOnlyProductOrderIntf extends ProductOrderIntf {
    public DateTime getRDLDate();
    public DateTime getReturnDate();
    public String getDayType();
    public boolean getLateReturnsFlag();

    /**
     * 
     * @return The code indicating if the draw field has changed, return field changed, both changed, or neither (D,R,B,N)
     */
    public String getChangeCode();
    
    public int getDraw();
    public boolean getAllowDrawEdits();
    public int getReturns();
    public int getMaxReturns();
    public String getZeroDrawReasonCode();
    public boolean getAllowReturnsEdits();
    public int getDrawDayReturns();    

}
