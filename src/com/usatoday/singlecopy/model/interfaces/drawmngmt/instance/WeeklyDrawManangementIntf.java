/*
 * Created on Apr 23, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt.instance;

import org.joda.time.DateTime;

/**
 * @author aeast
 * @date Apr 23, 2007
 * @class WeeklyDrawManangementIntf
 * 
 * This interface extends a draw management instance for weekly draw management.
 * 
 */
public interface WeeklyDrawManangementIntf extends DrawManagementInstanceIntf {

    public DateTime getWeekEndingDate();
    public String getProductCode();
}
