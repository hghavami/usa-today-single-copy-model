/*
 * Created on Nov 13, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt.instance;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;

/**
 * @author aeast
 * @date Nov 13, 2007
 * @class WeeklyReadOnlyProductOrderIntf
 * 
 * 
 */
public interface WeeklyReadOnlyProductOrderIntf extends ProductOrderIntf {

    // Monday
    public int getMondayDraw();
    public int getMondayMaxDrawThreshold(); 
    public int getMondayReturns();
    public boolean getAllowMondayDrawEdits();
    public boolean getAllowMondayReturnsEdits();
    public String getMondayDayType();
    public String getMondayZeroDrawReasonCode();
    /**
     * 
     * @return The code indicating if the draw/return field has changed for this day, return field changed, both changed, or neither (D,R,B,N)
     */
    public String getMondayChangeCode();

    // Tuesday
    public int getTuesdayDraw();
    public int getTuesdayMaxDrawThreshold(); 
    public int getTuesdayReturns();
    public boolean getAllowTuesdayDrawEdits();
    public boolean getAllowTuesdayReturnsEdits();
    public String getTuesdayDayType();
    public String getTuesdayZeroDrawReasonCode();
    /**
     * 
     * @return The code indicating if the draw/return field has changed for this day, return field changed, both changed, or neither (D,R,B,N)
     */
    public String getTuesdayChangeCode();

    // Wednesday
    public int getWednesdayDraw();
    public int getWednesdayMaxDrawThreshold(); 
    public int getWednesdayReturns();
    public boolean getAllowWednesdayDrawEdits();
    public boolean getAllowWednesdayReturnsEdits();
    public String getWednesdayDayType();
    public String getWednesdayZeroDrawReasonCode();
    /**
     * 
     * @return The code indicating if the draw/return field has changed for this day, return field changed, both changed, or neither (D,R,B,N)
     */
    public String getWednesdayChangeCode();

    // Thursday
    public int getThursdayDraw();
    public int getThursdayMaxDrawThreshold(); 
    public int getThursdayReturns();
    public boolean getAllowThursdayDrawEdits();
    public boolean getAllowThursdayReturnsEdits();
    public String getThursdayDayType();
    public String getThursdayZeroDrawReasonCode();
    /**
     * 
     * @return The code indicating if the draw/return field has changed for this day, return field changed, both changed, or neither (D,R,B,N)
     */
    public String getThursdayChangeCode();

    // Friday
    public int getFridayDraw();
    public int getFridayMaxDrawThreshold(); 
    public int getFridayReturns();
    public boolean getAllowFridayDrawEdits();
    public boolean getAllowFridayReturnsEdits();
    public String getFridayDayType();
    public String getFridayZeroDrawReasonCode();
    /**
     * 
     * @return The code indicating if the draw/return field has changed for this day, return field changed, both changed, or neither (D,R,B,N)
     */
    public String getFridayChangeCode();

    // Saturday
    public int getSaturdayDraw();
    public int getSaturdayMaxDrawThreshold(); 
    public int getSaturdayReturns();
    public boolean getAllowSaturdayDrawEdits();
    public boolean getAllowSaturdayReturnsEdits();
    public String getSaturdayDayType();
    public String getSaturdayZeroDrawReasonCode();
    /**
     * 
     * @return The code indicating if the draw/return field has changed for this day, return field changed, both changed, or neither (D,R,B,N)
     */
    public String getSaturdayChangeCode();

    // Sunday
    public int getSundayDraw();
    public int getSundayMaxDrawThreshold(); 
    public int getSundayReturns();
    public boolean getAllowSundayDrawEdits();
    public boolean getAllowSundayReturnsEdits();
    public String getSundayDayType();
    public String getSundayZeroDrawReasonCode();
    /**
     * 
     * @return The code indicating if the draw/return field has changed for this day, return field changed, both changed, or neither (D,R,B,N)
     */
    public String getSundayChangeCode();    
    
}
