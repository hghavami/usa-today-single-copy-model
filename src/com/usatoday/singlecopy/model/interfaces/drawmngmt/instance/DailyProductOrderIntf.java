/*
 * Created on Apr 9, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt.instance;

import com.usatoday.singlecopy.UsatException;

/**
 * @author aeast
 * @date Apr 9, 2007
 * @class DailyProductOrderIntf
 * 
 * This class represents a modifiable daily product order.
 * 
 */
public interface DailyProductOrderIntf extends DailyReadOnlyProductOrderIntf {
    public void setDraw(int draw) throws UsatException;
    public void setReturns(int returns) throws UsatException;
    public void setZeroDrawReasonCode(String code) throws UsatException;

    public DailyReadOnlyProductOrderIntf getOriginalProductOrder();
}
