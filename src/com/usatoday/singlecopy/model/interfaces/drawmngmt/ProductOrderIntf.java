/*
 * Created on Feb 13, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyProductOrderIntf;


/**
 * @author aeast
 * @date Feb 13, 2007
 * @class ProductOrderIntf
 * 
 * This is the base interface for product orders. It is extended for the daily and
 * weekly draw management interfaces.
 * 
 * @see WeeklyProductOrderIntf
 * @see DailyProductOrderIntf
 */
public interface ProductOrderIntf  {

    public static final String[] CHANGE_CODES = {"D", "R", "B", "N"};
    public static final String DRAW_CHANGED = ProductOrderIntf.CHANGE_CODES[0];
    public static final String RETURNS_CHANGED = ProductOrderIntf.CHANGE_CODES[1];;
    public static final String RETURNS_AND_DRAW_CHANGED = ProductOrderIntf.CHANGE_CODES[2];
    public static final String NEITHER_RETURNS_NOR_DRAW_CHANGED = ProductOrderIntf.CHANGE_CODES[3];
    
    /**
     * 
     * @return The product order id.
     */
    public String getProductOrderID();
    /**
     * 
     * @return The type (location type) of the product order. Used to get product order type descriptions like rack, newsstand, etc..
     * 
     */
    public String getProductOrderTypeCode();
    /**
     * 
     * @return The product code associated with this product order
     */
    public String getProductCode();
    /**
     * 
     * @return The product orders sequence as it relates to other product orders
     */
    public int getSequenceNumber();
    /**
     *
     * @return A description of the product
     */
    public String getProductDescription();
    /**
     * 
     * @return The maximum draw field value for this product order.
     */
    public int getMaxDrawThreshold();
    /**
     * 
     * @return true, if this product order has changed from it's original values, otherwise false
     */
    public boolean isChanged();
    
    /**
     * 
     * @return Total draw for this product order
     */
    public int getTotalDraw();
    
    /**
     * 
     * @return Total returns for this product order
     */
    public int getTotalReturns();
    
    /**
     * 
     * @return The location to which this product order belongs
     */
    public LocationIntf getOwningLocation();
    
    /**
     * 
     * @return The update error for this PO if it exists otherwise null
     */
    public DrawUpdateErrorIntf[] getUpdateErrors();

    public void setSaved(ProductOrderIntf newSource);
    
    public boolean hasEdittableFields();
    
    public void setPOAsDeleted();
    
    public void clearUpdateErrors();
    
    public void clear();
    
}
