/*
 * Created on Sep 18, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt;

/**
 * @author aeast
 * @date Sep 18, 2007
 * @class BroadcastMessageIntf
 * 
 * This class represents a message that pertains to a specific route.
 * 
 */
public interface BroadcastMessageIntf {

    public String getMarketID();
    
    public String getDistrictID();
    
    public String getRouteID();
    
    public String getMessageID();
    
    public String getMessage();
}
