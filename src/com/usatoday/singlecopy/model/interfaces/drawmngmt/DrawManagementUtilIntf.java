/*
 * Created on Oct 10, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;

/**
 * @author aeast
 * @date Oct 10, 2007
 * @class DrawManagementUtilIntf
 * 
 * Contains methods to get various informatino from Champion that pertains to
 * draw management but is not specific to a particular market or route.
 * 
 */
public interface DrawManagementUtilIntf {

    public DateTime getDistributionDate() throws UsatException;
    
    public DateTime getDefaultRDLDate() throws UsatException;
    
    // possible future method.
    // public DateTime getPressRunDateForPub(String pubCode) throws UsatException;
}
