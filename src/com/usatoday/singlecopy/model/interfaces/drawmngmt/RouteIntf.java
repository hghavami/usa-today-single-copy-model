/*
 * Created on Feb 13, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt;

import java.util.Collection;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;

/**
 * @author aeast
 * @date Feb 13, 2007
 * @class RouteIntf
 * 
 * This interface defines the API for USAT Routes.
 * 
 */
public interface RouteIntf  {

    /**
     * 
     * @return
     */
    public String getRouteID();
    /**
     * 
     * @return
     */
    public String getDistrictID();
    
    /**
     * 
     * @return
     */
    public String getMarketID();    

    /**
     * 
     * @return
     */       
    public String getRouteDescription();
    
    /**
     * 
     * @return
     */
    //FUTURE: public Collection getLocations();

    public DateTime getRDLBeginDate();
    public DateTime getRDLEndDate();
    
    public DateTime getWeeklyDrawBeginDate();
    public DateTime getWeeklyDrawEndDate();
    
    public Collection<String> getWeeklyDrawPublications();
    
    /**
     * 
     * @return A collection of BroadcastMessageIntf objects or an empty collection
     * @throws UsatException
     */
    public Collection<BroadcastMessageIntf> getRouteBroadcastMessages() throws UsatException;
    
    /**
     * Clears the references to other objects
     *
     */
    public void clear();
}
