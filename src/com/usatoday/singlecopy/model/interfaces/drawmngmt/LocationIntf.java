/*
 * Created on Feb 13, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt;

import java.util.Collection;

/**
 * @author aeast
 * @date Feb 13, 2007
 * @class LocationIntf
 * 
 * 
 * 
 */
public interface LocationIntf  {

    public String getLocationID();
    public String getLocationName();
    public String getAddress1();
    public String getPhoneNumber();
    public String getLocationHashKey();
    public int getSequenceNumber();
    public Collection<ProductOrderIntf> getProductOrders();
    public ProductOrderIntf getProductOrder(String productOrderID);
    public void clear();
}
