/*
 * Created on Mar 20, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;

/**
 * @author aeast
 * @date Mar 20, 2008
 * @class BlueChipProductOrderIntf
 * 
 * This class represents a single day for a PO, It is used to reduce code duplication
 * that occured during the route management project where we had attributes for every day of
 * the week. That design ended up having to much code duplication so I've abstracted it out
 * a bit more so each day becomes an object.
 * 
 */
public interface BlueChipProductOrderIntf extends ProductOrderIntf {

    /**
     * 
     * @return The date associated with this PO
     */
    public DateTime getDate();
    
    /**
     * 
     * @return Returns true if this instance represents the current distribution day
     */
    public boolean isNextDistributionDate();
    
    /**
     * 
     * @return The draw value (quantity)
     */
    public int getDraw();
    
    /**
     * 
     * @param quantity The number of papers desired
     * @throws UsatException If the quantity is invalid or the field is not open for edits.
     */
    public void setDraw(int quantity) throws UsatException;
    
    /**
     * 
     * @return The base (default) amount to be delivered on this date
     */
    public int getBaseDraw();
    
    /**
     * 
     * @return The minimum allowable draw value
     */
    public int getMinDrawThreshold();
    
    /**
     * 
     * @return True if this day is still available for draw edits, otherwise false
     */
    public boolean getAllowDrawEdits();
    
    /**
     * 
     * @return Code for day type (Holiday, Publishing Day, Special Pub Day, etc)
     */
    public String getDayType();
    
    /**
     * 
     * @return The original version of this object when it was loaded from DB.
     */
    public BlueChipProductOrderIntf getOriginalProductOrder();
    
    /**
     * 
     * @return The week of PO's for which this day belongs.
     */
    public BlueChipProductOrderWeekIntf getOwningWeek();
    
    /**
     * 
     * @return The name of this product order, Ex: Front Desk
     */
    public String getProductName();
            
    public void resetToOriginal();
    
    public DateTime getCutOffDateTime();
    
    public boolean isPastCutOffDateTime();
    
    public boolean isTodayCutOffDay();
    
}
