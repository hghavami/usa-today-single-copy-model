package com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.to;

public interface BlueChipProductOrderHistoryTOIntf {

	public abstract String getSessionID();
	
    public abstract String getEmailAddress();

    public abstract String getMarketID();

    public abstract String getLocationID();

    public abstract String getProductOrderID();

    public abstract String getWeekEndingDate();
    
    public abstract int getMondayDraw();
    public abstract int getMondayReturns();
    
    public abstract int getTuesdayDraw();
    public abstract int getTuesdayReturns();

    public abstract int getWednesdayDraw();
    public abstract int getWednesdayReturns();

    public abstract int getThursdayDraw();
    public abstract int getThursdayReturns();
    
    public abstract int getFridayDraw();
    public abstract int getFridayReturns();

    public abstract int getSaturdayDraw();
    public abstract int getSaturdayReturns();

    public abstract int getSundayDraw();
    public abstract int getSundayReturns();

}
