/*
 * Created on Mar 20, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip;

import java.util.Collection;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

/**
 * @author aeast
 * @date Mar 20, 2008
 * @class BlueChipProductOrderWeekIntf
 * 
 * This class presents a layer on top of the week's worth of product orders.
 * It contains helper methods that can be used to work with a week of PO data
 * 
 */
public interface BlueChipProductOrderWeekIntf {
    
    public static final Integer MONDAY = new Integer(DateTimeConstants.MONDAY);
    public static final Integer TUESDAY = new Integer(DateTimeConstants.TUESDAY);
    public static final Integer WEDNESDAY = new Integer(DateTimeConstants.WEDNESDAY);
    public static final Integer THURSDAY = new Integer(DateTimeConstants.THURSDAY);
    public static final Integer FRIDAY = new Integer(DateTimeConstants.FRIDAY);
    public static final Integer SATURDAY = new Integer(DateTimeConstants.SATURDAY);
    public static final Integer SUNDAY = new Integer(DateTimeConstants.SUNDAY);
    
    /**
     * 
     * @return The key to use to store this object in a hash Map
     */
    public String getHashKey();
    
    /**
     * 
     * @return The week ending date for this instance
     */
    public DateTime getWeekEndingDate();
    
    
    /**
     * 
     * @return The date that represents the next distribution date.
     */
    public DateTime getNextDistributionDate();

    /**
     * 
     * @return The product order id.
     */
    public String getProductOrderID();
    /**
     * 
     * @return The type (location type) of the product order. Used to get product order type descriptions like rack, newsstand, etc..
     * 
     */
    public String getProductOrderTypeCode();
    /**
     * 
     * @return The product code associated with this product order
     */
    public String getProductCode();
    /**
     * 
     * @return The product orders sequence as it relates to other product orders
     */
    public int getSequenceNumber();
    /**
     *
     * @return A description of the product
     */
    public String getProductDescription();
    /**
     * 
     * @return The maximum draw field value for this product order.
     */

    /**
     *  @return The description of the location, Ex: Front Desk
     */
    public String getProductName();

    public int getMaxDrawThreshold();
    
    public int getMinDrawTheshold();
    /**
     * 
     * @return true, if any day in this product order has changed from it's original values, otherwise false
     */
    public boolean isChanged();

    /**
     * 
     * @return The location to which this product order belongs
     */
    public BlueChipLocationIntf getOwningLocation();
    
    /**
     * 
     * @return The multi week PO object that this week belongs in.
     */
    public BlueChipMultiWeekProductOrderIntf getOwningMultiWeek();
    
    /**
     * 
     * @return A collection of BlueChipProductOrderIntf objects.
     */
    public Collection<BlueChipProductOrderIntf> getDailyProductOrders();

    /**
     * 
     * @return A collection of BlueChipProductOrderIntf objects with a cut off date of today.
     */
    public Collection<BlueChipProductOrderIntf> getDailyProductOrdersThatCutOffToday();
    
    /**
     * 
     * @return The PO for this day
     */
    public BlueChipProductOrderIntf getMondayPO();
    /**
     * 
     * @return The PO for this day
     */
    public BlueChipProductOrderIntf getTuesdayPO();
    /**
     * 
     * @return The PO for this day
     */
    public BlueChipProductOrderIntf getWednesdayPO();
    /**
     * 
     * @return The PO for this day
     */
    public BlueChipProductOrderIntf getThursdayPO();
    /**
     * 
     * @return The PO for this day
     */
    public BlueChipProductOrderIntf getFridayPO();
    /**
     * 
     * @return The PO for this day
     */
    public BlueChipProductOrderIntf getSaturdayPO();
    /**
     * 
     * @return The PO for this day
     */
    public BlueChipProductOrderIntf getSundayPO();

    /**
     * 
     * @param day The day of week 1-7 from monday-sunday as defined in joda time constants DateTimeConstants class.
     * @return
     */
    public BlueChipProductOrderIntf getProductOrderForDay(int day);
    /**
     * 
     * @return The total of all seven days draw
     */
    public int getWeeklyDrawTotal();
    
    /**
     * 
     * @return True if this week contains the next distributrion date otherwise false
     */
    public boolean containsNextDistributionDate();
    
    public boolean getHasCutOffDaysForToday();
    
    public void clear();
}
