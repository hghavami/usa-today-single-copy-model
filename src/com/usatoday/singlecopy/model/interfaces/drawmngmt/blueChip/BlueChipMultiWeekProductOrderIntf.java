/*
 * Created on Mar 26, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip;

import java.util.Collection;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;

/**
 * @author aeast
 * @date Mar 26, 2008
 * @class BlueChipMultiWeekProductOrderIntf
 * 
 * This class acts as a container for multiple weeks worth of product order data
 * It povides accessors meant to facilitate working with eiteher a week of data or 
 * a day of data.
 * 
 */
public interface BlueChipMultiWeekProductOrderIntf extends ProductOrderIntf {

    public String getProductName();
    public String getHashKey();
    
    public BlueChipProductOrderWeekIntf getNextDistributionDateProductOrderWeek();
    
    public Collection<BlueChipProductOrderIntf> getCutOffDateProductOrders();
    
    public BlueChipProductOrderIntf getNextDistributionDateProductOrder();
    public DateTime getNextDistributionDate();
    public Collection<BlueChipProductOrderWeekIntf> getChangedWeeks();
    public boolean containsChangedWeeks();
    public int getMinDrawThreshold();
    
    /**
     * 
     * @param thisWeek - The week to base getting the next week off of (null will return current distribution week)
     * @return The week after the specified week or null if none exists
     */
    public BlueChipProductOrderWeekIntf getWeekAfter(BlueChipProductOrderWeekIntf thisWeek);
    /**
     * 
     * @param thisWeek - The week to base getting the prevoius week off of (null will return current distribution week)
     * @return The week before the specified week or null if none exists
     */
    public BlueChipProductOrderWeekIntf getWeekBefore(BlueChipProductOrderWeekIntf thisWeek);
    
    /**
     * 
     * @param date The date that will be included in the returned week
     * @return The week containing the date of interest.
     */
    public BlueChipProductOrderWeekIntf getWeekContainingDate(DateTime date);
 
    /**
     * 
     * @param date The date of interest
     * @return The product order for the day of interest
     */
    public BlueChipProductOrderIntf getProductOrderForDate(DateTime date);
    
    public void clear();
    
    public Collection<BlueChipProductOrderWeekIntf> getAllWeeks();
    
    public BlueChipProductOrderHistoryIntf getProductOrderHistory();
    
}
