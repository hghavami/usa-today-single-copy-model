package com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip;

import org.joda.time.DateTime;

public interface BlueChipProductOrderHistoryWeekIntf {

    /**
     * 
     * @return The key to use to store this object in a hash Map
     */
    public String getHashKey();
    
    /**
     * 
     * @return The week ending date for this instance
     */
    public DateTime getWeekEndingDate();

    /**
     * 
     * @return The product order id.
     */
    public String getProductOrderID();

    /**
     * 
     * @return The PO for this day
     */
    public BlueChipProductOrderHistoryInstanceIntf getMondayPO();
    /**
     * 
     * @return The PO for this day
     */
    public BlueChipProductOrderHistoryInstanceIntf getTuesdayPO();
    /**
     * 
     * @return The PO for this day
     */
    public BlueChipProductOrderHistoryInstanceIntf getWednesdayPO();
    /**
     * 
     * @return The PO for this day
     */
    public BlueChipProductOrderHistoryInstanceIntf getThursdayPO();
    /**
     * 
     * @return The PO for this day
     */
    public BlueChipProductOrderHistoryInstanceIntf getFridayPO();
    /**
     * 
     * @return The PO for this day
     */
    public BlueChipProductOrderHistoryInstanceIntf getSaturdayPO();
    /**
     * 
     * @return The PO for this day
     */
    public BlueChipProductOrderHistoryInstanceIntf getSundayPO();

    /**
     * 
     * @param day The day of week 1-7 from monday-sunday as defined in joda time constants DateTimeConstants class.
     * @return
     */
    public BlueChipProductOrderHistoryInstanceIntf getProductOrderForDay(int day);
    /**
     * 
     * @return The total of all seven days draw
     */
    public int getWeeklyDrawTotal();
    
}
