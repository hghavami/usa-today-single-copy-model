/*
 * Created on May 14, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.to;

/**
 * @author aeast
 * @date May 14, 2008
 * @class BlueChipProductOrderUpdateTOIntf
 * 
 * 
 * 
 */
public interface BlueChipProductOrderUpdateTOIntf {
    public abstract int getFridayDraw();

    public abstract void setFridayDraw(int fridayDraw);

    public abstract String getLocationID();

    public abstract void setLocationID(String locationID);

    public abstract int getMondayDraw();

    public abstract void setMondayDraw(int mondayDraw);

    public abstract String getProductOrderID();

    public abstract void setProductOrderID(String productOrderID);

    public abstract int getSaturdayDraw();

    public abstract void setSaturdayDraw(int saturdayDraw);

    public abstract int getSundayDraw();

    public abstract void setSundayDraw(int sundayDraw);

    public abstract int getThursdayDraw();

    public abstract void setThursdayDraw(int thursdayDraw);

    public abstract int getTuesdayDraw();

    public abstract void setTuesdayDraw(int tuesdayDraw);

    public abstract boolean isUpdateFriday();

    public abstract void setUpdateFriday(boolean updateFriday);

    public abstract boolean isUpdateMonday();

    public abstract void setUpdateMonday(boolean updateMonday);

    public abstract boolean isUpdateSaturday();

    public abstract void setUpdateSaturday(boolean updateSaturday);

    public abstract boolean isUpdateSunday();

    public abstract void setUpdateSunday(boolean updateSunday);

    public abstract boolean isUpdateThursday();

    public abstract void setUpdateThursday(boolean updateThursday);

    public abstract boolean isUpdateTuesday();

    public abstract void setUpdateTuesday(boolean updateTuesday);

    public abstract boolean isUpdateWednesday();

    public abstract void setUpdateWednesday(boolean updatewednesday);

    public abstract int getWednesdayDraw();

    public abstract void setWednesdayDraw(int wednesdayDraw);

    public abstract String getWeekEndingDate();

    public abstract void setWeekEndingDate(String weekEndingDate);
}