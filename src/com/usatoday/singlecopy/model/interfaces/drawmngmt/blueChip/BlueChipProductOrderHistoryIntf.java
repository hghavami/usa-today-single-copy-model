package com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip;

import java.util.Collection;

public interface BlueChipProductOrderHistoryIntf {

	public int getNumberOfHistoryWeeks();
	
	public Collection<BlueChipProductOrderHistoryWeekIntf> getAllWeeks();
	
	public String getProductOrderID();
	
}
