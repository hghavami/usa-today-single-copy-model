/*
 * Created on Mar 19, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip;

import java.util.Collection;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf;
import com.usatoday.singlecopy.model.interfaces.markets.MarketIntf;
import com.usatoday.singlecopy.model.interfaces.users.UserIntf;

/**
 * @author aeast
 * @date Mar 19, 2008
 * @class BlueChipLocationIntf
 * 
 * Classes implementing this interface will be used for Blue Chip Draw Entry.
 * They contain methods to get Product Orders for a given week.
 * 
 */
public interface BlueChipLocationIntf extends LocationIntf {

    
    /**
     * 
     * @param day The day of interest
     * @return A collection of BlueChipProductOrderIntf objects for the requested day
     */
    public Collection<BlueChipProductOrderIntf> getProductOrdersForDay(DateTime day);
    
    /**
     * 
     * @return The collection of BlueChipMultiWeekProductOrderIntf objects that have changes made to them
     */
    public Collection<BlueChipMultiWeekProductOrderIntf> getChangedProductOrders();
    
    /**
     * 
     * @return The collection of DrawUpdateErrorIntf objects
     */
    public Collection<DrawUpdateErrorIntf> getAllUpdateErrors();
    
    public void clearAllUpdateErrors();    
    /**
     * 
     * @return A collection of BlueChipProductOrderIntf objects that are all for next distribution date
     */
    public Collection<BlueChipProductOrderIntf> getNextDistributionDateProductOrders();

    /**
     * 
     * @return A collection of BlueChipProductOrderIntf objects that have a cut Off Date equal to date
     */
    public Collection<BlueChipProductOrderIntf> getProductOrdersThatCutOffToday();
    
    /**
     * 
     * @return The market ID of the owning market
     */
    public String getOwningMarketID();
    
    /**
     * 
     * @return The owning market object
     */
    public MarketIntf getOwningMarket();
    
    /**
     * 
     * @return The current user that requested this data
     */
	public UserIntf getOwningUser();
}
