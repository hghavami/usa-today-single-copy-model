/*
 * Created on Mar 27, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.to;

/**
 * @author aeast
 * @date Mar 27, 2008
 * @class BlueChipProductOrderTOIntf
 * 
 * 
 * 
 */
public interface BlueChipProductOrderTOIntf {
    public abstract String getEmailAddress();

    public abstract boolean isFridayAllowDrawEdits();

    public abstract int getFridayBaseDraw();

    public abstract String getFridayDayType();

    public abstract int getFridayDraw();

    public abstract String getLocationAddress1();

    public abstract String getLocationID();

    public abstract String getLocationName();

    public abstract String getLocationPhone();

    public abstract String getMarketID();

    public abstract int getMaxDrawThreshold();
    
    public abstract int getMinDrawThreshold();

    public abstract boolean isMondayAllowDrawEdits();

    public abstract int getMondayBaseDraw();

    public abstract String getMondayDayType();

    public abstract int getMondayDraw();

    public abstract String getNextDistributionDate();

    public abstract String getProductID();

    public abstract String getProductOrderID();

    public String getProductName();
    
    public abstract int getProductOrderSequenceNumber();

    public abstract String getProductOrderType();

    public abstract boolean isSaturdayAllowDrawEdits();

    public abstract int getSaturdayBaseDraw();

    public abstract String getSaturdayDayType();

    public abstract int getSaturdayDraw();

    public abstract String getSessionID();

    public abstract boolean isSundayAllowDrawEdits();

    public abstract int getSundayBaseDraw();

    public abstract String getSundayDayType();

    public abstract int getSundayDraw();

    public abstract boolean isThursdayAllowDrawEdits();

    public abstract int getThursdayBaseDraw();

    public abstract String getThursdayDayType();

    public abstract int getThursdayDraw();

    public abstract boolean isTuesdayAllowDrawEdits();

    public abstract int getTuesdayBaseDraw();

    public abstract String getTuesdayDayType();

    public abstract int getTuesdayDraw();

    public abstract boolean isWednesdayAllowDrawEdits();

    public abstract int getWednesdayBaseDraw();

    public abstract String getWednesdayDayType();

    public abstract int getWednesdayDraw();

    public abstract String getWeekEndingDate();
    
    public abstract String getMondayCutOffTime();
    
    public abstract String getMondayCutOffDate();

    public abstract String getTuesdayCutOffTime();
    
    public abstract String getTuesdayCutOffDate();

    public abstract String getWednesdayCutOffTime();
    
    public abstract String getWednesdayCutOffDate();

    public abstract String getThursdayCutOffTime();
    
    public abstract String getThursdayCutOffDate();

    public abstract String getFridayCutOffTime();
    
    public abstract String getFridayCutOffDate();

    public abstract String getSaturdayCutOffTime();
    
    public abstract String getSaturdayCutOffDate();

    public abstract String getSundayCutOffTime();
    
    public abstract String getSundayCutOffDate();
    
}