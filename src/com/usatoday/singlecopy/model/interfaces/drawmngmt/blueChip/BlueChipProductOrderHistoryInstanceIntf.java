package com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip;

import org.joda.time.DateTime;

public interface BlueChipProductOrderHistoryInstanceIntf {

   /**
     * 
     * @return The date associated with this PO
     */
    public DateTime getDate();
    
    /**
     * 
     * @return The draw value (quantity)  -1 if not set
     */
    public int getDraw();
    
    public int getReturns();
 
    // returns a value of 0 to N
    public int getTotalDraw();
}
