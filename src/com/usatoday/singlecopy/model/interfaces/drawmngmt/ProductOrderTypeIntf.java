/*
 * Created on Mar 13, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt;

/**
 * @author aeast
 * @date Mar 13, 2007
 * @class ProductOrderTypeIntf
 * 
 * This interface represents the descriptions of product order types
 * 
 */
public interface ProductOrderTypeIntf {

    public String getDescription();
    public String getProductCode();
    public String getProductOrderTypeCode();
    public String getKey();
}
