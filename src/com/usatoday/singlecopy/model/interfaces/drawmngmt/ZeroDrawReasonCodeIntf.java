/*
 * Created on Jun 8, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.interfaces.drawmngmt;

/**
 * @author aeast
 * @date Jun 8, 2007
 * @class ZeroDrawReasonCodeIntf
 * 
 * This class represents a zero draw reason code
 *  
 */
public interface ZeroDrawReasonCodeIntf {
    public String getCode();
    public String getDescription();
}
