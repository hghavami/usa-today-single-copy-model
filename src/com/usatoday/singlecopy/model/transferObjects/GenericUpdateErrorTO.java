/*
 * Created on May 16, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.transferObjects;

import java.io.Serializable;

import com.usatoday.singlecopy.model.interfaces.users.GenericUpdateErrorIntf;

/**
 * @author aeast
 * @date May 16, 2008
 * @class GenericUpdateErrorTO
 * 
 * 
 * 
 */
public class GenericUpdateErrorTO implements GenericUpdateErrorIntf,
        Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2566339067950934291L;
	private String errorMessage = null;
    
    /**
     * 
     */
    public GenericUpdateErrorTO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.users.GenericUpdateErrorIntf#getErrorMessage()
     */
    public String getErrorMessage() {
        return this.errorMessage;
    }
    
    public void setErrorMessage(String msg) {
        this.errorMessage = msg;
    }

}
