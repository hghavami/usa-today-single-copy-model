/*
 * Created on Apr 9, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.transferObjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.BroadcastMessageIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf;

/**
 * @author aeast
 * @date Apr 9, 2007
 * @class RouteTO
 * 
 * This class is used to transfer Route Data between the business object 
 * layer and the integration (db) layer.
 * 
 */
public class RouteTO implements RouteIntf, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8337764228115031739L;
	private String routeID = null;
    private String districtID = null;
    private String marketID = null;
    private String routeDescription = null;
    
    // following dates are used to restrict the dates
    // a user can select to work the RDL by day
    private DateTime rdlBeginDate = null;
    private DateTime rdlEndDate = null;

    // following dates are used to restrict the dates
    // a user can select to work the weekly draw
    private DateTime weeklyDrawBeginDate = null;
    private DateTime weeklyDrawEndDate = null;
    
    private Collection<String> weeklyDrawPublications = null;
    
    /**
     * 
     */
    public RouteTO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf#getRouteID()
     */
    public String getRouteID() {
        return this.routeID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf#getDistrictID()
     */
    public String getDistrictID() {
        return this.districtID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf#getMarketID()
     */
    public String getMarketID() {
        return this.marketID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf#getRouteDescription()
     */
    public String getRouteDescription() {
        return this.routeDescription;
    }


    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf#getReturnDrawBeginDate()
     */
    public DateTime getRDLBeginDate() {
        return this.rdlBeginDate;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf#getReturnDrawEndDate()
     */
    public void setRDLEndDate(DateTime dt) {
        this.rdlEndDate = dt;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf#getReturnDrawBeginDate()
     */
    public void setRDLBeginDate(DateTime dt) {
        this.rdlBeginDate = dt;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf#getReturnDrawEndDate()
     */
    public DateTime getRDLEndDate() {
        return this.rdlEndDate;
    }
    
    /**
     * @param districtID The districtID to set.
     */
    public void setDistrictID(String districtID) {
        this.districtID = districtID;
    }
    /**
     * @param marketID The marketID to set.
     */
    public void setMarketID(String marketID) {
        this.marketID = marketID;
    }
    /**
     * @param routeDescription The routeDescription to set.
     */
    public void setRouteDescription(String routeDescription) {
        this.routeDescription = routeDescription;
    }
    /**
     * @param routeID The routeID to set.
     */
    public void setRouteID(String routeID) {
        this.routeID = routeID;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf#getWeeklyDrawBeginDate()
     */
    public DateTime getWeeklyDrawBeginDate() {
        return weeklyDrawBeginDate;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf#getWeeklyDrawEndDate()
     */
    public DateTime getWeeklyDrawEndDate() {
        return weeklyDrawEndDate;
    }
    
    
    /**
     * @param weeklyDrawBeginDate The weeklyDrawBeginDate to set.
     */
    public void setWeeklyDrawBeginDate(DateTime weeklyDrawBeginDate) {
        this.weeklyDrawBeginDate = weeklyDrawBeginDate;
    }
    /**
     * @param weeklyDrawEndDate The weeklyDrawEndDate to set.
     */
    public void setWeeklyDrawEndDate(DateTime weeklyDrawEndDate) {
        this.weeklyDrawEndDate = weeklyDrawEndDate;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf#getWeeklyDrawPublications()
     */
    public Collection<String> getWeeklyDrawPublications() {
        return this.weeklyDrawPublications;
    }
    
    /**
     * @param weeklyDrawPublications The weeklyDrawPublications to set.
     */
    public void setWeeklyDrawPublications(Collection<String> weeklyDrawPublications) {
        this.weeklyDrawPublications = weeklyDrawPublications;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf#getRouteBroadcastMessages()
     */
    public Collection<BroadcastMessageIntf> getRouteBroadcastMessages() throws UsatException {
        return new ArrayList<BroadcastMessageIntf>();
    }
    public void clear() {
        ;

    }
}
