/*
 * Created on Apr 3, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.transferObjects;

import java.io.Serializable;

import com.usatoday.singlecopy.model.interfaces.users.AccessRightsIntf;

/**
 * @author aeast
 * @date Apr 3, 2007
 * @class AccessRightsTO
 * 
 * 
 */
public class AccessRightsTO implements AccessRightsIntf, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8196039116727710781L;
	private String accessRight = null;
    private boolean receivesEmailReminders = false;
    private String emailReminderTimePreference = null;
    
    /**
     * 
     */
    public AccessRightsTO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.users.EmailScheduleSentTimeIntf#getAccessRight()
     */
    public String getAccessRight() {
        return this.accessRight;
    }

    /**
     * @param accessRight The accessRight to set.
     */
    public void setAccessRight(String accessRight) {
        this.accessRight = accessRight;
    }
    public String getEmailReminderTimePreference() {
        return this.emailReminderTimePreference;
    }
    public boolean isReceivingEmailReminders() {
        return this.receivesEmailReminders;
    }
    public void setEmailReminderTimePreference(
            String emailReminderTimePreference) {
        this.emailReminderTimePreference = emailReminderTimePreference;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.users.EmailScheduleSentTimeIntf#setReceivingEmailReminders(boolean)
     */
    public void setReceivingEmailReminders(boolean receivesEmailReminders) {
        this.receivesEmailReminders = receivesEmailReminders;
        
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.users.AccessRightsIntf#isChanged()
     */
    public boolean isChanged() {
        return false;
    }
    
    public void setSave() {
        
    }
}
