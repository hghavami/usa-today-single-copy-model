/*
 * Created on Aug 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.transferObjects;

import java.io.Serializable;

import com.usatoday.singlecopy.model.interfaces.EmailScheduleSentTimeIntf;

/**
 * @author hghavami
 * @date May 21, 2008
 * @class EmailScheduleSentTimeTO
 * 
 * EmailScheduleSentTime Transfer Object.
 * 
 */
public class EmailScheduleSentTimeTO implements EmailScheduleSentTimeIntf, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 2162601928050227835L;
	private String scheduleCode = null;
    private String scheduleCodeDesc = null;
    /**
     * 
     */
    public EmailScheduleSentTimeTO() {
        super();
    }
    /**
     * @return Returns the scheduleCode.
     */
    public String getScheduleCode() {
        return scheduleCode;
    }
    /**
     * @param scheduleCode The scheduleCode to set.
     */
    public void setScheduleCode(String scheduleCode) {
        this.scheduleCode = scheduleCode;
    }
    /**
     * @return Returns the scheduleCodeDesc.
     */
    public String getScheduleCodeDesc() {
        return scheduleCodeDesc;
    }
    /**
     * @param scheduleCodeDesc The scheduleCodeDesc to set.
     */
    public void setScheduleCodeDesc(String scheduleCodeDesc) {
        this.scheduleCodeDesc = scheduleCodeDesc;
    }
}
