/*
 * Created on Apr 9, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.transferObjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;

/**
 * @author aeast
 * @date Apr 9, 2007
 * @class LocationTO
 * 
 * This class is used to transfer location information from the data integration(DB) tier
 * to the business object tier/layer.
 * 
 */
public class LocationTO implements LocationIntf, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8696373436192023068L;
	private String locationHashKey = null;
    private String locationID = null;
    private String address1 = null;
    private String phoneNumber = null;
    private String locationName = null;
    private DateTime date = null;
    private Collection<ProductOrderIntf> productOrders = null;
    private int locationSequenceNumber = 0;
    
    /**
     * 
     */
    public LocationTO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf#getLocationID()
     */
    public String getLocationID() {
        return this.locationID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf#getLocationName()
     */
    public String getLocationName() {
        return this.locationName;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf#getAddress1()
     */
    public String getAddress1() {
        return this.address1;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf#getProductOrders()
     */
    public Collection<ProductOrderIntf> getProductOrders() {
        if (this.productOrders == null) {
            this.productOrders = new ArrayList<ProductOrderIntf>();
        }
        return this.productOrders;
    }

    public DateTime getDate() {
        return this.date;
    }
    
    public void setAddress1(String address1) {
        this.address1 = address1;
    }
    public void setDate(DateTime date) {
        this.date = date;
    }
    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }
    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
    public void setProductOrders(Collection<ProductOrderIntf> productOrders) {
        this.productOrders = productOrders;
    }
    public String getLocationHashKey() {
        return this.locationHashKey;
    }
    public void setLocationHashKey(String locationHashKey) {
        this.locationHashKey = locationHashKey;
    }
    public int getSequenceNumber() {
        return this.locationSequenceNumber;
    }
    public void setLocationSequenceNumber(int locationSequenceNumber) {
        this.locationSequenceNumber = locationSequenceNumber;
    }
    /**
     * @return Returns the phoneNumber.
     */
    public String getPhoneNumber() {
        return this.phoneNumber;
    }
    /**
     * @param phoneNumber The phoneNumber to set.
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf#getProductOrder(java.lang.String)
     */
    public ProductOrderIntf getProductOrder(String productOrderID) {
        // This method intentionally left unwritten because it's a TO
        return null;
    }
    public void clear() {

    }
}
