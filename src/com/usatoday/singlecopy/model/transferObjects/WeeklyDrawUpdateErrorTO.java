/*
 * Created on Jun 13, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.transferObjects;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyDrawUpdateErrorIntf;

/**
 * @author aeast
 * @date Jun 13, 2007
 * @class WeeklyDrawUpdateErrorTO
 * 
 * 
 * 
 */
public class WeeklyDrawUpdateErrorTO extends DrawUpdateErrorTO implements
        WeeklyDrawUpdateErrorIntf {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8700325141306832570L;
	private DateTime weekEndingDate = null;
    /**
     * 
     */
    public WeeklyDrawUpdateErrorTO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyDrawUpdateErrorIntf#getWeekEndingDate()
     */
    public DateTime getWeekEndingDate() {
        return this.weekEndingDate;
    }

    /**
     * @param weekEndingDate The weekEndingDate to set.
     */
    public void setWeekEndingDate(DateTime weekEndingDate) {
        this.weekEndingDate = weekEndingDate;
    }
}
