/*
 * Created on May 14, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.transferObjects.blueChip;

import java.io.Serializable;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.to.BlueChipProductOrderUpdateTOIntf;

/**
 * @author aeast
 * @date May 14, 2008
 * @class BlueChipProductOrderUpdateTO
 * 
 * 
 * 
 */
public class BlueChipProductOrderUpdateTO implements Serializable, BlueChipProductOrderUpdateTOIntf {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1555899100451838760L;
	private String locationID = "";
    private String productOrderID = "";
    private String weekEndingDate = "";
    private int mondayDraw = -1;
    private boolean updateMonday = false;
    private int tuesdayDraw = -1;
    private boolean updateTuesday = false;
    private int wednesdayDraw = -1;
    private boolean updateWednesday = false;
    private int thursdayDraw = -1;
    private boolean updateThursday = false;
    private int fridayDraw = -1;
    private boolean updateFriday = false;
    private int saturdayDraw = -1;
    private boolean updateSaturday = false;
    private int sundayDraw = -1;
    private boolean updateSunday = false;
    
    /**
     * 
     */
    public BlueChipProductOrderUpdateTO() {
        super();
    }

    public int getFridayDraw() {
        return this.fridayDraw;
    }
    public void setFridayDraw(int fridayDraw) {
        this.fridayDraw = fridayDraw;
    }
    public String getLocationID() {
        return this.locationID;
    }
    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }
    public int getMondayDraw() {
        return this.mondayDraw;
    }
    public void setMondayDraw(int mondayDraw) {
        this.mondayDraw = mondayDraw;
    }
    public String getProductOrderID() {
        return this.productOrderID;
    }
    public void setProductOrderID(String productOrderID) {
        this.productOrderID = productOrderID;
    }
    public int getSaturdayDraw() {
        return this.saturdayDraw;
    }
    public void setSaturdayDraw(int saturdayDraw) {
        this.saturdayDraw = saturdayDraw;
    }
    public int getSundayDraw() {
        return this.sundayDraw;
    }
    public void setSundayDraw(int sundayDraw) {
        this.sundayDraw = sundayDraw;
    }
    public int getThursdayDraw() {
        return this.thursdayDraw;
    }
    public void setThursdayDraw(int thursdayDraw) {
        this.thursdayDraw = thursdayDraw;
    }
    public int getTuesdayDraw() {
        return this.tuesdayDraw;
    }
    public void setTuesdayDraw(int tuesdayDraw) {
        this.tuesdayDraw = tuesdayDraw;
    }
    public boolean isUpdateFriday() {
        return this.updateFriday;
    }
    public void setUpdateFriday(boolean updateFriday) {
        this.updateFriday = updateFriday;
    }
    public boolean isUpdateMonday() {
        return this.updateMonday;
    }
    public void setUpdateMonday(boolean updateMonday) {
        this.updateMonday = updateMonday;
    }
    public boolean isUpdateSaturday() {
        return this.updateSaturday;
    }
    public void setUpdateSaturday(boolean updateSaturday) {
        this.updateSaturday = updateSaturday;
    }
    public boolean isUpdateSunday() {
        return this.updateSunday;
    }
    public void setUpdateSunday(boolean updateSunday) {
        this.updateSunday = updateSunday;
    }
    public boolean isUpdateThursday() {
        return this.updateThursday;
    }
    public void setUpdateThursday(boolean updateThursday) {
        this.updateThursday = updateThursday;
    }
    public boolean isUpdateTuesday() {
        return this.updateTuesday;
    }
    public void setUpdateTuesday(boolean updateTuesday) {
        this.updateTuesday = updateTuesday;
    }
    public boolean isUpdateWednesday() {
        return this.updateWednesday;
    }
    public void setUpdateWednesday(boolean updateWednesday) {
        this.updateWednesday = updateWednesday;
    }
    public int getWednesdayDraw() {
        return this.wednesdayDraw;
    }
    public void setWednesdayDraw(int wednesdayDraw) {
        this.wednesdayDraw = wednesdayDraw;
    }
    public String getWeekEndingDate() {
        return this.weekEndingDate;
    }
    public void setWeekEndingDate(String weekEndingDate) {
        this.weekEndingDate = weekEndingDate;
    }
}
