/*
 * Created on Mar 21, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.transferObjects.blueChip;

import java.io.Serializable;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.to.BlueChipProductOrderTOIntf;

/**
 * @author aeast
 * @date Mar 21, 2008
 * @class BlueChipProductOrderTO
 * 
 * This class is used to tranfer information about a Blue Chip PO as it is received from the database 
 * This class contains no business logic. It mimics the layout of the API transaction for fetching 
 * Blue Chip Product orders. This class interface is used to generate an actual object model in the object factory.
 * 
 */
public class BlueChipProductOrderTO implements Serializable, BlueChipProductOrderTOIntf {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2544399318218804997L;
	private String emailAddress = null;
    private String sessionID = null;
    private String marketID = null;
    private String locationID = null;
    private String locationName = null;
    private String locationAddress1 = null;
    private String locationPhone = null;
    private String productOrderID  = null;
    private String productName = null;
    private int productOrderSequenceNumber = 0;
    private String productID = null;
    private String productOrderType = null;
    private int maxDrawThreshold = 0;
    private int minDrawThreshold = 0;
    private String weekEndingDate = null;
    private String nextDistributionDate = null;

    // monday
    private int mondayBaseDraw = -1;
    private int mondayDraw = -1;
    private boolean mondayAllowDrawEdits = false;
    private String mondayDayType = null;
    private String mondayCutOffTime = null;
    private String mondayCutOffDate = null;
    
    // tuesday
    private int tuesdayBaseDraw = -1;
    private int tuesdayDraw = -1;
    private boolean tuesdayAllowDrawEdits = false;
    private String tuesdayDayType = null;
    private String tuesdayCutOffTime = null;
    private String tuesdayCutOffDate = null;
    
    // wednesday
    private int wednesdayBaseDraw = -1;
    private int wednesdayDraw = -1;
    private boolean wednesdayAllowDrawEdits = false;
    private String wednesdayDayType = null;
    private String wednesdayCutOffTime = null;
    private String wednesdayCutOffDate = null;
    
    // thursday
    private int thursdayBaseDraw = -1;
    private int thursdayDraw = -1;
    private boolean thursdayAllowDrawEdits = false;
    private String thursdayDayType = null;
    private String thursdayCutOffTime = null;
    private String thursdayCutOffDate = null;
    
    // friday
    private int fridayBaseDraw = -1;
    private int fridayDraw = -1;
    private boolean fridayAllowDrawEdits = false;
    private String fridayDayType = null;
    private String fridayCutOffTime = null;
    private String fridayCutOffDate = null;
    
    // saturday
    private int saturdayBaseDraw = -1;
    private int saturdayDraw = -1;
    private boolean saturdayAllowDrawEdits = false;
    private String saturdayDayType = null;
    private String saturdayCutOffTime = null;
    private String saturdayCutOffDate = null;
    
    // sunday
    private int sundayBaseDraw = -1;
    private int sundayDraw = -1;
    private boolean sundayAllowDrawEdits = false;
    private String sundayDayType = null;
    private String sundayCutOffTime = null;
    private String sundayCutOffDate = null;
    
    public String getEmailAddress() {
        return this.emailAddress;
    }
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    public boolean isFridayAllowDrawEdits() {
        return this.fridayAllowDrawEdits;
    }
    public void setFridayAllowDrawEdits(boolean fridayAllowDrawEdits) {
        this.fridayAllowDrawEdits = fridayAllowDrawEdits;
    }
    public int getFridayBaseDraw() {
        return this.fridayBaseDraw;
    }
    public void setFridayBaseDraw(int fridayBaseDraw) {
        this.fridayBaseDraw = fridayBaseDraw;
    }
    public String getFridayDayType() {
        return this.fridayDayType;
    }
    public void setFridayDayType(String fridayDayType) {
        this.fridayDayType = fridayDayType;
    }
    public int getFridayDraw() {
        return this.fridayDraw;
    }
    public void setFridayDraw(int fridayDraw) {
        this.fridayDraw = fridayDraw;
    }
    public String getLocationAddress1() {
        return this.locationAddress1;
    }
    public void setLocationAddress1(String locationAddress1) {
        this.locationAddress1 = locationAddress1;
    }
    public String getLocationID() {
        return this.locationID;
    }
    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }
    public String getLocationName() {
        return this.locationName;
    }
    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
    public String getLocationPhone() {
        return this.locationPhone;
    }
    public void setLocationPhone(String locationPhone) {
        this.locationPhone = locationPhone;
    }
    public String getMarketID() {
        return this.marketID;
    }
    public void setMarketID(String marketID) {
        this.marketID = marketID;
    }
    public int getMaxDrawThreshold() {
        return this.maxDrawThreshold;
    }
    public void setMaxDrawThreshold(int maxDrawThreshold) {
        this.maxDrawThreshold = maxDrawThreshold;
    }
    public boolean isMondayAllowDrawEdits() {
        return this.mondayAllowDrawEdits;
    }
    public void setMondayAllowDrawEdits(boolean mondayAllowDrawEdits) {
        this.mondayAllowDrawEdits = mondayAllowDrawEdits;
    }
    public int getMondayBaseDraw() {
        return this.mondayBaseDraw;
    }
    public void setMondayBaseDraw(int mondayBaseDraw) {
        this.mondayBaseDraw = mondayBaseDraw;
    }
    public String getMondayDayType() {
        return this.mondayDayType;
    }
    public void setMondayDayType(String mondayDayType) {
        this.mondayDayType = mondayDayType;
    }
    public int getMondayDraw() {
        return this.mondayDraw;
    }
    public void setMondayDraw(int mondayDraw) {
        this.mondayDraw = mondayDraw;
    }
    public String getNextDistributionDate() {
        return this.nextDistributionDate;
    }
    public void setNextDistributionDate(String nextDistributionDate) {
        this.nextDistributionDate = nextDistributionDate;
    }
    public String getProductID() {
        return this.productID;
    }
    public void setProductID(String productID) {
        this.productID = productID;
    }
    public String getProductOrderID() {
        return this.productOrderID;
    }
    public void setProductOrderID(String productOrderID) {
        this.productOrderID = productOrderID;
    }
    public int getProductOrderSequenceNumber() {
        return this.productOrderSequenceNumber;
    }
    public void setProductOrderSequenceNumber(int productOrderSequenceNumber) {
        this.productOrderSequenceNumber = productOrderSequenceNumber;
    }
    public String getProductOrderType() {
        return this.productOrderType;
    }
    public void setProductOrderType(String productOrderType) {
        this.productOrderType = productOrderType;
    }
    public boolean isSaturdayAllowDrawEdits() {
        return this.saturdayAllowDrawEdits;
    }
    public void setSaturdayAllowDrawEdits(boolean saturdayAllowDrawEdits) {
        this.saturdayAllowDrawEdits = saturdayAllowDrawEdits;
    }
    public int getSaturdayBaseDraw() {
        return this.saturdayBaseDraw;
    }
    public void setSaturdayBaseDraw(int saturdayBaseDraw) {
        this.saturdayBaseDraw = saturdayBaseDraw;
    }
    public String getSaturdayDayType() {
        return this.saturdayDayType;
    }
    public void setSaturdayDayType(String saturdayDayType) {
        this.saturdayDayType = saturdayDayType;
    }
    public int getSaturdayDraw() {
        return this.saturdayDraw;
    }
    public void setSaturdayDraw(int saturdayDraw) {
        this.saturdayDraw = saturdayDraw;
    }
    public String getSessionID() {
        return this.sessionID;
    }
    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }
    public boolean isSundayAllowDrawEdits() {
        return this.sundayAllowDrawEdits;
    }
    public void setSundayAllowDrawEdits(boolean sundayAllowDrawEdits) {
        this.sundayAllowDrawEdits = sundayAllowDrawEdits;
    }
    public int getSundayBaseDraw() {
        return this.sundayBaseDraw;
    }
    public void setSundayBaseDraw(int sundayBaseDraw) {
        this.sundayBaseDraw = sundayBaseDraw;
    }
    public String getSundayDayType() {
        return this.sundayDayType;
    }
    public void setSundayDayType(String sundayDayType) {
        this.sundayDayType = sundayDayType;
    }
    public int getSundayDraw() {
        return this.sundayDraw;
    }
    public void setSundayDraw(int sundayDraw) {
        this.sundayDraw = sundayDraw;
    }
    public boolean isThursdayAllowDrawEdits() {
        return this.thursdayAllowDrawEdits;
    }
    public void setThursdayAllowDrawEdits(boolean thursdayAllowDrawEdits) {
        this.thursdayAllowDrawEdits = thursdayAllowDrawEdits;
    }
    public int getThursdayBaseDraw() {
        return this.thursdayBaseDraw;
    }
    public void setThursdayBaseDraw(int thursdayBaseDraw) {
        this.thursdayBaseDraw = thursdayBaseDraw;
    }
    public String getThursdayDayType() {
        return this.thursdayDayType;
    }
    public void setThursdayDayType(String thursdayDayType) {
        this.thursdayDayType = thursdayDayType;
    }
    public int getThursdayDraw() {
        return this.thursdayDraw;
    }
    public void setThursdayDraw(int thursdayDraw) {
        this.thursdayDraw = thursdayDraw;
    }
    public boolean isTuesdayAllowDrawEdits() {
        return this.tuesdayAllowDrawEdits;
    }
    public void setTuesdayAllowDrawEdits(boolean tuesdayAllowDrawEdits) {
        this.tuesdayAllowDrawEdits = tuesdayAllowDrawEdits;
    }
    public int getTuesdayBaseDraw() {
        return this.tuesdayBaseDraw;
    }
    public void setTuesdayBaseDraw(int tuesdayBaseDraw) {
        this.tuesdayBaseDraw = tuesdayBaseDraw;
    }
    public String getTuesdayDayType() {
        return this.tuesdayDayType;
    }
    public void setTuesdayDayType(String tuesdayDayType) {
        this.tuesdayDayType = tuesdayDayType;
    }
    public int getTuesdayDraw() {
        return this.tuesdayDraw;
    }
    public void setTuesdayDraw(int tuesdayDraw) {
        this.tuesdayDraw = tuesdayDraw;
    }
    public boolean isWednesdayAllowDrawEdits() {
        return this.wednesdayAllowDrawEdits;
    }
    public void setWednesdayAllowDrawEdits(boolean wednesdayAllowDrawEdits) {
        this.wednesdayAllowDrawEdits = wednesdayAllowDrawEdits;
    }
    public int getWednesdayBaseDraw() {
        return this.wednesdayBaseDraw;
    }
    public void setWednesdayBaseDraw(int wednesdayBaseDraw) {
        this.wednesdayBaseDraw = wednesdayBaseDraw;
    }
    public String getWednesdayDayType() {
        return this.wednesdayDayType;
    }
    public void setWednesdayDayType(String wednesdayDayType) {
        this.wednesdayDayType = wednesdayDayType;
    }
    public int getWednesdayDraw() {
        return this.wednesdayDraw;
    }
    public void setWednesdayDraw(int wednesdayDraw) {
        this.wednesdayDraw = wednesdayDraw;
    }
    public String getWeekEndingDate() {
        return this.weekEndingDate;
    }
    public void setWeekEndingDate(String weekEndingDate) {
        this.weekEndingDate = weekEndingDate;
    }
    public String getProductName() {
        return this.productName;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }
    public int getMinDrawThreshold() {
        return this.minDrawThreshold;
    }
    public void setMinDrawThreshold(int minDrawThreshold) {
        this.minDrawThreshold = minDrawThreshold;
    }
	@Override
	public String getFridayCutOffDate() {
		return this.fridayCutOffDate;
	}
	@Override
	public String getFridayCutOffTime() {
		return this.fridayCutOffTime;
	}
	@Override
	public String getMondayCutOffDate() {
		return this.mondayCutOffDate;
	}
	@Override
	public String getMondayCutOffTime() {
		return this.mondayCutOffTime;
	}
	@Override
	public String getSaturdayCutOffDate() {
		return this.saturdayCutOffDate;
	}
	@Override
	public String getSaturdayCutOffTime() {
		return this.saturdayCutOffTime;
	}
	@Override
	public String getSundayCutOffDate() {
		return this.sundayCutOffDate;
	}
	@Override
	public String getSundayCutOffTime() {
		return this.sundayCutOffTime;
	}
	@Override
	public String getThursdayCutOffDate() {
		return this.thursdayCutOffDate;
	}
	@Override
	public String getThursdayCutOffTime() {
		return this.thursdayCutOffTime;
	}
	@Override
	public String getTuesdayCutOffDate() {
		return this.tuesdayCutOffDate;
	}
	@Override
	public String getTuesdayCutOffTime() {
		return this.tuesdayCutOffTime;
	}
	@Override
	public String getWednesdayCutOffDate() {
		return this.wednesdayCutOffDate;
	}
	@Override
	public String getWednesdayCutOffTime() {
		return this.wednesdayCutOffTime;
	}
	public void setMondayCutOffTime(String mondayCutOffTime) {
		this.mondayCutOffTime = mondayCutOffTime;
	}
	public void setMondayCutOffDate(String mondayCutOffDate) {
		this.mondayCutOffDate = mondayCutOffDate;
	}
	public void setTuesdayCutOffTime(String tuesdayCutOffTime) {
		this.tuesdayCutOffTime = tuesdayCutOffTime;
	}
	public void setTuesdayCutOffDate(String tuesdayCutOffDate) {
		this.tuesdayCutOffDate = tuesdayCutOffDate;
	}
	public void setWednesdayCutOffTime(String wednesdayCutOffTime) {
		this.wednesdayCutOffTime = wednesdayCutOffTime;
	}
	public void setWednesdayCutOffDate(String wednesdayCutOffDate) {
		this.wednesdayCutOffDate = wednesdayCutOffDate;
	}
	public void setThursdayCutOffTime(String thursdayCutOffTime) {
		this.thursdayCutOffTime = thursdayCutOffTime;
	}
	public void setThursdayCutOffDate(String thursdayCutOffDate) {
		this.thursdayCutOffDate = thursdayCutOffDate;
	}
	public void setFridayCutOffTime(String fridayCutOffTime) {
		this.fridayCutOffTime = fridayCutOffTime;
	}
	public void setFridayCutOffDate(String fridayCutOffDate) {
		this.fridayCutOffDate = fridayCutOffDate;
	}
	public void setSaturdayCutOffTime(String saturdayCutOffTime) {
		this.saturdayCutOffTime = saturdayCutOffTime;
	}
	public void setSaturdayCutOffDate(String saturdayCutOffDate) {
		this.saturdayCutOffDate = saturdayCutOffDate;
	}
	public void setSundayCutOffTime(String sundayCutOffTime) {
		this.sundayCutOffTime = sundayCutOffTime;
	}
	public void setSundayCutOffDate(String sundayCutOffDate) {
		this.sundayCutOffDate = sundayCutOffDate;
	}
	
	
}
