package com.usatoday.singlecopy.model.transferObjects.blueChip;

import java.io.Serializable;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.to.BlueChipProductOrderHistoryTOIntf;

public class BlueChipProductOrderHistoryTO implements
		BlueChipProductOrderHistoryTOIntf, Serializable {

	private String emailAddress = null;
    private String sessionID = null;
    private String marketID = null;
    private String locationID = null;
    private String productOrderID  = null;
    private String weekEndingDate = null;

    // monday
    private int mondayDraw = -1;
    private int mondayReturns = -1;
    
    // tuesday
    private int tuesdayDraw = -1;
    private int tuesdayReturns = -1;

    // wednesday
    private int wednesdayDraw = -1;
    private int wednesdayReturns = -1;
    
    // thursday
    private int thursdayDraw = -1;
    private int thursdayReturns = -1;
    
    // friday
    private int fridayDraw = -1;
    private int fridayReturns = -1;
    
    // saturday
    private int saturdayDraw = -1;
    private int saturdayReturns = -1;
    
    // sunday
    private int sundayDraw = -1;
    private int sundayReturns = -1;
    
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1624974027785984886L;

	@Override
	public String getEmailAddress() {
		return this.emailAddress;
	}

	@Override
	public int getFridayDraw() {
		return this.fridayDraw;
	}

	@Override
	public int getFridayReturns() {
		return this.fridayReturns;
	}

	@Override
	public String getLocationID() {
		return this.locationID;
	}

	@Override
	public String getMarketID() {
		return this.marketID;
	}

	@Override
	public int getMondayDraw() {
		return this.mondayDraw;
	}

	@Override
	public int getMondayReturns() {
		return this.mondayReturns;
	}

	@Override
	public String getProductOrderID() {
		return this.productOrderID;
	}

	@Override
	public int getSaturdayDraw() {
		return this.saturdayDraw;
	}

	@Override
	public int getSaturdayReturns() {
		return this.saturdayReturns;
	}

	@Override
	public String getSessionID() {
		return this.sessionID;
	}

	@Override
	public int getSundayDraw() {
		return this.sundayDraw;
	}

	@Override
	public int getSundayReturns() {
		return this.sundayReturns;
	}

	@Override
	public int getThursdayDraw() {
		return this.thursdayDraw;
	}

	@Override
	public int getThursdayReturns() {
		return this.thursdayReturns;
	}

	@Override
	public int getTuesdayDraw() {
		return this.tuesdayDraw;
	}

	@Override
	public int getTuesdayReturns() {
		return this.tuesdayReturns;
	}

	@Override
	public int getWednesdayDraw() {
		return this.wednesdayDraw;
	}

	@Override
	public int getWednesdayReturns() {
		return this.wednesdayReturns;
	}

	@Override
	public String getWeekEndingDate() {
		return this.weekEndingDate;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setMarketID(String marketID) {
		this.marketID = marketID;
	}

	public void setLocationID(String locationID) {
		this.locationID = locationID;
	}

	public void setProductOrderID(String productOrderID) {
		this.productOrderID = productOrderID;
	}

	public void setWeekEndingDate(String weekEndingDate) {
		this.weekEndingDate = weekEndingDate;
	}

	public void setMondayDraw(int mondayDraw) {
		this.mondayDraw = mondayDraw;
	}

	public void setMondayReturns(int mondayReturns) {
		this.mondayReturns = mondayReturns;
	}

	public void setTuesdayDraw(int tuesdayDraw) {
		this.tuesdayDraw = tuesdayDraw;
	}

	public void setTuesdayReturns(int tuesdayReturns) {
		this.tuesdayReturns = tuesdayReturns;
	}

	public void setWednesdayDraw(int wednesdayDraw) {
		this.wednesdayDraw = wednesdayDraw;
	}

	public void setWednesdayReturns(int wednesdayReturns) {
		this.wednesdayReturns = wednesdayReturns;
	}

	public void setThursdayDraw(int thursdayDraw) {
		this.thursdayDraw = thursdayDraw;
	}

	public void setThursdayReturns(int thursdayReturns) {
		this.thursdayReturns = thursdayReturns;
	}

	public void setFridayDraw(int fridayDraw) {
		this.fridayDraw = fridayDraw;
	}

	public void setFridayReturns(int fridayReturns) {
		this.fridayReturns = fridayReturns;
	}

	public void setSaturdayDraw(int saturdayDraw) {
		this.saturdayDraw = saturdayDraw;
	}

	public void setSaturdayReturns(int saturdayReturns) {
		this.saturdayReturns = saturdayReturns;
	}

	public void setSundayDraw(int sundayDraw) {
		this.sundayDraw = sundayDraw;
	}

	public void setSundayReturns(int sundayReturns) {
		this.sundayReturns = sundayReturns;
	}

}
