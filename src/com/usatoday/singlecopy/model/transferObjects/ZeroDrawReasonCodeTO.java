/*
 * Created on Jun 8, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.transferObjects;

import java.io.Serializable;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.ZeroDrawReasonCodeIntf;

/**
 * @author aeast
 * @date Jun 8, 2007
 * @class ZeroDrawReasonCodeTO
 * 
 * 
 * 
 */
public class ZeroDrawReasonCodeTO implements ZeroDrawReasonCodeIntf,
        Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 3548931503964209219L;
	private String code = null;
    private String description = null;

    /**
     * 
     */
    public ZeroDrawReasonCodeTO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ZeroDrawReasonCodeIntf#getCode()
     */
    public String getCode() {
        return this.code;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ZeroDrawReasonCodeIntf#getDescription()
     */
    public String getDescription() {
        return this.description;
    }
    
    /**
     * @param code The code to set.
     */
    public void setCode(String code) {
        this.code = code;
    }
    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
