/*
 * Created on May 4, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.transferObjects;

import java.io.Serializable;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductDescriptionIntf;

/**
 * @author aeast
 * @date May 4, 2007
 * @class ProductDescriptionTO
 * 
 * Transfer Object for Product Descriptions
 * 
 */
public class ProductDescriptionTO implements ProductDescriptionIntf,
        Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5481343985002485145L;
	private String productCode = null;
    private String description = null;
    private String abbreviation = null;
    /**
     * 
     */
    public ProductDescriptionTO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductDescriptionIntf#getProductCode()
     */
    public String getProductCode() {
        return this.productCode;
    }

    /**
     * @return Returns the abbreviation.
     */
    public String getAbbreviation() {
        return this.abbreviation;
    }
    /**
     * @param abbreviation The abbreviation to set.
     */
    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }
    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return this.description;
    }
    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @param productCode The productCode to set.
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
}
