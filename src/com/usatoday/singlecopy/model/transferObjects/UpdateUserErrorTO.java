/*
 * Created on Aug 30, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.transferObjects;

import java.io.Serializable;

import com.usatoday.singlecopy.model.interfaces.users.UpdateUserErrorIntf;

/**
 * @author aeast
 * @date Aug 30, 2007
 * @class UpdateUserErrorTO
 * 
 * 
 * 
 */
public class UpdateUserErrorTO extends GenericUpdateErrorTO implements Serializable, UpdateUserErrorIntf {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5672433910715933660L;
	private String emailAddress = null;
    /**
     * 
     */
    public UpdateUserErrorTO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.users.UpdateUserErrorIntf#getEmailAddress()
     */
    public String getEmailAddress() {
        return emailAddress;
    }
    /**
     * @param emailAddress The emailAddress to set.
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
