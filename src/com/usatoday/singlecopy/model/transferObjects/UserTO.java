/*
 * Created on Apr 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.transferObjects;

import java.io.Serializable;
import java.util.Collection;

import com.usatoday.singlecopy.UsatException;

import com.usatoday.singlecopy.model.interfaces.users.AccessRightsIntf;
import com.usatoday.singlecopy.model.interfaces.users.UserIntf;

/**
 * @author aeast
 * @date Apr 2, 2007
 * @class UserTO
 * 
 * 
 * 
 */
public class UserTO implements UserIntf, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7071490648569567133L;
	private Collection<AccessRightsIntf> accessRights = null;
    private String displayName = null;
    private String emailAddress = null;
    private String phoneNumber = null;
    private String password = null;
    private boolean userAuthenticated = false;
    
    /**
     * 
     */
    public UserTO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.users.UserIntf#getEmailAddress()
     */
    public String getEmailAddress() {
        return this.emailAddress;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.users.UserIntf#setEmailAddress(java.lang.String)
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.users.UserIntf#getPassword()
     */
    public String getPassword() {
        return this.password;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.users.UserIntf#setPassword(java.lang.String)
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.users.UserIntf#setDisplayName(java.lang.String)
     */
    public void setDisplayName(String name) {
        this.displayName = name;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.users.UserIntf#getDisplayName()
     */
    public String getDisplayName() {
        return this.displayName;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.users.UserIntf#isUserAuthenticated()
     */
    public boolean isUserAuthenticated() {
        return this.userAuthenticated;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.users.UserIntf#getAccessRights()
     */
    public Collection<AccessRightsIntf> getAccessRights() {
        return this.accessRights;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.users.UserIntf#hasAccess(java.lang.String)
     */
    public boolean hasAccess(String accessRightCode) {
        // always return false in the Transfer Object
        return false;
    }
    /**
     * @param accessRights The accessRights to set.
     */
    public void setAccessRights(Collection<AccessRightsIntf> accessRights) {
        this.accessRights = accessRights;
    }
    /**
     * @param userAuthenticated The userAuthenticated to set.
     */
    public void setUserAuthenticated(boolean authenticated) {
        this.userAuthenticated = authenticated;
    }
    /**
     * @return Returns the phoneNumber.
     */
    public String getPhoneNumber() {
        return this.phoneNumber;
    }
    /**
     * @param phoneNumber The phoneNumber to set.
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    /* (non-Javadoc)
	 * @see com.usatoday.singlecopy.model.interfaces.users.UserIntf#save()
	 */
	public void save() throws UsatException {
         
		

	}
    public String getSessionID() {
        return null;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.users.UserIntf#getAccessRight(java.lang.String)
     */
    public AccessRightsIntf getAccessRight(String type) {
        return null;
    }
}
