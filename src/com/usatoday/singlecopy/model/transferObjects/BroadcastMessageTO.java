/*
 * Created on Sep 18, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.transferObjects;

import java.io.Serializable;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.BroadcastMessageIntf;

/**
 * @author aeast
 * @date Sep 18, 2007
 * @class BroadcastMessageTO
 * 
 * Transfer Object for a Broadcast Message.
 * 
 */
public class BroadcastMessageTO implements BroadcastMessageIntf, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7626079439441318836L;
	private String marketID = "";
    private String districtID = "";
    private String routeID = "";
    
    private String messageID = "";
    private String message = "";
    
    /**
     * 
     */
    public BroadcastMessageTO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.BroadcastMessageIntf#getMarketID()
     */
    public String getMarketID() {
        return this.marketID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.BroadcastMessageIntf#getDistrictID()
     */
    public String getDistrictID() {
        return this.districtID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.BroadcastMessageIntf#getRouteID()
     */
    public String getRouteID() {
        return this.routeID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.BroadcastMessageIntf#getMessageID()
     */
    public String getMessageID() {
        return this.messageID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.BroadcastMessageIntf#getMessage()
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * @param districtID The districtID to set.
     */
    public void setDistrictID(String districtID) {
        this.districtID = districtID;
    }
    /**
     * @param marketID The marketID to set.
     */
    public void setMarketID(String marketID) {
        this.marketID = marketID;
    }
    /**
     * @param message The message to set.
     */
    public void setMessage(String message) {
        this.message = message;
    }
    /**
     * @param messageID The messageID to set.
     */
    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }
    /**
     * @param routeID The routeID to set.
     */
    public void setRouteID(String routeID) {
        this.routeID = routeID;
    }
}
