/*
 * Created on Jun 5, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.transferObjects;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author aeast
 * @date Jun 5, 2007
 * @class DrawManagementTO
 * 
 * This class was created to handle the inital paramater of late returns week that must accompany
 * the locations.
 * 
 */
public class DrawManagementTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -162410445870786065L;
	
	private boolean lateReturnsWeek = false;
    private Collection<LocationTO> locations = null;
    /**
     * 
     */
    public DrawManagementTO() {
        super();
    }

    /**
     * @return Returns the lateReturnsWeek.
     */
    public boolean isLateReturnsWeek() {
        return this.lateReturnsWeek;
    }
    /**
     * @param lateReturnsWeek The lateReturnsWeek to set.
     */
    public void setLateReturnsWeek(boolean lateReturnsWeek) {
        this.lateReturnsWeek = lateReturnsWeek;
    }
    /**
     * @return Returns the locations.
     */
    public Collection<LocationTO> getLocations() {
        return this.locations;
    }
    /**
     * @param locations The locations to set.
     */
    public void setLocations(Collection<LocationTO> locations) {
        this.locations = locations;
    }
}
