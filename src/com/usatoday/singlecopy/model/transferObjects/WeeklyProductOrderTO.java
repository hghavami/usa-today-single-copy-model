/*
 * Created on Apr 9, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.transferObjects;

import java.io.Serializable;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyReadOnlyProductOrderIntf;

/**
 * @author aeast
 * @date Apr 9, 2007
 * @class WeeklyProductOrderTO
 * 
 * 
 * 
 */
public class WeeklyProductOrderTO extends ProductOrderTO implements WeeklyProductOrderIntf, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -3129648118023425836L;
	// Monday
    private int mondayDraw = -1;
    private int mondayReturns = -1;
    private boolean allowMondayDrawEdits = false;
    private boolean allowMondayReturnsEdits = false;
    private String mondayDayType = null;
    private String mondayZeroDrawReasonCode = "";
    private String mondayChangeCode = "N";

    // Tuesday
    private int tuesdayDraw = -1;
    private int tuesdayReturns = -1;
    private boolean allowTuesdayDrawEdits = false;
    private boolean allowTuesdayReturnsEdits = false;
    private String tuesdayDayType = null;
    private String tuesdayZeroDrawReasonCode = "";
    private String tuesdayChangeCode = "N";

    // Wednesday
    private int wednesdayDraw = -1;
    private int wednesdayReturns = -1;
    private boolean allowWednesdayDrawEdits = false;
    private boolean allowWednesdayReturnsEdits = false;
    private String wednesdayDayType = null;
    private String wednesdayZeroDrawReasonCode = "";
    private String wednesdayChangeCode = "N";

    // Thursday
    private int thursdayDraw = -1;
    private int thursdayReturns = -1;
    private boolean allowThursdayDrawEdits = false;
    private boolean allowThursdayReturnsEdits = false;
    private String thursdayDayType = null;
    private String thursdayZeroDrawReasonCode = "";
    private String thursdayChangeCode = "N";

    // Friday
    private int fridayDraw = -1;
    private int fridayReturns = -1;
    private boolean allowFridayDrawEdits = false;
    private boolean allowFridayReturnsEdits = false;
    private String fridayDayType = null;
    private String fridayZeroDrawReasonCode = "";
    private String fridayChangeCode = "N";

    // Saturday
    private int saturdayDraw = -1;
    private int saturdayReturns = -1;
    private boolean allowSaturdayDrawEdits = false;
    private boolean allowSaturdayReturnsEdits = false;
    private String saturdayDayType = null;
    private String saturdayZeroDrawReasonCode = "";
    private String saturdayChangeCode = "N";

    // Sunday
    private int sundayDraw = -1;
    private int sundayReturns = -1;
    private boolean allowSundayDrawEdits = false;
    private boolean allowSundayReturnsEdits = false;
    private String sundayDayType = null;
    private String sundayZeroDrawReasonCode = "";
    private String sundayChangeCode = "N";

    /**
     * 
     */
    public WeeklyProductOrderTO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getMondayDraw()
     */
    public int getMondayDraw() {
        return this.mondayDraw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getMondayReturns()
     */
    public int getMondayReturns() {
        return this.mondayReturns;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#allowMondayDrawEdits()
     */
    public boolean getAllowMondayDrawEdits() {
        return this.allowMondayDrawEdits;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#allowMondayReturnsEdits()
     */
    public boolean getAllowMondayReturnsEdits() {
        return this.allowMondayReturnsEdits;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getMondayDayType()
     */
    public String getMondayDayType() {
        return this.mondayDayType;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getMondayZeroDrawReasonCode()
     */
    public String getMondayZeroDrawReasonCode() {
        return this.mondayZeroDrawReasonCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setMondayZeroDrawReasonCode(java.lang.String)
     */
    public void setMondayZeroDrawReasonCode(String code) {
        this.mondayZeroDrawReasonCode = code;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setMondayDraw(int)
     */
    public void setMondayDraw(int draw) {
        this.mondayDraw = draw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setMondayReturns(int)
     */
    public void setMondayReturns(int returns) {
        this.mondayReturns = returns;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getTuesdayDraw()
     */
    public int getTuesdayDraw() {
        return this.tuesdayDraw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getTuesdayReturns()
     */
    public int getTuesdayReturns() {
        return this.tuesdayReturns;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#allowTuesdayDrawEdits()
     */
    public boolean getAllowTuesdayDrawEdits() {
        return this.allowTuesdayDrawEdits;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#allowTuesdayReturnsEdits()
     */
    public boolean getAllowTuesdayReturnsEdits() {
        return this.allowTuesdayReturnsEdits;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getTuesdayDayType()
     */
    public String getTuesdayDayType() {
        return this.tuesdayDayType;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getTuesdayZeroDrawReasonCode()
     */
    public String getTuesdayZeroDrawReasonCode() {
        return this.tuesdayZeroDrawReasonCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setTuesdayZeroDrawReasonCode(java.lang.String)
     */
    public void setTuesdayZeroDrawReasonCode(String code) {
        this.tuesdayZeroDrawReasonCode = code;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setTuesdayDraw(int)
     */
    public void setTuesdayDraw(int draw) {
        this.tuesdayDraw = draw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setTuesdayReturns(int)
     */
    public void setTuesdayReturns(int returns) {
        this.tuesdayReturns = returns;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getWednesdayDraw()
     */
    public int getWednesdayDraw() {
        return this.wednesdayDraw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getWednesdayReturns()
     */
    public int getWednesdayReturns() {
        return this.wednesdayReturns;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#allowWednesdayDrawEdits()
     */
    public boolean getAllowWednesdayDrawEdits() {
        return this.allowWednesdayDrawEdits;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#allowWednesdayReturnsEdits()
     */
    public boolean getAllowWednesdayReturnsEdits() {
        return this.allowWednesdayReturnsEdits;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getWednesdayDayType()
     */
    public String getWednesdayDayType() {
        return this.wednesdayDayType;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getWednesdayZeroDrawReasonCode()
     */
    public String getWednesdayZeroDrawReasonCode() {
        return this.wednesdayZeroDrawReasonCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setWednesdayZeroDrawReasonCode(java.lang.String)
     */
    public void setWednesdayZeroDrawReasonCode(String code) {
        this.wednesdayZeroDrawReasonCode = code;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setWednesdayDraw(int)
     */
    public void setWednesdayDraw(int draw) {
        this.wednesdayDraw = draw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setWednesdayReturns(int)
     */
    public void setWednesdayReturns(int returns) {
        this.wednesdayReturns = returns;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getThursdayDraw()
     */
    public int getThursdayDraw() {
        return this.thursdayDraw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getThursdayReturns()
     */
    public int getThursdayReturns() {
        return this.thursdayReturns;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#allowThursdayDrawEdits()
     */
    public boolean getAllowThursdayDrawEdits() {
        return this.allowThursdayDrawEdits;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#allowThursdayReturnsEdits()
     */
    public boolean getAllowThursdayReturnsEdits() {
        return this.allowThursdayReturnsEdits;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getThursdayDayType()
     */
    public String getThursdayDayType() {
        return this.thursdayDayType;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getThursdayZeroDrawReasonCode()
     */
    public String getThursdayZeroDrawReasonCode() {
        return this.thursdayZeroDrawReasonCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setThursdayZeroDrawReasonCode(java.lang.String)
     */
    public void setThursdayZeroDrawReasonCode(String code) {
        this.thursdayZeroDrawReasonCode = code;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setThursdayDraw(int)
     */
    public void setThursdayDraw(int draw) {
        this.thursdayDraw = draw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setThursdayReturns(int)
     */
    public void setThursdayReturns(int returns) {
        this.thursdayReturns = returns;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getFridayDraw()
     */
    public int getFridayDraw() {
        return this.fridayDraw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getFridayReturns()
     */
    public int getFridayReturns() {
        return this.fridayReturns;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#allowFridayDrawEdits()
     */
    public boolean getAllowFridayDrawEdits() {
        return this.allowFridayDrawEdits;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#allowFridayReturnsEdits()
     */
    public boolean getAllowFridayReturnsEdits() {
        return this.allowFridayReturnsEdits;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getFridayDayType()
     */
    public String getFridayDayType() {
        return this.fridayDayType;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getFridayZeroDrawReasonCode()
     */
    public String getFridayZeroDrawReasonCode() {
        return fridayZeroDrawReasonCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setFridayZeroDrawReasonCode(java.lang.String)
     */
    public void setFridayZeroDrawReasonCode(String code) {
        this.fridayZeroDrawReasonCode = code;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setFridayDraw(int)
     */
    public void setFridayDraw(int draw) {
        this.fridayDraw = draw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setFridayReturns(int)
     */
    public void setFridayReturns(int returns) {
        this.fridayReturns = returns;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getSaturdayDraw()
     */
    public int getSaturdayDraw() {
        return this.saturdayDraw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getSaturdayReturns()
     */
    public int getSaturdayReturns() {
        return this.saturdayReturns;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#allowSaturdayDrawEdits()
     */
    public boolean getAllowSaturdayDrawEdits() {
        return this.allowSaturdayDrawEdits;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#allowSaturdayReturnsEdits()
     */
    public boolean getAllowSaturdayReturnsEdits() {
        return this.allowSaturdayReturnsEdits;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getSaturdayDayType()
     */
    public String getSaturdayDayType() {
        return this.saturdayDayType;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getSaturdayZeroDrawReasonCode()
     */
    public String getSaturdayZeroDrawReasonCode() {
        return this.saturdayZeroDrawReasonCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setSaturdayZeroDrawReasonCode(java.lang.String)
     */
    public void setSaturdayZeroDrawReasonCode(String code) {
        this.saturdayZeroDrawReasonCode = code;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setSaturdayDraw(int)
     */
    public void setSaturdayDraw(int draw) {
        this.saturdayDraw = draw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setSaturdayReturns(int)
     */
    public void setSaturdayReturns(int returns) {
        this.saturdayReturns = returns;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getSundayDraw()
     */
    public int getSundayDraw() {
        return this.sundayDraw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getSundayReturns()
     */
    public int getSundayReturns() {
        return this.sundayReturns;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#allowSundayDrawEdits()
     */
    public boolean getAllowSundayDrawEdits() {
        return this.allowSundayDrawEdits;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#allowSundayReturnsEdits()
     */
    public boolean getAllowSundayReturnsEdits() {
        return this.allowSundayReturnsEdits;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getSundayDayType()
     */
    public String getSundayDayType() {
        return this.sundayDayType;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#getSundayZeroDrawReasonCode()
     */
    public String getSundayZeroDrawReasonCode() {
        return sundayZeroDrawReasonCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setSundayZeroDrawReasonCode(java.lang.String)
     */
    public void setSundayZeroDrawReasonCode(String code) {
        this.sundayZeroDrawReasonCode = code;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setSundayDraw(int)
     */
    public void setSundayDraw(int draw) {
        this.sundayDraw = draw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.WeeklyProductOrderIntf#setSundayReturns(int)
     */
    public void setSundayReturns(int returns) {
        this.sundayReturns = returns;
    }

    public void setAllowFridayDrawEdits(boolean allowFridayDrawEdits) {
        this.allowFridayDrawEdits = allowFridayDrawEdits;
    }
    public void setAllowFridayReturnsEdits(boolean allowFridayReturnsEdits) {
        this.allowFridayReturnsEdits = allowFridayReturnsEdits;
    }
    public void setAllowMondayDrawEdits(boolean allowMondayDrawEdits) {
        this.allowMondayDrawEdits = allowMondayDrawEdits;
    }
    public void setAllowMondayReturnsEdits(boolean allowMondayReturnsEdits) {
        this.allowMondayReturnsEdits = allowMondayReturnsEdits;
    }
    public void setAllowSaturdayDrawEdits(boolean allowSaturdayDrawEdits) {
        this.allowSaturdayDrawEdits = allowSaturdayDrawEdits;
    }
    public void setAllowSaturdayReturnsEdits(boolean allowSaturdayReturnsEdits) {
        this.allowSaturdayReturnsEdits = allowSaturdayReturnsEdits;
    }
    public void setAllowSundayDrawEdits(boolean allowSundayDrawEdits) {
        this.allowSundayDrawEdits = allowSundayDrawEdits;
    }
    public void setAllowSundayReturnsEdits(boolean allowSundayReturnsEdits) {
        this.allowSundayReturnsEdits = allowSundayReturnsEdits;
    }
    public void setAllowThursdayDrawEdits(boolean allowThursdayDrawEdits) {
        this.allowThursdayDrawEdits = allowThursdayDrawEdits;
    }
    public void setAllowThursdayReturnsEdits(boolean allowThursdayReturnsEdits) {
        this.allowThursdayReturnsEdits = allowThursdayReturnsEdits;
    }
    public void setAllowTuesdayDrawEdits(boolean allowTuesdayDrawEdits) {
        this.allowTuesdayDrawEdits = allowTuesdayDrawEdits;
    }
    public void setAllowTuesdayReturnsEdits(boolean allowTuesdayReturnsEdits) {
        this.allowTuesdayReturnsEdits = allowTuesdayReturnsEdits;
    }
    public void setAllowWednesdayDrawEdits(boolean allowWednesdayDrawEdits) {
        this.allowWednesdayDrawEdits = allowWednesdayDrawEdits;
    }
    public void setAllowWednesdayReturnsEdits(boolean allowWednesdayReturnsEdits) {
        this.allowWednesdayReturnsEdits = allowWednesdayReturnsEdits;
    }
    public void setFridayDayType(String fridayDayType) {
        this.fridayDayType = fridayDayType;
    }
    public void setMondayDayType(String mondayDayType) {
        this.mondayDayType = mondayDayType;
    }
    public void setSaturdayDayType(String saturdayDayType) {
        this.saturdayDayType = saturdayDayType;
    }
    public void setSundayDayType(String sundayDayType) {
        this.sundayDayType = sundayDayType;
    }
    public void setThursdayDayType(String thursdayDayType) {
        this.thursdayDayType = thursdayDayType;
    }
    public void setTuesdayDayType(String tuesdayDayType) {
        this.tuesdayDayType = tuesdayDayType;
    }
    public void setWednesdayDayType(String wednesdayDayType) {
        this.wednesdayDayType = wednesdayDayType;
    }
    public String getFridayChangeCode() {
        return this.fridayChangeCode;
    }
    public String getMondayChangeCode() {
        return this.mondayChangeCode;
    }
    public String getSaturdayChangeCode() {
        return this.saturdayChangeCode;
    }
    public String getSundayChangeCode() {
        return this.sundayChangeCode;
    }
    public String getThursdayChangeCode() {
        return this.thursdayChangeCode;
    }
    public String getTuesdayChangeCode() {
        return this.tuesdayChangeCode;
    }
    public String getWednesdayChangeCode() {
        return this.wednesdayChangeCode;
    }
    
    public void setFridayChangeCode(String fridayChangeCode) {
        this.fridayChangeCode = fridayChangeCode;
    }
    public void setMondayChangeCode(String mondayChangeCode) {
        this.mondayChangeCode = mondayChangeCode;
    }
    public void setSaturdayChangeCode(String saturdayChangeCode) {
        this.saturdayChangeCode = saturdayChangeCode;
    }
    public void setSundayChangeCode(String sundayChangeCode) {
        this.sundayChangeCode = sundayChangeCode;
    }
    public void setThursdayChangeCode(String thursdayChangeCode) {
        this.thursdayChangeCode = thursdayChangeCode;
    }
    public void setTuesdayChangeCode(String tuesdayChangeCode) {
        this.tuesdayChangeCode = tuesdayChangeCode;
    }
    public void setWednesdayChangeCode(String wednesdayChangeCode) {
        this.wednesdayChangeCode = wednesdayChangeCode;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getTotalDraw()
     */
    public int getTotalDraw() {
        int totalDraw = (this.getMondayDraw() > -1) ? this.getMondayDraw() : 0;
        totalDraw += (this.getTuesdayDraw() > -1) ? this.getTuesdayDraw() : 0;
        totalDraw += (this.getWednesdayDraw() > -1) ? this.getWednesdayDraw() : 0;
        totalDraw += (this.getThursdayDraw() > -1) ? this.getThursdayDraw() : 0;
        totalDraw += (this.getFridayDraw() > -1) ? this.getFridayDraw() : 0;
        totalDraw += (this.getSaturdayDraw() > -1) ? this.getSaturdayDraw() : 0;
        totalDraw += (this.getSundayDraw() > -1) ? this.getSundayDraw() : 0;
        
        return totalDraw;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getTotalReturns()
     */
    public int getTotalReturns() {
        int totalReturns = (this.getMondayReturns() > -1) ? this.getMondayReturns() : 0;
        totalReturns += (this.getTuesdayReturns() > -1) ? this.getTuesdayReturns() : 0;
        totalReturns += (this.getWednesdayReturns() > -1) ? this.getWednesdayReturns() : 0;
        totalReturns += (this.getThursdayReturns() > -1) ? this.getThursdayReturns() : 0;
        totalReturns += (this.getFridayReturns() > -1) ? this.getFridayReturns() : 0;
        totalReturns += (this.getSaturdayReturns() > -1) ? this.getSaturdayReturns() : 0;
        totalReturns += (this.getSundayReturns() > -1) ? this.getSundayReturns() : 0;

        return totalReturns;
    }    
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyProductOrderIntf#getFridayMaxDrawThreshold()
     */
    
    // max draw thresholds
    public int getFridayMaxDrawThreshold() {
        int maxDraw = (this.fridayDraw > this.getMaxDrawThreshold()) ? this.fridayDraw : this.getMaxDrawThreshold();
        return maxDraw;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyProductOrderIntf#getMondayMaxDrawThreshold()
     */
    public int getMondayMaxDrawThreshold() {
        int maxDraw = (this.mondayDraw > this.getMaxDrawThreshold()) ? this.mondayDraw : this.getMaxDrawThreshold();
        return maxDraw;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyProductOrderIntf#getSaturdayMaxDrawThreshold()
     */
    public int getSaturdayMaxDrawThreshold() {
        int maxDraw = (this.saturdayDraw > this.getMaxDrawThreshold()) ? this.saturdayDraw : this.getMaxDrawThreshold();
        return maxDraw;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyProductOrderIntf#getSundayMaxDrawThreshold()
     */
    public int getSundayMaxDrawThreshold() {
        int maxDraw = (this.sundayDraw > this.getMaxDrawThreshold()) ? this.sundayDraw : this.getMaxDrawThreshold();
        return maxDraw;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyProductOrderIntf#getThursdayMaxDrawThreshold()
     */
    public int getThursdayMaxDrawThreshold() {
        int maxDraw = (this.thursdayDraw > this.getMaxDrawThreshold()) ? this.thursdayDraw : this.getMaxDrawThreshold();
        return maxDraw;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyProductOrderIntf#getTuesdayMaxDrawThreshold()
     */
    public int getTuesdayMaxDrawThreshold() {
        int maxDraw = (this.tuesdayDraw > this.getMaxDrawThreshold()) ? this.tuesdayDraw : this.getMaxDrawThreshold();
        return maxDraw;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyProductOrderIntf#getWednesdayMaxDrawThreshold()
     */
    public int getWednesdayMaxDrawThreshold() {
        int maxDraw = (this.wednesdayDraw > this.getMaxDrawThreshold()) ? this.wednesdayDraw : this.getMaxDrawThreshold();
        return maxDraw;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyProductOrderIntf#getOriginalProductOrder()
     */
    public WeeklyReadOnlyProductOrderIntf getOriginalProductOrder() {
        return this;
    }
    public void clear() {
       

    }
}
