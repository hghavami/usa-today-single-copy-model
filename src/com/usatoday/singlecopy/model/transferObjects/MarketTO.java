/*
 * Created on Aug 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.transferObjects;

import java.io.Serializable;

import com.usatoday.singlecopy.model.interfaces.markets.MarketIntf;

/**
 * @author aeast
 * @date Aug 2, 2007
 * @class MarketTO
 * 
 * Market Transfer Object.
 * 
 */
public class MarketTO implements MarketIntf, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8048726925845646620L;
	private String id = null;
    private String description = null;
    private String localPhone = null;
    private String rdlPhone = null;
    private String emailAddress = null;
    /**
     * 
     */
    public MarketTO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.markets.MarketIntf#getID()
     */
    public String getID() {
        return this.id;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.markets.MarketIntf#getDescription()
     */
    public String getDescription() {
        return this.description;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.markets.MarketIntf#getLocalPhone()
     */
    public String getLocalPhone() {
        return this.localPhone;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.markets.MarketIntf#getRDLPhone()
     */
    public String getRDLPhone() {
        return this.rdlPhone;
    }

    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @param id The id to set.
     */
    public void setId(String id) {
        this.id = id;
    }
    /**
     * @param localPhone The localPhone to set.
     */
    public void setLocalPhone(String localPhone) {
        this.localPhone = localPhone;
    }
    /**
     * @param rdlPhone The rdlPhone to set.
     */
    public void setRdlPhone(String rdlPhone) {
        this.rdlPhone = rdlPhone;
    }
    public String getEmailAddress() {
        return this.emailAddress;
    }
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    public boolean getHasEmailAddress() {
        return this.emailAddress == null ? false : true;
    }
}
