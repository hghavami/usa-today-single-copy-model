/*
 * Created on Jun 11, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.transferObjects;

import java.io.Serializable;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyReadOnlyProductOrderIntf;

/**
 * @author aeast
 * @date Jun 11, 2007
 * @class DailyProductOrderTO
 * 
 * This class transfers the data for a RDL Product order
 * 
 */
public class DailyProductOrderTO extends ProductOrderTO implements
        DailyProductOrderIntf, Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 5943562461487402709L;
	private boolean lateReturnsFlag = false;
    private boolean allowDrawEdits = false;
    private boolean allowReturnsEdits = false;
    private String changeCode = "N";
    private String dayType = null;
    private int draw = -1;
    private int returns = -1;
    private int maxReturns = 0;
    private DateTime RDLDate = null;
    private DateTime returnDate = null;
    private int drawDayReturns = 0;
    private String zeroDrawReasonCode = "";    

    /**
     * 
     */
    public DailyProductOrderTO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getRDLDate()
     */
    public DateTime getRDLDate() {
        return this.RDLDate;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getReturnDate()
     */
    public DateTime getReturnDate() {
        return this.returnDate;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getDayType()
     */
    public String getDayType() {
        return this.dayType;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getChangeCode()
     */
    public String getChangeCode() {
        return this.changeCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getDraw()
     */
    public int getDraw() {
        return this.draw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#setDraw(int)
     */
    public void setDraw(int draw) {
        this.draw = draw;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getAllowDrawEdits()
     */
    public boolean getAllowDrawEdits() {
        return this.allowDrawEdits;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getReturns()
     */
    public int getReturns() {
        return this.returns;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#setReturns(int)
     */
    public void setReturns(int returns) {
        this.returns = returns;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getAllowReturnsEdits()
     */
    public boolean getAllowReturnsEdits() {
        return this.allowReturnsEdits;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getZeroDrawReasonCode()
     */
    public String getZeroDrawReasonCode() {
        return this.zeroDrawReasonCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#setZeroDrawReasonCode(java.lang.String)
     */
    public void setZeroDrawReasonCode(String code) {
        this.zeroDrawReasonCode = code;
    }


    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getTotalDraw()
     */
    public int getTotalDraw() {
        if (this.draw < 0){
            return 0;
        }
        else {
            return this.draw;
        }
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getTotalReturns()
     */
    public int getTotalReturns() {
        if (this.returns < 0) {
            return 0;
        }
        else {
            return this.returns;
        }
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getLateReturnsWeek()
     */
    public boolean getLateReturnsFlag() {
        return this.lateReturnsFlag;
    }
    /**
     * @param allowDrawEdits The allowDrawEdits to set.
     */
    public void setAllowDrawEdits(boolean allowDrawEdits) {
        this.allowDrawEdits = allowDrawEdits;
    }
    /**
     * @param allowReturnsEdits The allowReturnsEdits to set.
     */
    public void setAllowReturnsEdits(boolean allowReturnsEdits) {
        this.allowReturnsEdits = allowReturnsEdits;
    }
    /**
     * @param changeCode The changeCode to set.
     */
    public void setChangeCode(String changeCode) {
        this.changeCode = changeCode;
    }
    /**
     * @param dayType The dayType to set.
     */
    public void setDayType(String dayType) {
        this.dayType = dayType;
    }
    /**
     * @param lateReturnsWeek The lateReturnsWeek to set.
     */
    public void setLateReturnsFlag(boolean lateReturnsFlag) {
        this.lateReturnsFlag = lateReturnsFlag;
    }
    /**
     * @param date The rDLDate to set.
     */
    public void setRDLDate(DateTime date) {
        this.RDLDate = date;
    }
    /**
     * @param returnDate The returnDate to set.
     */
    public void setReturnDate(DateTime returnDate) {
        this.returnDate = returnDate;
    }
    /**
     * @return Returns the maxReturns.
     */
    public int getMaxReturns() {
        return this.maxReturns;
    }
    /**
     * @param maxReturns The maxReturns to set.
     */
    public void setMaxReturns(int maxReturns) {
        this.maxReturns = maxReturns;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf#getOriginalProductOrder()
     */
    public DailyReadOnlyProductOrderIntf getOriginalProductOrder() {
        return this;
    }

    public void clear() {
        ;
    }
    
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyReadOnlyProductOrderIntf#getDrawDayReturns()
     */
    public int getDrawDayReturns() {
        return drawDayReturns;
    }
    /**
     * @param drawDayReturns The drawDayReturns to set.
     */
    public void setDrawDayReturns(int drawDayReturns) {
        this.drawDayReturns = drawDayReturns;
    }
}
