/*
 * Created on Apr 4, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.transferObjects;

import java.io.Serializable;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderTypeIntf;

/**
 * @author aeast
 * @date Apr 4, 2007
 * @class ProductOrderTypeTO
 * 
 * 
 * 
 */
public class ProductOrderTypeTO implements ProductOrderTypeIntf, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8661369444612736454L;
	private String productCode = null; 
    private String productOrderTypeCode = null;
    private String description = null;
    
    /**
     * 
     */
    public ProductOrderTypeTO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderTypeIntf#getDescription()
     */
    public String getDescription() {
        return this.description;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderTypeIntf#getProductCode()
     */
    public String getProductCode() {
        return this.productCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderTypeIntf#getProductOrderTypeCode()
     */
    public String getProductOrderTypeCode() {
        return this.productOrderTypeCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderTypeIntf#getKey()
     */
    public String getKey() {
        return this.getProductCode() + this.getProductOrderTypeCode();
    }

    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @param productCode The productCode to set.
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    /**
     * @param productOrderTypeCode The productOrderTypeCode to set.
     */
    public void setProductOrderTypeCode(String productOrderTypeCode) {
        this.productOrderTypeCode = productOrderTypeCode;
    }
}
