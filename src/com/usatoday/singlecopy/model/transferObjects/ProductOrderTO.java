/*
 * Created on Apr 9, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.transferObjects;

import java.io.Serializable;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf;

/**
 * @author aeast
 * @date Apr 9, 2007
 * @class ProductOrderTO
 * 
 * 
 * 
 */
public abstract class ProductOrderTO implements ProductOrderIntf, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6287511395756843498L;
	private String productOrderID = null;
    private String productOrderTypeCode = null;
    private String productCode = null;
    private String productDescription = "";
    private int maxDrawThreshold = -1;
    private int sequenceNumber = -1;
    private boolean changed = false;
    
    
    /**
     * 
     */
    public ProductOrderTO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getProductOrderID()
     */
    public String getProductOrderID() {
        return this.productOrderID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getProductOrderTypeCode()
     */
    public String getProductOrderTypeCode() {
        return this.productOrderTypeCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getProductCode()
     */
    public String getProductCode() {
        return this.productCode;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getSequenceNumber()
     */
    public int getSequenceNumber() {
        return this.sequenceNumber;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getProductDescription()
     */
    public String getProductDescription() {
        return this.productDescription;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getMaxDrawThreshold()
     */
    public int getMaxDrawThreshold() {
        return this.maxDrawThreshold;
    }

    public void setMaxDrawThreshold(int maxDrawThreshold) {
        this.maxDrawThreshold = maxDrawThreshold;
    }
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }
    public void setProductOrderID(String productOrderID) {
        this.productOrderID = productOrderID;
    }
    public void setProductOrderTypeCode(String productOrderTypeCode) {
        this.productOrderTypeCode = productOrderTypeCode;
    }
    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }
    /**
     * @return Returns the changed.
     */
    public boolean isChanged() {
        return this.changed;
    }
    /**
     * @param changed The changed to set.
     */
    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getOwningLocation()
     */
    public LocationIntf getOwningLocation() {
        // not implemented
        return null;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#getUpdateError()
     */
    public DrawUpdateErrorIntf[] getUpdateErrors() {
        // TO's never have update errors
        return null;
    }
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#setSaved()
     */
    public void setSaved(ProductOrderIntf newSource) {
        // TO does nothing here
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#hasEdittableFields()
     */
    public boolean hasEdittableFields() {
        // Just return false..BO calculates this
        return false;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#setPOAsDeleted()
     */
    public void setPOAsDeleted() {
       // do nothing;

    }
    
    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf#clearUpdateErrors()
     */
    public void clearUpdateErrors() {

    }
}
