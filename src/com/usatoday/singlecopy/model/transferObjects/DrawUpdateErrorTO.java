/*
 * Created on Jun 13, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.transferObjects;

import java.io.Serializable;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf;

/**
 * @author aeast
 * @date Jun 13, 2007
 * @class DrawUpdateErrorTO
 * 
 * 
 * 
 */
public class DrawUpdateErrorTO implements DrawUpdateErrorIntf, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6657712944698525561L;
	private String marketID = null;
    private String districtID = null;
    private String routeID = null;
    private String locationID = null;
    private DateTime errorDate = null;
    private String errorMessage = null;
    private String appliesTo = null;
    private String productOrderID = null;
    
    /**
     * 
     */
    public DrawUpdateErrorTO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf#getMarketID()
     */
    public String getMarketID() {
        return this.marketID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf#getDistrictID()
     */
    public String getDistrictID() {
        return this.districtID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf#getRouteID()
     */
    public String getRouteID() {
        return this.routeID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf#getLocationID()
     */
    public String getLocationID() {
        return this.locationID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf#getProductOrderID()
     */
    public String getProductOrderID() {
        return this.productOrderID;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf#getErrorDate()
     */
    public DateTime getErrorDate() {
        return this.errorDate;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf#getAppliesTo()
     */
    public String getAppliesTo() {
        return this.appliesTo;
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DrawUpdateErrorIntf#getErrorMessage()
     */
    public String getErrorMessage() {
        return this.errorMessage;
    }

    /**
     * @param appliesTo The appliesTo to set.
     */
    public void setAppliesTo(String appliesTo) {
        this.appliesTo = appliesTo;
    }
    /**
     * @param districtID The districtID to set.
     */
    public void setDistrictID(String districtID) {
        this.districtID = districtID;
    }
    /**
     * @param errorDate The errorDate to set.
     */
    public void setErrorDate(DateTime errorDate) {
        this.errorDate = errorDate;
    }
    /**
     * @param errorMessage The errorMessage to set.
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    /**
     * @param locationID The locationID to set.
     */
    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }
    /**
     * @param marketID The marketID to set.
     */
    public void setMarketID(String marketID) {
        this.marketID = marketID;
    }
    /**
     * @param productOrderID The productOrderID to set.
     */
    public void setProductOrderID(String productOrderID) {
        this.productOrderID = productOrderID;
    }
    /**
     * @param routeID The routeID to set.
     */
    public void setRouteID(String routeID) {
        this.routeID = routeID;
    }
}
