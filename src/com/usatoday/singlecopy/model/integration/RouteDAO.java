/*
 * Created on Apr 4, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.integration;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.ibm.websphere.ce.cm.DuplicateKeyException;
import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.DailyProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyProductOrderIntf;
import com.usatoday.singlecopy.model.transferObjects.BroadcastMessageTO;
import com.usatoday.singlecopy.model.transferObjects.DailyProductOrderTO;
import com.usatoday.singlecopy.model.transferObjects.DrawUpdateErrorTO;
import com.usatoday.singlecopy.model.transferObjects.LocationTO;
import com.usatoday.singlecopy.model.transferObjects.RouteTO;
import com.usatoday.singlecopy.model.transferObjects.DrawManagementTO;
import com.usatoday.singlecopy.model.transferObjects.WeeklyDrawUpdateErrorTO;
import com.usatoday.singlecopy.model.transferObjects.WeeklyProductOrderTO;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;
import com.usatoday.singlecopy.model.util.ZeroFilledNumeric;

/**
 * @author aeast
 * @date Apr 4, 2007
 * @class RouteDAO
 * 
 * This class interfaces with the AS/400 to retrieve Route related data.
 * 
 */
public class RouteDAO extends USATodayDAO {
	public static final String tempDelim = "p";
	
    // Constants
    public static final int MKTID_MAX_LENGTH = 2;
    public static final int DSTID_MAX_LENGTH = 3;
    public static final int RTEID_MAX_LENGTH = 3;
    public static final int LOCID_MAX_LENGTH = 7;
    public static final int POID_MAX_LENGTH = 3;
    
    // SQL
    // Fields
    //private static final String ROUTE_FIELDS = "MKTID, DSTID, RTEID, RTEDSC, RDLEDT, RDLLDT, MWKEDT, MWKLDT";
    
    // Selects
   
    // inserts
    private static final String WEEKLY_UPDATE_TABLE = "WPUPDW ";
    private static final String WEEKLY_UPDATE_COLS = " (EADRS, MKTID, DSTID, RTEID, LOCID, POID, DWKEDT, DTMONN, MONZDR, DTMONR, MONUPD, DTTUEN, TUEZDR, DTTUER, TUEUPD, DTWEDN, WEDZDR, DTWEDR, WEDUPD, DTTHRN, THRZDR, DTTHRR, THRUPD, DTFRIN, FRIZDR, DTFRIR, FRIUPD, DTSATN, SATZDR, DTSATR, SATUPD, DTSUNN, SUNZDR, DTSUNR, SUNUPD) ";
    private static final String DAILY_UPDATE_TABLE = "WPUPDR ";
    private static final String DAILY_UPDATE_COLS = "(EADRS, MKTID, DSTID, RTEID, LOCID, POID, RDLDTE, DTDAYN, DAYZDR, DTDAYR, RTNDTE, LRTNWK, DAYUPD)";
    
    /**
     * 
     * @param rs - the source records
     * @return - collection of RouteTO transfer objects
     * @throws UsatException
     */
    private HashMap<String, RouteTO> objectFactoryRoute(ResultSet rs) throws UsatException {
        HashMap<String, RouteTO> records = new HashMap<String, RouteTO>();
        
        try {
            // iterate over result set and build objects
            
            while (rs.next()){

                RouteTO routeTO = new RouteTO();
                
                String tempStr = null; 
                int tempInt = rs.getInt("MKTID"); //
                
                routeTO.setMarketID(ZeroFilledNumeric.getValue(RouteDAO.MKTID_MAX_LENGTH, tempInt));

                tempInt = rs.getInt("DSTID"); //  
                routeTO.setDistrictID(ZeroFilledNumeric.getValue(RouteDAO.DSTID_MAX_LENGTH, tempInt));

                tempInt = rs.getInt("RTEID"); //  
                routeTO.setRouteID(ZeroFilledNumeric.getValue(RouteDAO.RTEID_MAX_LENGTH, tempInt));
                
                tempStr = rs.getString("RTEDSC"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    routeTO.setRouteDescription(tempStr.trim());
                }
                else {
                    routeTO.setRouteDescription("");
                }

                DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
                
                tempStr = rs.getString("RDLEDT"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {

                    DateTime dt = formatter.parseDateTime(tempStr);
                    
                    routeTO.setRDLBeginDate(dt);
                }
                else {
                    routeTO.setRDLBeginDate(null);
                }

                tempStr = rs.getString("RDLLDT"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {

                    DateTime dt = formatter.parseDateTime(tempStr);
                    
                    routeTO.setRDLEndDate(dt);
                }
                else {
                    routeTO.setRDLEndDate(null);
                }

                tempStr = rs.getString("MWKEDT"); //  earliest weekly draw date
                if (tempStr != null && tempStr.trim().length() > 0) {

                    DateTime dt = formatter.parseDateTime(tempStr);
                    
                    routeTO.setWeeklyDrawBeginDate(dt);
                }
                else {
                    routeTO.setWeeklyDrawBeginDate(null);
                }

                tempStr = rs.getString("MWKLDT"); //  latest weekly draw date
                if (tempStr != null && tempStr.trim().length() > 0) {

                    DateTime dt = formatter.parseDateTime(tempStr);
                    
                    routeTO.setWeeklyDrawEndDate(dt);
                }
                else {
                    routeTO.setWeeklyDrawEndDate(null);
                }
                
                String key = routeTO.getMarketID() + routeTO.getDistrictID() + routeTO.getRouteID();
                records.put(key, routeTO);
            }
        }
        catch (Exception e) {
            throw new UsatException("RouteDAO::objectFactoryRoute() - Failed to create RouteTO object: " + e.getMessage());
        }
        
        return records;
    }

    /**
     * 
     * @param rs
     * @return
     * @throws UsatException
     */        
    private HashMap<String, LocationTO> objectFactoryWeeklyDrawInstanceLocations(ResultSet rs) throws UsatException {
        HashMap<String, LocationTO> records = new HashMap<String, LocationTO>();

        try {
            // iterate over result set and build objects

            int count = 0;
            while (rs.next()){
                count++;

                LocationTO locationTO = new LocationTO();
                
                String tempStr = null; //
                int tempInt = rs.getInt("MKTID"); //
                
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.MKTID_MAX_LENGTH, tempInt);
                tempStr += tempDelim;

                tempInt = rs.getInt("DSTID"); //  
                tempStr += ZeroFilledNumeric.getValue(RouteDAO.DSTID_MAX_LENGTH, tempInt);
                tempStr += tempDelim;

                tempInt = rs.getInt("RTEID"); //  
                tempStr += ZeroFilledNumeric.getValue(RouteDAO.RTEID_MAX_LENGTH, tempInt);
                tempStr += tempDelim;
                
                tempInt = rs.getInt("LOCID"); //  
                String locIDStr = ZeroFilledNumeric.getValue(RouteDAO.LOCID_MAX_LENGTH, tempInt);
                tempStr += locIDStr;
                locationTO.setLocationID(locIDStr);

                locationTO.setLocationHashKey(tempStr);

                DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
                
                tempStr = rs.getString("DWKEDT"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    DateTime dt = formatter.parseDateTime(tempStr);
                    locationTO.setDate(dt);
                }
                else {
                    locationTO.setDate(null);
                }

                tempInt = rs.getInt("LOCSQ"); //  
                locationTO.setLocationSequenceNumber(tempInt);

                tempStr = rs.getString("LOCNAM"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    locationTO.setLocationName(tempStr.trim());
                }
                else {
                    locationTO.setLocationName("");
                }
                tempStr = rs.getString("ADDR01"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    locationTO.setAddress1(tempStr.trim());
                }
                else {
                    locationTO.setAddress1("");
                }

                tempStr = rs.getString("LOCCPH");
                if (tempStr.length() == 10){
                    locationTO.setPhoneNumber(tempStr);
                }
                else {
                    locationTO.setPhoneNumber("");
                }
                
                records.put(locationTO.getLocationHashKey(), locationTO);
            }
            if (USATApplicationConstants.debug) {
                System.out.println("Processed " + count + "locations in location factory");
            }
        }
        catch (Exception e) {
            throw new UsatException("RouteDAO::objectFactoryDrawInstanceLocations() - Failed to create LocationTO object: " + e.getMessage());
        }
        
        return records;
    }
    
    /**
     * 
     * @param rs
     * @return
     * @throws UsatException
     */        
    private HashMap<String, LocationTO> objectFactoryDailyDrawInstanceLocations(ResultSet rs, DateTime date) throws UsatException {
        HashMap<String, LocationTO> records = new HashMap<String, LocationTO>();
        
        try {
            // iterate over result set and build objects

            int count = 0;
            while (rs.next()){
                count++;

                LocationTO locationTO = new LocationTO();
                
                String tempStr = null; //
                int tempInt = rs.getInt("MKTID"); //
                
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.MKTID_MAX_LENGTH, tempInt);
                tempStr += tempDelim;
                
                tempInt = rs.getInt("DSTID"); //  
                tempStr += ZeroFilledNumeric.getValue(RouteDAO.DSTID_MAX_LENGTH, tempInt);
                tempStr += tempDelim;

                tempInt = rs.getInt("RTEID"); //  
                tempStr += ZeroFilledNumeric.getValue(RouteDAO.RTEID_MAX_LENGTH, tempInt);
                tempStr += tempDelim;
                
                tempInt = rs.getInt("LOCID"); //  
                String locIDStr = ZeroFilledNumeric.getValue(RouteDAO.LOCID_MAX_LENGTH, tempInt);
                tempStr += locIDStr;
                locationTO.setLocationID(locIDStr);

                locationTO.setLocationHashKey(tempStr);
                
                tempInt = rs.getInt("LOCSQ"); //  
                locationTO.setLocationSequenceNumber(tempInt);

                tempStr = rs.getString("LOCNAM"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    locationTO.setLocationName(tempStr.trim());
                }
                else {
                    locationTO.setLocationName("");
                }
                tempStr = rs.getString("ADDR01"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    locationTO.setAddress1(tempStr.trim());
                }
                else {
                    locationTO.setAddress1("");
                }

                tempStr = rs.getString("LOCCPH");
                if (tempStr.length() == 10){
                    locationTO.setPhoneNumber(tempStr);
                }
                else {
                    locationTO.setPhoneNumber("");
                }
                
                locationTO.setDate(date);
                
                records.put(locationTO.getLocationHashKey(), locationTO);
            }
            if (USATApplicationConstants.debug) {
                System.out.println("Processed " + count + "locations in location factory");
            }
        }
        catch (Exception e) {
            throw new UsatException("RouteDAO::objectFactoryDrawInstanceLocations() - Failed to create LocationTO object: " + e.getMessage());
        }
        
        return records;
    }
    
    /**
     * 
     * @param rs
     * @param locations
     * @return
     * @throws UsatException
     */
    private Collection<LocationTO> objectFactoryWeeklyProductOrders(ResultSet rs, HashMap<String, LocationTO> locations) throws UsatException {
        
        try {
            // iterate over result set and build objects

            int count = 0;
            while (rs.next()){
                count++;
                WeeklyProductOrderTO poTO = new WeeklyProductOrderTO();
                
                String tempStr = null; //
                int tempInt = rs.getInt("MKTID"); //
                
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.MKTID_MAX_LENGTH, tempInt);
                tempStr += tempDelim;

                tempInt = rs.getInt("DSTID"); //  
                tempStr += ZeroFilledNumeric.getValue(RouteDAO.DSTID_MAX_LENGTH, tempInt);
                tempStr += tempDelim;

                tempInt = rs.getInt("RTEID"); //  
                tempStr += ZeroFilledNumeric.getValue(RouteDAO.RTEID_MAX_LENGTH, tempInt);
                tempStr += tempDelim;
                
                tempInt = rs.getInt("LOCID"); //  
                String locIDStr = ZeroFilledNumeric.getValue(RouteDAO.LOCID_MAX_LENGTH, tempInt);
                tempStr += locIDStr;

                String parentKey = tempStr;
                
                LocationTO location = locations.get(parentKey);
                
                if (location == null) {
                    // Error Condition
                    System.out.println("RouteDAO::objectFactoryWeeklyProductOrders() - Got a Product order for a location that doesn't exist. Key: " + parentKey);
                    continue;
                }
                
                Collection<ProductOrderIntf> locPos = location.getProductOrders();
                
                tempInt = rs.getInt("POID"); //  
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.POID_MAX_LENGTH, tempInt);
                poTO.setProductOrderID(tempStr);

                tempInt = rs.getInt("POSEQ");
                poTO.setSequenceNumber(tempInt);
                
                tempStr = rs.getString("PRODID");
                if (tempStr != null && tempStr.trim().length() > 0) {
                    poTO.setProductCode(tempStr.trim());
                }
                else {
                    poTO.setProductCode("");
                }

                tempStr = rs.getString("POTYID");
                if (tempStr != null && tempStr.trim().length() > 0) {
                   poTO.setProductOrderTypeCode(tempStr.trim());
                }
                else {
                    poTO.setProductOrderTypeCode("");
                }
                
                tempInt = rs.getInt("PODMAX");
                poTO.setMaxDrawThreshold(tempInt);
                
                tempInt = rs.getInt("DTMONN");
                poTO.setMondayDraw(tempInt);
                
                tempInt = rs.getInt("DTMONR");
                poTO.setMondayReturns(tempInt);
                
                tempStr = rs.getString("MONDOK");
                if (tempStr == null || tempStr.trim().length() == 0 || "N".equalsIgnoreCase(tempStr.trim())) {
                    poTO.setAllowMondayDrawEdits(false);
                }
                else {
                    poTO.setAllowMondayDrawEdits(true);
                }
                
                tempStr = rs.getString("MONROK");
                if (tempStr == null || tempStr.trim().length() == 0 || "N".equalsIgnoreCase(tempStr.trim())) {
                    poTO.setAllowMondayReturnsEdits(false);
                }
                else {
                    poTO.setAllowMondayReturnsEdits(true);
                }

                tempStr = rs.getString("MONDTP");
                if (tempStr != null && tempStr.trim().length() > 0) {
                    poTO.setMondayDayType(tempStr.trim());
                }
                else {
                    poTO.setMondayDayType("");
                }
                
                tempStr = rs.getString("MONZDR"); 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    poTO.setMondayZeroDrawReasonCode(tempStr.trim());
                }
                else {
                    poTO.setMondayZeroDrawReasonCode("");
                }
                
                // tuesday
                tempInt = rs.getInt("DTTUEN");
                poTO.setTuesdayDraw(tempInt);
                
                tempInt = rs.getInt("DTTUER");
                poTO.setTuesdayReturns(tempInt);
                
                tempStr = rs.getString("TUEDOK");
                if (tempStr == null || tempStr.trim().length() == 0 || "N".equalsIgnoreCase(tempStr.trim())) {
                    poTO.setAllowTuesdayDrawEdits(false);
                }
                else {
                    poTO.setAllowTuesdayDrawEdits(true);
                }
                
                tempStr = rs.getString("TUEROK");
                if (tempStr == null || tempStr.trim().length() == 0 || "N".equalsIgnoreCase(tempStr.trim())) {
                    poTO.setAllowTuesdayReturnsEdits(false);
                }
                else {
                    poTO.setAllowTuesdayReturnsEdits(true);
                }

                tempStr = rs.getString("TUEDTP");
                if (tempStr != null && tempStr.trim().length() > 0) {
                    poTO.setTuesdayDayType(tempStr.trim());
                }
                else {
                    poTO.setTuesdayDayType("");
                }

                tempStr = rs.getString("TUEZDR"); 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    poTO.setTuesdayZeroDrawReasonCode(tempStr.trim());
                }
                else {
                    poTO.setTuesdayZeroDrawReasonCode("");
                }

                // wednesday
                tempInt = rs.getInt("DTWEDN");
                poTO.setWednesdayDraw(tempInt);
                
                tempInt = rs.getInt("DTWEDR");
                poTO.setWednesdayReturns(tempInt);
                
                tempStr = rs.getString("WEDDOK");
                if (tempStr == null || tempStr.trim().length() == 0 || "N".equalsIgnoreCase(tempStr.trim())) {
                    poTO.setAllowWednesdayDrawEdits(false);
                }
                else {
                    poTO.setAllowWednesdayDrawEdits(true);
                }
                
                tempStr = rs.getString("WEDROK");
                if (tempStr == null || tempStr.trim().length() == 0 || "N".equalsIgnoreCase(tempStr.trim())) {
                    poTO.setAllowWednesdayReturnsEdits(false);
                }
                else {
                    poTO.setAllowWednesdayReturnsEdits(true);
                }

                tempStr = rs.getString("WEDDTP");
                if (tempStr != null && tempStr.trim().length() > 0) {
                    poTO.setWednesdayDayType(tempStr.trim());
                }
                else {
                    poTO.setWednesdayDayType("");
                }

                tempStr = rs.getString("WEDZDR"); 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    poTO.setWednesdayZeroDrawReasonCode(tempStr.trim());
                }
                else {
                    poTO.setWednesdayZeroDrawReasonCode("");
                }

                // Thursday
                tempInt = rs.getInt("DTTHRN");
                poTO.setThursdayDraw(tempInt);
                
                tempInt = rs.getInt("DTTHRR");
                poTO.setThursdayReturns(tempInt);
                
                tempStr = rs.getString("THRDOK");
                if (tempStr == null || tempStr.trim().length() == 0 || "N".equalsIgnoreCase(tempStr.trim())) {
                    poTO.setAllowThursdayDrawEdits(false);
                }
                else {
                    poTO.setAllowThursdayDrawEdits(true);
                }
                
                tempStr = rs.getString("THRROK");
                if (tempStr == null || tempStr.trim().length() == 0 || "N".equalsIgnoreCase(tempStr.trim())) {
                    poTO.setAllowThursdayReturnsEdits(false);
                }
                else {
                    poTO.setAllowThursdayReturnsEdits(true);
                }

                tempStr = rs.getString("THRDTP");
                if (tempStr != null && tempStr.trim().length() > 0) {
                    poTO.setThursdayDayType(tempStr.trim());
                }
                else {
                    poTO.setThursdayDayType("");
                }

                tempStr = rs.getString("THRZDR"); 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    poTO.setThursdayZeroDrawReasonCode(tempStr.trim());
                }
                else {
                    poTO.setThursdayZeroDrawReasonCode("");
                }
                
                // Friday
                tempInt = rs.getInt("DTFRIN");
                poTO.setFridayDraw(tempInt);
                
                tempInt = rs.getInt("DTFRIR");
                poTO.setFridayReturns(tempInt);
                
                tempStr = rs.getString("FRIDOK");
                if (tempStr == null || tempStr.trim().length() == 0 || "N".equalsIgnoreCase(tempStr.trim())) {
                    poTO.setAllowFridayDrawEdits(false);
                }
                else {
                    poTO.setAllowFridayDrawEdits(true);
                }
                
                tempStr = rs.getString("FRIROK");
                if (tempStr == null || tempStr.trim().length() == 0 || "N".equalsIgnoreCase(tempStr.trim())) {
                    poTO.setAllowFridayReturnsEdits(false);
                }
                else {
                    poTO.setAllowFridayReturnsEdits(true);
                }

                tempStr = rs.getString("FRIDTP");
                if (tempStr != null && tempStr.trim().length() > 0) {
                    poTO.setFridayDayType(tempStr.trim());
                }
                else {
                    poTO.setFridayDayType("");
                }

                tempStr = rs.getString("FRIZDR"); 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    poTO.setFridayZeroDrawReasonCode(tempStr.trim());
                }
                else {
                    poTO.setFridayZeroDrawReasonCode("");
                }

                // Saturday
                tempInt = rs.getInt("DTSATN");
                poTO.setSaturdayDraw(tempInt);
                
                tempInt = rs.getInt("DTSATR");
                poTO.setSaturdayReturns(tempInt);
                
                tempStr = rs.getString("SATDOK");
                if (tempStr == null || tempStr.trim().length() == 0 || "N".equalsIgnoreCase(tempStr.trim())) {
                    poTO.setAllowSaturdayDrawEdits(false);
                }
                else {
                    poTO.setAllowSaturdayDrawEdits(true);
                }
                
                tempStr = rs.getString("SATROK");
                if (tempStr == null || tempStr.trim().length() == 0 || "N".equalsIgnoreCase(tempStr.trim())) {
                    poTO.setAllowSaturdayReturnsEdits(false);
                }
                else {
                    poTO.setAllowSaturdayReturnsEdits(true);
                }

                tempStr = rs.getString("SATDTP");
                if (tempStr != null && tempStr.trim().length() > 0) {
                    poTO.setSaturdayDayType(tempStr.trim());
                }
                else {
                    poTO.setSaturdayDayType("");
                }

                tempStr = rs.getString("SATZDR");
                if (tempStr != null && tempStr.trim().length() > 0) {
                    poTO.setSaturdayZeroDrawReasonCode(tempStr.trim());
                }
                else {
                    poTO.setSaturdayZeroDrawReasonCode("");
                }

                // Sunday
                tempInt = rs.getInt("DTSUNN");
                poTO.setSundayDraw(tempInt);
                
                tempInt = rs.getInt("DTSUNR");
                poTO.setSundayReturns(tempInt);
                
                tempStr = rs.getString("SUNDOK");
                if (tempStr == null || tempStr.trim().length() == 0 || "N".equalsIgnoreCase(tempStr.trim())) {
                    poTO.setAllowSundayDrawEdits(false);
                }
                else {
                    poTO.setAllowSundayDrawEdits(true);
                }
                
                tempStr = rs.getString("SUNROK");
                if (tempStr == null || tempStr.trim().length() == 0 || "N".equalsIgnoreCase(tempStr.trim())) {
                    poTO.setAllowSundayReturnsEdits(false);
                }
                else {
                    poTO.setAllowSundayReturnsEdits(true);
                }

                tempStr = rs.getString("SUNDTP");
                if (tempStr != null && tempStr.trim().length() > 0) {
                    poTO.setSundayDayType(tempStr.trim());
                }
                else {
                    poTO.setSundayDayType("");
                }

                tempStr = rs.getString("SUNZDR"); 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    poTO.setSundayZeroDrawReasonCode(tempStr.trim());
                }
                else {
                    poTO.setSundayZeroDrawReasonCode("");
                }

                locPos.add(poTO);
            }
            if (USATApplicationConstants.debug) {
                System.out.println("Done iterating over product order result set: " + count);
            }
        }
        catch (Exception e) {
            System.out.println("RouteDAO::objectFactoryWeeklyProductOrders() - Failed to create WeeklyProductOrder object: " + e.getMessage());
        }
        
        return new ArrayList<LocationTO>(locations.values());
    }

    /**
     * 
     * @param rs
     * @return
     * @throws UsatException
     */
    private Collection<WeeklyDrawUpdateErrorTO> objectFactoryWeeklyUpdateErrors(ResultSet rs) throws UsatException {
        Collection<WeeklyDrawUpdateErrorTO> records = new ArrayList<WeeklyDrawUpdateErrorTO>();
        
        try {
            // iterate over result set and build objects
            
            while (rs.next()){

                WeeklyDrawUpdateErrorTO to = new WeeklyDrawUpdateErrorTO();
                String tempStr = null;
                
                int tempInt = rs.getInt("MKTID"); //  
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.MKTID_MAX_LENGTH, tempInt);
                to.setMarketID(tempStr.trim());

                tempInt = rs.getInt("DSTID"); //
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.DSTID_MAX_LENGTH, tempInt);
                to.setDistrictID(tempStr.trim());

                tempInt = rs.getInt("RTEID"); //  
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.RTEID_MAX_LENGTH, tempInt);
                to.setRouteID(tempStr.trim());
                
                tempInt = rs.getInt("LOCID"); //  
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.LOCID_MAX_LENGTH, tempInt);
                to.setLocationID(tempStr.trim());
                
                tempInt = rs.getInt("POID"); //  
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.POID_MAX_LENGTH, tempInt);
                to.setProductOrderID(tempStr.trim());
                
                tempStr = rs.getString("ERRFLD"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    to.setAppliesTo(tempStr.trim());
                }
                else {
                    to.setAppliesTo("");
                }
                
                tempStr = rs.getString("ERRMSG"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    to.setErrorMessage(tempStr.trim());
                }
                else {
                    to.setErrorMessage("");
                }
                
                DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
                
                // week ending date
                tempStr = rs.getString("DWKEDT"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    DateTime dt = formatter.parseDateTime(tempStr);
                    to.setWeekEndingDate(dt);
                }
                else {
                    to.setWeekEndingDate(null);
                }
                
                // error date
                tempStr = rs.getString("ERRDTE"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    DateTime dt = formatter.parseDateTime(tempStr);
                    to.setErrorDate(dt);
                }
                else {
                    //to.setErrorDate(null);
                    to.setErrorDate(to.getWeekEndingDate());
                }

                records.add(to);
            }
        }
        catch (Exception e) {
            System.out.println("RouteDAO::objectFactoryWeeklyUpdateErrors() - Failed to create WeeklyDrawUpdateErrorTO object: " + e.getMessage());
        }
        
        return records;
    }
    
    /**
     * 
     * @param rs
     * @return
     * @throws UsatException
     */
    private Collection<DrawUpdateErrorTO> objectFactoryDailyUpdateErrors(ResultSet rs) throws UsatException {
        Collection<DrawUpdateErrorTO> records = new ArrayList<DrawUpdateErrorTO>();
        
        try {
            // iterate over result set and build objects
            
            while (rs.next()){

                DrawUpdateErrorTO to = new DrawUpdateErrorTO();
                String tempStr = null;
                
                int tempInt = rs.getInt("MKTID"); //  
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.MKTID_MAX_LENGTH, tempInt);
                to.setMarketID(tempStr.trim());

                tempInt = rs.getInt("DSTID"); //
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.DSTID_MAX_LENGTH, tempInt);
                to.setDistrictID(tempStr.trim());

                tempInt = rs.getInt("RTEID"); //  
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.RTEID_MAX_LENGTH, tempInt);
                to.setRouteID(tempStr.trim());
                
                tempInt = rs.getInt("LOCID"); //  
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.LOCID_MAX_LENGTH, tempInt);
                to.setLocationID(tempStr.trim());
                
                try {
	                tempInt = rs.getInt("POID"); //  
	                tempStr = ZeroFilledNumeric.getValue(RouteDAO.POID_MAX_LENGTH, tempInt);
	                to.setProductOrderID(tempStr.trim());
                }
                catch (NullPointerException np) {
                	System.out.println("RouteDAO::objectFactoryDailyUpdateErrors() - Null Product order id returned. MKTID: " + to.getMarketID() + " DISTID: " + to.getDistrictID() + " LocID: " + to.getLocationID());
                }
                
                tempStr = rs.getString("ERRFLD"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    to.setAppliesTo(tempStr.trim());
                }
                else {
                    to.setAppliesTo("");
                }
                
                tempStr = rs.getString("ERRMSG"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    to.setErrorMessage(tempStr.trim());
                }
                else {
                    to.setErrorMessage("");
                }
                
                DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
                
                // error date
                tempStr = rs.getString("ERRDTE"); //  
                try {
	                if (tempStr != null && tempStr.trim().length() > 0) {
	                    DateTime dt = formatter.parseDateTime(tempStr);
	                    to.setErrorDate(dt);
	                }
	                else {
	                    to.setErrorDate(null);
	                }
                }
                catch (NullPointerException npe) {
                	System.out.println("RouteDAO::objectFactoryDailyUpdateErrors() - Null Product order id returned. MKTID: " + to.getMarketID() + " DISTID: " + to.getDistrictID() + " LocID: " + to.getLocationID() + "ErrorDate: " + tempStr);
                }

                records.add(to);
            }
        }
        catch (Exception e) {
            System.out.println("RouteDAO::objectFactoryDailyUpdateErrors() - Failed to create DrawUpdateErrorTO object: " + e.getMessage());
        }
        
        return records;
    }

    /**
     * 
     * @param rs
     * @return
     * @throws UsatException
     */
    private Collection<BroadcastMessageTO> objectFactoryRouteMessages(ResultSet rs) throws UsatException {
        Collection<BroadcastMessageTO> records = new ArrayList<BroadcastMessageTO>();
        
        try {
            // iterate over result set and build objects
            
            while (rs.next()){

                BroadcastMessageTO to = new BroadcastMessageTO();
                String tempStr = null;
                
                int tempInt = rs.getInt("MKTID"); //  
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.MKTID_MAX_LENGTH, tempInt);
                to.setMarketID(tempStr.trim());

                tempInt = rs.getInt("DSTID"); //
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.DSTID_MAX_LENGTH, tempInt);
                to.setDistrictID(tempStr.trim());

                tempInt = rs.getInt("RTEID"); //  
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.RTEID_MAX_LENGTH, tempInt);
                to.setRouteID(tempStr.trim());
                                
                tempStr = rs.getString("BMSGID"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    to.setMessageID(tempStr.trim());
                }
                else {
                    to.setMessageID("");
                }
                
                tempStr = rs.getString("BMSGTX"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    to.setMessage(tempStr.trim());
                }
                else {
                    to.setMessage("");
                }
                
                records.add(to);
            }
        }
        catch (Exception e) {
            System.out.println("RouteDAO::objectFactoryDailyUpdateErrors() - Failed to create WeeklyDrawUpdateErrorTO object: " + e.getMessage());
        }
        
        return records;
    }
    
    /**
     * 
     * @param routes
     * @param pubs
     */
    private void attachPublicationsToRoutes(HashMap<String, RouteTO> routes, ResultSet pubs) throws UsatException {
        // convert routes to a HashMap
        
        try {
	        while (pubs.next()){
	            int tempInt = 0;
	            tempInt = pubs.getInt("MKTID");
	            String marketID = ZeroFilledNumeric.getValue(RouteDAO.MKTID_MAX_LENGTH, tempInt);
	            tempInt = pubs.getInt("DSTID");
	            String districtID = ZeroFilledNumeric.getValue(RouteDAO.DSTID_MAX_LENGTH, tempInt);
	            tempInt = pubs.getInt("RTEID");
	            String routeID = ZeroFilledNumeric.getValue(RouteDAO.RTEID_MAX_LENGTH, tempInt);
	            
	            String key = marketID + districtID + routeID;
	            
	            RouteTO r = routes.get(key);
	            if (r == null) {
	                System.out.println("Got pubs for a route that does not exist: " + key);
	                continue;
	            }
	            
	            Collection<String> pubsCol = r.getWeeklyDrawPublications();
	            if (pubsCol == null) {
	                pubsCol = new ArrayList<String>();
	                r.setWeeklyDrawPublications(pubsCol);
	            }
	            
	            pubsCol.add(pubs.getString("PRODID").trim());
	            
	        } // end while more pubs

        }
        catch (SQLException sqle) {
            System.out.println("SQL Exception - ErrorCode:  " + sqle.getErrorCode() + " Message:" + sqle.getMessage());
            throw new UsatException("Problem assigning publications to route. " + sqle.getMessage());
        }
        catch (Exception e) {
            System.out.println(" Exception -  " + e.getMessage());
            throw new UsatException("Problem assigning publications to route. " + e.getMessage());
        }
    }

    
    private Collection<LocationTO> objectFactoryDailyProductOrders(ResultSet rs, HashMap<String, LocationTO> locations, DateTime date) throws UsatException {
        
        try {
            // iterate over result set and build objects

            int count = 0;
            while (rs.next()){
                count++;
                DailyProductOrderTO poTO = new DailyProductOrderTO();
                
                String tempStr = null; //
                int tempInt = rs.getInt("MKTID"); //
                
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.MKTID_MAX_LENGTH, tempInt);
                tempStr += tempDelim;
                
                tempInt = rs.getInt("DSTID"); //  
                tempStr += ZeroFilledNumeric.getValue(RouteDAO.DSTID_MAX_LENGTH, tempInt);
                tempStr += tempDelim;

                tempInt = rs.getInt("RTEID"); //  
                tempStr += ZeroFilledNumeric.getValue(RouteDAO.RTEID_MAX_LENGTH, tempInt);
                tempStr += tempDelim;
                
                tempInt = rs.getInt("LOCID"); //  
                String locIDStr = ZeroFilledNumeric.getValue(RouteDAO.LOCID_MAX_LENGTH, tempInt);
                tempStr += locIDStr;

                String parentKey = tempStr;
                
                LocationTO location = locations.get(parentKey);
                
                if (location == null) {
                    // Error Condition
                    System.out.println("RouteDAO::objectFactoryDailyProductOrders() - Got a Product order for a location that doesn't exist. Key: " + parentKey);
                    continue;
                }
                
                Collection<ProductOrderIntf> locPos = location.getProductOrders();
                
                DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
                
                tempInt = rs.getInt("POID"); //  
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.POID_MAX_LENGTH, tempInt);
                poTO.setProductOrderID(tempStr);

                tempInt = rs.getInt("POSEQ");
                poTO.setSequenceNumber(tempInt);
                
                tempStr = rs.getString("PRODID");
                if (tempStr != null && tempStr.trim().length() > 0) {
                    poTO.setProductCode(tempStr.trim());
                }
                else {
                    poTO.setProductCode("");
                }

                tempStr = rs.getString("DAYZDR"); 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    poTO.setZeroDrawReasonCode(tempStr.trim());
                }
                else {
                    poTO.setZeroDrawReasonCode("");
                }
                
                tempStr = rs.getString("POTYID");
                if (tempStr != null && tempStr.trim().length() > 0) {
                   poTO.setProductOrderTypeCode(tempStr.trim());
                }
                else {
                    poTO.setProductOrderTypeCode("");
                }
                
                tempInt = rs.getInt("PODMAX");
                poTO.setMaxDrawThreshold(tempInt);
                                
                tempInt = rs.getInt("DTDRWR");
                poTO.setDrawDayReturns(tempInt);
                                
                // return date time
                tempStr = rs.getString("RTNDTE"); //   
                if (tempStr != null && tempStr.trim().length() == 8) {
                    DateTime dt = null;
                    try {
                         dt = formatter.parseDateTime(tempStr);
                    }
                    catch (Exception e) {
                        // failed to format date
                        System.out.println("Invalid Return Date Returned for POID: " + poTO.getProductOrderID() + "  Date from DB: " + tempStr);
                    }
                    poTO.setReturnDate(dt);
                }
                else {
                    poTO.setReturnDate(null);
                }

                // late return week 
                tempStr = rs.getString("LRTNWK"); //   
                if (tempStr != null && tempStr.equalsIgnoreCase("Y")) {
                    poTO.setLateReturnsFlag(true);
                }
                else {
                    poTO.setLateReturnsFlag(false);
                }
                                
                // draw value
                tempInt = rs.getInt("DTDAYN");
                poTO.setDraw(tempInt);
                
                // return value
                tempInt = rs.getInt("DTDAYR");
                poTO.setReturns(tempInt);
                
                // max return value (previous day's draw)
                tempInt = rs.getInt("DTRTNN");
                poTO.setMaxReturns(tempInt);
                
                // Draw field open flag
                tempStr = rs.getString("DAYDOK");
                if ("Y".equalsIgnoreCase(tempStr.trim())) {
                    poTO.setAllowDrawEdits(true);
                }

                // Return field open flag
                tempStr = rs.getString("DAYROK");
                if ("Y".equalsIgnoreCase(tempStr.trim())) {
                    poTO.setAllowReturnsEdits(true);
                }
                
                // day type
                tempStr = rs.getString("DAYTYP");
                if (tempStr != null && tempStr.trim().length() > 0) {
                   poTO.setDayType(tempStr.trim());
                }
                else {
                    poTO.setDayType("");
                }
                
                poTO.setRDLDate(date);
                                
                locPos.add(poTO);
            }
            if (USATApplicationConstants.debug) {
                System.out.println("Done iterating over product order result set: " + count);
            }
        }
        catch (Exception e) {
            System.out.println("RouteDAO:objectFactoryDailyProductOrders() - Failed to create DaillyProductOrder object: " + e.getMessage());
        }
        
        return new ArrayList<LocationTO>(locations.values());
    }

    /**
     * 
     * @param emailAddress - the user idnetifier
     * @return - The collection of routes assigned to the user
     * @throws UsatException
     */    
    public HashMap<String, RouteTO> fetchRouteList(String emailAddress) throws UsatException {
        HashMap<String, RouteTO> records = null;
       	java.sql.Connection conn = null;
       	
    	try {
            DateTime startTime = null;
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    	    if (this.connectionManager != null) {
    	        conn = this.connectionManager.getConnection();
    	    }
    	    else {
    	        conn = ConnectionManager.getInstance().getConnection();
    	    }

    		DateTime stopTime = null;
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("RouteDAO:: fetchRouteList() => It took " + timeToRun + " milliseconds to get a DB Connection.");    	        
    	    }

    	    StringBuffer sqlBuf = new StringBuffer("{call ");
    	    if (USATodayDAO.getSTORED_PROC_LIB() != null && USATodayDAO.getSTORED_PROC_LIB().length() > 0) {
    	        sqlBuf.append(USATodayDAO.getSTORED_PROC_LIB());
    	    }
    	    sqlBuf.append("USAT_SC_FETCH_ROUTE_LIST(?, ?)}");
    	    
    	    java.sql.CallableStatement statement = conn.prepareCall(sqlBuf.toString());
            if (emailAddress != null) {
                statement.setString(1, emailAddress);
            }
            else {
                statement.setNull(1, Types.CHAR);
            }
            statement.registerOutParameter(2, Types.CHAR);
            
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
            boolean  firstIsRS = statement.execute();
            
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("RouteDAO:: fetchRouteList() => It took " + timeToRun + " milliseconds to fetch the results.");    	        
    	    }
   		    
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    	    if (firstIsRS) {
       		    String routesAssigned = statement.getString(2);
       		           		    
       		    if (routesAssigned != null && "Y".equalsIgnoreCase(routesAssigned.trim())) {
           		    ResultSet rs = statement.getResultSet();
       		        // convert result set to java transfer objects
       		        records = this.objectFactoryRoute(rs);
       		        
       		        // process publications associated with the routes
   		            statement.getMoreResults();
   		            ResultSet publications = statement.getResultSet();
   		            this.attachPublicationsToRoutes(records, publications);

   		            publications.close();
       		    }
    	    }
            
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("RouteDAO:: fetchRouteList() => It took " + timeToRun + " milliseconds to convert result set to Java objects.");    	        
    	    }
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("RouteDAO : fetchRouteList() " + e.getMessage());
            throw new UsatException(e);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}        
        return records;
    }

    
    /**
     * 
     * @param emailAddress
     * @param marketID
     * @param districtID
     * @param routeID
     * @param productID
     * @param weekEndingDate
     * @param records
     * @return
     * @throws UsatException
     */
    public Collection<WeeklyDrawUpdateErrorTO> updateWeeklyDraw(String emailAddress, String marketID, String districtID, String routeID, String productID, DateTime weekEndingDate, boolean lateReturnsWeek, Collection<ProductOrderIntf> records ) throws UsatException {
        Collection<WeeklyDrawUpdateErrorTO> errorRecords = null;
       	java.sql.Connection conn = null;
	    if (records == null || records.size() == 0) {
	        throw new UsatException("WeeklyDrawUpdate::No data sent for update.");
	    }

	    String escapedEmail = USATodayDAO.escapeApostrophes(emailAddress);
 	    
    	try {
            DateTime startTime = null;
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    	    if (this.connectionManager != null) {
    	        conn = this.connectionManager.getConnection();
    	    }
    	    else {
    	        conn = ConnectionManager.getInstance().getConnection();
    	    }

    		DateTime stopTime = null;
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("RouteDAO:: updateWeeklyDraw() => It took " + timeToRun + " milliseconds to get a DB Connection.");    	        
    	    }

    	    Statement statement = conn.createStatement();
    	    
    	    StringBuffer sqlBuf = new StringBuffer("insert into ");
    	    if (USATodayDAO.getTABLE_LIB() != null && USATodayDAO.getTABLE_LIB().length() > 0) {
    	        sqlBuf.append(USATodayDAO.getTABLE_LIB());
    	    }
    	    sqlBuf.append(RouteDAO.WEEKLY_UPDATE_TABLE);
    	    sqlBuf.append(RouteDAO.WEEKLY_UPDATE_COLS);
    	    sqlBuf.append(" values ");
    	    Iterator<ProductOrderIntf> itr = records.iterator();
    	    String wEndDateStr = weekEndingDate.toString("yyyyMMdd");
    	    while (itr.hasNext()) {
    	        WeeklyProductOrderIntf wpo = (WeeklyProductOrderIntf)itr.next();
    	        
    	        sqlBuf.append("(");

    	        // add all column values
    	        sqlBuf.append("'").append(escapedEmail).append("',");
    	        sqlBuf.append("'").append(marketID).append("',");
    	        sqlBuf.append("'").append(districtID).append("',");
    	        sqlBuf.append("'").append(routeID).append("',");
    	        sqlBuf.append("'").append(wpo.getOwningLocation().getLocationID()).append("',");
    	        sqlBuf.append("'").append(wpo.getProductOrderID()).append("',");
    	        sqlBuf.append("'").append(wEndDateStr).append("',");
    	        
    	        // Monday
    	        sqlBuf.append("'").append(wpo.getMondayDraw()).append("',");
    	        sqlBuf.append("'").append(wpo.getMondayZeroDrawReasonCode()).append("',");
    	        sqlBuf.append("'").append(wpo.getMondayReturns()).append("',");
    	        sqlBuf.append("'").append(wpo.getMondayChangeCode()).append("',");
    	        // Tuesday
    	        sqlBuf.append("'").append(wpo.getTuesdayDraw()).append("',");
    	        sqlBuf.append("'").append(wpo.getTuesdayZeroDrawReasonCode()).append("',");
    	        sqlBuf.append("'").append(wpo.getTuesdayReturns()).append("',");
    	        sqlBuf.append("'").append(wpo.getTuesdayChangeCode()).append("',");
    	        // Wed
    	        sqlBuf.append("'").append(wpo.getWednesdayDraw()).append("',");
    	        sqlBuf.append("'").append(wpo.getWednesdayZeroDrawReasonCode()).append("',");
    	        sqlBuf.append("'").append(wpo.getWednesdayReturns()).append("',");
    	        sqlBuf.append("'").append(wpo.getWednesdayChangeCode()).append("',");
    	        // Thursday
    	        sqlBuf.append("'").append(wpo.getThursdayDraw()).append("',");
    	        sqlBuf.append("'").append(wpo.getThursdayZeroDrawReasonCode()).append("',");
    	        sqlBuf.append("'").append(wpo.getThursdayReturns()).append("',");
    	        sqlBuf.append("'").append(wpo.getThursdayChangeCode()).append("',");
    	        // Friday
    	        sqlBuf.append("'").append(wpo.getFridayDraw()).append("',");
    	        sqlBuf.append("'").append(wpo.getFridayZeroDrawReasonCode()).append("',");
    	        sqlBuf.append("'").append(wpo.getFridayReturns()).append("',");
    	        sqlBuf.append("'").append(wpo.getFridayChangeCode()).append("',");
    	        // Saturday
    	        sqlBuf.append("'").append(wpo.getSaturdayDraw()).append("',");
    	        sqlBuf.append("'").append(wpo.getSaturdayZeroDrawReasonCode()).append("',");
    	        sqlBuf.append("'").append(wpo.getSaturdayReturns()).append("',");
    	        sqlBuf.append("'").append(wpo.getSaturdayChangeCode()).append("',");
    	        // Sunday
    	        sqlBuf.append("'").append(wpo.getSundayDraw()).append("',");
    	        sqlBuf.append("'").append(wpo.getSundayZeroDrawReasonCode()).append("',");
    	        sqlBuf.append("'").append(wpo.getSundayReturns()).append("',");
    	        sqlBuf.append("'").append(wpo.getSundayChangeCode()).append("'");
    	        
    	        sqlBuf.append(")");
    	        if (itr.hasNext() ) {
    	            sqlBuf.append(",");
    	        }
    	    }

    	    // debug
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	        System.out.println("Update SQL: " + sqlBuf.toString());
    	    }
    	    
    	    int rowCount = statement.executeUpdate(sqlBuf.toString());

    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("RouteDAO:: updateWeeklyDraw() => It took " + timeToRun + " milliseconds to insert the records for update.");    	        
    	    }
    	    
    	    if(rowCount != records.size()) {
    	        conn.rollback();
    	        System.out.println("Failed to insert records: " + sqlBuf.toString());
    	        throw new UsatException("UpdateWeeklyDraw::Not all records could be updated.");
    	    }
    	    statement.close();

    	    // call stored proc to retrieve errors
    	    // "{call CHP_OBJ.USAT_SC_FETCH_WEEKLY_UPDATE_RESULTS(?,?)}"
    	    StringBuffer resultBuf = new StringBuffer("{call ");
    	    if (USATodayDAO.getSTORED_PROC_LIB() != null && USATodayDAO.getSTORED_PROC_LIB().length() > 0) {
    	        resultBuf.append(USATodayDAO.getSTORED_PROC_LIB());
    	    }
    	    resultBuf.append("USAT_SC_UPDATE_WEEKLY_DRAW(?,?,?,?,?,?,?)}");

    	    java.sql.CallableStatement statement2 = conn.prepareCall(resultBuf.toString());
    	    statement2.setString(1, emailAddress);
    	    statement2.setString(2, productID);
    	    statement2.setString(3, marketID);
    	    statement2.setString(4, districtID);
    	    statement2.setString(5, routeID);
    	    
    	    if (lateReturnsWeek) {
        	    statement2.setString(6, "Y");
    	        
    	    }
    	    else {
        	    statement2.setString(6, "N");    	        
    	    }
    	    statement2.registerOutParameter(7, Types.CHAR);

    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
            statement2.execute();
            
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("RouteDAO:: updateWeeklyDraw() => It took " + timeToRun + " milliseconds to fetch the update results.");    	        
    	    }
    	    
    	    String errorFlag = statement2.getString(7);
	        if ("Y".equalsIgnoreCase(errorFlag)) {
	            // errors exist
	            ResultSet rs = statement2.getResultSet();
	            
	            try {
	                errorRecords = this.objectFactoryWeeklyUpdateErrors(rs);
	            }
	            catch (Exception e) {
	                throw e;
	            }
	        }
	        else {
	            // want to return an empty list
	            errorRecords = new ArrayList<WeeklyDrawUpdateErrorTO>();
	        }
    	        
    	}
    	catch (Exception e) {
    	    StringBuffer updateErrorMsg = new StringBuffer();
    	    updateErrorMsg.append("RouteDAO : WeeklyDrawUpdate() ").append(e.getMessage());
    	    updateErrorMsg.append("  For ID: ").append(emailAddress);
    	    updateErrorMsg.append("  MRKT: ").append(marketID);
    	    updateErrorMsg.append("  DIST: ").append(districtID);
    	    updateErrorMsg.append("  RTE: ").append(routeID);
    	    
            System.out.println(updateErrorMsg.toString());
            
            if (e instanceof DuplicateKeyException) {
            	DuplicateKeyException dke = (DuplicateKeyException)e;
            	System.out.println("Duplicate Key: Error Code: " + dke.getErrorCode() + "  SQLState: " + dke.getSQLState());
            	if (conn != null) {
            		
            		try {
                		// pause for 2 seconds to give first request chance to finish
            			Thread.sleep(4000);
            			
                	    StringBuffer sqlBuf = new StringBuffer("delete from ");
                	    if (USATodayDAO.getTABLE_LIB() != null && USATodayDAO.getTABLE_LIB().length() > 0) {
                	        sqlBuf.append(USATodayDAO.getTABLE_LIB());
                	    }
                	    sqlBuf.append(RouteDAO.WEEKLY_UPDATE_TABLE);
                	    
                	    sqlBuf.append(" where EADRS ='").append(escapedEmail);
                	    sqlBuf.append("' AND MKTID = '").append(marketID);
                	    sqlBuf.append("' AND DSTID = '").append(districtID);
                	    sqlBuf.append("' AND RTEID = '").append(routeID);
                	    sqlBuf.append("'");
                	    
                	    Statement statement = conn.createStatement();

                	    int rowsDeleted = statement.executeUpdate(sqlBuf.toString());
                	    
                	    System.out.println("Deleted " + rowsDeleted + " rows of data from weekly work file following dup key exception: " + sqlBuf.toString());
            			
            		}
            		catch (Exception cleanupE) {
            			System.out.println("Failed to delete after duplicate key: " + cleanupE.getMessage());
            		}
            	}
            }
            
            
            throw new UsatException(" DETAILS: Save Failed. Please try again. If the problem persists call your USA TODAY regional office for assistance.", e);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}        
        
        return errorRecords;
    }

    /**
     * 
     * @param emailAddress
     * @param marketID
     * @param districtID
     * @param routeID
     * @param productID
     * @param rdlDate
     * @param lateReturnsWeek
     * @param records
     * @return
     * @throws UsatException
     */
    public Collection<DrawUpdateErrorTO> updateDailyDraw(String emailAddress, String marketID, String districtID, String routeID, DateTime rdlDate, Collection<ProductOrderIntf> records ) throws UsatException {
        Collection<DrawUpdateErrorTO> errorRecords = null;
       	java.sql.Connection conn = null;
	    if (records == null || records.size() == 0) {
	        throw new UsatException("DailyDrawUpdate::No data sent for update.");
	    }

	    String escapedEmail = USATodayDAO.escapeApostrophes(emailAddress);
	    
    	try {
            DateTime startTime = null;
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    	    if (this.connectionManager != null) {
    	        conn = this.connectionManager.getConnection();
    	    }
    	    else {
    	        conn = ConnectionManager.getInstance().getConnection();
    	    }

    		DateTime stopTime = null;
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("RouteDAO:: updateDailyDraw() => It took " + timeToRun + " milliseconds to get a DB Connection.");    	        
    	    }

    	    Statement statement = conn.createStatement();
    	    
    	    StringBuffer sqlBuf = new StringBuffer("insert into ");
    	    if (USATodayDAO.getTABLE_LIB() != null && USATodayDAO.getTABLE_LIB().length() > 0) {
    	        sqlBuf.append(USATodayDAO.getTABLE_LIB());
    	    }
    	    sqlBuf.append(RouteDAO.DAILY_UPDATE_TABLE);
    	    sqlBuf.append(RouteDAO.DAILY_UPDATE_COLS);
    	    sqlBuf.append(" values ");
    	    Iterator<ProductOrderIntf> itr = records.iterator();
    	    String rdlDateStr = rdlDate.toString("yyyyMMdd");
    	    
    	    //EADRS, MKTID, DSTID, RTEID, LOCID, POID, RDLDTE, DTDAYN, DAYZDR, DTDAYR, RTNDTE, LRTNWK, DTDUPD
    	    while (itr.hasNext()) {
    	        DailyProductOrderIntf dpo = (DailyProductOrderIntf)itr.next();
    	        
    	        sqlBuf.append("(");

    	        // add all column values
    	        sqlBuf.append("'").append(escapedEmail).append("',");
    	        sqlBuf.append("'").append(marketID).append("',");
    	        sqlBuf.append("'").append(districtID).append("',");
    	        sqlBuf.append("'").append(routeID).append("',");
    	        sqlBuf.append("'").append(dpo.getOwningLocation().getLocationID()).append("',");
    	        sqlBuf.append("'").append(dpo.getProductOrderID()).append("',");
    	        sqlBuf.append("'").append(rdlDateStr).append("',");
    	        
    	        // Draw/Return Change fields
    	        sqlBuf.append("'").append(dpo.getDraw()).append("',");
    	        sqlBuf.append("'").append(dpo.getZeroDrawReasonCode()).append("',");
    	        sqlBuf.append("'").append(dpo.getReturns()).append("',");
    	        String returnDateStr = "0";
    	        if (dpo.getReturnDate() != null) {
    	            returnDateStr = dpo.getReturnDate().toString("yyyyMMdd");
    	        }
    	        sqlBuf.append("'").append(returnDateStr).append("',");
    	        String lateRtnFlag = "N";
    	        if (dpo.getLateReturnsFlag()) {
    	            lateRtnFlag = "Y";
    	        }
    	        sqlBuf.append("'").append(lateRtnFlag).append("',");
    	        sqlBuf.append("'").append(dpo.getChangeCode()).append("'");
    	        
    	        sqlBuf.append(")");
    	        if (itr.hasNext() ) {
    	            sqlBuf.append(",");
    	        }
    	    }

    	    // debug
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	        System.out.println("Update SQL: " + sqlBuf.toString());
    	    }
    	    
    	    int rowCount = statement.executeUpdate(sqlBuf.toString());

    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("RouteDAO:: updateDailyDraw() => It took " + timeToRun + " milliseconds to insert the records for update.");    	        
    	    }
    	    
    	    if(rowCount != records.size()) {
    	        conn.rollback();
    	        System.out.println("Failed to insert records: " + sqlBuf.toString());
    	        throw new UsatException("updateDailyDraw::Not all records could be updated.");
    	    }
    	    statement.close();

    	    // call stored proc to process records and retrieve errors
    	    StringBuffer resultBuf = new StringBuffer("{call ");
    	    if (USATodayDAO.getSTORED_PROC_LIB() != null && USATodayDAO.getSTORED_PROC_LIB().length() > 0) {
    	        resultBuf.append(USATodayDAO.getSTORED_PROC_LIB());
    	    }
    	    resultBuf.append("USAT_SC_UPDATE_DAILY_DRAW(?,?,?,?,?,?)}");

    	    java.sql.CallableStatement statement2 = conn.prepareCall(resultBuf.toString());
    	    statement2.setString(1, emailAddress);
    	    statement2.setString(2, marketID);
    	    statement2.setString(3, districtID);
    	    statement2.setString(4, routeID);
    	    statement2.setString(5, rdlDateStr);
       	    
    	    statement2.registerOutParameter(6, Types.CHAR);

    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
            statement2.execute();
            
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("RouteDAO:: updateDailyDraw() => It took " + timeToRun + " milliseconds to fetch the update results.");    	        
    	    }
    	    
    	    String errorFlag = statement2.getString(6);
	        if ("Y".equalsIgnoreCase(errorFlag)) {
	            // errors exist
	            ResultSet rs = statement2.getResultSet();
	            
	            try {
	                errorRecords = this.objectFactoryDailyUpdateErrors(rs);
	            }
	            catch (Exception e) {
	                // if error occur converting to Java objects we still want to commit changes.
	                throw e;
	            }
	        }
	        else {
	            // want to return an empty list
	            errorRecords = new ArrayList<DrawUpdateErrorTO>();
	        }
    	        
    	}
    	catch (Exception e) {
    		
    	    StringBuffer updateErrorMsg = new StringBuffer();
    	    updateErrorMsg.append("RouteDAO : updateDailyDraw() ").append(e.getMessage());
    	    updateErrorMsg.append("  For ID: ").append(emailAddress);
    	    updateErrorMsg.append("  MRKT: ").append(marketID);
    	    updateErrorMsg.append("  DIST: ").append(districtID);
    	    updateErrorMsg.append("  RTE: ").append(routeID);
    	    
            System.out.println(updateErrorMsg.toString());
            
            if (e instanceof DuplicateKeyException) {
            	DuplicateKeyException dke = (DuplicateKeyException)e;
            	System.out.println("Duplicate Key: Error Code: " + dke.getErrorCode() + "  SQLState: " + dke.getSQLState());
            	if (conn != null) {
            		
            		try {
                		// pause for 2 seconds to give first request chance to finish
            			Thread.sleep(4000);
            			
                	    StringBuffer sqlBuf = new StringBuffer("delete from ");
                	    if (USATodayDAO.getTABLE_LIB() != null && USATodayDAO.getTABLE_LIB().length() > 0) {
                	        sqlBuf.append(USATodayDAO.getTABLE_LIB());
                	    }
                	    sqlBuf.append(RouteDAO.DAILY_UPDATE_TABLE);
                	    
                	    sqlBuf.append(" where EADRS ='").append(escapedEmail);
                	    sqlBuf.append("' AND MKTID = '").append(marketID);
                	    sqlBuf.append("' AND DSTID = '").append(districtID);
                	    sqlBuf.append("' AND RTEID = '").append(routeID);
                	    sqlBuf.append("'");
                	    
                	    Statement statement = conn.createStatement();

                	    int rowsDeleted = statement.executeUpdate(sqlBuf.toString());
                	    
                	    System.out.println("Deleted " + rowsDeleted + " rows of data from daily work file following dup key exception: " + sqlBuf.toString());
            			
            		}
            		catch (Exception cleanupE) {
            			System.out.println("Failed to delete after duplicate key: " + cleanupE.getMessage());
            		}
            	}
            }
            throw new UsatException(" DETAILS: Save Failed. Please try again. If the problem persists call your USA TODAY regional office for assistance.", e);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}        
        
        return errorRecords;
    }
        
    /**
     * 
     * @param emailAddress
     * @param marketID
     * @param districtID
     * @param routeID
     * @param weekEndingDate
     * @param productID
     * @return
     * @throws UsatException
     */
    public DrawManagementTO fetchDrawByWeek(String emailAddress, String marketID, String districtID, String routeID, String weekEndingDate, String productID, boolean returnsOnly) throws UsatException {
        DrawManagementTO to = new DrawManagementTO();
        Collection<LocationTO> records = null;
       	java.sql.Connection conn = null;

       	
    	try {
            DateTime startTime = null;
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    	    if (this.connectionManager != null) {
    	        conn = this.connectionManager.getConnection();
    	    }
    	    else {
    	        conn = ConnectionManager.getInstance().getConnection();
    	    }

    		DateTime stopTime = null;
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("RouteDAO:: fetchDrawByWeek() => It took " + timeToRun + " milliseconds to get a DB Connection.");    	        
    	    }

    	    // "{call CHP_OBJ.USAT_SC_FETCH_DRAW_BY_WEEK(?,?,?,?,?,?,?,?)}"
    	    StringBuffer sqlBuf = new StringBuffer("{call ");
    	    if (USATodayDAO.getSTORED_PROC_LIB() != null && USATodayDAO.getSTORED_PROC_LIB().length() > 0) {
    	        sqlBuf.append(USATodayDAO.getSTORED_PROC_LIB());
    	    }
    	    sqlBuf.append("USAT_SC_FETCH_DRAW_BY_WEEK(?,?,?,?,?,?,?,?)}");
    		
    	    java.sql.CallableStatement statement = conn.prepareCall(sqlBuf.toString());
            if (emailAddress != null) {
                statement.setString(1, emailAddress);
            }
            else {
                statement.setNull(1, Types.CHAR);
            }
            
            if (marketID != null) {
                statement.setString(2, marketID);
            }
            else {
                statement.setNull(2, Types.CHAR);
            }
            
            if (districtID != null) {
                statement.setString(3, districtID);
            }
            else {
                statement.setNull(3, Types.CHAR);
            }
            
            if (routeID != null) {
                statement.setString(4, routeID);
            }
            else {
                statement.setNull(4, Types.CHAR);
            }
            
            if (weekEndingDate != null) {
                statement.setString(5, weekEndingDate);
            }
            else {
                statement.setNull(5, Types.CHAR);
            }
            
            if (productID != null) {
                statement.setString(6, productID);
            }
            else {
                statement.setNull(6, Types.CHAR);
            }
            
            if (returnsOnly) {
                statement.setString(7, "Y");
            }
            else {
                statement.setString(7, "N");
            }
            
            statement.registerOutParameter(8, Types.CHAR);
            
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
            statement.execute();
            
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("RouteDAO:: fetchDrawByWeek() => It took " + timeToRun + " milliseconds to fetch the results.");    	        
    	    }
   		    
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
	        String isLateReturnsStr = statement.getString(8);
	        if ("Y".equalsIgnoreCase(isLateReturnsStr)) {
	            to.setLateReturnsWeek(true);
	        }
	        
	        
	        // get location result set
   		    ResultSet rs = statement.getResultSet();
   		    
   		    if (rs != null) {
   		        // convert result set to java transfer objects
       		    HashMap<String, LocationTO> locationsMap = this.objectFactoryWeeklyDrawInstanceLocations(rs);
   		        
   		        // get product order result set
   		        statement.getMoreResults();
   		        rs = statement.getResultSet();
   		        
   		        records = this.objectFactoryWeeklyProductOrders(rs, locationsMap);
       		    }
            
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("RouteDAO:: fetchDrawByWeek() => It took " + timeToRun + " milliseconds to convert result set to Java objects.");    	        
    	    }
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("RouteDAO : fetchDrawByWeek() " + e.getMessage());
            throw new UsatException(e);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}       
    	to.setLocations(records);
        return to;
    }
    
    /**
     * 
     * @param emailAddress
     * @param marketID
     * @param districtID
     * @param routeID
     * @param rdlDate
     * @return
     * @throws UsatException
     */
    public DrawManagementTO fetchRDL(String emailAddress, String marketID, String districtID, String routeID, DateTime rdlDate) throws UsatException {
        DrawManagementTO to = new DrawManagementTO();
        Collection<LocationTO> records = null;
       	java.sql.Connection conn = null;
       	
    	try {
            DateTime startTime = null;
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    	    if (this.connectionManager != null) {
    	        conn = this.connectionManager.getConnection();
    	    }
    	    else {
    	        conn = ConnectionManager.getInstance().getConnection();
    	    }

    		DateTime stopTime = null;
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("RouteDAO:: fetchRDL() => It took " + timeToRun + " milliseconds to get a DB Connection.");    	        
    	    }

    	    // "{call CHP_OBJ.USAT_SC_FETCH_RDL_FOR_DATE(?,?,?,?,?,?)}"
    	    StringBuffer sqlBuf = new StringBuffer("{call ");
    	    if (USATodayDAO.getSTORED_PROC_LIB() != null && USATodayDAO.getSTORED_PROC_LIB().length() > 0) {
    	        sqlBuf.append(USATodayDAO.getSTORED_PROC_LIB());
    	    }
    	    sqlBuf.append("USAT_SC_FETCH_RDL_FOR_DATE(?,?,?,?,?)}");

    	    java.sql.CallableStatement statement = conn.prepareCall(sqlBuf.toString());
            if (emailAddress != null) {
                statement.setString(1, emailAddress);
            }
            else {
                statement.setNull(1, Types.CHAR);
            }
            
            if (marketID != null) {
                statement.setString(2, marketID);
            }
            else {
                statement.setNull(2, Types.CHAR);
            }
            
            if (districtID != null) {
                statement.setString(3, districtID);
            }
            else {
                statement.setNull(3, Types.CHAR);
            }
            
            if (routeID != null) {
                statement.setString(4, routeID);
            }
            else {
                statement.setNull(4, Types.CHAR);
            }
            
            String requestedDate = rdlDate.toString("yyyyMMdd");
            
            if (rdlDate != null) {
                statement.setString(5, requestedDate);
            }
            
            //statement.registerOutParameter(6, Types.CHAR);
            
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
            statement.execute();
            
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("RouteDAO:: fetchRDL() => It took " + timeToRun + " milliseconds to fetch the results.");    	        
    	    }
   		    
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	        	        
  	        // get location result set
    		ResultSet rs = statement.getResultSet();
       		    
   		    if (rs != null) {
   		        // convert result set to java transfer objects
       		    HashMap<String, LocationTO> locationsMap = this.objectFactoryDailyDrawInstanceLocations(rs, rdlDate);
   		        
   		        // get product order result set
   		        statement.getMoreResults();
   		        rs = statement.getResultSet();
   		        
   		        records = this.objectFactoryDailyProductOrders(rs, locationsMap, rdlDate);
   		    }
        
		    if (USATApplicationConstants.debug) {
		        stopTime = new DateTime();
	            long timeToRun = (stopTime.getMillis() - startTime.getMillis());
	            System.out.println("RouteDAO:: fetchRDL() => It took " + timeToRun + " milliseconds to convert result set to Java objects.");    	        
		    }
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("RouteDAO : fetchRDL() " + e.getMessage());
            throw new UsatException(e);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
    	to.setLocations(records);
        return to;
    }
    
    
    /**
     * 
     * @param emailAddress
     * @param marketID
     * @param districtID
     * @param routeID
     * @return
     * @throws UsatException
     */
    public Collection<BroadcastMessageTO> fetchRouteMessages(String emailAddress, String marketID, String districtID, String routeID) throws UsatException {
        Collection<BroadcastMessageTO> records = null;
       	java.sql.Connection conn = null;
       	
       	if (emailAddress == null ||
       	    marketID == null || districtID == null || routeID == null ) {
            throw new UsatException("RouteDAO::fetchRouteMessages() - A Required Parameter is Missing.");
       	}

    	try {
            DateTime startTime = null;
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    	    if (this.connectionManager != null) {
    	        conn = this.connectionManager.getConnection();
    	    }
    	    else {
    	        conn = ConnectionManager.getInstance().getConnection();
    	    }

    		DateTime stopTime = null;
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("RouteDAO:: fetchRouteMessages() => It took " + timeToRun + " milliseconds to get a DB Connection.");    	        
    	    }

    	    StringBuffer sqlBuf = new StringBuffer("{call ");
    	    if (USATodayDAO.getSTORED_PROC_LIB() != null && USATodayDAO.getSTORED_PROC_LIB().length() > 0) {
    	        sqlBuf.append(USATodayDAO.getSTORED_PROC_LIB());
    	    }
    	    sqlBuf.append("USAT_SC_FETCH_ROUTE_MSG(?,?,?,?,?)}");
    	    
    	    java.sql.CallableStatement statement = conn.prepareCall(sqlBuf.toString());
            statement.setString(1, emailAddress);
            statement.setString(2, marketID);
            statement.setString(3, districtID);
            statement.setString(4, routeID);
            
            statement.registerOutParameter(5, Types.CHAR);
            
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
            boolean resultSetReturned = statement.execute();
            
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("RouteDAO:: fetchRouteMessages() => It took " + timeToRun + " milliseconds to fetch the results.");    	        
    	    }
   		    
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    	    if (resultSetReturned) {
       		    String routeMessagesIndicator = statement.getString(5);
       		           		    
       		    if (routeMessagesIndicator != null && "Y".equalsIgnoreCase(routeMessagesIndicator.trim())) {
           		    ResultSet rs = statement.getResultSet();
       		        // convert result set to java transfer objects
       		        records = this.objectFactoryRouteMessages(rs);
       		    }
    	    }
            
    	    if (records == null) {
   		        // return an empty list
   		        records = new ArrayList<BroadcastMessageTO>();
    	    }
    	    
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("RouteDAO:: fetchRouteMessages() => It took " + timeToRun + " milliseconds to convert result set to Java objects.");    	        
    	    }
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("RouteDAO : fetchRouteMessages() " + e.getMessage());
            throw new UsatException(e);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}        
        return records;
    }
    
}

