package com.usatoday.singlecopy.model.integration;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400JDBCDriver;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;
import com.usatoday.singlecopy.model.util.USATApplicationPropertyFileLoader;


/**
 * ConnectionManager
 * 
 * This class handles getting connections to the database through JNDI.
 * 
 */
public class ConnectionManager {

    /**
     * Setting this to false will disable the connection pool - should only be used for testing the application
     */
    protected static boolean USING_JNDI = true;
    protected static String UID = null;
    protected static String PWD = null;
    protected static String jdbcDriverName = "com.ibm.as400.access.AS400JDBCDriver";
    protected static String jdbcConnectionString = "jdbc:as400://usat-mrwizard/SCPRDDTA";
   // protected static String jdbcConnectionString = "jdbc:as400://usat-mrgannett/SCPRDDTA";
    
    protected static String jndiDataSourceName = "jdbc/champion";

    public static Context ctx = null;
    
    private static ConnectionManager instance = null;

    private DataSource singlecopyPortalDataSource = null;

    public static synchronized ConnectionManager getInstance() {
        if (instance == null) {
            instance = new ConnectionManager();
        }
        return instance;
    }

    private ConnectionManager() {
        Exception originalEx = null;
        try {            
            // read properties
            try {
                String tempStr = "";
                Properties config = new USATApplicationPropertyFileLoader().loadapplicationProperties();
                
                tempStr = config.getProperty("com.usatoday.db.connectionMode.useJNDI");
                if (tempStr != null && "false".equalsIgnoreCase(tempStr)) {
                    ConnectionManager.USING_JNDI = false;
                }
                else {
                    ConnectionManager.USING_JNDI = true;
                }
                
                tempStr = config.getProperty("usat.jdbc.driver.jdbcdriver");
                if (tempStr != null && tempStr.trim().length() > 0) {
                    ConnectionManager.jdbcDriverName = tempStr.trim();
                }
                
                tempStr = config.getProperty("usat.default.jdbc.schema.LIBRARY");
                if (tempStr != null && tempStr.trim().length() > 0) {
                    USATodayDAO.setTABLE_LIB(tempStr.trim());
                }
                
                tempStr = config.getProperty("usat.default.jdbc.storedProc.LIBRARY");
                if (tempStr != null && tempStr.trim().length() > 0) {
                    USATodayDAO.setSTORED_PROC_LIB(tempStr.trim());
                }
                
                tempStr = config.getProperty("usat.naming.datasource.name");
                if (tempStr != null && tempStr.trim().length() > 0) {
                    ConnectionManager.jndiDataSourceName = tempStr.trim();
                }

                tempStr = config.getProperty("usat.default.jdbcdsnname");
                if (tempStr != null && tempStr.trim().length() > 0) {
                    ConnectionManager.jdbcConnectionString = tempStr.trim();
                }
                
                tempStr = config.getProperty("usat.default.jdbc.UID");
                if (tempStr != null && tempStr.trim().length() > 0) {
                    ConnectionManager.UID = tempStr.trim();
                }
                
                tempStr = config.getProperty("usat.default.jdbc.PWD");
                if (tempStr != null && tempStr.trim().length() > 0) {
                    ConnectionManager.PWD = tempStr.trim();
                }
                                
            } catch (Exception e) {
                System.err.println("Error configuring Connection Manager- defaulting to JNDI");
                ConnectionManager.USING_JNDI = true;
            }
            
            if (ConnectionManager.USING_JNDI) {
                try {
                    this.initJNDI();
                }
                catch (Exception e) {
                    System.out.println("FAILED TO CONNECT VIA JNDI! " + e.getMessage());
                    System.out.println("Tried to connect to JNDI source : " + ConnectionManager.jndiDataSourceName);
                    originalEx = e;
                    System.out.println("Setting up JDBC Connection Mode");
                    // switch modes
                    ConnectionManager.USING_JNDI = false;
                    this.initJDBC();
                }
            } else {
    		    this.initJDBC();
            }
        } 
        catch (Exception e) {
            e.printStackTrace();
            
            // throw the original issue if exists
            if (originalEx != null) {
                e = originalEx;
            }
            //this is fatal so thow an Error
            throw new Error(e);
        }
    }

    private void initJNDI() throws Exception {
        if (ConnectionManager.ctx == null) {
            ConnectionManager.ctx = new InitialContext();
            System.out.println("SC ctx: " + ConnectionManager.ctx.getClass().getName());
        }
        singlecopyPortalDataSource = (DataSource) ctx.lookup(ConnectionManager.jndiDataSourceName);
    }
    
    public Connection getConnection() {
        Connection connection = null;
        try {
            if (USING_JNDI) {
                try {
                    connection = singlecopyPortalDataSource.getConnection();
                    if (USATApplicationConstants.debug) {
                        System.out.println("Got a JNDI Connection.");
                    }
                }
                catch (SQLException sqle) {
					try {
						// as a backup try the user id and password in config file
					    if (ConnectionManager.UID != null && ConnectionManager.PWD != null) {
					        connection = singlecopyPortalDataSource.getConnection(ConnectionManager.UID, ConnectionManager.PWD);
		                    if (USATApplicationConstants.debug) {
		                        System.out.println("Got a JNDI Connection using UID: " + ConnectionManager.UID );
		                    }
					    }
					    else {
					        throw sqle;
					    }
					}
					catch (SQLException f2){
						// throw original exception
						throw sqle;
					}
                }
            } else {
			    if (ConnectionManager.UID != null && ConnectionManager.PWD != null) {
			        connection = DriverManager.getConnection(ConnectionManager.jdbcConnectionString, ConnectionManager.UID, ConnectionManager.PWD);
                    if (USATApplicationConstants.debug) {
                        System.out.println("Got a JDBC Connection using UID: " + ConnectionManager.UID );
                    }
			    }
			    else {
			        connection = DriverManager.getConnection(ConnectionManager.jdbcConnectionString);
                    if (USATApplicationConstants.debug) {
                        System.out.println("Got a JDBC Connection using just the connection URL");
                    }
			    }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new Error(e);
        }
        return connection;
    }

    private void initJDBC() throws Exception {
        Class.forName(ConnectionManager.jdbcDriverName);
        @SuppressWarnings("unused")
		AS400JDBCDriver driver = new AS400JDBCDriver();
        AS400 as400 = new AS400();
        as400.setGuiAvailable(false);
        DriverManager.registerDriver(new AS400JDBCDriver());
    }
    
    public static void resetManager() {
        ConnectionManager.instance = null;        
        ConnectionManager.instance = new ConnectionManager();
    }
    public static String getJndiDataSourceName() {
        return jndiDataSourceName;
    }
}

