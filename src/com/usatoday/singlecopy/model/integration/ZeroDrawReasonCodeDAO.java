/*
 * Created on Jun 8, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.integration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.transferObjects.ZeroDrawReasonCodeTO;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;

/**
 * @author aeast
 * @date Jun 8, 2007
 * @class ZeroDrawReasonCodeDAO
 * 
 * This class queries the reference table for the set of zero draw reason codes.
 * 
 */
public class ZeroDrawReasonCodeDAO extends USATodayDAO {

    // SQL
    public static final String FIELDS = "PTCODE, PTDESC";
    public static String TABLE_NAME = "PTZD";

    /*
     * 
     */
    private Collection<ZeroDrawReasonCodeTO> objectFactory(ResultSet rs) throws UsatException {
        Collection<ZeroDrawReasonCodeTO> records = new ArrayList<ZeroDrawReasonCodeTO>();
        
        try {
            // iterate over result set and build objects
            
            while (rs.next()){

                ZeroDrawReasonCodeTO zTO = new ZeroDrawReasonCodeTO();
                
                String tempStr = rs.getString("PTCODE"); // CHAR 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    zTO.setCode(tempStr.trim());
                }
                else {
                    zTO.setCode("");
                }
                
                tempStr = rs.getString("PTDESC");  // NUM (10,0)
                if (tempStr != null && tempStr.trim().length() > 0) {
                    zTO.setDescription(tempStr.trim());
                }
                else {
                    zTO.setDescription("Unknown");
                }
                
                records.add(zTO);
            }
        }
        catch (Exception e) {
            System.out.println("ZeroDrawReasonDAO::objectFactory() - Failed to create ZeroDrawReasonCode object: " + e.getMessage());
        }
        
        return records;
    }

    /**
     * 
     * @return
     * @throws UsatException
     */
    public Collection<ZeroDrawReasonCodeTO> fetchZeroDrawReasonCodes() throws UsatException {
        Collection<ZeroDrawReasonCodeTO> records = null;
       	java.sql.Connection conn = null;
    	try {
            DateTime startTime = null;
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    		conn = ConnectionManager.getInstance().getConnection();

    		String sql = "select " + ZeroDrawReasonCodeDAO.FIELDS + " from " + USATodayDAO.TABLE_LIB + ZeroDrawReasonCodeDAO.TABLE_NAME;
    		DateTime stopTime = null;
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("ZeroDrawReasonCodeDAO:: fetchZeroDrawReasonCodes() => It took " + timeToRun + " milliseconds to get a DB Connection.");    	        
    	        System.out.println("Running SQL: '" + sql + "'");
    	    }

    		
    	    PreparedStatement statement = conn.prepareStatement(sql);
            
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
            ResultSet rs = statement.executeQuery();
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("ZeroDrawReasonCodeDAO:: fetchZeroDrawReasonCodes() => It took " + timeToRun + " milliseconds to fetch the results.");    	        
    	    }
   		    
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
            records = this.objectFactory(rs);
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("ZeroDrawReasonCodeDAO:: fetchZeroDrawReasonCodesfetchZeroDrawReasonCodes() => It took " + timeToRun + " milliseconds to convert result set to Java objects.");    	        
    	    }
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("ZeroDrawReasonCodeDAO : fetchZeroDrawReasonCodes() " + e.getMessage());
            throw new UsatException(e);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        return records;
    }
    
}
