/*
 * Created on Mar 30, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.integration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.transferObjects.ProductOrderTypeTO;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;

/**
 * @author aeast
 * @date Mar 30, 2007
 * @class ProductOrderTypeDAO
 * 
 * This class is responsible for pulling all product order types
 * from the Champion system
 * 
 */
public class ProductOrderTypeDAO extends USATodayDAO {

    // SQL
    public static final String FIELDS = "PRODID, POTYID, POTDSC";
    public static String TABLE_NAME = "PORT";

    /*
     * 
     */
    private Collection<ProductOrderTypeTO> objectFactory(ResultSet rs) throws UsatException {
        Collection<ProductOrderTypeTO> records = new ArrayList<ProductOrderTypeTO>();
        
        try {
            // iterate over result set and build objects
            
            while (rs.next()){

                ProductOrderTypeTO potTO = new ProductOrderTypeTO();
                
                String tempStr = rs.getString("PRODID"); // CHAR 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    potTO.setProductCode(tempStr.trim());
                }
                else {
                    potTO.setProductCode("");
                }
                
                tempStr = rs.getString("POTYID");  // NUM (10,0)
                if (tempStr != null && tempStr.trim().length() > 0) {
                    potTO.setProductOrderTypeCode(tempStr.trim());
                }
                else {
                    potTO.setProductOrderTypeCode("");
                }
                
                tempStr = rs.getString("POTDSC");  // CHAR (50)
                if (tempStr != null && tempStr.trim().length() > 0) {
                    potTO.setDescription(tempStr.trim());
                }
                else {
                    potTO.setDescription("");
                }

                records.add(potTO);
            }
        }
        catch (Exception e) {
            System.out.println("ProductOrderTypeDAO::objectFactory() - Failed to create Product Order Type object: " + e.getMessage());
        }
        
        return records;
    }
    
    /**
     * 
     * @return
     * @throws UsatException
     */
    public Collection<ProductOrderTypeTO> fetchProductOrderTypes() throws UsatException {
        Collection<ProductOrderTypeTO> records = null;
       	java.sql.Connection conn = null;
    	try {
            DateTime startTime = null;
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    		conn = ConnectionManager.getInstance().getConnection();

    		String sql = "select " + ProductOrderTypeDAO.FIELDS + " from " + USATodayDAO.TABLE_LIB + ProductOrderTypeDAO.TABLE_NAME;
    		DateTime stopTime = null;
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("ProductOrderTypeDAO:: fetchProductOrderTypes() => It took " + timeToRun + " milliseconds to get a DB Connection.");    	        
    	        System.out.println("Running SQL: '" + sql + "'");
    	    }

    		
            //PreparedStatement statement = conn.prepareStatement(ProductOrderTypeDAO.SELECT_ALL);
    	    PreparedStatement statement = conn.prepareStatement(sql);
            
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
            ResultSet rs = statement.executeQuery();
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("ProductOrderTypeDAO:: fetchProductOrderTypes() => It took " + timeToRun + " milliseconds to fetch the results.");    	        
    	    }
   		    
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
            records = this.objectFactory(rs);
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("ProductOrderTypeDAO:: fetchProductOrderTypes() => It took " + timeToRun + " milliseconds to convert result set to Java objects.");    	        
    	    }
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("ProductOrderTypeDAO : fetchProductOrderTypes() " + e.getMessage());
            throw new UsatException(e);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        return records;
    }
}
