/*
 * Created on Apr 11, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.integration;

import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.ibm.websphere.ce.cm.DuplicateKeyException;
import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.blueChip.to.BlueChipProductOrderUpdateTOIntf;
import com.usatoday.singlecopy.model.transferObjects.DrawUpdateErrorTO;
import com.usatoday.singlecopy.model.transferObjects.blueChip.BlueChipProductOrderHistoryTO;
import com.usatoday.singlecopy.model.transferObjects.blueChip.BlueChipProductOrderTO;
import com.usatoday.singlecopy.model.transferObjects.blueChip.BlueChipProductOrderUpdateTO;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;
import com.usatoday.singlecopy.model.util.ZeroFilledNumeric;

/**
 * @author aeast
 * @date Apr 11, 2008
 * @class BlueChipDrawDAO
 * 
 * This Data Access Object supports Blue Chip Draw
 * 
 */
public class BlueChipDrawDAO extends USATodayDAO {

    //private static final String BLUE_CHIP_DRAW_TABLE = "WPBCDW ";
    //private static final String BLUE_CHIP_DRAW_COLS = " (EADRS, SESSID, MKTID, LOCID, LOCNAM, ADDR01, LOCCPH, POID, POSEQ, PRODID, POTYID, PODESC, PODMAX, PODMIN, WEKEDT, PRODDT, DTMOND, DTMONN, MONDOK, MONDTP, WPMONX, WPMONT, DTTUED, DTTUEN, TUEDOK, TUEDTP, WPTUEX, WPTUET, DTWEDD, WEDDOK, WEDDTP, WPWEDX, WPWEDT, DTTHRD, DTTHRN, THRDOK, THRDTP, WPTHRX, WPTHRT, DTFRID, DTFRIN, FRIDOK, FRIDTP, WPFRIX, WPFRIT, DTSATD, DTSATN, SATDOK, SATDTP, WPSATX, WPSATT, DTSUND, DTSUNN, SUNDOK, SUNDTP, WPSUNX, WPSUNT) ";
    private static final String BLUE_CHIP_DRAW_UPDATE_TABLE = "WPBCUP ";
    private static final String BLUE_CHIP_DRAW_UPDATE_COLS = " (SESSID, EADRS, LOCID, POID, DWKEDT, DTMONN, MONUPD, DTTUEN, TUEUPD, DTWEDN, WEDUPD, DTTHRN, THRUPD, DTFRIN, FRIUPD, DTSATN, SATUPD, DTSUNN, SUNUPD) ";
    
    /**
     * 
     * @param emailAddress
     * @param sessionID
     * @return
     * @throws UsatException
     */
    public Collection<BlueChipProductOrderTO> fetchBlueChipLocationsForUser(String emailAddress, String sessionID) throws UsatException {
        Collection<BlueChipProductOrderTO> records = null;
       	java.sql.Connection conn = null;
       	
       	if (emailAddress == null ||
       	    sessionID == null ) {
            throw new UsatException("BlueChipDrawDAO::fetchBlueChipLocationsForUser() - A Required Parameter is Missing.");
       	}

    	try {
            DateTime startTime = null;
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    	    if (this.connectionManager != null) {
    	        conn = this.connectionManager.getConnection();
    	    }
    	    else {
    	        conn = ConnectionManager.getInstance().getConnection();
    	    }

    		DateTime stopTime = null;
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("BlueChipDrawDAO::fetchBlueChipLocationsForUser() => It took " + timeToRun + " milliseconds to get a DB Connection.");    	        
    	    }

    	    StringBuffer sqlBuf = new StringBuffer("{call ");
    	    if (USATodayDAO.getSTORED_PROC_LIB() != null && USATodayDAO.getSTORED_PROC_LIB().length() > 0) {
    	        sqlBuf.append(USATodayDAO.getSTORED_PROC_LIB());
    	    }
    	    sqlBuf.append("USAT_SC_FETCH_BLUECHIP_DRAW(?,?,?,?)}");
    	    
    	    java.sql.CallableStatement statement = conn.prepareCall(sqlBuf.toString());
            statement.setString(1, sessionID);
            statement.setString(2, emailAddress);
            
            statement.registerOutParameter(3, Types.CHAR);
            statement.registerOutParameter(4, Types.CHAR);
            
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
            boolean resultSetReturned = statement.execute();
            
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("BlueChipDrawDAO::fetchBlueChipLocationsForUser() => It took " + timeToRun + " milliseconds to fetch the results.");    	        
    	    }
   		    
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
   		    String blueChipLocationsIndicator = statement.getString(3);
   		    
    	    if (resultSetReturned) {
       		           		    
       		    if (blueChipLocationsIndicator != null && "Y".equalsIgnoreCase(blueChipLocationsIndicator.trim())) {
           		    ResultSet rs = statement.getResultSet();
       		        // convert result set to java transfer objects
           		    
       		        records = this.objectFactoryBlueChipDraw(rs);
       		    }
       		    else {
       		        // return an empty list
       		        records = new ArrayList<BlueChipProductOrderTO>();
       		    }
        	    if (USATApplicationConstants.debug) {
                    System.out.println("BlueChipDrawDAO::fetchBlueChipLocationsForUser() => " + records.size() + " Records returned from DB.");    	        
        	    }
    	    }
    	    else {
        	    if (USATApplicationConstants.debug) {
                    System.out.println("BlueChipDrawDAO::fetchBlueChipLocationsForUser() => No Result Sets Returned");    	        
        	    }
    	    }
            
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("BlueChipDrawDAO::fetchBlueChipLocationsForUser() => It took " + timeToRun + " milliseconds to convert result set to Java objects.");    	        
    	    }
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("BlueChipDrawDAO::fetchBlueChipLocationsForUser() " + e.getMessage());
            throw new UsatException(e);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}        
        return records;
    }


    /**
     * 
     * @param emailAddress
     * @param sessionID
     * @param locationID
     * @param poid
     * @param weekEndingDate
     * @param numWeeks
     * @return
     * @throws UsatException
     */
    public Collection<BlueChipProductOrderHistoryTO> fetchBlueChipProductOrderHistory(String emailAddress, String sessionID, String locationID, String poid) throws UsatException {
        Collection<BlueChipProductOrderHistoryTO> records = null;
       	java.sql.Connection conn = null;
       	
       	if (emailAddress == null ||
       	    sessionID == null || poid == null ) {
            throw new UsatException("BlueChipDrawDAO::fetchBlueChipProductOrderHistory() - A Required Parameter is Missing.");
       	}

    	try {
            DateTime startTime = null;
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    	    if (this.connectionManager != null) {
    	        conn = this.connectionManager.getConnection();
    	    }
    	    else {
    	        conn = ConnectionManager.getInstance().getConnection();
    	    }

    		DateTime stopTime = null;
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("BlueChipDrawDAO::fetchBlueChipProductOrderHistory() => It took " + timeToRun + " milliseconds to get a DB Connection.");    	        
    	    }

    	    StringBuffer sqlBuf = new StringBuffer("{call ");
    	    if (USATodayDAO.getSTORED_PROC_LIB() != null && USATodayDAO.getSTORED_PROC_LIB().length() > 0) {
    	        sqlBuf.append(USATodayDAO.getSTORED_PROC_LIB());
    	    }
    	    sqlBuf.append("USAT_SC_FETCH_BLUECHIP_DRAW_HIST(?,?,?,?,?)}");
    	        	    
    	    java.sql.CallableStatement statement = conn.prepareCall(sqlBuf.toString());
            statement.setString(1, sessionID);
            statement.setString(2, emailAddress);
            statement.setString(3, locationID);
            statement.setString(4, poid);
            
            statement.registerOutParameter(5, Types.CHAR);
            
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
            boolean resultSetReturned = statement.execute();
            
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("BlueChipDrawDAO::fetchBlueChipProductOrderHistory() => It took " + timeToRun + " milliseconds to fetch the results.");    	        
    	    }
   		    
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
   		    String numWeeksReturned = statement.getString(5);
   		    
    	    if (resultSetReturned) {
       		        
    	    	int numWeeksRet = Integer.parseInt(numWeeksReturned);
    	    	
       		    if (numWeeksRet > 0) {
           		    ResultSet rs = statement.getResultSet();
       		        // convert result set to java transfer objects
           		    
       		        records = this.objectFactoryBlueChipDrawHistory(rs);
       		    }
       		    else {
       		        // return an empty list
       		        records = new ArrayList<BlueChipProductOrderHistoryTO>();
       		    }
        	    if (USATApplicationConstants.debug) {
                    System.out.println("BlueChipDrawDAO::fetchBlueChipProductOrderHistory() => " + records.size() + " Records returned from DB.");    	        
        	    }
    	    }
    	    else {
        	    if (USATApplicationConstants.debug) {
                    System.out.println("BlueChipDrawDAO::fetchBlueChipProductOrderHistory() => No Result Sets Returned");    	        
        	    }
    	    }
            
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("BlueChipDrawDAO::fetchBlueChipProductOrderHistory() => It took " + timeToRun + " milliseconds to convert result set to Java objects.");    	        
    	    }
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("BlueChipDrawDAO::fetchBlueChipProductOrderHistory() " + e.getMessage());
            throw new UsatException(e);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}        
        return records;
    }
    
    /**
     * 
     * @param emailAddress
     * @param sessionID
     * @param records - The collection of BlueChipProductOrderUpdateTOIntf objects.
     * @return - Collection of update errors or an empty collection
     * @throws UsatException
     */
    public Collection<DrawUpdateErrorTO> updateBlueChipDraw(String sessionID, String emailAddress, Collection<BlueChipProductOrderUpdateTO> records ) throws UsatException {
        Collection<DrawUpdateErrorTO> errorRecords = null;
       	java.sql.Connection conn = null;
	    if (records == null || records.size() == 0) {
	        throw new UsatException("updateBlueChipDraw::No data sent for update.");
	    }
	    
	    String escapedEmailAddress = USATodayDAO.escapeApostrophes(emailAddress);
    	try {
            DateTime startTime = null;
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    	    if (this.connectionManager != null) {
    	        conn = this.connectionManager.getConnection();
    	    }
    	    else {
    	        conn = ConnectionManager.getInstance().getConnection();
    	    }

    		DateTime stopTime = null;
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("BlueChipDrawDAO:: updateBlueChipDraw() => It took " + timeToRun + " milliseconds to get a DB Connection.");    	        
    	    }

    	    Statement statement = conn.createStatement();
    	    
    	    StringBuffer sqlBuf = new StringBuffer("insert into ");
    	    if (USATodayDAO.getTABLE_LIB() != null && USATodayDAO.getTABLE_LIB().length() > 0) {
    	        sqlBuf.append(USATodayDAO.getTABLE_LIB());
    	    }
    	    sqlBuf.append(BlueChipDrawDAO.BLUE_CHIP_DRAW_UPDATE_TABLE);
    	    sqlBuf.append(BlueChipDrawDAO.BLUE_CHIP_DRAW_UPDATE_COLS);
    	    sqlBuf.append(" values ");
    	    Iterator<BlueChipProductOrderUpdateTO> itr = records.iterator();
    	    
    	    //
    	    while (itr.hasNext()) {
    	        BlueChipProductOrderUpdateTOIntf po = itr.next();
    	        
    	        sqlBuf.append("(");

    	        //  SESSID, EADRS, LOCID, POID, DWKEDT, DTMONN, MONUPD, 
    	        // DTTUEN, TUEUPD, DTWEDN, WEDUPD, DTTHRN, THRUPD, DTFRIN, 
    	        // FRIUPD, DTSATN, SATUPD, DTSUNN, SUNUPD

    	        // add all column values
    	        sqlBuf.append("'").append(sessionID).append("',");
    	        sqlBuf.append("'").append(escapedEmailAddress).append("',");
    	        
    	        // Draw fields
    	        sqlBuf.append("'").append(po.getLocationID()).append("',");
    	        sqlBuf.append("'").append(po.getProductOrderID()).append("',");
    	        sqlBuf.append("'").append(po.getWeekEndingDate()).append("',");
    	        // monday
    	        sqlBuf.append("").append(po.getMondayDraw()).append(",");
    	        if (po.isUpdateMonday()) {
        	        sqlBuf.append("'").append("Y").append("',");
    	        }
    	        else {
        	        sqlBuf.append("'").append("N").append("',");
    	        }
    	        // tuesday
    	        sqlBuf.append("").append(po.getTuesdayDraw()).append(",");
    	        if (po.isUpdateTuesday()) {
        	        sqlBuf.append("'").append("Y").append("',");
    	        }
    	        else {
        	        sqlBuf.append("'").append("N").append("',");
    	        }
    	        // wednesday
    	        sqlBuf.append("").append(po.getWednesdayDraw()).append(",");
    	        if (po.isUpdateWednesday()) {
        	        sqlBuf.append("'").append("Y").append("',");
    	        }
    	        else {
        	        sqlBuf.append("'").append("N").append("',");
    	        }
    	        // thursday
    	        sqlBuf.append("").append(po.getThursdayDraw()).append(",");
    	        if (po.isUpdateThursday()) {
        	        sqlBuf.append("'").append("Y").append("',");
    	        }
    	        else {
        	        sqlBuf.append("'").append("N").append("',");
    	        }
    	        // friday
    	        sqlBuf.append("").append(po.getFridayDraw()).append(",");
    	        if (po.isUpdateFriday()) {
        	        sqlBuf.append("'").append("Y").append("',");
    	        }
    	        else {
        	        sqlBuf.append("'").append("N").append("',");
    	        }
    	        // saturday
    	        sqlBuf.append("").append(po.getSaturdayDraw()).append(",");
    	        if (po.isUpdateSaturday()) {
        	        sqlBuf.append("'").append("Y").append("',");
    	        }
    	        else {
        	        sqlBuf.append("'").append("N").append("',");
    	        }
    	        // sunday
    	        sqlBuf.append("").append(po.getSundayDraw()).append(",");
    	        if (po.isUpdateSunday()) {
        	        sqlBuf.append("'").append("Y").append("'");
    	        }
    	        else {
        	        sqlBuf.append("'").append("N").append("'");
    	        }
    	        
    	        sqlBuf.append(")");
    	        if (itr.hasNext() ) {
    	            sqlBuf.append(",");
    	        }
    	    }

    	    // debug
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	        System.out.println("Update SQL: " + sqlBuf.toString());
    	    }
    	    
    	    int rowCount = statement.executeUpdate(sqlBuf.toString());

    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("BlueChipDrawDAO:: updateBlueChipDraw() => It took " + timeToRun + " milliseconds to insert the records for update.");    	        
    	    }
    	    
    	    if(rowCount != records.size()) {
    	        conn.rollback();
    	        System.out.println("Failed to insert records: " + sqlBuf.toString());
    	        throw new UsatException("updateBlueChipDraw::Not all records could be updated so all were rolled back");
    	    }
    	    statement.close();

    	    // call stored proc to process records and retrieve errors
    	    StringBuffer resultBuf = new StringBuffer("{call ");
    	    if (USATodayDAO.getSTORED_PROC_LIB() != null && USATodayDAO.getSTORED_PROC_LIB().length() > 0) {
    	        resultBuf.append(USATodayDAO.getSTORED_PROC_LIB());
    	    }
    	    resultBuf.append("USAT_SC_UPDATE_BLUECHIP_DRAW(?,?,?)}");

    	    java.sql.CallableStatement statement2 = conn.prepareCall(resultBuf.toString());
    	    statement2.setString(1, sessionID);
    	    statement2.setString(2, emailAddress);
       	    statement2.setString(3, "z");
       	    
    	    statement2.registerOutParameter(3, Types.CHAR);

    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
            statement2.execute();
            
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("BlueChipDrawDAO:: updateBlueChipDraw() => It took " + timeToRun + " milliseconds to fetch the update results.");    	        
    	    }
    	    
    	    String errorFlag = statement2.getString(3);
	        if ("Y".equalsIgnoreCase(errorFlag)) {
	            // errors exist
	            ResultSet rs = statement2.getResultSet();
	            
	            try {
	                errorRecords = this.objectFactoryUpdateErrors(rs);
	            }
	            catch (Exception e) {
	                // if error occur converting to Java objects we still want to commit changes.
	                throw e;
	            }
	        }
	        else {
	            // want to return an empty list
	            errorRecords = new ArrayList<DrawUpdateErrorTO>();
	        }
    	        
    	}
    	catch (Exception e) {
    	    StringBuffer updateErrorMsg = new StringBuffer();
    	    updateErrorMsg.append("BlueChipDrawDAO : updateBlueChipDraw() ").append(e.getMessage());
    	    updateErrorMsg.append("  For ID: ").append(emailAddress);
    	    updateErrorMsg.append("  Temp SessID: ").append(sessionID);
    	    
            System.out.println(updateErrorMsg.toString());
            
            if (e instanceof DuplicateKeyException) {
            	DuplicateKeyException dke = (DuplicateKeyException)e;
            	System.out.println("Duplicate Key: Error Code: " + dke.getErrorCode() + "  SQLState: " + dke.getSQLState());
            	if (conn != null) {
            		
            		try {
                		// pause for 2 seconds to give first request chance to finish
            			Thread.sleep(3000);
            			
                	    StringBuffer sqlBuf = new StringBuffer("delete from ");
                	    if (USATodayDAO.getTABLE_LIB() != null && USATodayDAO.getTABLE_LIB().length() > 0) {
                	        sqlBuf.append(USATodayDAO.getTABLE_LIB());
                	    }
                	    sqlBuf.append(BlueChipDrawDAO.BLUE_CHIP_DRAW_UPDATE_TABLE);
                	    
                	    sqlBuf.append(" where SESSID ='").append(sessionID);
                	    sqlBuf.append("'");
                	    
                	    Statement statement = conn.createStatement();

                	    int rowsDeleted = statement.executeUpdate(sqlBuf.toString());
                	    
                	    System.out.println("Deleted " + rowsDeleted + " rows of data from Blue Chip draw work file following dup key exception: " + sqlBuf.toString());
            			
            		}
            		catch (Exception cleanupE) {
            			System.out.println("Failed to delete after duplicate key: " + cleanupE.getMessage());
            		}
            	}
            }
            throw new UsatException(" DETAILS: Save Failed. Please try again. If the problem persists call your USA TODAY regional office for assistance.", e);
            
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}        
        
        return errorRecords;
    }

    /**
     * 
     * @param rs
     * @return
     * @throws UsatException
     */
    private Collection<BlueChipProductOrderTO> objectFactoryBlueChipDraw(ResultSet rs) throws UsatException {
        Collection<BlueChipProductOrderTO> records = new ArrayList<BlueChipProductOrderTO>();
        
        try {
            // iterate over result set and build objects

            while (rs.next()){

                BlueChipProductOrderTO to = new BlueChipProductOrderTO();
                String tempStr = null;

                
                tempStr = rs.getString("EADRS"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    to.setEmailAddress(tempStr.trim());
                }

                int tempInt = rs.getInt("MKTID"); //
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.MKTID_MAX_LENGTH, tempInt);
                to.setMarketID(tempStr);
                
                
                tempInt = rs.getInt("LOCID"); //  
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.LOCID_MAX_LENGTH, tempInt);
                to.setLocationID(tempStr.trim());
                
                tempStr = rs.getString("LOCNAM");
                if (tempStr != null && tempStr.trim().length() > 0) {
                    to.setLocationName(tempStr.trim());
                }
                else {
                    to.setLocationName("");
                }
                
                tempStr = rs.getString("ADDR01"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    to.setLocationAddress1(tempStr.trim());
                }
                else {
                    to.setLocationAddress1("");
                }

                tempStr = rs.getString("LOCCPH");
                if (tempStr.length() == 10){
                    to.setLocationPhone(tempStr);
                }
                else {
                    to.setLocationPhone("");
                }
                
                tempInt = rs.getInt("POID"); //  
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.POID_MAX_LENGTH, tempInt);
                to.setProductOrderID(tempStr.trim());
                
                tempInt = rs.getInt("POSEQ");
                to.setProductOrderSequenceNumber(tempInt);
                
                tempStr = rs.getString("PRODID");
                if (tempStr != null && tempStr.trim().length() > 0) {
                    to.setProductID(tempStr.trim());
                }
                else {
                    to.setProductID("");
                }

                tempStr = rs.getString("POTYID");
                if (tempStr != null && tempStr.trim().length() > 0) {
                   to.setProductOrderType(tempStr.trim());
                }
                else {
                    to.setProductOrderType("");
                }

                tempStr = rs.getString("PODESC");
                if (tempStr != null && tempStr.trim().length() > 0) {
                    to.setProductName(tempStr.trim());
                }
                else {
                    to.setProductName(" ");
                }

                tempInt = rs.getInt("PODMAX");
                to.setMaxDrawThreshold(tempInt);
                
                tempInt = rs.getInt("PODMIN");
                to.setMinDrawThreshold(tempInt);
                
                // week ending date
                tempStr = rs.getString("DWKEDT"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    to.setWeekEndingDate(tempStr.trim());
                }

                tempStr = rs.getString("PRODDT"); //
                if (tempStr != null && tempStr.trim().length() > 0) {
                    to.setNextDistributionDate(tempStr.trim());
                }
                
                // monday
                tempInt = rs.getInt("DTMOND");
                to.setMondayBaseDraw(tempInt);

                tempInt = rs.getInt("DTMONN");
                to.setMondayDraw(tempInt);

                tempStr = rs.getString("MONDOK");
                if ("Y".equalsIgnoreCase(tempStr.trim())) {
                    to.setMondayAllowDrawEdits(true);
                }

                tempStr = rs.getString("MONDTP");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setMondayDayType(tempStr.trim());
                }

                tempStr = rs.getString("WPMONX");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setMondayCutOffDate(tempStr.trim());
                }
                
                tempStr = rs.getString("WPMONT");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setMondayCutOffTime(tempStr.trim());
                }
                
                // Tuesday
                tempInt = rs.getInt("DTTUED");
                to.setTuesdayBaseDraw(tempInt);

                tempInt = rs.getInt("DTTUEN");
                to.setTuesdayDraw(tempInt);

                tempStr = rs.getString("TUEDOK");
                if ("Y".equalsIgnoreCase(tempStr.trim())) {
                    to.setTuesdayAllowDrawEdits(true);
                }

                tempStr = rs.getString("TUEDTP");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setTuesdayDayType(tempStr.trim());
                }

                tempStr = rs.getString("WPTUEX");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setTuesdayCutOffDate(tempStr.trim());
                }
                
                tempStr = rs.getString("WPTUET");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setTuesdayCutOffTime(tempStr.trim());
                }

                // Wednesday
                tempInt = rs.getInt("DTWEDD");
                to.setWednesdayBaseDraw(tempInt);

                tempInt = rs.getInt("DTWEDN");
                to.setWednesdayDraw(tempInt);

                tempStr = rs.getString("WEDDOK");
                if ("Y".equalsIgnoreCase(tempStr.trim())) {
                    to.setWednesdayAllowDrawEdits(true);
                }

                tempStr = rs.getString("WEDDTP");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setWednesdayDayType(tempStr.trim());
                }

                tempStr = rs.getString("WPWEDX");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setWednesdayCutOffDate(tempStr.trim());
                }
                
                tempStr = rs.getString("WPWEDT");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setWednesdayCutOffTime(tempStr.trim());
                }
                
                // Thursday
                tempInt = rs.getInt("DTTHRD");
                to.setThursdayBaseDraw(tempInt);

                tempInt = rs.getInt("DTTHRN");
                to.setThursdayDraw(tempInt);

                tempStr = rs.getString("THRDOK");
                if ("Y".equalsIgnoreCase(tempStr.trim())) {
                    to.setThursdayAllowDrawEdits(true);
                }

                tempStr = rs.getString("THRDTP");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setThursdayDayType(tempStr.trim());
                }

                tempStr = rs.getString("WPTHRX");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setThursdayCutOffDate(tempStr.trim());
                }
                
                tempStr = rs.getString("WPTHRT");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setThursdayCutOffTime(tempStr.trim());
                }
                
                // Friday
                tempInt = rs.getInt("DTFRID");
                to.setFridayBaseDraw(tempInt);

                tempInt = rs.getInt("DTFRIN");
                to.setFridayDraw(tempInt);

                tempStr = rs.getString("FRIDOK");
                if ("Y".equalsIgnoreCase(tempStr.trim())) {
                    to.setFridayAllowDrawEdits(true);
                }

                tempStr = rs.getString("FRIDTP");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setFridayDayType(tempStr.trim());
                }

                tempStr = rs.getString("WPFRIX");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setFridayCutOffDate(tempStr.trim());
                }
                
                tempStr = rs.getString("WPFRIT");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setFridayCutOffTime(tempStr.trim());
                }
                
                // Saturday
                tempInt = rs.getInt("DTSATD");
                to.setSaturdayBaseDraw(tempInt);

                tempInt = rs.getInt("DTSATN");
                to.setSaturdayDraw(tempInt);

                tempStr = rs.getString("SATDOK");
                if ("Y".equalsIgnoreCase(tempStr.trim())) {
                    to.setSaturdayAllowDrawEdits(true);
                }

                tempStr = rs.getString("SATDTP");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setSaturdayDayType(tempStr.trim());
                }

                tempStr = rs.getString("WPSATX");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setSaturdayCutOffDate(tempStr.trim());
                }
                
                tempStr = rs.getString("WPSATT");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setSaturdayCutOffTime(tempStr.trim());
                }

                // Sunday
                tempInt = rs.getInt("DTSUND");
                to.setSundayBaseDraw(tempInt);

                tempInt = rs.getInt("DTSUNN");
                to.setSundayDraw(tempInt);

                tempStr = rs.getString("SUNDOK");
                if ("Y".equalsIgnoreCase(tempStr.trim())) {
                    to.setSundayAllowDrawEdits(true);
                }

                tempStr = rs.getString("SUNDTP");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setSundayDayType(tempStr.trim());
                }

                tempStr = rs.getString("WPSUNX");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setSundayCutOffDate(tempStr.trim());
                }
                
                tempStr = rs.getString("WPSUNT");
                if (tempStr != null && (tempStr.trim().length() > 0)) {
                    to.setSundayCutOffTime(tempStr.trim());
                }
                
                records.add(to);
            }
        }
        catch (Exception e) {
            System.out.println("RouteDAO::objectFactoryBlueChipDraw() - Failed to create BlueChipProductOrderTO object: " + e.getMessage());
        }
        
        return records;
    }
    
    /**
     * 
     * @param rs
     * @return
     * @throws UsatException
     */
    private Collection<DrawUpdateErrorTO> objectFactoryUpdateErrors(ResultSet rs) throws UsatException {
        Collection<DrawUpdateErrorTO> records = new ArrayList<DrawUpdateErrorTO>();
        
        try {
            // iterate over result set and build objects
            
            while (rs.next()){

                DrawUpdateErrorTO to = new DrawUpdateErrorTO();
                String tempStr = null;

                int tempInt = 0;
                tempInt = rs.getInt("LOCID"); //  
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.LOCID_MAX_LENGTH, tempInt);
                to.setLocationID(tempStr.trim());
                
                tempInt = rs.getInt("POID"); //  
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.POID_MAX_LENGTH, tempInt);
                to.setProductOrderID(tempStr.trim());
                
                tempStr = rs.getString("ERRMSG"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    to.setErrorMessage(tempStr.trim());
                }
                else {
                    to.setErrorMessage("");
                }
                
                DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");

                // week ending date - not using now
                //tempStr = rs.getString("DWKEDT"); //  
                //if (tempStr != null && tempStr.trim().length() > 0) {
                //    DateTime dt = formatter.parseDateTime(tempStr);
                //    to.set(dt);
                //}
                //else {
                    //to.
                //}

                // error date
                tempStr = rs.getString("ERRDTE"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    DateTime dt = formatter.parseDateTime(tempStr);
                    to.setErrorDate(dt);
                }
                else {
                    to.setErrorDate(null);
                }

                records.add(to);
            }
        }
        catch (Exception e) {
            System.out.println("BlueChipDrawManagmentDAO::objectFactoryDrawUpdateErrors() - Failed to create BlueChipDrawUpdateErrorTO object: " + e.getMessage());
        }
        
        return records;
    }

    private Collection<BlueChipProductOrderHistoryTO> objectFactoryBlueChipDrawHistory(ResultSet rs) throws UsatException {
        Collection<BlueChipProductOrderHistoryTO> records = new ArrayList<BlueChipProductOrderHistoryTO>();
        
        try {
            // iterate over result set and build objects

            while (rs.next()){

            	BlueChipProductOrderHistoryTO to = new BlueChipProductOrderHistoryTO();
                String tempStr = null;

                
                tempStr = rs.getString("EADRS"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    to.setEmailAddress(tempStr.trim());
                }

                tempStr = rs.getString("SESSID"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    to.setSessionID(tempStr.trim());
                }
                
                int tempInt = rs.getInt("MKTID"); //
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.MKTID_MAX_LENGTH, tempInt);
                to.setMarketID(tempStr);
                
                
                tempInt = rs.getInt("LOCID"); //  
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.LOCID_MAX_LENGTH, tempInt);
                to.setLocationID(tempStr.trim());
                
                tempInt = rs.getInt("POID"); //  
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.POID_MAX_LENGTH, tempInt);
                to.setProductOrderID(tempStr.trim());
                
                // week ending date
                tempStr = rs.getString("DWKEDT"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    to.setWeekEndingDate(tempStr.trim());
                }

                // MONDAY
                // draw
                tempInt = rs.getInt("DTMONN");
                to.setMondayDraw(tempInt);
                // returns
                tempInt = rs.getInt("DTMONR");
                to.setMondayReturns(tempInt);

                // TUESDAY
                // draw
                tempInt = rs.getInt("DTTUEN");
                to.setTuesdayDraw(tempInt);

                // returns
                tempInt = rs.getInt("DTTUER");
                to.setTuesdayReturns(tempInt);
                
                // WEDNESDAY
                // draw
                tempInt = rs.getInt("DTWEDN");
                to.setWednesdayDraw(tempInt);

                // returns
                tempInt = rs.getInt("DTWEDR");
                to.setWednesdayReturns(tempInt);

                // Thursday
                // draw
                tempInt = rs.getInt("DTTHRN");
                to.setThursdayDraw(tempInt);

                // returns
                tempInt = rs.getInt("DTTHRR");
                to.setThursdayReturns(tempInt);
                
                // FRIDAY
                // draw
                tempInt = rs.getInt("DTFRIN");
                to.setFridayDraw(tempInt);

                // returns
                tempInt = rs.getInt("DTFRIR");
                to.setFridayReturns(tempInt);
                // SATURDAY
                // draw
                tempInt = rs.getInt("DTSATN");
                to.setSaturdayDraw(tempInt);

                // returns
                tempInt = rs.getInt("DTSATR");
                to.setSaturdayReturns(tempInt);
                
                // SUNDAY
                // draw
                tempInt = rs.getInt("DTSUNN");
                to.setSundayDraw(tempInt);

                // returns
                tempInt = rs.getInt("DTSUNR");
                to.setSundayReturns(tempInt);
                
                records.add(to);
            }
        }
        catch (Exception e) {
            System.out.println("RouteDAO::objectFactoryBlueChipDrawHistory() - Failed to create BlueChipProductOrderTO object: " + e.getMessage());
        }
        
        return records;
    }

}
