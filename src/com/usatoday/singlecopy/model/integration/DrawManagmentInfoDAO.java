/*
 * Created on Oct 10, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.integration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;

/**
 * @author aeast
 * @date Oct 10, 2007
 * @class DrawManagmentInfoDAO
 * 
 * Interacts with Champion DB to pull data related to draw management.
 * 
 */
public class DrawManagmentInfoDAO extends USATodayDAO {

    // SQL
    public static final String DISTRIBUTION_DATE_FIELDS = "DDSTDT";
    public static String DISTRIBUTION_DATE_TABLE_NAME = "DCTL";
    public static String DEFAULT_RDL_DATE_TABLE_NAME = "DLOG";


    /**
     * 
     * @return The current distribution date
     * @throws UsatException
     */
    public DateTime fetchDistributionDate() throws UsatException {
        DateTime dDate = null;
       	java.sql.Connection conn = null;
    	try {
            DateTime startTime = null;
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    		conn = ConnectionManager.getInstance().getConnection();

    	    String sql = "select " + DrawManagmentInfoDAO.DISTRIBUTION_DATE_FIELDS + " from " + USATodayDAO.TABLE_LIB + DrawManagmentInfoDAO.DISTRIBUTION_DATE_TABLE_NAME;

    		DateTime stopTime = null;
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("DrawManagmentInfoDAO:: fetchDistributionDate() => It took " + timeToRun + " milliseconds to get a DB Connection.");    	        
    	        System.out.println("Running SQL: '" + sql + "'");
    	    }
    		
    	    PreparedStatement statement = conn.prepareStatement(sql);
            
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
            ResultSet rs = statement.executeQuery();
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("DrawManagmentInfoDAO:: fetchDistributionDate() => It took " + timeToRun + " milliseconds to fetch the results.");    	        
    	    }
   		    
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    	    if (rs.next()) {
                DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
                
                String tempStr = rs.getString("DDSTDT"); //  
                if (tempStr != null && tempStr.trim().length() > 0) {

                    dDate = formatter.parseDateTime(tempStr);
                    
                }
    	    }
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("DrawManagmentInfoDAO : fetchDistributionDate() " + e.getMessage());
            throw new UsatException(e);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        return dDate;
    }
    
    /**
     * 
     * @return
     * @throws UsatException
     */
    public DateTime fetchDefaultRDLDate() throws UsatException {
        DateTime dDate = null;
       	java.sql.Connection conn = null;
    	try {
            DateTime startTime = null;
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    		conn = ConnectionManager.getInstance().getConnection();

    	    String sql = "select distinct MAX(" + DrawManagmentInfoDAO.DISTRIBUTION_DATE_FIELDS + ") from " + USATodayDAO.TABLE_LIB + DrawManagmentInfoDAO.DEFAULT_RDL_DATE_TABLE_NAME;

    		DateTime stopTime = null;
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("DrawManagmentInfoDAO:: fetchDefaultRDLDate() => It took " + timeToRun + " milliseconds to get a DB Connection.");    	        
    	        System.out.println("Running SQL: '" + sql + "'");
    	    }
    		
    	    PreparedStatement statement = conn.prepareStatement(sql);
            
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
            ResultSet rs = statement.executeQuery();
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("DrawManagmentInfoDAO:: fetchDefaultRDLDate() => It took " + timeToRun + " milliseconds to fetch the results.");    	        
    	    }
   		    
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    	    if (rs.next()) {
                DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
                
                String tempStr = rs.getString(1);; //rs.getString("DEFRDL"); //  
                
                if (tempStr != null && tempStr.trim().length() > 0) {

                    dDate = formatter.parseDateTime(tempStr);
                    
                }
    	    }
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("DrawManagmentInfoDAO : fetchDefaultRDLDate() " + e.getMessage());
            throw new UsatException(e);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        return dDate;
    }
    
    
}
