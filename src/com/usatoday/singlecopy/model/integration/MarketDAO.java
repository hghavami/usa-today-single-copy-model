/*
 * Created on Aug 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.integration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.transferObjects.MarketTO;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;
import com.usatoday.singlecopy.model.util.ZeroFilledNumeric;

/**
 * @author aeast
 * @date Aug 2, 2007
 * @class MarketDAO
 * 
 * This class interfaces with the database to work with market data
 * 
 */
public class MarketDAO extends USATodayDAO {

	public static final String FIELDS = "MRKT.MKTID AS MKTID, MRKT.MKTDSC AS MKTDSC, MRKT.LMPHNO AS LMPHNO, MRKT.RDLPHN aS RDLPHN, WPMK.MKTEML AS MKTEML, WPMK.MKTPHN as WPPHONE ";
    public static String MRKT_TABLE_NAME = "MRKT";
    public static String WPMRKT_TABLE_NAME = "WPMK";
    
    private Collection<MarketTO> objectFactory(ResultSet rs) throws UsatException {
        Collection<MarketTO> records = new ArrayList<MarketTO>();
        
        try {
            // iterate over result set and build objects
            
            while (rs.next()){

                MarketTO mTO = new MarketTO();
                
                // FIELD NAMES
                // MRKT.MKTID AS MKTID, MRKT.MKTDSC AS MKTDSC, MRKT.LMPHNO AS LMPHNO, 
                // MRKT.RDLPHN aS RDLPHN, WPMK.MKTEML AS MKTEML, WPMK.MKTPHN as WPPHONE, 
                // 
                String tempStr = rs.getString("MKTID"); // CHAR 
                
                int tempInt = rs.getInt("MKTID"); //
                
                tempStr = ZeroFilledNumeric.getValue(RouteDAO.MKTID_MAX_LENGTH, tempInt);
                
                if (tempStr != null && tempStr.trim().length() > 0) {
                    mTO.setId(tempStr.trim());
                }
                else {
                    mTO.setId("");
                }
                
                tempStr = rs.getString("MKTDSC");  //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    // description
                    mTO.setDescription(tempStr.trim());
                }
                else {
                    mTO.setDescription("");
                }
                
                tempStr = rs.getString("LMPHNO");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    mTO.setLocalPhone(tempStr.trim());
                }
                else {
                    mTO.setLocalPhone("");
                }

                tempStr = rs.getString("WPPHONE");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    mTO.setRdlPhone(tempStr.trim());
                }
                else {
                    mTO.setRdlPhone("");
                }
                
                tempStr = rs.getString("MKTEML");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    mTO.setEmailAddress(tempStr.trim());
                }

                records.add(mTO);
            }
        }
        catch (Exception e) {
            System.out.println("MarketDAO::objectFactory() - Failed to create Market TO object: " + e.getMessage());
        }
        
        return records;
    }
    
    /**
     * 
     * @return
     * @throws UsatException
     */
    public Collection<MarketTO> fetchMarkets() throws UsatException {
        Collection<MarketTO> records = null;
       	java.sql.Connection conn = null;
    	try {
            DateTime startTime = null;
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    		conn = ConnectionManager.getInstance().getConnection();

    		// SQL QUERY TO PULL MARKET DATA
			//  SELECT 
			//     MRKT.MKTID AS MKTID, MRKT.MKTDSC AS MKTDSC, MRKT.LMPHNO AS LMPHNO, MRKT.RDLPHN aS RDLPHN, WPMK.MKTEML AS MKTEML, WPMK.MKTPHN as WPPHONE, WPMK.WPCUTTIM as CUTOFFTIME
			//  FROM SCDEVDTA.MRKT AS MRKT LEFT OUTER JOIN SCDEVDTA.WPMK AS WPMK ON MRKT.MKTID = WPMK.MKTID
			//  	ORDER BY MRKT.MKTID ASC;
    		
    		String sql = "select " + MarketDAO.FIELDS + " from " + USATodayDAO.TABLE_LIB + MarketDAO.MRKT_TABLE_NAME + " AS MRKT LEFT OUTER JOIN " +  USATodayDAO.TABLE_LIB + MarketDAO.WPMRKT_TABLE_NAME +" AS WPMK ON MRKT.MKTID = WPMK.MKTID ORDER BY MRKT.MKTID ASC";
    		DateTime stopTime = null;
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("MarketDAO:: fetchMarkets() => It took " + timeToRun + " milliseconds to get a DB Connection.");    	        
    	        System.out.println("Running SQL: '" + sql + "'");
    	    }
    		
    	    PreparedStatement statement = conn.prepareStatement(sql);
            
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
            ResultSet rs = statement.executeQuery();
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("MarketDAO:: fetchMarkets() => It took " + timeToRun + " milliseconds to fetch the results.");    	        
    	    }
   		    
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
            records = this.objectFactory(rs);
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("MarketDAO:: fetchMarkets() => It took " + timeToRun + " milliseconds to convert result set to Java objects.");    	        
    	    }
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("MarketDAO : fetchMarkets() " + e.getMessage());
            throw new UsatException(e);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        return records;
    }

    public MarketTO fetchMarket(String marketID) throws UsatException {
    	MarketTO market = null;
        Collection<MarketTO> records = null;
       	java.sql.Connection conn = null;
    	try {
            DateTime startTime = null;
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    		conn = ConnectionManager.getInstance().getConnection();

    		// SQL QUERY TO PULL MARKET DATA
			//  SELECT 
			//     MRKT.MKTID AS MKTID, MRKT.MKTDSC AS MKTDSC, MRKT.LMPHNO AS LMPHNO, MRKT.RDLPHN aS RDLPHN, WPMK.MKTEML AS MKTEML, WPMK.MKTPHN as WPPHONE, WPMK.WPCUTTIM as CUTOFFTIME
			//  FROM SCDEVDTA.MRKT AS MRKT LEFT OUTER JOIN SCDEVDTA.WPMK AS WPMK ON MRKT.MKTID = WPMK.MKTID
			//  	WHERE MRKT.MKTID = ?;
    		
    		String sql = "select " + MarketDAO.FIELDS + " from " + USATodayDAO.TABLE_LIB + MarketDAO.MRKT_TABLE_NAME + " AS MRKT LEFT OUTER JOIN " +  USATodayDAO.TABLE_LIB + MarketDAO.WPMRKT_TABLE_NAME +" AS WPMK ON MRKT.MKTID = WPMK.MKTID WHERE MRKT.MKTID = '" + marketID + "'";

    		
    		DateTime stopTime = null;
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("MarketDAO:: fetchMarket() => It took " + timeToRun + " milliseconds to get a DB Connection.");    	        
    	        System.out.println("Running SQL: '" + sql + "'");
    	    }
    		
    	    PreparedStatement statement = conn.prepareStatement(sql);
            
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
            ResultSet rs = statement.executeQuery();
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("MarketDAO:: fetchMarket() => It took " + timeToRun + " milliseconds to fetch the results.");    	        
    	    }
   		    
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
            records = this.objectFactory(rs);
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("MarketDAO:: fetchMarket() => It took " + timeToRun + " milliseconds to convert result set to Java objects.");    	        
    	    }
    	    
    	    if (records.size() == 1) {
    	        market = records.iterator().next();
    	    }
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("MarketDAO : fetchMarket() " + e.getMessage());
            throw new UsatException(e);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        return market;
    }
    
}
