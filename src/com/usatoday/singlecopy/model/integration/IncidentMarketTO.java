/*
 * Created on Aug 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.integration;

import java.io.Serializable;

/**
 * @author aeast
 * @date Aug 2, 2007
 * @class MarketTO
 * 
 * Market Transfer Object.
 * 
 */
public class IncidentMarketTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5737106754050527848L;
	private long id = 0;
    private String marketID = null;
    /**
     * 
     */
    public IncidentMarketTO() {
        super();
    }

    /* (non-Javadoc)
     * @see com.usatoday.singlecopy.model.interfaces.markets.MarketIntf#getID()
     */
    public long getID() {
        return this.id;
    }

    /**
     * @param description The description to set.
     */
    public void setMarketID(String marketID) {
        this.marketID = marketID;
    }
    /**
     * @param id The id to set.
     */
    public void setID(long id) {
        this.id = id;
    }
    /**
     * @return Returns the marketID.
     */
    public String getMarketID() {
        return marketID;
    }
}
