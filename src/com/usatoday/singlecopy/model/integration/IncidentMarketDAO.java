 /*
 * Created on Nov 5, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.integration;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import com.usatoday.singlecopy.UsatException;

/**
 * @author aeast
 * @date Nov 5, 2007
 * @class IncidentMarketDAO
 * 
 * Retrieves data from market table
 * 
 */
public class IncidentMarketDAO {

    public static final String MARKET_FIELDS = "ID, marketID";
    public static String MARKET_TABLE_NAME = "NCS_market";

   // private static final String MARKET_LOCATOR_BY_ZIP = "{call USAT_NCS_LOCATE_MARKET(?, ?)}";

    protected EsubConnectionManager connectionManager = null;    
    /**
     * 
     */
    public IncidentMarketDAO() {
        super();
        this.connectionManager = EsubConnectionManager.getInstance();
    }


    /**
     * 
     */

    /**
     * 
     * @return A collection of valid markets.
     * @throws Exception
     */
    public Collection<IncidentMarketTO> fetchMarkets() throws Exception {
        Collection<IncidentMarketTO> records = null;
        java.sql.Connection conn = null;
        try {
            
            conn = connectionManager.getDBConnection();

            String sql = "select " + IncidentMarketDAO.MARKET_FIELDS + " from " + IncidentMarketDAO.MARKET_TABLE_NAME + " order by marketID";
            
            PreparedStatement statement = conn.prepareStatement(sql);
            
            ResultSet rs = statement.executeQuery();
            
            records = this.objectFactoryMarkets(rs);
                        
        }
        catch (Exception e) {
            System.out.println("IncidentMarketDAO : fetchMarkets() " + e.getMessage());
            throw e;
        }
        finally {
            if (conn != null){
                this.cleanupConnection(conn);
            }
        }
        return records;
    }


    protected void cleanupConnection(Connection c) throws UsatException {
        if (c != null) {
            try {
                c.close();
            } catch (SQLException e) {
                throw new UsatException(e);
            }
        }
    }

    /**
     * 
     * @param rs
     * @return
     * @throws Exception
     */
    private Collection<IncidentMarketTO> objectFactoryMarkets(ResultSet rs) throws Exception {
        Collection<IncidentMarketTO> records = new ArrayList<IncidentMarketTO>();
        
        try {
            // iterate over result set and build objects
            
            while (rs.next()){

                IncidentMarketTO mTO = new IncidentMarketTO();
                
                String tempStr = rs.getString("marketID"); // CHAR 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    mTO.setMarketID(tempStr.trim());
                }
                else {
                    mTO.setMarketID("");
                }
                
                long tempLng = rs.getLong("ID");  //  

                mTO.setID(tempLng);
                
                records.add(mTO);
            }
        }
        catch (Exception e) {
            System.out.println("IncidentMarketDAO::objectFactoryMarkets() - Failed to create Market TO object: " + e.getMessage());
        }
        
        return records;
    }
    
}
