/*
 * Created on Aug 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.integration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.transferObjects.EmailScheduleSentTimeTO;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;

/**
 * @author hghavami
 * @date May 21, 2008
 * @class EmailScheduleSentTimeDAO
 * 
 * This class interfaces with the database to work with email schedule sent time data
 * 
 */
public class EmailScheduleSentTimeDAO extends USATodayDAO {
    public static final String FIELDS = "SCHCOD, SCHDSC";
    public static String TABLE_NAME = "SCHD";
    
    private Collection<EmailScheduleSentTimeTO> objectFactory(ResultSet rs) throws UsatException {
        Collection<EmailScheduleSentTimeTO> records = new ArrayList<EmailScheduleSentTimeTO>();
        
        try {
            // iterate over result set and build objects
            
            while (rs.next()){

                EmailScheduleSentTimeTO esstTO = new EmailScheduleSentTimeTO();
                
                String tempStr = rs.getString("SCHCOD"); // CHAR 
                
                if (tempStr != null && tempStr.trim().length() > 0) {
                    esstTO.setScheduleCode(tempStr.trim());
                }
                else {
                    esstTO.setScheduleCode("");
                }
                
                tempStr = rs.getString("SCHDSC");  //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    // description
                    esstTO.setScheduleCodeDesc(tempStr.trim());
                }
                else {
                    esstTO.setScheduleCodeDesc("");
                }
                
                records.add(esstTO);
            }
        }
        catch (Exception e) {
            System.out.println("EmailScheduleSentTimeDAO::objectFactory() - Failed to create EmailScheduleSentTime TO object: " + e.getMessage());
        }
        
        return records;
    }
    
    /**
     * 
     * @return
     * @throws UsatException
     */
    public Collection<EmailScheduleSentTimeTO> fetchEmailScheduleSentTimes() throws UsatException {
        Collection<EmailScheduleSentTimeTO> records = null;
       	java.sql.Connection conn = null;
    	try {
            DateTime startTime = null;
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    		conn = ConnectionManager.getInstance().getConnection();

    		String sql = "select " + EmailScheduleSentTimeDAO.FIELDS + " from " + USATodayDAO.TABLE_LIB + EmailScheduleSentTimeDAO.TABLE_NAME;
    		DateTime stopTime = null;
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("EmailScheduleSentTimeDAO:: fetchEmailScheduleSentTimes() => It took " + timeToRun + " milliseconds to get a DB Connection.");    	        
    	        System.out.println("Running SQL: '" + sql + "'");
    	    }
    		
    	    PreparedStatement statement = conn.prepareStatement(sql);
            
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
            ResultSet rs = statement.executeQuery();
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("EmailScheduleSentTimeDAO:: fetchEmailScheduleSentTimes() => It took " + timeToRun + " milliseconds to fetch the results.");    	        
    	    }
   		    
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
            records = this.objectFactory(rs);
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("EmailScheduleSentTimeDAO:: fetchEmailScheduleSentTimes() => It took " + timeToRun + " milliseconds to convert result set to Java objects.");    	        
    	    }
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("EmailScheduleSentTimeDAO : fetchEmailScheduleSentTimes() " + e.getMessage());
            throw new UsatException(e);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        return records;
    }
    
}
