/*
 * Created on Nov 8, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.integration;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.bo.markets.IncidentBO;

/**
 * @author aeast
 * @date Nov 8, 2007
 * @class IncidentDAO
 * 
 * Interfaces with the Incident DB
 * 
 */
public class IncidentDAO {

    public final static String NEW_INCIDENT = "NEW";
    public final static String ACKNOWLEDGED_INCIDENT = "ACKNOWLEDGED";
    public final static String WORKING_INCIDENT = "WORKING";
    public final static String RESOLVED_INCIDENT = "RESOLVED";
    
    public static final String TABLE_NAME = "NCS_incident";
    
    public static final String INCIDENT_FIELDS = "ID, marketKey, ncsSiteKey, refundRequest, incidentZip, status, incidentType, incidentRequestType," +
    		"incidentLocName, insert_timestamp, update_timestamp, incidentDate, updatedBy, enteredBy, incidentAddress, incidentCity," +
    		"incidentState, custAccountNum, custFirstName, custLastName, custAddr1, custAddr2, custCity, custState, custZip, custPhone, custEmail, " +
    		"deptid, deptName, deptContactID, deptContact, deptEmailAddress, incidentNotes, responseNotes";

    protected EsubConnectionManager connectionManager = null;
    /**
     * 
     */
    public IncidentDAO() {
        super();
        this.connectionManager = EsubConnectionManager.getInstance();
    }

    /**
     * 
     * @param rs
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unused")
	private Collection<IncidentBO> objectFactory(ResultSet rs) throws Exception {
        Collection<IncidentBO> records = new ArrayList<IncidentBO>();
        
        try {
            // iterate over result set and build objects
            
            while (rs.next()){

                IncidentBO incident = new IncidentBO();

            //  "ID, marketKey, ncsSiteKey, refundRequest, incidentZip, status, incidentType,incidentRequestType" +
        	//	"incidentLocName, insert_timestamp, update_timestamp, incidentDate, updatedBy, enteredBy, incidentAddress, incidentCity," +
        	//	"incidentState, custFirstName, custLastName, custAccountNum, custAddr1, custAddr2, custCity, custState, custZip, custPhone, custEmail, " +
        	//	"deptid, deptName, deptContactID, deptContact, deptEmailAddress, incidentNotes, responseNotes";
                
                long id = rs.getLong("ID");
                incident.setId(id);
                
                id = rs.getLong("marketKey");
                incident.setMarketKey(id);
                
                id = rs.getLong("ncsSiteKey");
                incident.setNcsSiteKey(id);
                
                String tempStr = rs.getString("incidentZip");  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    incident.setIncidentZip(tempStr);
                }
                else {
                    incident.setIncidentZip("");
                }
                
                tempStr = rs.getString("refundRequest");    
                if (tempStr != null && tempStr.trim().equalsIgnoreCase("Y")) {
                    // description
                    incident.setRefundRequest(true);
                }
                else {
                    incident.setRefundRequest(false);
                }
                
                tempStr = rs.getString("status");    
                if (tempStr != null && tempStr.trim().length() > 0) {
                    // description
                    incident.setStatus(tempStr);
                }
                else {
                    incident.setStatus("");
                }
                
                tempStr = rs.getString("incidentType");  // 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    incident.setIncidentType(tempStr);
                }
                else {
                    incident.setIncidentType("");
                }

                tempStr = rs.getString("incidentRequestType");  // 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    incident.setIncidentRequestType(tempStr);
                }
                else {
                    incident.setIncidentRequestType("");
                }
                
                tempStr = rs.getString("incidentLocName");  // 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    incident.setIncidentLocationName(tempStr);
                }
                else {
                    incident.setIncidentLocationName("");
                }
                
                Timestamp ts = rs.getTimestamp("insert_timestamp");
                if (ts != null) {
                    DateTime dt = new DateTime(ts.getTime());
                    incident.setInserted(dt);
                }

                ts = rs.getTimestamp("update_timestamp");
                if (ts != null) {
                    DateTime dt = new DateTime(ts.getTime());
                    incident.setUpdated(dt);
                }

                ts = rs.getTimestamp("incidentDate");
                if (ts != null) {
                    DateTime dt = new DateTime(ts.getTime());
                    incident.setIncidentDate(dt.toDate());
                }
                
                tempStr = rs.getString("updatedBy");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setUpdatedBy(tempStr);
                }
                else {
                    incident.setUpdatedBy("");
                }
                
                tempStr = rs.getString("enteredBy");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setEnteredBy(tempStr.trim());
                }
                else {
                    incident.setEnteredBy("");
                }

                tempStr = rs.getString("incidentAddress");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setIncidentAddress(tempStr.trim());
                }
                else {
                    incident.setIncidentAddress("");
                }
                
                tempStr = rs.getString("incidentCity");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setIncidentCity(tempStr.trim());
                }
                else {
                    incident.setIncidentCity("");
                }
                tempStr = rs.getString("incidentState");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setIncidentState(tempStr.trim());
                }
                else {
                    incident.setIncidentState("");
                }
                
                tempStr = rs.getString("custFirstName");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setCustomerFirstName(tempStr.trim());
                }
                else {
                    incident.setCustomerFirstName("");
                }

                tempStr = rs.getString("custLastName");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setCustomerLastName(tempStr.trim());
                }
                else {
                    incident.setCustomerLastName("");
                }
                
                tempStr = rs.getString("custAccountNum");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setCustomerAccountNumber(tempStr.trim());
                }
                else {
                    incident.setCustomerAccountNumber("");
                }

                tempStr = rs.getString("custAddr1");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setCustomerAddress1(tempStr.trim());
                }
                else {
                    incident.setCustomerAddress1("");
                }

                tempStr = rs.getString("custAddr2");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setCustomerAddress2(tempStr.trim());
                }
                else {
                    incident.setCustomerAddress2("");
                }

                tempStr = rs.getString("custCity");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setCustomerCity(tempStr.trim());
                }
                else {
                    incident.setCustomerCity("");
                }

                tempStr = rs.getString("custState");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setCustomerState(tempStr.trim());
                }
                else {
                    incident.setCustomerState("");
                }
                
                tempStr = rs.getString("custZip");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setCustomerZip(tempStr.trim());
                }
                else {
                    incident.setCustomerZip("");
                }
                
                tempStr = rs.getString("custPhone");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setCustomerPhone(tempStr.trim());
                }
                else {
                    incident.setCustomerPhone("");
                }

                tempStr = rs.getString("custEmail");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setCustomerEmail(tempStr.trim());
                }
                else {
                    incident.setCustomerEmail("");
                }

                tempStr = rs.getString("deptid");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setDeptID(tempStr.trim());
                }
                else {
                    incident.setDeptID("");
                }

                tempStr = rs.getString("deptName");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setDeptName(tempStr.trim());
                }
                else {
                    incident.setDeptName("");
                }

                tempStr = rs.getString("deptContactID");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setDeptContactID(tempStr.trim());
                }
                else {
                    incident.setDeptContactID("");
                }

                tempStr = rs.getString("deptContact");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setDeptContact(tempStr.trim());
                }
                else {
                    incident.setDeptContact("");
                }

                tempStr = rs.getString("deptEmailAddress");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setDeptEmailAddress(tempStr.trim());
                }
                else {
                    incident.setDeptEmailAddress("");
                }

                tempStr = rs.getString("incidentNotes");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setNotes(tempStr.trim());
                }
                else {
                    incident.setNotes("");
                }

                tempStr = rs.getString("responseNotes");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setResponseNotes(tempStr.trim());
                }
                else {
                    incident.setResponseNotes("");
                }

                records.add(incident);
            }
        }
        catch (Exception e) {
            System.out.println("IncidentDAO::objectFactory() - Failed to create Incident object: " + e.getMessage());
        }
        
        return records;
    }

	/**
	 * 
	 * @param incident
	 * @return
	 * @throws Exception
	 */
    public IncidentBO insertIncident(IncidentBO incident) throws Exception {

        Connection conn = null;
    	try {
    	    
    		conn = connectionManager.getDBConnection();
        
    		java.sql.Statement statement = conn.createStatement();
    		
    		StringBuffer sql = new StringBuffer("insert into NCS_incident (marketKey, ncsSiteKey, refundRequest, incidentZip, status, incidentType, incidentRequestType, incidentLocName, insert_timestamp, update_timestamp, incidentDate, enteredBy, incidentAddress, incidentCity, " +
    				"incidentState, custFirstName, custLastName, custAccountNum, custAddr1, custAddr2, custCity, custState, custZip, custPhone, custEmail, incidentNotes, responseNotes) values (");
		
    		if (incident.getMarketKey() > 0) {
    		    sql.append("").append(incident.getMarketKey()).append(",");
    		}
    		else {
    		    // default market key should be Headquarters
    		    sql.append("1,");
    		}
    		
    		if (incident.getNcsSiteKey() > 0) {
    		    sql.append("").append(incident.getNcsSiteKey()).append(",");
    		}
    		else {
    		    // default to first site
    		    sql.append("1,");
    		}
    		
    		if (incident.isRefundRequest()){
    		    sql.append("'Y'").append(",");
    		}
    		else {
    		    sql.append("'N'").append(",");
    		}
    		
    		if (incident.getIncidentZip() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getIncidentZip())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getStatus() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getStatus())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getIncidentType() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getIncidentType())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getIncidentRequestType() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getIncidentRequestType())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getIncidentLocationName() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getIncidentLocationName())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}

    		// insert timestamp
    		Timestamp ts = new Timestamp(System.currentTimeMillis());
    		DateTime insertTS = new DateTime(ts.getTime());
    		sql.append("'").append(ts.toString());
    		sql.append("',");

    		// update timestamp
    		sql.append("'").append(ts.toString());
    		sql.append("',");
    		
    		// incident date
    		if (incident.getIncidentDate() != null) {
    		    Timestamp tempts = new Timestamp(incident.getIncidentDate().getTime());
    		    sql.append("'").append(tempts.toString());
    		    sql.append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getEnteredBy() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getEnteredBy())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getIncidentAddress() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getIncidentAddress())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getIncidentCity() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getIncidentCity())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}

    		if (incident.getIncidentState() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getIncidentState())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getCustomerFirstName() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getCustomerFirstName())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getCustomerLastName() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getCustomerLastName())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getCustomerAccountNumber() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getCustomerAccountNumber())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getCustomerAddress1() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getCustomerAddress1())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		if (incident.getCustomerAddress2() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getCustomerAddress2())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}

    		// 	"custCity, custState, custZip, custPhone, custEmail, incidentNotes, responseNotes) values (");
    		if (incident.getCustomerCity() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getCustomerCity())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getCustomerState() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getCustomerState())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getCustomerZip() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getCustomerZip())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		if (incident.getCustomerPhone() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getCustomerPhone())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		if (incident.getCustomerEmail() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getCustomerEmail())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getNotes() != null) {
    		    String notes = USATodayDAO.escapeApostrophes(incident.getNotes());
    		    sql.append("'").append(notes).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getResponseNotes() != null) {
    		    String notes = USATodayDAO.escapeApostrophes(incident.getResponseNotes());
    		    sql.append("'").append(notes).append("',");
    		}
    		else {
    		    sql.append("null)");
    		}
    		
    		//execute the SQL
    		int rowsAffected = statement.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS );

    		if (rowsAffected != 1) {
    		    throw new Exception("Incident Failed to Insert Or other error and yet no exception condition exists: Number rows affected: " + rowsAffected);
    		}

    		ResultSet rs = statement.getGeneratedKeys();
		
    		rs.next();
    		long primaryKey = rs.getLong(1);
		
    		incident.setId(primaryKey);
    		incident.setInserted(insertTS);
    		incident.setUpdated(insertTS);
		
    		statement.close();

    	}
    	catch (Exception e) {
            System.out.println("IncidentDAO : insertIncident() " + e.getMessage());
            throw e;
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        
        return incident;
    }

    protected void cleanupConnection(Connection c) throws UsatException {
        if (c != null) {
            try {
                c.close();
            } catch (SQLException e) {
                throw new UsatException(e);
            }
        }
    }
}
