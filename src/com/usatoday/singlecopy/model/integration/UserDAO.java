/*
 * Created on Mar 30, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.integration;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.UsatLoginFailedException;
import com.usatoday.singlecopy.model.interfaces.users.AccessRightsIntf;
import com.usatoday.singlecopy.model.transferObjects.AccessRightsTO;
import com.usatoday.singlecopy.model.transferObjects.GenericUpdateErrorTO;
import com.usatoday.singlecopy.model.transferObjects.UpdateUserErrorTO;
import com.usatoday.singlecopy.model.transferObjects.UserTO;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;

/**
 * @author aeast
 * @date Mar 30, 2007
 * @class UserDAO
 * 
 * This class interfaces with the AS/400 to perform user management.
 * 
 */
public class UserDAO extends USATodayDAO {
    private static final String EMAIL_AUTO_OPTOUT = "{call CHP_OBJ.USAT_SC_EMAIL_OPTOUT(?, ?, ?)}";

    // SQL
    private static final String SELECT_USER_DATA = "{call CHP_OBJ.USAT_SC_AUTHORIZE_USER(?, ?, ?, ?, ?)}";
    private static final String SELECT_USER_DATA_AUTO = "{call CHP_OBJ.USAT_SC_AUTOAUTH_USER(?, ?, ?, ?, ?)}";
    private static final String UPDATE_EMAIL_REM = "{call CHP_OBJ.USAT_SC_UPDATE_EMAIL_REM(?, ?, ?, ?, ?, ?)}";
    private static final String UPDATE_USER_DATA = "{call CHP_OBJ.USAT_SC_UPDATE_USER(?, ?, ?, ?, ?)}";
    
    /**
     * 
     * @param uid
     * @param sessionID
     * @return A collection of GenericUpdateErrorIntf error messages
     * @throws UsatException
     */
    public Collection<GenericUpdateErrorTO> autoOptOut(String sessionID, String uid) throws UsatException {
        // Collection of GenericUpdateErrorIntf (TO) objects
        //Collection<GenericUpdateErrorIntf> errorMsgs = null;
        
       	java.sql.Connection conn = null;
       	Collection<GenericUpdateErrorTO> errorMessages = null;
		String errorFlag = "";   
    	try {

    		conn = ConnectionManager.getInstance().getConnection();
    	
            java.sql.CallableStatement statement = conn.prepareCall(UserDAO.EMAIL_AUTO_OPTOUT);
    		
            statement.setString(1, sessionID);
            
            statement.setString(2, uid);
            
            statement.registerOutParameter(3, Types.CHAR);    
            
   		    statement.execute();
   		    
   		    errorFlag = statement.getString(3);
   		    
   		    // N = no errors
   		    if ("N".equalsIgnoreCase(errorFlag.trim()) ) {
   		        ;	
   		    }
   		    else {
   		        
   		        // error exist
   		      
 		        ResultSet updateRS = statement.getResultSet();
   		        
   		        errorMessages = this.objectFactoryGenericUpdateErrors(updateRS);

   		        if (errorMessages.size() == 0) {
   		        	GenericUpdateErrorTO eTO = new GenericUpdateErrorTO();
   		        	eTO.setErrorMessage("Opt Out Failed: Unknown Reason.");
   		            errorMessages.add(eTO);
   		        }
   		    }
   		       		    
    	}
    	catch (SQLException sqle) {
            System.out.println("UserDAO : autoOptOut() " + sqle.getMessage());
            
            throw new UsatException(sqle);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        
    	if (errorMessages == null) {
    	    errorMessages = new ArrayList<GenericUpdateErrorTO>();
    	}
    	
        return errorMessages;
        
    }
    
    /**
     * 
     * @param emailAddress
     * @param password
     * @return
     * @throws UsatException
     */
    public boolean forgotPasswordRequest(String emailAddress) throws UsatException {
       	java.sql.Connection conn = null;
        boolean emailSent = false;
		   
    	try {

    		conn = ConnectionManager.getInstance().getConnection();

    		StringBuffer sqlBuf = new StringBuffer("{call ");
     	    if (USATodayDAO.getSTORED_PROC_LIB() != null && USATodayDAO.getSTORED_PROC_LIB().length() > 0) {
     	        sqlBuf.append(USATodayDAO.getSTORED_PROC_LIB());
     	    }
     	    sqlBuf.append("USAT_SC_FORGOT_PWD_CL(?, ?)}");     	
    		
    		
            java.sql.CallableStatement statement = conn.prepareCall(sqlBuf.toString());
 

            String upperCaseEmail = emailAddress.toUpperCase();
            statement.setString(1, upperCaseEmail);
            statement.registerOutParameter(2, Types.CHAR);
            
   		    statement.execute();
   		    
   		    
   		    String emailStr = statement.getString(2);
   		    
   		    // N means there were no issues sending the forgot password request.
   		    if ("N".equalsIgnoreCase(emailStr.trim()) ) {
   		        emailSent = true;
   		    }
   		    else {
   		     emailSent = false;
   		    }
   		       		    
    	}
    	catch (SQLException sqle) {
            System.out.println("UserDAO : getUserByEmailAndPassword() " + sqle.getMessage());
            throw new UsatException(sqle);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        
        return emailSent;
    }
    
    /**
     * 
     * @param uid
     * @param sessionID
     * @return
     * @throws UsatException
     * @throws UsatLoginFailedException
     */
    public UserTO getUserByAutoLogin(String uid, String sessionID) throws UsatException, UsatLoginFailedException {
        Collection<UserTO> userCol = null;
       	java.sql.Connection conn = null;
       	UserTO user = null;
    	try {

    	    DateTime startTime = null;
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    	    conn = ConnectionManager.getInstance().getConnection();

    		DateTime stopTime = null;
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("UserDAO:: getUserByAutoLogin() => It took " + timeToRun + " milliseconds to get a DB Connection.");    	        
    	    }

            java.sql.CallableStatement statement = conn.prepareCall(UserDAO.SELECT_USER_DATA_AUTO);

            statement.setString(1, uid);
            statement.setString(2, sessionID);
            statement.registerOutParameter(3, Types.CHAR);
            statement.registerOutParameter(4, Types.CHAR);
            statement.registerOutParameter(5, Types.CHAR);
            
   		    statement.execute();

    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("UserDAO:: getUserByAutoLogin() => It took " + timeToRun + " milliseconds to fetch the results.");    	        
    	    }
   		    
   		    String passwordGood = statement.getString(3);
   		    String activeUser = statement.getString(4);
   		    String  emailAddress = statement.getString(5);
   		    
   		    if ("Y".equalsIgnoreCase(passwordGood.trim()) && "Y".equalsIgnoreCase(activeUser.trim())) {
   		        ResultSet userRS = statement.getResultSet();
   		        
   		        userCol = this.objectFactoryUser(userRS);
   		        
   		        userRS.close();
   		        
   		        if (userCol.size() == 1) {
   		            UserTO userTO = (UserTO)userCol.iterator().next();
   		            statement.getMoreResults();
   		            ResultSet accessRights = statement.getResultSet();
   		            ArrayList<AccessRightsIntf> rights = new ArrayList<AccessRightsIntf>(this.objectFactoryAccessRights(accessRights));
   		            userTO.setAccessRights(rights);
   		            user = userTO;
   		            accessRights.close();
   		        }
   		        else {
   		            throw new UsatException("Retrieved invalid number of users for this email/password: " + emailAddress + "  # returned:" + userCol.size());
   		        }
   		    }
   		    else {
   		        if (passwordGood.equalsIgnoreCase("N")) {
   		            throw new UsatLoginFailedException("The email link has expired. Use the latest email reminder or log in with a user id and password.", UsatLoginFailedException.BAD_AUTO_AUTH);
   		        }
   		        else {
   		            throw new UsatLoginFailedException("Problem logging in the email link. Please enter your email address and password to continue.", UsatLoginFailedException.BAD_AUTO_AUTH);
   		        }
   		    }
   		       		    
    	}
    	catch (SQLException sqle) {
            System.out.println("UserDAO : getUserByAutoLogin() " + sqle.getMessage());
            throw new UsatException(sqle);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        
        return user;
    }
    
    /**
     * 
     * @param emailAddress
     * @param password
     * @return
     * @throws UsatException
     */
    public UserTO getUserByEmailAndPassword(String emailAddress, String password, String sessionID) throws UsatException, UsatLoginFailedException {
        Collection<UserTO> userCol = null;
       	java.sql.Connection conn = null;
       	UserTO user = null;
    	try {

            DateTime startTime = null;
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    	    conn = ConnectionManager.getInstance().getConnection();

    		DateTime stopTime = null;
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("UserDAO:: getUserByEmailAndPassword() => It took " + timeToRun + " milliseconds to get a DB Connection.");    	        
    	    }
    	    
            java.sql.CallableStatement statement = conn.prepareCall(UserDAO.SELECT_USER_DATA);

            String upperCaseEmail = emailAddress.toUpperCase();
            statement.setString(1, sessionID);
            statement.setString(2, upperCaseEmail);
            statement.setString(3, password);
            statement.registerOutParameter(4, Types.CHAR);
            statement.registerOutParameter(5, Types.CHAR);
            
   		    statement.execute();
   		    
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("UserDAO:: getUserByEmailAndPassword() => It took " + timeToRun + " milliseconds to fetch the results.");    	        
    	    }
   		    
   		    String passwordGood = statement.getString(4);
   		    String accountActive = statement.getString(5);
   		    if ("Y".equalsIgnoreCase(accountActive.trim()) && "Y".equalsIgnoreCase(passwordGood.trim())) {
   		        ResultSet userRS = statement.getResultSet();
   		        
   		        userCol = this.objectFactoryUser(userRS);
   		        
   		        userRS.close();
   		        
   		        if (userCol.size() == 1) {
   		            UserTO userTO = userCol.iterator().next();
   		            statement.getMoreResults();
   		            ResultSet accessRights = statement.getResultSet();
   		            
   		            ArrayList<AccessRightsIntf> rights = new ArrayList<AccessRightsIntf>(this.objectFactoryAccessRights(accessRights));
   		            userTO.setAccessRights(rights);
   		            user = userTO;
   		            accessRights.close();
   		        }
   		        else {
   		            throw new UsatException("Retrieved invalid number of users for this email/password: " + emailAddress + "  # returned:" + userCol.size());
   		        }
   		    }
   		    else {
   		        if ("Y".equalsIgnoreCase(accountActive)) {
   	   		        throw new UsatLoginFailedException("Inavlid User ID and Password combination.", UsatLoginFailedException.BAD_PASSWORD);
   		        }
   		        else {
   		            throw new UsatLoginFailedException("Your account has not been activated for web site access. Please call your market for assistance.", UsatLoginFailedException.ACCOUNT_NOT_ACTIVE);
   		        }
   		    }
   		       		    
    	}
    	catch (SQLException sqle) {
            System.out.println("UserDAO : getUserByEmailAndPassword() " + sqle.getMessage());
            throw new UsatException(sqle);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        
        return user;
    }

    /*
     * 
     */
    private Collection<AccessRightsTO> objectFactoryAccessRights(ResultSet rs) throws UsatException {
        Collection<AccessRightsTO> records = new ArrayList<AccessRightsTO>();
        
        try {
            // iterate over result set and build objects
            while (rs.next()) {
	            String tempStr = rs.getString("ASGTYP");
                AccessRightsTO aTO = new AccessRightsTO();
	            if (tempStr != null && tempStr.trim().length() > 0) {
	                aTO.setAccessRight(tempStr);
	            }
	            tempStr = rs.getString("EMLREM");
	            if (tempStr != null && tempStr.trim().equalsIgnoreCase("Y")) {
	                aTO.setReceivingEmailReminders(true);
	            }
	            else {
	                aTO.setReceivingEmailReminders(false);
	            }
	            
	            // hh:mm; 24hr clock
	            tempStr = rs.getString("SCHCOD");
	            if (tempStr != null && tempStr.trim().length() > 0) {
	                aTO.setEmailReminderTimePreference(tempStr.trim());
	            }
	            
	            records.add(aTO);
            }
        }
        catch (Exception e) {
            System.out.println("UserDAO::objectFactoryAccessRights() - Failed to create user access rights object: " + e.getMessage());
        }
        
        return records;
    }

    private Collection<GenericUpdateErrorTO> objectFactoryGenericUpdateErrors(ResultSet rs) throws UsatException {
        Collection<GenericUpdateErrorTO> records = new ArrayList<GenericUpdateErrorTO>();
        
        try {
            // iterate over result set and build objects
            while (rs.next()) {

                GenericUpdateErrorTO eTO = new GenericUpdateErrorTO();

	            String tempStr = rs.getString("ERRMSG");
	            if (tempStr != null && tempStr.trim().length() > 0) {
	                // Create Access Right TO and store in inner collection
	                eTO.setErrorMessage(tempStr);
	            }
	            
	            records.add(eTO);
            }
        }
        catch (Exception e) {
            System.out.println("UserDAO::objectFactoryGenericUpdateErrors() - Failed to create GenericUpdateErrorTO: " + e.getMessage());
        }
        
        return records;
    }

    /*
     * 
     */
    private Collection<UpdateUserErrorTO> objectFactoryUpdateErrors(ResultSet rs) throws UsatException {
        Collection<UpdateUserErrorTO> records = new ArrayList<UpdateUserErrorTO>();
        
        try {
            // iterate over result set and build objects
            while (rs.next()) {

                UpdateUserErrorTO eTO = new UpdateUserErrorTO();

	            String tempStr = rs.getString("EADRS");
	            if (tempStr != null && tempStr.trim().length() > 0) {
	                // Create Access Right TO and store in inner collection
	                eTO.setEmailAddress(tempStr);
	            }

	            tempStr = rs.getString("ERRMSG");
	            if (tempStr != null && tempStr.trim().length() > 0) {
	                // Create Access Right TO and store in inner collection
	                eTO.setErrorMessage(tempStr);
	            }
	            
	            records.add(eTO);
            }
        }
        catch (Exception e) {
            System.out.println("UserDAO::objectFactoryUpdateErrors() - Failed to create UpdateUserErrorTO object: " + e.getMessage());
        }
        
        return records;
    }
    /*
     * 
     */
    private Collection<UserTO> objectFactoryUser(ResultSet rs) throws UsatException {
        Collection<UserTO> records = new ArrayList<UserTO>();
        
        try {
            // iterate over result set and build objects
            
            while (rs.next()){

                UserTO userTO = new UserTO();
                
                String tempStr = rs.getString("WPUNAM"); // CHAR 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    userTO.setDisplayName(tempStr.trim());
                }
                else {
                    userTO.setDisplayName("Authorized User");
                }
                
                tempStr = rs.getString("PHNNBR");  // NUM (10,0)
                if (tempStr != null && tempStr.trim().length() > 0) {
                    userTO.setPhoneNumber(tempStr.trim());
                }
                else {
                    userTO.setPhoneNumber("");
                }
                
                tempStr = rs.getString("EADRS");  // CHAR (50)
                if (tempStr != null && tempStr.trim().length() > 0) {
                    userTO.setEmailAddress(tempStr.trim());
                }
                else {
                    userTO.setEmailAddress("");
                }

                records.add(userTO);
            }
        }
        catch (Exception e) {
            System.out.println("UserDAO::objectFactory() - Failed to create user object: " + e.getMessage());
        }
        
        return records;
    }
    
    /**
     * 
     * @param emailAddress
     * @param sessionID
     * @param emailAccessRight The access right containing the email preference to be updated
     * @return a Collection of generic update errror messages or an empty collection
     * @throws UsatException
     */
    public Collection<UpdateUserErrorTO> updateAccessRight(String emailAddress,String sessionID, AccessRightsIntf emailAccessRight) throws UsatException {
        Collection<UpdateUserErrorTO> errorMessages = null;
        
        java.sql.Connection conn = null;
        String isUpdated = "";
        String receiveEmailReminders = "Y";
        try {

            conn = ConnectionManager.getInstance().getConnection();
        
            java.sql.CallableStatement statement = conn.prepareCall(UserDAO.UPDATE_EMAIL_REM);
            
            statement.setString(1, sessionID);
            String upperCaseEmail = emailAddress.toUpperCase();
            statement.setString(2, upperCaseEmail);
            

            statement.setString(3, emailAccessRight.getAccessRight());
            
            if (!emailAccessRight.isReceivingEmailReminders()) {
                receiveEmailReminders = "N";
            }
            statement.setString(4, receiveEmailReminders);
            statement.setString(5, "");
            if (emailAccessRight.getEmailReminderTimePreference().toString() != null &&
                    !emailAccessRight.getEmailReminderTimePreference().toString().trim().equals("")) {
                statement.setString(5, emailAccessRight.getEmailReminderTimePreference().toString());                
            }
            statement.setString(6,"");
            statement.registerOutParameter(6, Types.CHAR);    
            
            statement.execute();             
            isUpdated = statement.getString(6);
            
            // N = no errors
            if ("N".equalsIgnoreCase(isUpdated.trim()) ) { 
            }
            else {
                
                ResultSet updateRS = statement.getResultSet();
                
                errorMessages = this.objectFactoryUpdateErrors(updateRS);
              
            }
                        
        }
        catch (SQLException sqle) {
            System.out.println("UserDAO : updateUserEmailPreferences() " + sqle.getMessage());
            
            throw new UsatException(sqle);
        }
        finally {
            if (conn != null){
                this.cleanupConnection(conn);
            }
        }
        
        if (errorMessages == null) {
            errorMessages = new ArrayList<UpdateUserErrorTO>();
        }
        
        return errorMessages;
    }
    
    /**
     * 
     * @param emailAddress
     * @param password
     * @return
     * @throws UsatException
     */
    public Collection<UpdateUserErrorTO> updateUser(String sessionID, String oldEmailAddress,String newEmailAddress, String newPassword) throws UsatException {
       	java.sql.Connection conn = null;
       	Collection<UpdateUserErrorTO> errorMessages = null;
        String isUpdated = "";   
    	try {

    		conn = ConnectionManager.getInstance().getConnection();

   // ONLY ESCAPE Apostrophes when creating a dynamic sql statement
    		
           java.sql.CallableStatement statement = conn.prepareCall(UserDAO.UPDATE_USER_DATA);
    		
            String upperCaseOldEmail = oldEmailAddress.toUpperCase();
            //upperCaseOldEmail = USATodayDAO.escapeApostrophes(upperCaseOldEmail);
            
            statement.setString(1, sessionID);
            
            statement.setString(2, upperCaseOldEmail);
            
            String upperCaseNewEmail = newEmailAddress.toUpperCase();
//            upperCaseNewEmail = USATodayDAO.escapeApostrophes(upperCaseNewEmail);
            
            statement.setString(3, upperCaseNewEmail);
             
 //           newPassword = USATodayDAO.escapeApostrophes(newPassword);
            
            statement.setString(4, newPassword);
            
            statement.registerOutParameter(5,Types.CHAR);    
            
   		    statement.execute();   		    
   		    isUpdated = statement.getString(5);
   		    
   		    // N = no errors
   		    if ("N".equalsIgnoreCase(isUpdated.trim()) ) {	
   		    }
   		    else {
   		        
   		        ResultSet updateRS = statement.getResultSet();
   		        
   		        errorMessages = this.objectFactoryUpdateErrors(updateRS);
   		      
   		    }
   		       		    
    	}
    	catch (SQLException sqle) {
            System.out.println("UserDAO : changePasswordRequest() " + sqle.getMessage());
            
            throw new UsatException(sqle);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        
    	if (errorMessages == null) {
    	    errorMessages = new ArrayList<UpdateUserErrorTO>();
    	}
    	
        return errorMessages;
    }
}
