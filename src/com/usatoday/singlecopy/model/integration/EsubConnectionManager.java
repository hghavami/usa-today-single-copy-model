package com.usatoday.singlecopy.model.integration;
import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;


/**
 * ConnectionManager
 * 
 * This class handles getting connections to the database through JNDI.
 * 
 */
public class EsubConnectionManager {

    private static DataSource db = null;
    protected static String jndiDataSourceName = "jdbc/esub";
    private static EsubConnectionManager instance = null;
    
    /**
     * 
     */
    private EsubConnectionManager() {
        super();
        
        if (EsubConnectionManager.db == null) {
            try {
                Context ctx = new InitialContext();
                EsubConnectionManager.db = (DataSource) ctx.lookup(EsubConnectionManager.jndiDataSourceName);
            }
            catch (Exception e) {
                System.out.println("Champion Web APPLICATION: Failed to lookup Data source: " + EsubConnectionManager.jndiDataSourceName + " Message: " + e.getMessage());
            }
        }
    }

    protected void cleanupConnection(Connection c) throws Exception {
        if (c != null) {
            c.close();
        }
    }

    public Connection getDBConnection() throws Exception {
        Connection connection = null;
        
        connection = db.getConnection();
        
        return connection;
    }

    public static synchronized EsubConnectionManager getInstance() {
        if (instance == null) {
            instance = new EsubConnectionManager();
        }
        return instance;
    }
}

