/*
 * Created on May 4, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.integration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.transferObjects.ProductDescriptionTO;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;

/**
 * @author aeast
 * @date May 4, 2007
 * @class ProductDescriptionDAO
 * 
 * Data Access Object for PROD table
 * 
 */
public class ProductDescriptionDAO extends USATodayDAO {
    public static final String FIELDS = "PRODID, PRODNM, PRODRD";
    public static String TABLE_NAME = "PROD";

    private Collection<ProductDescriptionTO> objectFactory(ResultSet rs) throws UsatException {
        Collection<ProductDescriptionTO> records = new ArrayList<ProductDescriptionTO>();
        
        try {
            // iterate over result set and build objects
            
            while (rs.next()){

                ProductDescriptionTO pdTO = new ProductDescriptionTO();
                
                String tempStr = rs.getString("PRODID"); // CHAR 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    pdTO.setProductCode(tempStr.trim());
                }
                else {
                    pdTO.setProductCode("");
                }
                
                tempStr = rs.getString("PRODNM");  // NUM (10,0)
                if (tempStr != null && tempStr.trim().length() > 0) {
                    // description
                    pdTO.setDescription(tempStr.trim());
                }
                else {
                    pdTO.setDescription("");
                }
                
                tempStr = rs.getString("PRODRD");  // CHAR (50)
                if (tempStr != null && tempStr.trim().length() > 0) {
                    pdTO.setAbbreviation(tempStr.trim());
                }
                else {
                    pdTO.setAbbreviation("");
                }

                records.add(pdTO);
            }
        }
        catch (Exception e) {
            System.out.println("ProductDescriptionDAO::objectFactory() - Failed to create Product Description object: " + e.getMessage());
        }
        
        return records;
    }

    /**
     * 
     * @return
     * @throws UsatException
     */
    public Collection<ProductDescriptionTO> fetchProductDescriptions() throws UsatException {
        Collection<ProductDescriptionTO> records = null;
       	java.sql.Connection conn = null;
    	try {
            DateTime startTime = null;
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
    	    
    		conn = ConnectionManager.getInstance().getConnection();

    		String sql = "select " + ProductDescriptionDAO.FIELDS + " from " + USATodayDAO.TABLE_LIB + ProductDescriptionDAO.TABLE_NAME;
    		DateTime stopTime = null;
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("ProductDescriptionDAO:: fetchProductDescriptionss() => It took " + timeToRun + " milliseconds to get a DB Connection.");    	        
    	        System.out.println("Running SQL: '" + sql + "'");
    	    }
    		
    	    PreparedStatement statement = conn.prepareStatement(sql);
            
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
            ResultSet rs = statement.executeQuery();
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("ProductDescriptionDAO:: fetchProductDescriptionss() => It took " + timeToRun + " milliseconds to fetch the results.");    	        
    	    }
   		    
    	    if (USATApplicationConstants.debug) {
    	        startTime = new DateTime();
    	    }
            records = this.objectFactory(rs);
    	    if (USATApplicationConstants.debug) {
    	        stopTime = new DateTime();
                long timeToRun = (stopTime.getMillis() - startTime.getMillis());
                System.out.println("ProductDescriptionDAO:: fetchProductDescriptionss() => It took " + timeToRun + " milliseconds to convert result set to Java objects.");    	        
    	    }
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("ProductDescriptionDAO : fetchProductDescriptionss() " + e.getMessage());
            throw new UsatException(e);
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        return records;
    }

}
