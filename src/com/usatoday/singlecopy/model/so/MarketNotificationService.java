/*
 * Created on Nov 5, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.so;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.bo.markets.IncidentBO;
import com.usatoday.singlecopy.model.interfaces.markets.MarketIntf;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;
import com.usatoday.singlecopy.model.util.email.SmtpMailSender;

/**
 * @author aeast
 * @date Nov 5, 2007
 * @class MarketNotificationService
 * 
 * Service Object to handle market email notfications
 * 
 */
public class MarketNotificationService {

    
    /**
     * 
     */
    public MarketNotificationService() {
        super();
    }

    public void notifyMarket(IncidentBO incident, MarketIntf m) {
        
        // if market not set on incident check parameter
        if (m == null) {
            System.out.println("No market to notify was passed.");
        }
        
        if (m != null && m.getHasEmailAddress()) {
            
            try {
                SmtpMailSender mail = new SmtpMailSender();
            
                mail.setSender(USATApplicationConstants.marketNotifySenderEmail);
                
                String siteDescription = "BLUE CHIP PORTAL USER: ";
                try {
                    siteDescription = m.getDescription();
                }
                catch (Exception exp) {
                    // ignore
                }
                StringBuffer msgSubject = new StringBuffer(incident.getIncidentType().toUpperCase());
                msgSubject.append(" Request from '" + siteDescription + "', ID#: ");
                msgSubject.append(incident.getIdAsString());
                
                mail.setMessageSubject(msgSubject.toString());
                
                mail.addTORecipient(m.getEmailAddress());
                
                String emailBody = this.generateEmailBody(incident, m);
                mail.setMessageText(emailBody);
                
                mail.sendMessage();
            }
            catch (Exception e) {
                System.out.println("Failed to Send Market Notification: " + e.getMessage());
                System.out.println(incident.toString());
            }
            
        }
        
    }
    
    private String generateEmailBody(IncidentBO incident, MarketIntf m) {
        
        StringBuffer body = new StringBuffer("#\n");
        body.append("#  NCS Request for Market ID: ").append(m.getID()).append(", '").append(m.getDescription().trim()).append("'\n");
        body.append("#\n");
        DateTime tempDate = new DateTime(incident.getIncidentDate());
        body.append("Incident ID:        ").append(incident.getIdAsString()).append("\n");
        body.append("Request Date:       ").append(tempDate.toString("MM/dd/yyyy")).append("\n");
        body.append("Service Category:   ").append(incident.getIncidentType().toUpperCase()).append("\n");
        body.append("Request Type:       ").append(incident.getIncidentRequestType().toUpperCase()).append("\n");
        body.append("Web Portal User ID: ").append(incident.getEnteredBy()).append("\n");
        if (incident.getIncidentLocationName() != null && !incident.getIncidentLocationName().trim().equals("")) {
            body.append("Location Name:      ").append(incident.getIncidentLocationName()).append("\n");            
        }
        if (incident.getIncidentAddress() != null && !incident.getIncidentAddress().trim().equals("")) {
            body.append("Address:            ").append(incident.getIncidentAddress()).append("\n");
        }
        body.append("#\n");
        body.append("Response Requested: ");
        if (incident.isResponseRequested()) {
            body.append("YES");
        }
        else {
            body.append("NO");
        }
        body.append("\n#\n");
        body.append("Comments:           ").append(incident.getNotes().toUpperCase());
        body.append("\n#\n");
        body.append("# Customer Name and Address ").append("\n");
        body.append("# ==========================").append("\n");
        body.append("Name:           ").append(incident.getCustomerFirstName()).append(" ").append(incident.getCustomerLastName()).append("\n");
        body.append("Phone:          ").append(incident.getCustomerPhone()).append("\n");
        body.append("Email:          ").append(incident.getCustomerEmail()).append("\n");
        if (incident.getCustomerAccountNumber() != null && incident.getCustomerAccountNumber().trim().length() > 0) {
            body.append("Account Number: ").append(incident.getCustomerAccountNumber()).append("\n");
        }
        return body.toString();
    }
}
