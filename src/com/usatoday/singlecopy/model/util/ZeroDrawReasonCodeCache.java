/*
 * Created on Jun 8, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.util;

import java.util.Collection;
import java.util.HashMap;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.integration.ZeroDrawReasonCodeDAO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ZeroDrawReasonCodeIntf;
import com.usatoday.singlecopy.model.transferObjects.ZeroDrawReasonCodeTO;

/**
 * @author aeast
 * @date Jun 8, 2007
 * @class ZeroDrawReasonCodeCache
 * 
 * Caches the set of zero draw reason codes
 * 
 */
public class ZeroDrawReasonCodeCache {

    private static ZeroDrawReasonCodeCache _instance = null;
    
    private HashMap<String, ZeroDrawReasonCodeIntf> descriptions = null;
    private DateTime cacheResetTime = null;
    

    /**
     * 
     */
    private ZeroDrawReasonCodeCache() {
        super();
        
        this.descriptions = new HashMap<String, ZeroDrawReasonCodeIntf>();
        Collection<ZeroDrawReasonCodeTO> zTO = null;
        try {
            ZeroDrawReasonCodeDAO dao = new ZeroDrawReasonCodeDAO();
            
            zTO = dao.fetchZeroDrawReasonCodes();
            
            for (ZeroDrawReasonCodeTO zIntf : zTO) {
                this.descriptions.put(zIntf.getCode(), zIntf);            	
            }
            this.cacheResetTime = new DateTime().plusDays(1);
        }
        catch (Exception e) {
            System.out.println("Failed to load Product Descriptions Cache. " + e.getMessage());
        }

    }

    public static synchronized final ZeroDrawReasonCodeCache getInstance() {
        if (ZeroDrawReasonCodeCache._instance == null) {
            ZeroDrawReasonCodeCache._instance = new ZeroDrawReasonCodeCache();
        }
        
        if (ZeroDrawReasonCodeCache._instance.cacheResetTime.isBeforeNow()) {
            System.out.println("Resetting Zero Draw Reason Code Cache.");
            ZeroDrawReasonCodeCache._instance = new ZeroDrawReasonCodeCache();
        }
        return ZeroDrawReasonCodeCache._instance;
    }
    
    public Collection<ZeroDrawReasonCodeIntf> getZeroDrawReasonCodes() {
        return this.descriptions.values();
    }
    
    public String getDescription(String code) {
        try {
            return this.descriptions.get(code).getDescription();
        }
        catch (Exception e) {
            return "Code Not Found";
        }
    }
    
    /**
     * 
     * @param code
     * @return true if the code exists, otherwise false
     */
    public boolean containsCode(String code) {
        return this.descriptions.containsKey(code);
    }
    
    public void resetCache() {
        ZeroDrawReasonCodeCache._instance = null;
        ZeroDrawReasonCodeCache._instance = new ZeroDrawReasonCodeCache();
    }
}
