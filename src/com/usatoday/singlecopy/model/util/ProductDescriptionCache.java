/*
 * Created on May 7, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.integration.ProductDescriptionDAO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductDescriptionIntf;
import com.usatoday.singlecopy.model.transferObjects.ProductDescriptionTO;

/**
 * @author aeast
 * @date May 7, 2007
 * @class ProductDescriptionCache
 * 
 * Caches the colllection of product order descriptions
 * 
 */
public class ProductDescriptionCache {

    private static ProductDescriptionCache _instance = null;
    
    private HashMap<String, ProductDescriptionIntf> productDescriptions = null;
    private ProductDescriptionIntf defaultDescription = null;
    
    private DateTime cacheResetTime = null;
    
    /**
     * 
     */
    private ProductDescriptionCache() {
        super();
        
        this.productDescriptions = new HashMap<String, ProductDescriptionIntf>();
        Collection<ProductDescriptionTO> pdTO = null;
        try {
            ProductDescriptionDAO dao = new ProductDescriptionDAO();
            
            pdTO = dao.fetchProductDescriptions();
            ProductDescriptionTO defaultDes = new ProductDescriptionTO();
            defaultDes.setAbbreviation("Unknwn");
            defaultDes.setDescription("No Publication Description Available.");
            defaultDes.setProductCode("XX");
            this.defaultDescription = defaultDes;
            
            Iterator<ProductDescriptionTO> itr = pdTO.iterator();
            while (itr.hasNext()) {
                ProductDescriptionIntf pdIntf = (ProductDescriptionIntf) itr.next();
                
                this.productDescriptions.put(pdIntf.getProductCode(), pdIntf);
            }
            
            cacheResetTime = new DateTime().plusDays(1);
        }
        catch (Exception e) {
            System.out.println("Failed to load Product Descriptions Cache. " + e.getMessage());
        }
    }

    /**
     * 
     * @return 
     */
    public static synchronized final ProductDescriptionCache getInstance() {
        if (ProductDescriptionCache._instance == null) {
            ProductDescriptionCache._instance = new ProductDescriptionCache();
        }
        return ProductDescriptionCache._instance;
    }
    
    /**
     * 
     * @param product
     * @return The ProductDescriptionIntf object for the requested product.
     */
    public ProductDescriptionIntf getProductDesription(String product) {
        ProductDescriptionIntf pd = null;
        
        if (this.cacheResetTime.isBeforeNow()) {
            System.out.println("Resetting Product Description Cache");
            this.resetCache();
        }
        
        pd = this.productDescriptions.get(product);
        
        if (pd == null) {
            pd = this.defaultDescription;
        }
        
        return pd;
    }
    
    /**
     * 
     * @return Collection of ProductDescriptionIntf objects
     */
    public Collection<ProductDescriptionIntf> getAllDescriptions() {
        if (this.cacheResetTime.isBeforeNow()) {
            System.out.println("Resetting Product Description Cache");
            this.resetCache();
        }
        
        return this.productDescriptions.values();
    }

    /**
     * This method resets the instance variable to a new object.
     *
     */
    public void resetCache() {
    	ProductDescriptionCache newCache = new ProductDescriptionCache();
        synchronized (ProductDescriptionCache._instance) {
            
            ProductDescriptionCache._instance = newCache;

            this.productDescriptions.clear();
            this.productDescriptions = newCache.productDescriptions;
            
            this.cacheResetTime = newCache.cacheResetTime;
        }
    }
}
