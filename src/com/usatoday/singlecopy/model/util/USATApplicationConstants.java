/*
 * Created on Apr 9, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.util;

import java.util.Properties;

/**
 * @author aeast
 * @date Apr 9, 2007
 * @class USATApplicationConstants
 * 
 * 
 * 
 */
public class USATApplicationConstants {
    // Application Defaults
    public static boolean debug = false;
    public static String as400Server = null;
    // milliseconds between as400 status checks if doing them
    public static long backEndCheckFrequency = 600000;
    
    // alarms
    public static String alarmRecipients = "";
    public static boolean alarmsEnabled = true;
    public static String alarmSender = "";
    public static String smtpServer = null;
    public static boolean iQuestUpdate = true;
    
    public static boolean webAnalyticsActive = false;
    
    public static String marketNotifySenderEmail = "incorres@usatoday.com";

    /**
     * 
     */
    public USATApplicationConstants() {
        super();
    }

    public static void loadProperties() {
        try {
            Properties props = new USATApplicationPropertyFileLoader().loadapplicationProperties();
            
            String tempStr = null;
            tempStr = props.getProperty("com.usatoday.runtime.debug");
            if (tempStr != null && tempStr.trim().equalsIgnoreCase("true")){
                USATApplicationConstants.debug = true;
            }
            else {
                USATApplicationConstants.debug = false;
            }

            tempStr = props.getProperty("com.usatoday.AS400.server");
            if (tempStr != null && tempStr.trim().length() > 0){
                USATApplicationConstants.as400Server = tempStr.trim();
            }
            else {
                USATApplicationConstants.as400Server = "";
            }

            tempStr = props.getProperty("com.usatoday.AS400.server.checkFrequency");
            if (tempStr != null && tempStr.trim().length() > 0){
                try {
                    USATApplicationConstants.backEndCheckFrequency = Long.parseLong(tempStr.trim());
                    if (USATApplicationConstants.backEndCheckFrequency < 1000)  {
                        USATApplicationConstants.backEndCheckFrequency = 60000; // if unaceptable go to default
                    }
                }
                catch (Exception e) {
                    ; // ignore and use default of one minute
                }
            }
            else {
                USATApplicationConstants.as400Server = "";
            }

            tempStr = props.getProperty("com.usatoday.mail.alertreceiveremail");
            if (tempStr != null && tempStr.trim().length() > 0){
                USATApplicationConstants.alarmRecipients = tempStr.trim();
            }
            else {
                USATApplicationConstants.as400Server = "";
            }
            
            tempStr = props.getProperty("com.usatoday.mail.enablealerts");
            if (tempStr != null && tempStr.trim().equalsIgnoreCase("true")){
                USATApplicationConstants.alarmsEnabled = true;
            }
            else {
                USATApplicationConstants.alarmsEnabled = false;
            }
                        
            tempStr = props.getProperty("com.usatoday.web.analytics");
            if (tempStr != null && tempStr.trim().equalsIgnoreCase("true")){
                USATApplicationConstants.webAnalyticsActive = true;
            }
            else {
                USATApplicationConstants.webAnalyticsActive = false;
            }

            tempStr = props.getProperty("com.usatoday.mail.alertsender");
            if (tempStr != null && tempStr.trim().length() > 0){
                USATApplicationConstants.alarmSender = tempStr.trim();
            }
            else {
                USATApplicationConstants.alarmSender = "aeast@usatoday.com";
            }
            
            tempStr = props.getProperty("com.usatoday.mail.smtpserver");
            if (tempStr != null && tempStr.trim().length() > 0){
                USATApplicationConstants.smtpServer = tempStr.trim();
            }
            else {
                USATApplicationConstants.smtpServer = null;
            }

            tempStr = props.getProperty("com.usatoday.iQuest.database.update");
            if (tempStr != null && tempStr.trim().equalsIgnoreCase("true")){
                USATApplicationConstants.iQuestUpdate = true;
            }
            else {
                USATApplicationConstants.iQuestUpdate = false;

            }

            tempStr = props.getProperty("com.usatoday.mail.marketNotifyFromEmail");
            if (tempStr != null && tempStr.trim().length() > 0){
                USATApplicationConstants.marketNotifySenderEmail = tempStr.trim();
            }
            
        }    
        catch (Exception e) {
            System.out.println("FAILED TO LOAD APPLICATION PROPERTIES");
            e.printStackTrace();
        }
    }
}
