/*
 * Created on Mar 13, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.UsatException;
import com.usatoday.singlecopy.model.bo.drawmngmt.ProductOrderTypeBO;
import com.usatoday.singlecopy.model.integration.ProductOrderTypeDAO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderTypeIntf;
import com.usatoday.singlecopy.model.transferObjects.ProductOrderTypeTO;

/**
 * @author aeast
 * @date Mar 13, 2007
 * @class ProductOrderTypesCache
 * 
 * 
 * 
 */
public class ProductOrderTypesCache {

    private DateTime cacheResetTime = null;
    HashMap<String, ProductOrderTypeIntf> productOrderTypes = new HashMap<String, ProductOrderTypeIntf>();
    private static ProductOrderTypesCache _cache = null;
    
    public synchronized static final ProductOrderTypesCache getProductOrderTypesCache() {
        if (ProductOrderTypesCache._cache == null) {
            ProductOrderTypesCache._cache = new ProductOrderTypesCache();
        }
        return ProductOrderTypesCache._cache;
    }
    /**
     * 
     */
    private ProductOrderTypesCache() {
        super();
        
        this.cacheResetTime = new DateTime().plusDays(1);
        this.loadCache();
    }
    
    public ProductOrderTypeIntf getProductOrderType(String key) {
        ProductOrderTypeIntf p = null;
        
        if (this.cacheResetTime.isBeforeNow()) {
            System.out.println("Resetting Product Order Type Cache");
            this.loadCache();
        }

        p = this.productOrderTypes.get(key);
        
        if (p==null) {
            String pubCode = key.substring(0,2);
            // get the unknown one
            p = this.productOrderTypes.get(pubCode + "??");
        }

        return p;
    }

    public ProductOrderTypeIntf getProductOrderType(String pubCode, String productOrderTypeCode) {
        ProductOrderTypeIntf p = null;
        
        if (this.cacheResetTime.isBeforeNow()) {
            System.out.println("Resetting Product Order Type Cache");
            this.loadCache();
        }

        p = this.productOrderTypes.get(pubCode + productOrderTypeCode);
        if (p==null) {
            // get the unknown one
            p = this.productOrderTypes.get(pubCode + "??");
        }
        
        return p;
    }

    /**
     * 
     * @return The number of product order types in the cache
     */
    public int getCacheSize() {
        return this.productOrderTypes.size();
    }
    
    private void loadCache() {
        this.productOrderTypes.clear();
        
        this.cacheResetTime = new DateTime().plusDays(1);
        
        ProductOrderTypeDAO dao = new ProductOrderTypeDAO();
        
        try {
            Collection<ProductOrderTypeTO> toCollection = dao.fetchProductOrderTypes();
            
            // Convert transfer objects to business objects
            Iterator<ProductOrderTypeTO> itr = toCollection.iterator();
            while (itr.hasNext()) {
                ProductOrderTypeIntf potIntf = (ProductOrderTypeIntf)itr.next();
                ProductOrderTypeBO pot = new ProductOrderTypeBO(potIntf);
                this.productOrderTypes.put(pot.getKey(), pot);                
            }
            // unkown ones
            ProductOrderTypeBO pot = null;
            pot = new ProductOrderTypeBO();
            pot.setDescription("Unknown");
            pot.setProductCode("UT");
            pot.setProductOrderTypeCode("??");
            this.productOrderTypes.put(pot.getKey(), pot);
            pot = new ProductOrderTypeBO();
            pot.setDescription("Unknown");
            pot.setProductCode("BW");
            pot.setProductOrderTypeCode("??");
            this.productOrderTypes.put(pot.getKey(), pot);
        }
        catch (UsatException ue) {
            // failed to load product order types ... send alert
            ue.printStackTrace();
        }
        
   }
    
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        Collection<ProductOrderTypeIntf> poc = this.productOrderTypes.values();
        
        StringBuffer outStr = new StringBuffer("Product Order Type Cache (" + this.productOrderTypes.size() + ") :\n================================\n");
        Iterator<ProductOrderTypeIntf> itr = poc.iterator();
        while (itr.hasNext()) {
            ProductOrderTypeIntf pot = itr.next();
            outStr.append("\tPUB: ").append(pot.getProductCode()).append(" ID: ").append(pot.getProductOrderTypeCode()).append("  DESCRIPTION: ").append(pot.getDescription()).append("\n");
        }
        return outStr.toString();
    }
    
    public void resetCache() {
        ProductOrderTypesCache._cache = null;
        ProductOrderTypesCache._cache = new ProductOrderTypesCache();
    }
}
