/*
 * Created on Apr 21, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author aeast
 * @date Apr 21, 2006
 * @class USATApplicationPropertyFileLoader
 * 
 * This class loads property files from the classpath or the file system.
 * 
 */
public class USATApplicationPropertyFileLoader {

    /**
     * This method attempts to load the usat_sc_application.properties file from one of 
     * three places in the followig order:
     * 1.   From the file system in /USAT/extranet/usat_sc_application.properties
     * 2.   /com/usatoday/singlecopy folder of classpath
     * 3.   current directory that the USATApplicationPropertyFileLoader was loaded from
     * 4.   From the /WEB-INF folder
     * 
     * @return The application properties
     * @throws Exception
     */
    public Properties loadapplicationProperties() throws Exception {
        Properties config = new Properties();

        // attempt to load from file system
        File f = null;
        InputStream s = null;
        String path = System.getProperty("com.usatoday.singlecopy.usatAppPropertiesFile");
        if (path != null && path.trim().length() > 0) {
            f = new File(path.trim());
	        if (f.exists()) {
	            s = new FileInputStream(f);
	            System.out.println("Using File System Property File From system property: com.usatoday.singlecopy.usatAppPropertiesFile: " + f.getAbsolutePath());
	        }
        }
        if (s == null){
            f = new File("/USAT/extranet/usat_sc_application.properties");
	        if (f.exists()) {
	            s = new FileInputStream(f);
	            System.out.println("Using File Default System Property File From system property:: " + f.getAbsolutePath());
	        }
        }

        // check in class path /com/usatoday/singlecopy for the file
        if (s == null) {
            s = this.getClass().getResourceAsStream("/com/usatoday/singlecopy/usat_sc_application.properties");
            // look in the location where this class is loaded from.
            if (s == null) {
                s = this.getClass().getResourceAsStream("usat_sc_application.properties");
	            if (s == null) {
	                // try in a WEB-INF folder
	                s = this.getClass().getResourceAsStream("/WEB-INF/usat_sc_application.properties");
	                if (s != null) {
	                    System.out.println("Using property file '/WEB-INF/usat_sc_application.properties'");
	                }
	            }
	            else {
	                System.out.println("Using property file :" + this.getClass().getPackage() + "/usat_sc_application.properties");
	            }
            } 
            else {
                System.out.println("Using property file /com/usatoday/usat_sc_application.properties");
            }
        }
            
        config.load(s);
        return config;
    }

    /**
     * 
     * @param file The full path (within the class path) of the property file to be loaded
     *             If you do not begin with / then the current directory will be searched
     *             If no file is found in the classpath, the file system will be checked for
     *             file.
     * @return
     * @throws Exception
     */
    public Properties loadPropertyFile(String file) throws Exception {
        Properties config = new Properties();
        // first check in class path /com/usatoday for the file
        InputStream s = this.getClass().getResourceAsStream(file);
        if (s == null) {
            File f = new File(file);
            if (f.exists()) {
                s = new FileInputStream(f);
                System.out.println("Loading File System Property File: " + f.getAbsolutePath());
            }
        }
        config.load(s);
        return config;
    }    
}
