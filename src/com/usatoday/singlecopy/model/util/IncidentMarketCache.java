/*
 * Created on May 8, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.util;

import java.util.Collection;
import java.util.HashMap;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.integration.IncidentMarketDAO;
import com.usatoday.singlecopy.model.integration.IncidentMarketTO;

/**
 * @author aeast
 * @date May 8, 2008
 * @class MarketCache
 * 
 * 
 * 
 */
public class IncidentMarketCache {

    private static IncidentMarketCache _instance = null;
    
    private HashMap<String, IncidentMarketTO> markets = null;
    private DateTime cacheResetTime = null;
    
    
    /**
     * 
     */
    private IncidentMarketCache() {
        super();

        this.markets = new HashMap<String, IncidentMarketTO>();
  
        try {
            IncidentMarketDAO marketDAO = new IncidentMarketDAO(); 
            Collection<IncidentMarketTO> marketList = marketDAO.fetchMarkets();
            
            for (IncidentMarketTO m : marketList) {
            	this.markets.put(m.getMarketID(), m);
            }
            
            cacheResetTime = new DateTime().plusDays(1);
        }
        catch (Exception e) {
            System.out.println("Failed to load Product Descriptions Cache. " + e.getMessage());
        }
        
    }

    /**
     * 
     * @return 
     */
    public static synchronized final IncidentMarketCache getInstance() {
        if (IncidentMarketCache._instance == null) {
            IncidentMarketCache._instance = new IncidentMarketCache();
        }
        return IncidentMarketCache._instance;
    }
    
    /**
     * This method clears and reloads the data from the data store.
     *
     */
    public void resetCache() {
        synchronized (IncidentMarketCache._instance) {
            //this.markets.clear();
            IncidentMarketCache._instance = null;

            IncidentMarketCache.getInstance();
        }
    }
    
    public IncidentMarketTO getMarket(String marketID) {
        IncidentMarketTO m = null;
        
        if (this.cacheResetTime.isBeforeNow()) {
            System.out.println("Resetting Incident Market Cache");
            this.resetCache();
            this.cacheResetTime = this.cacheResetTime.plusDays(1);            
        }
        
        if (!this.markets.isEmpty()) {
            m = this.markets.get(marketID);
        }
        
        return m;
    }
}
