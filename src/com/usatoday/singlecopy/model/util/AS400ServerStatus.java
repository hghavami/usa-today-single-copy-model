/*
 * Created on Oct 19, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.util;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400JPing;

/**
 * @author aeast
 * @date Oct 19, 2006
 * @class AS400ServerStatus
 * 
 * This class handles pinging various AS/400 service ports to see if they are running.
 * 
 */
public class AS400ServerStatus {

    private String serverName = "";
    
    /**
     * 
     */
    public AS400ServerStatus() {
        super();
    }

    public boolean isCommandServiceUp() {
        return this.serviceUp(AS400.COMMAND, false);
    }

    public boolean isSSLCommandServiceUp() {
        return this.serviceUp(AS400.COMMAND, true);
    }
    
    public boolean isJDBCServiceUp() {
        return this.serviceUp(AS400.DATABASE, false);
    }

    public boolean isSSLJDBCServiceUp() {
        
        return this.serviceUp(AS400.DATABASE, true);
    }

    public boolean isDataQueueServiceUp() {
        return this.serviceUp(AS400.DATAQUEUE, false);
    }

    public boolean isSSLDataQueueServiceUp() {
        return this.serviceUp(AS400.DATAQUEUE, true);
    }

    public boolean isIFSAccessServiceUp() {
        return this.serviceUp(AS400.FILE, false);
    }    

    public boolean isSSLIFSAccessServiceUp() {
        return this.serviceUp(AS400.FILE, true);
    }    

    public boolean isRecordLevelAccessServiceUp() {
        return this.serviceUp(AS400.RECORDACCESS, false);
    }    

    public boolean isSSLRecordLevelAccessServiceUp() {
        return this.serviceUp(AS400.RECORDACCESS, true);
    }    
    
    public boolean isASCentralServiceUp() {
    	return this.serviceUp(AS400.CENTRAL, true);
    }

    public boolean isSSLASCentralServiceUp() {
    	return this.serviceUp(AS400.CENTRAL, true);
    }

    public boolean isSignOnServiceUp() {
    	return this.serviceUp(AS400.SIGNON, true);
    }
    
    public boolean isSSLSignOnServiceUp() {
    	return this.serviceUp(AS400.SIGNON, true);
    }

    /**
     * @return Returns the serverName.
     */
    public String getServerName() {
        return this.serverName;
    }
    /**
     * @param serverName The serverName to set.
     */
    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    /**
     * 
     * @param service
     * @param ssl
     * @return
     */
    private boolean serviceUp(int service, boolean ssl) {
    	try {
    		AS400JPing pinger = new AS400JPing(this.serverName, service, ssl);
    		return pinger.ping();
    	}
    	catch (Exception e) {
    		System.out.println("Exception trying to ping iSeries: Server:  " + this.serverName + "  Servcie: " + service + "  over ssl:" + ssl + "  Error Detail: "+ e.getMessage());
    		return false;
		}
    }
    
    
}
