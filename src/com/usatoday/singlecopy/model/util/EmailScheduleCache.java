/*
 * Created on May 8, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.integration.EmailScheduleSentTimeDAO;
import com.usatoday.singlecopy.model.interfaces.EmailScheduleSentTimeIntf;
import com.usatoday.singlecopy.model.transferObjects.EmailScheduleSentTimeTO;

/**
 * @author hghavami
 * @date May 21, 2008
 * @class EmailScheduleSentTimeCache
 * 
 * This class interfaces with the database to work with email schedule sent time cache data
 * 
 */
public class EmailScheduleCache {

    private static EmailScheduleCache _instance = null;
    
    private HashMap<String, EmailScheduleSentTimeIntf> scheduleTimes = null;
    private DateTime cacheResetTime = null;
    
    
    /**
     * 
     */
    private EmailScheduleCache() {
        super();
        this.loadCache();
    }

    /**
     * 
     * @return 
     */
    public static synchronized final EmailScheduleCache getInstance() {
        if (EmailScheduleCache._instance == null) {
            EmailScheduleCache._instance = new EmailScheduleCache();
        }
        return EmailScheduleCache._instance;
    }
    
    /**
     * This method clears and reloads the data from the data store.
     *
     */
    public void resetCache() {
        this.loadCache();
    }
    
    public EmailScheduleSentTimeIntf getScheduledTime(String scheduleID) {
        EmailScheduleSentTimeIntf m = null;
        
        if (this.cacheResetTime.isBeforeNow() || this.scheduleTimes == null || this.scheduleTimes.size() == 0) {
            System.out.println("Resetting Email Schedule Cache");
            this.loadCache();
        }
        
        if (!this.scheduleTimes.isEmpty()) {
            m = this.scheduleTimes.get(scheduleID);
        }
        
        return m;
    }
    
    /**
     * 
     * @return
     */
    public Collection<EmailScheduleSentTimeIntf> getScheduleTimes() {
        ArrayList<EmailScheduleSentTimeIntf> a = null;
        
        if (this.cacheResetTime.isBeforeNow() || this.scheduleTimes == null || this.scheduleTimes.size() == 0) {
            System.out.println("Resetting Email Schedule Cache");
            this.loadCache();
        }
        
        if (this.scheduleTimes != null) {
            a = new ArrayList<EmailScheduleSentTimeIntf>(this.scheduleTimes.values());
        }
        else {
            a = new ArrayList<EmailScheduleSentTimeIntf>();
        }
        return a;
    }
    
    private void loadCache() {
    	HashMap<String, EmailScheduleSentTimeIntf> oldMap = this.scheduleTimes;
    	HashMap<String, EmailScheduleSentTimeIntf> newMap = new HashMap<String, EmailScheduleSentTimeIntf>();  
        
        EmailScheduleSentTimeDAO esstDAO = new EmailScheduleSentTimeDAO();
        
        try {
            Collection<EmailScheduleSentTimeTO> scheduleTimesList = esstDAO.fetchEmailScheduleSentTimes();
            
            for (EmailScheduleSentTimeIntf esstIntf : scheduleTimesList) {
            	newMap.put(esstIntf.getScheduleCode(), esstIntf);            	
            }

            this.scheduleTimes = newMap; 
            
            cacheResetTime = new DateTime().plusDays(1);
            
            oldMap.clear();
        }
        catch (Exception e) {
            System.out.println("Failed to load Email Schedule Sent Time Cache. " + e.getMessage());
            cacheResetTime = new DateTime().plusDays(1);
        }
    }
}
