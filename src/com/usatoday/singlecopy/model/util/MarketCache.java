/*
 * Created on May 8, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.model.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.bo.markets.MarketBO;
import com.usatoday.singlecopy.model.interfaces.markets.MarketIntf;

/**
 * @author aeast
 * @date May 8, 2008
 * @class MarketCache
 * 
 * 
 * 
 */
public class MarketCache {

    private static MarketCache _instance = null;
    
    private HashMap<String, MarketIntf> markets = null;
    private DateTime cacheResetTime = null;
    
    
    /**
     * 
     */
    private MarketCache() {
        super();

        this.markets = new HashMap<String, MarketIntf>();
        
        try {
            Collection<MarketIntf> marketList = MarketBO.fetchMarkets();
            
            for (MarketIntf m : marketList) {
            	this.markets.put(m.getID(), m);
            }
            
            cacheResetTime = new DateTime().plusDays(1);
        }
        catch (Exception e) {
            System.out.println("Failed to load Product Descriptions Cache. " + e.getMessage());
        }
        
    }

    /**
     * 
     * @return 
     */
    public static synchronized final MarketCache getInstance() {
        if (MarketCache._instance == null) {
            MarketCache._instance = new MarketCache();
        }
        return MarketCache._instance;
    }
    
    /**
     * This method clears and reloads the data from the data store.
     *
     */
    public void resetCache() {
        synchronized (MarketCache._instance) {
            this.markets.clear();
            MarketCache._instance = null;

            MarketCache.getInstance();
        }
    }
    
    public MarketIntf getMarket(String marketID) {
        MarketIntf m = null;
        
        if (this.cacheResetTime.isBeforeNow()) {
            System.out.println("Resetting Market Cache");
            this.resetCache();
            this.cacheResetTime = this.cacheResetTime.plusDays(1);
        }
        
        if (!MarketCache._instance.markets.isEmpty()) {
            m = MarketCache._instance.markets.get(marketID);
        }
        
        return m;
    }
    
    /**
     * 
     * @return
     */
    public Collection<MarketIntf> getMarkets() {
        ArrayList<MarketIntf> a = null;
        
        if (this.cacheResetTime.isBeforeNow()) {
            System.out.println("Resetting Market Cache");
            this.resetCache();
            this.cacheResetTime = this.cacheResetTime.plusDays(1);
        }

        if (MarketCache._instance.markets != null) {
            a = new ArrayList<MarketIntf>(MarketCache._instance.markets.values());
        }
        else {
            a = new ArrayList<MarketIntf>();
        }
        return a;
    }
}
