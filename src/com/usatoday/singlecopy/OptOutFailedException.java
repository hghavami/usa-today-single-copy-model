/*
 * Created on May 16, 2008
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy;

import java.util.Collection;

import com.usatoday.singlecopy.model.interfaces.users.GenericUpdateErrorIntf;

/**
 * @author aeast
 * @date May 16, 2008
 * @class OptOutFailedException
 * 
 * 
 * 
 */
public class OptOutFailedException extends UsatException {

    
    /**
	 * 
	 */
	private static final long serialVersionUID = 9007774915273953601L;
	Collection<GenericUpdateErrorIntf> errorMessages = null;
    /**
     * 
     */
    public OptOutFailedException() {
        super();
    }

    /**
     * @param message
     */
    public OptOutFailedException(String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public OptOutFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public OptOutFailedException(Throwable cause) {
        super(cause);
    }

    public Collection<GenericUpdateErrorIntf> getErrorMessages() {
        return this.errorMessages;
    }
    public void setErrorMessages(Collection<GenericUpdateErrorIntf> errorMessages) {
        this.errorMessages = errorMessages;
    }
}
