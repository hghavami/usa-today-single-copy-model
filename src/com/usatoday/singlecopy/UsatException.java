package com.usatoday.singlecopy;

public class UsatException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    public UsatException() {
        super();
    }
    public UsatException(String message) {
        super(message);
    }
    public UsatException(String message, Throwable cause) {
        super(message, cause);
    }
    public UsatException(Throwable cause) {
        super(cause);
    }
}
