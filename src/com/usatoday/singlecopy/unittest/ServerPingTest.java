/*
 * Created on May 11, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.unittest;

import com.usatoday.singlecopy.model.util.AS400ServerStatus;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;

/**
 * @author aeast
 * @date May 11, 2007
 * @class ServerPingTest
 * 
 * 
 * 
 */
public class ServerPingTest {

    /**
     * 
     */
    public ServerPingTest() {
        super();
    }

    public static void main(String[] args) {
        
        try {
            USATApplicationConstants.loadProperties();
            
            
           

            AS400ServerStatus serverStat = new AS400ServerStatus();
            serverStat.setServerName(USATApplicationConstants.as400Server);
            
            System.out.println(" JDBC Available: " + serverStat.isJDBCServiceUp());
            System.out.println(" SSL JDBC Available: " + serverStat.isSSLJDBCServiceUp());
            System.out.println(" Command Service Available: " + serverStat.isCommandServiceUp());
            System.out.println(" SSL Command Service Available: " + serverStat.isSSLCommandServiceUp());
            System.out.println(" DataQ Service Available: " + serverStat.isDataQueueServiceUp());
            System.out.println(" SSL DataQ Service Available: " + serverStat.isSSLDataQueueServiceUp());
            System.out.println(" IFS Service Available: " + serverStat.isIFSAccessServiceUp());
            System.out.println(" SSL IFS Service Available: " + serverStat.isSSLIFSAccessServiceUp());
            System.out.println(" Record Level Access Available: " + serverStat.isRecordLevelAccessServiceUp());
            System.out.println(" SSL Record Level Access Available: " + serverStat.isSSLRecordLevelAccessServiceUp());
            
        }
        catch (Exception e) {
            System.out.println("Exception caught during test: " + e.getMessage());
            e.printStackTrace();
        }
        
    }
}
