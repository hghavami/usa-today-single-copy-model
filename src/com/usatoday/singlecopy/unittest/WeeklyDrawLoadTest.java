/*
 * Created on Apr 10, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.unittest;

import java.util.ArrayList;

import com.usatoday.singlecopy.model.integration.ConnectionManager;

/**
 * @author aeast
 * @date Apr 10, 2007
 * @class WeeklyDrawLoadTest
 * 
 * 
 * 
 */
public class WeeklyDrawLoadTest {

    //

    public static final String[] mktids = {"25","25","25","25","05","05","05","05","05","05","05","25","05","25","25","05","05","25","05","05","25","05","05","25","25","05","25","05","25","05","25","05","25","25","25","05","25","05","25","25","05","25","05","25","05","25","25","05","05","05","05","25","05","25","25","25","05","25","05","05","25","05","25","05","05","05","05","25","25","25","25","25","25","05","25","25","25","05","05","05","25","25","25","05","05","05","25","25","05","25","05","25","05","05","05","25","05","05","25","25","25","05","05","05","05","05","05","25","25","05","25","05","25","05","25","05","05","25","05","25","05","25","05","25","25","25","25","25","25","05","05","25","05","25","25","05","05","05","25","05","25","25","25","25","25","25","25","05","05","25","25","25","05","05","05","05","05","25","25","05","25","25","25","05","25","25","05","25","05","25","25","25","05","05","05","25","05","05","25","25","05","05","25","05","05","05","05","25","25","25","05","05","25","05","25","05","25","05","25","05","05","05","25","25","05","25","25","05","05","25","05","05","25","25","05","25","25","25","05","25","25","25","05","25","25","25","25","05","25","05","25","05","25","25","05","05","05","25","25","05","25","05","25","25","25","05","25","05","25"};
    public static final String[] dstids = {"303","401","301","306","499","315","410","450","301","210","440","203","470","202","302","316","324","401","260","321","403","499","500","303","104","498","513","320","305","324","301","310","402","101","200","450","203","500","101","303","500","202","310","201","210","204","104","400","210","405","460","610","300","610","301","401","322","515","300","301","306","470","850","500","312","301","405","301","104","402","200","104","516","909","202","104","515","318","321","470","515","104","305","499","260","320","201","306","324","202","470","101","410","400","909","401","270","500","201","202","822","324","318","313","405","315","410","850","204","300","101","405","822","400","204","260","499","203","450","403","270","204","460","301","850","401","303","204","404","310","270","301","310","103","306","324","310","500","202","270","305","204","513","305","305","303","104","405","405","201","302","302","300","310","310","499","550","105","515","460","203","403","201","500","302","200","405","105","405","204","301","101","210","321","310","305","440","550","850","303","260","323","204","500","500","301","310","403","302","513","400","470","850","318","202","450","302","301","301","260","301","499","303","200","320","200","105","310","260","303","325","315","850","302","313","202","610","302","405","306","202","303","312","301","822","403","104","324","402","440","104","300","404","103","440","440","460","301","303","201","401","420","202","402","202","470","104","410","103"};
    public static final String[] rteids = {"062","051","707","041","087","002","352","545","581","500","215","110","806","960","600","213","610","400","321","120","401","075","031","750","501","106","200","861","503","652","282","230","294","321","001","536","210","019","338","160","038","964","400","312","770","175","386","350","970","018","495","161","092","146","702","054","454","400","568","306","292","705","841","033","005","670","019","107","395","016","581","275","430","084","962","350","166","006","516","901","165","401","805","074","126","549","273","040","654","961","403","310","353","145","177","557","425","005","192","955","948","656","004","015","209","004","300","633","380","540","325","006","524","115","400","305","076","180","520","864","795","350","888","700","880","056","065","134","827","241","508","280","580","909","025","661","254","006","956","420","502","335","500","744","634","734","200","055","206","270","246","226","089","107","800","077","005","007","855","887","130","851","327","004","743","465","361","009","007","102","735","323","450","515","256","636","220","035","801","551","350","558","330","030","017","660","237","850","980","801","025","630","860","003","287","535","285","295","281","335","580","072","034","265","862","450","008","831","360","783","009","016","850","590","012","957","154","746","002","009","215","055","003","730","316","860","365","651","998","450","102","088","820","285","405","321","885","704","061","722","052","319","600","038","963","903","104","305","902"};

    ArrayList<Thread> retrieveDrawThreadPool = new ArrayList<Thread>();
    ArrayList<WeeklyDrawTest> retrieveDrawPool = new ArrayList<WeeklyDrawTest>();

    int numberOfThreads = 10;

    int lastIndexUsed = 0;
    
    String retrieveDrawOutput = "";
    
    String emailAddres = "aeast";
    
    ConnectionManager connectionManager = null;
    
    /**
     * 
     */
    public WeeklyDrawLoadTest() {
        super();
    }
    
    public void runLoadTest() {
        retrieveDrawThreadPool.clear();
        retrieveDrawPool.clear();
        retrieveDrawOutput = "";
        
        
        for (int i = 0; i < this.numberOfThreads; i++) {
            WeeklyDrawTest wdt = new WeeklyDrawTest();
            
            String emailAddress = lastIndexUsed + this.getEmailAddres();
            wdt.setEmailAddress(emailAddress);
            wdt.setMarketID(mktids[lastIndexUsed]);
            wdt.setDistrictID(dstids[lastIndexUsed]);
            wdt.setRouteID(rteids[lastIndexUsed]);
            if (this.connectionManager != null) {
                wdt.setConnectionManager(this.connectionManager);
            }
            
            lastIndexUsed++;
            if (lastIndexUsed >= WeeklyDrawLoadTest.mktids.length) {
                lastIndexUsed = 0;
            }
            Thread t = new Thread(wdt);
            
            retrieveDrawThreadPool.add(t);
            
            retrieveDrawPool.add(wdt);
        }
        
        // Start the threads
        for (int j = 0; j < retrieveDrawThreadPool.size(); j++) {
            Thread t = (Thread)retrieveDrawThreadPool.get(j);
            t.start();
        }

        // join the threads
        for (int j = 0; j < retrieveDrawThreadPool.size(); j++) {
            Thread t = (Thread)retrieveDrawThreadPool.get(j);
            try {
                t.join();
            }
            catch (Exception e) {
                System.out.println("Thread run method interrupted.." + e.getMessage());
            }
        }   
        
        this.generateOutputString();
        
    }

    private void generateOutputString() {
        // join the threads
        StringBuffer outStr = new StringBuffer();
        for (int j = 0; j < retrieveDrawPool.size(); j++) {
            WeeklyDrawTest t = (WeeklyDrawTest)retrieveDrawPool.get(j);
            outStr.append("\tThread: " + j).append(" ").append(t.getExecutionOutput()).append("\n");
        }   
        this.retrieveDrawOutput = outStr.toString();
    }
    
    public int getNumberOfThreads() {
        return this.numberOfThreads;
    }
    public void setNumberOfThreads(int numberOfThreads) {
        if (numberOfThreads > WeeklyDrawLoadTest.mktids.length) {
            this.numberOfThreads = WeeklyDrawLoadTest.mktids.length;
        }
        else {
            this.numberOfThreads = numberOfThreads;
        }
    }
    public String getRetrieveDrawOutput() {
        return this.retrieveDrawOutput;
    }
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }
    public String getEmailAddres() {
        return this.emailAddres;
    }
    public void setEmailAddres(String emailAddres) {
        this.emailAddres = emailAddres;
    }
}
