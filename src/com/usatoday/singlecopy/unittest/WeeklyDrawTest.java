/*
 * Created on Apr 9, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.unittest;

import java.util.Collection;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.integration.ConnectionManager;
import com.usatoday.singlecopy.model.integration.RouteDAO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.transferObjects.LocationTO;
import com.usatoday.singlecopy.model.transferObjects.DrawManagementTO;
import com.usatoday.singlecopy.model.transferObjects.WeeklyProductOrderTO;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;

/**
 * @author aeast
 * @date Apr 9, 2007
 * @class WeeklyDrawTest
 * 
 * 
 * 
 */
public class WeeklyDrawTest implements Runnable {
    
    private long testStartTime = 0;
    private long testEndTime = 0;
    
    private int numLocations = 0;
    
    private String emailAddress = "dhamilto@usatoday.com";
    private String marketID = "25";
    private String districtID = "301";
    private String routeID = "736";
    private String date = "20070304";
    private String productCode = "UT";

    private ConnectionManager connectionManager = null;
    
    private StringBuffer outputMsgs = new StringBuffer();
    /**
     * 
     */
    public WeeklyDrawTest() {
        super();
        
    }

    /**
     * 
     *
     */
    public void runSinglePull() {
        RouteDAO dao = new RouteDAO();
        
        if (this.connectionManager != null) {
            dao.setConnectionManager(this.connectionManager);
        }
        
        DateTime startTime = null;
        DateTime stopTime = null;
        
        try {
            
            startTime = new DateTime();
            this.testStartTime = startTime.getMillis(); 

            
            DrawManagementTO wdmto = dao.fetchDrawByWeek(this.emailAddress, this.marketID, this.districtID, this.routeID, this.date, this.productCode, false);
            Collection<LocationTO> weeklyDrawData = wdmto.getLocations(); 
            this.numLocations = weeklyDrawData.size();
            
            //Collection weeklyDrawData = null;
            //dao.fetchSPLSData();

            stopTime = new DateTime();

            
            // this.outputData(weeklyDrawData);
            
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (stopTime == null) {
                stopTime = new DateTime();
            }
            this.testEndTime = stopTime.getMillis();
            
            long timeToRun = (stopTime.getMillis() - startTime.getMillis());
            
            System.out.println("WeeklyDrawTest:: runSinglePull() => It took " + timeToRun + " milliseconds to pull the weekly draw data. Num Locations: " + numLocations);
            this.outputMsgs.append("\tIt took " + (this.testEndTime - this.testStartTime) + " milliseconds to pull weekly draw for " + this.numLocations + " locations. Email: " + this.emailAddress + " MKT: " + this.marketID + " DST: " + this.districtID + " RTE: " + this.routeID).append("\n");
        }
    }
    
    @SuppressWarnings("unused")
	private void outputData(Collection<LocationTO> weeklyDrawData) {
        Iterator<LocationTO> itr = weeklyDrawData.iterator();
        boolean done = false;
        int count = 0;
        try {
	        while(itr.hasNext() && !done) {
	            LocationTO wdl = (LocationTO)itr.next();
	            count++;
	            
	            System.out.println("Location ID: " + wdl.getLocationID() + " Week Ending Date: " + wdl.getDate().toString("MM/dd/yyyy"));
	            Iterator<ProductOrderIntf> itr2 = wdl.getProductOrders().iterator();
	            while (itr2.hasNext()) {
	                WeeklyProductOrderTO poto = (WeeklyProductOrderTO)(itr2.next());
	                System.out.println("\tProduct Order ID: " + poto.getProductOrderID() + "  Mon Draw: " + poto.getMondayDraw() + " Mon Returns: " + poto.getMondayReturns());
	            }
	            if (count > 50) {
	                done = true;
	            }
	        }
        } catch (Exception e) {
            System.out.println("Failed to output the data: " + e.getMessage());
        }
    }
    
    public static void main(String[] args) {
        
        WeeklyDrawTest wdt = new WeeklyDrawTest();
        
        USATApplicationConstants.loadProperties();
        
        wdt.runSinglePull();
        
    }

    
    public void run() {
        
        this.runSinglePull();

    }
    
    public String getExecutionOutput() {
        return this.outputMsgs.toString();
    }
    public String getDate() {
        return this.date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getDistrictID() {
        return this.districtID;
    }
    public void setDistrictID(String districtID) {
        this.districtID = districtID;
    }
    public String getEmailAddress() {
        return this.emailAddress;
    }
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    public String getMarketID() {
        return this.marketID;
    }
    public void setMarketID(String marketID) {
        this.marketID = marketID;
    }
    public String getProductCode() {
        return this.productCode;
    }
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    public String getRouteID() {
        return this.routeID;
    }
    public void setRouteID(String routeID) {
        this.routeID = routeID;
    }
    public void setConnectionManager(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }
}
