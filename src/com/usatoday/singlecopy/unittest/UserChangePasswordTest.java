/*
 * Created on May 21, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.unittest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
//import java.util.Collection;
//import java.util.Iterator;

import com.usatoday.singlecopy.model.bo.users.UserBO;
//import com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;

/**
 * @author aeast
 * @date May 21, 2007
 * @class UserAuthTest
 * 
 * 
 * 
 */
public class UserChangePasswordTest {

    /**
     * 
     */
    public UserChangePasswordTest() {
        super();
    }

    public static void main(String[] args) {
        USATApplicationConstants.loadProperties();

        UserChangePasswordTest uat = new UserChangePasswordTest();
        
        uat.runInteractiveTest();
       // uat.runNonInteractiveTest();
    }
    
    public void runInteractiveTest() {
        
        System.out.print("Enter email: ");
        
        //      open up standard input 
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 

        String email = null; 

        //  read the username from the command-line; need to use try/catch with the 
        //  readLine() method
        try { 
            email = br.readLine(); 
        } catch (IOException ioe) { 
           System.out.println("IO error trying to read your name!"); 
           System.exit(1); 
        } 

        System.out.print("Enter Password: ");
        
        //      open up standard input 
        br = new BufferedReader(new InputStreamReader(System.in)); 

        String pwd = null; 

        //  read the username from the command-line; need to use try/catch with the 
        //  readLine() method
        try { 
            pwd = br.readLine(); 
        } catch (IOException ioe) { 
           System.out.println("IO error trying to read your name!"); 
           System.exit(1); 
        } 
        
        UserBO user = null;
        try {
            user = UserBO.authenticateUser(email, pwd);
            
            if (user.isUserAuthenticated()) {
                System.out.println("Welcome, " + user.getDisplayName());
            }
            
            System.out.print("Enter new 'Email Address': ");
            
            //      open up standard input 
           // BufferedReader brdr = new BufferedReader(new InputStreamReader(System.in)); 
            br = new BufferedReader(new InputStreamReader(System.in)); 

            String newEmail = null; 

            //  read the username from the command-line; need to use try/catch with the 
            //  readLine() method
            try { 
                newEmail = br.readLine(); 
            } catch (IOException ioe) { 
               System.out.println("IO error trying to read your new Email Address!"); 
               System.exit(1); 
            } 

            System.out.print("Enter new 'Password': ");
            
            //      open up standard input 
           // brdr = new BufferedReader(new InputStreamReader(System.in)); 
            br = new BufferedReader(new InputStreamReader(System.in)); 

            String newPwd = null;
            
             //  read the username from the command-line; need to use try/catch with the 
            //  readLine() method
            try { 
                newPwd = br.readLine(); 
            } catch (IOException ioe) { 
               System.out.println("IO error trying to read your new password!"); 
               System.exit(1); 
            } 
            
           user.setEmailAddress(newEmail); 
           user.setPassword(newPwd);
            
           user.save();
            
            
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
        
        
    
        
    }
    
   
}
