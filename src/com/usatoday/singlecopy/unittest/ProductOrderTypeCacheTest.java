/*
 * Created on Apr 4, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.unittest;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.util.ProductOrderTypesCache;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;

/**
 * @author aeast
 * @date Apr 4, 2007
 * @class ProductOrderTypeCacheTest
 * 
 * This class test the ProductOrderTypeCacheTest class
 * 
 */
public class ProductOrderTypeCacheTest {

    /**
     * 
     */
    public ProductOrderTypeCacheTest() {
        super();
        
    }

    public static void main(String[] args) {
        
        try {
            USATApplicationConstants.loadProperties();
            
            
            DateTime startTime = new DateTime();
            
            ProductOrderTypesCache cache = ProductOrderTypesCache.getProductOrderTypesCache();

            DateTime stopTime = new DateTime();
           

            long timeToRun = (stopTime.getMillis() - startTime.getMillis());
            
            System.out.println(cache.toString());
            
            System.out.println("ProductOrderTypCacheTest:: main() => It took " + timeToRun + " milliseconds to pull the product order type cache.");
        }
        catch (Exception e) {
            System.out.println("Exception caught during test: " + e.getMessage());
            e.printStackTrace();
        }
        
    }
}
