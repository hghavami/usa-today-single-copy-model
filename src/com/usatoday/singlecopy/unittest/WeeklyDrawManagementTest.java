/*
 * Created on Apr 23, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.unittest;

import java.util.Collection;
import java.util.Iterator;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.usatoday.singlecopy.model.bo.drawmngmt.RouteBO;
import com.usatoday.singlecopy.model.bo.users.UserBO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.LocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.ProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.RouteDrawManagementIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyDrawLocationIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyDrawManangementIntf;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.instance.WeeklyProductOrderIntf;
import com.usatoday.singlecopy.model.interfaces.users.UserIntf;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;

/**
 * @author aeast
 * @date Apr 23, 2007
 * @class WeeklyDrawManagementTest
 * 
 * 
 */
public class WeeklyDrawManagementTest {

    UserIntf user = null;
    RouteDrawManagementIntf route = null;
    
    /**
     * 
     */
    public WeeklyDrawManagementTest() {
        super();
    }
    
    public static void main(String[] args) {
        
        WeeklyDrawManagementTest test = new WeeklyDrawManagementTest();
        
        USATApplicationConstants.loadProperties();

        try {
            UserBO u = UserBO.authenticateUser("aeast@usatoday.com", "AEAS101232");
            

            // get the route of intereste market/district/route as key
        	RouteBO r = u.getRoute("05201800");
            

            test.setUser(u);
            test.setRoute(r);
            
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
        test.runTest();
    }   
    
    public void runTest() {

        String tempStr = "20070204";
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMdd");
        DateTime dt = formatter.parseDateTime(tempStr);
        
        try {
            WeeklyDrawManangementIntf wdm = this.route.retrieveWeeklyDraw(dt, "UT", user, false);
            
            System.out.println("Got the Weekly draw: Num Location: " + wdm.getNumberOfLocations());
            
            System.out.println("Total Draw: " + wdm.getTotalDraw() + "  Total Returns: " + wdm.getTotalReturns());
            
            Collection<LocationIntf> locations = wdm.getLocations();
            Iterator<LocationIntf>  itr = locations.iterator();
            while (itr.hasNext()) {
                System.out.println();
                WeeklyDrawLocationIntf loc = (WeeklyDrawLocationIntf)itr.next();
                System.out.println("Location: " + loc.getLocationID() + "  Loc Name: " +loc.getLocationName() + "  Address1: " + loc.getAddress1() + " Seq: " + loc.getSequenceNumber());
                System.out.println("== Product Orders =====================================================================");
                Collection<ProductOrderIntf> pos = loc.getProductOrders();
                Iterator<ProductOrderIntf> poItr = pos.iterator();
                while (poItr.hasNext()) {
                    WeeklyProductOrderIntf wpo = (WeeklyProductOrderIntf)poItr.next();
                    System.out.println(wpo.toString());
                    
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            
        }
        
        
    }

    /**
     * @return Returns the route.
     */
    public RouteDrawManagementIntf getRoute() {
        return this.route;
    }
    /**
     * @param route The route to set.
     */
    public void setRoute(RouteDrawManagementIntf route) {
        this.route = route;
    }
    /**
     * @return Returns the user.
     */
    public UserIntf getUser() {
        return this.user;
    }
    /**
     * @param user The user to set.
     */
    public void setUser(UserIntf user) {
        this.user = user;
    }
}
