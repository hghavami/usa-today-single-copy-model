/*
 * Created on May 21, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.unittest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Iterator;

import com.usatoday.singlecopy.model.bo.users.UserBO;
import com.usatoday.singlecopy.model.interfaces.drawmngmt.RouteIntf;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;

/**
 * @author aeast
 * @date May 21, 2007
 * @class UserAuthTest
 * 
 * 
 * 
 */
public class UserAuthTest {

    /**
     * 
     */
    public UserAuthTest() {
        super();
    }

    public static void main(String[] args) {
        USATApplicationConstants.loadProperties();

        UserAuthTest uat = new UserAuthTest();
        
        uat.runInteractiveTest();
       // uat.runNonInteractiveTest();
    }
    
    public void runInteractiveTest() {
        
        System.out.print("Enter email: ");
        
        //      open up standard input 
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 

        String email = null; 

        //  read the username from the command-line; need to use try/catch with the 
        //  readLine() method
        try { 
            email = br.readLine(); 
        } catch (IOException ioe) { 
           System.out.println("IO error trying to read your name!"); 
           System.exit(1); 
        } 

        System.out.print("Enter Password: ");
        
        //      open up standard input 
        br = new BufferedReader(new InputStreamReader(System.in)); 

        String pwd = null; 

        //  read the username from the command-line; need to use try/catch with the 
        //  readLine() method
        try { 
            pwd = br.readLine(); 
        } catch (IOException ioe) { 
           System.out.println("IO error trying to read your name!"); 
           System.exit(1); 
        } 
        
        UserBO user = null;
        try {
            user = UserBO.authenticateUser(email, pwd);
            
            if (user.isUserAuthenticated()) {
                System.out.println("Welcome, " + user.getDisplayName());
            }
            
            Collection<RouteIntf> routes = user.getRouteAsCollection();
            Iterator<RouteIntf> routeItr = routes.iterator();
            System.out.println("You have " + routes.size() + " routes assigned:");
            while (routeItr.hasNext()) {
                RouteIntf route = routeItr.next();
                System.out.println("Route ID: " + route.getRouteID() + " Market: " + route.getMarketID() + " District: " + route.getDistrictID() + " Description: " + route.getRouteDescription() + " RDL Begin Date: " + route.getRDLBeginDate().toString("MM/d/yyyy")  + " RDL End Date: " + route.getRDLEndDate().toString("MM/d/yyyy")+ " Weekly Begin Date: " + route.getWeeklyDrawBeginDate().toString("MM/d/yyyy")  + " Weekly End Date: " + route.getWeeklyDrawEndDate().toString("MM/d/yyyy"));
                System.out.print("\tPubs: ");
                Collection<String> pubs = route.getWeeklyDrawPublications();
                if (pubs != null) {
	                Iterator<String> pubItr = pubs.iterator();
	                while(pubItr.hasNext()) {
	                    String pub = pubItr.next();
	                    System.out.print(pub);
	                    if (pubItr.hasNext()) {
	                        System.out.print(", ");
	                    }
	                }
	                System.out.println();
                }
                else {
                    System.out.println("No pubs!");
                }
            }
            
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    public void runNonInteractiveTest() {
        

        String email = "swong@usatoday.com"; 

        String pwd = "SWON141812"; 
        
        UserBO user = null;
        try {
            user = UserBO.authenticateUser(email, pwd);
            
            if (user.isUserAuthenticated()) {
                System.out.println("Welcome, " + user.getDisplayName());
            }
            
            Collection<RouteIntf> routes = user.getRouteAsCollection();
            Iterator<RouteIntf> routeItr = routes.iterator();
            System.out.println("You have " + routes.size() + " routes assigned:");
            while (routeItr.hasNext()) {
                RouteIntf route = routeItr.next();
                System.out.println("Route ID: " + route.getRouteID() + " Market: " + route.getMarketID() + " District: " + route.getDistrictID() + " Description: " + route.getRouteDescription() + " RDL Begin Date: " + route.getRDLBeginDate().toString("MM/d/yyyy")  + " RDL End Date: " + route.getRDLEndDate().toString("MM/d/yyyy")+ " Weekly Begin Date: " + route.getWeeklyDrawBeginDate().toString("MM/d/yyyy")  + " Weekly End Date: " + route.getWeeklyDrawEndDate().toString("MM/d/yyyy"));
                System.out.print("\tPubs: ");
                Collection<String> pubs = route.getWeeklyDrawPublications();
                if (pubs != null) {
	                Iterator<String> pubItr = pubs.iterator();
	                while(pubItr.hasNext()) {
	                    String pub = pubItr.next();
	                    System.out.print(pub);
	                    if (pubItr.hasNext()) {
	                        System.out.print(", ");
	                    }
	                }
	                System.out.println();
                }
                else {
                    System.out.println("No pubs!");
                }
            }
            
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
}
