/*
 * Created on Aug 2, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy.unittest;

import java.util.Collection;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.usatoday.singlecopy.model.bo.markets.MarketBO;
import com.usatoday.singlecopy.model.interfaces.markets.MarketIntf;
import com.usatoday.singlecopy.model.util.USATApplicationConstants;

/**
 * @author aeast
 * @date Aug 2, 2007
 * @class MarketTest
 * 
 * 
 * 
 */
public class MarketTest {

    /**
     * 
     */
    public MarketTest() {
        super();
       
    }

    public static void main(String[] args) {
        
        try {
            USATApplicationConstants.loadProperties();
            
            
            DateTime startTime = new DateTime();

            Collection<MarketIntf> markets = MarketBO.fetchMarkets();

            DateTime stopTime = new DateTime();
            

            long timeToRun = (stopTime.getMillis() - startTime.getMillis());
            
            System.out.println("MarketTest:: main() => It took " + timeToRun + " milliseconds to pull the markets.");
            
            Iterator<MarketIntf> itr = markets.iterator();
            while (itr.hasNext()) {
                MarketIntf m = itr.next();
                System.out.println("Market ID: " + m.getID() + "  Description: " + m.getDescription() + "  Local Phone: " + m.getLocalPhone() + "  RDL Phone: " + m.getRDLPhone());
            }
        }
        catch (Exception e) {
            System.out.println("Exception caught during test: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
