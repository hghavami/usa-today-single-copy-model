/*
 * Created on May 30, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy;

/**
 * @author aeast
 * @date May 30, 2007
 * @class UsatLoginFailedException
 * 
 * 
 * 
 */
public class UsatLoginFailedException extends UsatException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5191295135428682768L;
	public static final int BAD_PASSWORD = 0;
    public static final int ACCOUNT_NOT_ACTIVE = 1;
    public static final int BAD_PASSWORD_AND_NOT_ACTIVE = 2;
    public static final int BAD_AUTO_AUTH = 3;
    
    private int errorCode = UsatLoginFailedException.BAD_PASSWORD;
    
    /**
     * 
     */
    public UsatLoginFailedException() {
        super();
    }

    /**
     * @param message
     */
    public UsatLoginFailedException(String message) {
        super(message);
    }

    public UsatLoginFailedException(String message, int code) {
        super(message);
        this.errorCode = code;
    }
    
    /**
     * @param message
     * @param cause
     */
    public UsatLoginFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public UsatLoginFailedException(Throwable cause) {
        super(cause);
    }

    public static void main(String[] args) {
    }
    /**
     * @return Returns the errorCode.
     */
    public int getErrorCode() {
        return this.errorCode;
    }
    /**
     * @param errorCode The errorCode to set.
     */
    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}
