/*
 * Created on Apr 19, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.singlecopy;

/**
 * @author aeast
 * @date Apr 19, 2007
 * @class NotAuthorizedException
 * 
 * 
 * 
 */
public class NotAuthorizedException extends UsatException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2455684574873122945L;

	/**
     * 
     */
    public NotAuthorizedException() {
        super();
    }

    /**
     * @param message
     */
    public NotAuthorizedException(String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public NotAuthorizedException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public NotAuthorizedException(Throwable cause) {
        super(cause);
    }

}
